/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.visualization;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.shape.Rectangle;

/**
 * Class to extend Rectangle to include the position on the screen
 *
 * @author Christph Schmidt
 */
public class OrderRectangle extends Rectangle {

    private DoubleProperty screenXPosition  = new SimpleDoubleProperty();
    private DoubleProperty screenYPosition = new SimpleDoubleProperty();

    public OrderRectangle() {
        screenXPosition = new SimpleDoubleProperty(0);
        screenYPosition = new SimpleDoubleProperty(0);
    }

    public double getScreenXPosition() {
        return screenXPosition.get();
    }

    public void setScreenXPosition(double value) {
        screenXPosition.set(value);
    }

    public DoubleProperty screenXPositionProperty() {
        return screenXPosition;
    }

    public double getScreenYPosition() {
        return screenYPosition.get();
    }

    public void setScreenYPosition(double value) {
        screenYPosition.set(value);
    }

    public DoubleProperty screenYPositionProperty() {
        return screenYPosition;
    }

}
