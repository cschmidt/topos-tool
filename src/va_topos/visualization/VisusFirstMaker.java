/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.visualization;

import java.time.Duration;
import va_topos.data.DataModel;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import va_topos.ui.MainWindowBuilder;
import va_topos.ui.MainWindowBuilder.DotGrid;
import va_topos.VA_topos;
//import static va_topos.VA_topos.drawInjections;
//import static va_topos.VA_topos.drawVisusValues;
import va_topos.data.Patient;
import static va_topos.ui.MainWindowBuilder.visusFirstDrawObjectAnchorPane;
import static va_topos.VA_topos.observablePatients;
import va_topos.color.CustomColors;
import static va_topos.color.CustomColors.ANNO_BORDERCOLOR;
import static va_topos.color.CustomColors.STD_BORDERCOLOR;
import va_topos.data.MedicationChange;
import va_topos.events.VisFirstViewEvents;
import static va_topos.ui.MainWindowBuilder.patientRadio;
import static va_topos.ui.MainWindowBuilder.showMedChangeCheckBox;
import static va_topos.ui.MainWindowBuilder.summaryAnchorPane;
import static va_topos.color.CustomColors.HSB_STDBLUE;
import static va_topos.color.CustomColors.HSB_STDORANGE;
import static va_topos.ui.MainWindowBuilder.backColPaintObject;
import static va_topos.ui.MainWindowBuilder.brightnessSlider;
import static va_topos.ui.MainWindowBuilder.legendAnchorPane;
import static va_topos.ui.MainWindowBuilder.legendMaxValueLabel;
import static va_topos.ui.MainWindowBuilder.legendMidValueLabel;
import static va_topos.ui.MainWindowBuilder.legendMinValueLabel;
import static va_topos.ui.MainWindowBuilder.legendScaleRectangle;
import static va_topos.ui.MainWindowBuilder.legendShowCheckBox;
import static va_topos.ui.MainWindowBuilder.regressionShowCheckBox;
import static va_topos.ui.MainWindowBuilder.saturationSlider;
import static va_topos.ui.MainWindowBuilder.scenarioComboBox;
import static va_topos.ui.MainWindowBuilder.suddenIncidentShowCheckBox;
import static va_topos.ui.MainWindowBuilder.timeBeginn;
import static va_topos.ui.MainWindowBuilder.visFirstCheckBox;
import static va_topos.ui.MainWindowBuilder.yearLegendLabel;

/**
 *
 * @author lokal-admin
 */
@SuppressWarnings("unchecked")
public class VisusFirstMaker {

    public static DoubleProperty scrollPosition = new SimpleDoubleProperty(0);
    public static DoubleProperty timeBeginOffset = new SimpleDoubleProperty();
    public static DoubleProperty timeBeginOffsetSave = new SimpleDoubleProperty();
    public static IntegerProperty timeBeginYearOffset = new SimpleIntegerProperty();
    public static IntegerProperty timeBeginYearOffsetSave = new SimpleIntegerProperty();

    public static LocalDate minDate = LocalDate.MAX;
    public static LocalDate maxDate = LocalDate.MIN;

    List<OrderLine> yearLegend = new ArrayList();

    //DoubleProperty yTranslation = new SimpleDoubleProperty();
    private final double STD_BORDERWIDTH = MainWindowBuilder.borderWidthSlider.getValue();
    private static final double HORIZONTAL_SPACE = 360d;
    private static final double VERTICAL_SPACE = 0d;

    private final boolean isNormBegin = MainWindowBuilder.normBegin.isSelected();
    private boolean invert = false;

    public static double topOffset = 0d; // Center of screen on Zooming
    public static double oldTopOffset = 0d; // Center of screen on Zooming
    public static double leftOffset = 0d; // Center of screen on zooming
    public static double oldLeftOffset = 0d; // Center of screen on zooming

    public static double mouseXinit = 0d;
    public static double mouseYinit = 0d;

    public static double zoomFactor = 1d;
    public static double oldZoomFactor = 1d;
    public static final double MIN_ZOOM_FACTOR = 0.1d;
    public static final double MAX_ZOOM_FACTOR = 50d;
    public static DoubleProperty growZoomFactor = new SimpleDoubleProperty();

    // 228,26,28
    private static final int RED[] = {228, 55, 77, 152, 255, 255, 215}; //d / 255d;
    private static final int GREEN[] = {26, 126, 175, 78, 127, 255, 215}; //d / 255d;
    private static final int BLUE[] = {28, 184, 74, 163, 0, 51, 215}; //d / 255d;

    // Dreieck
//    private static final Double[] POLYSHAPE = new Double[]{
//        0d, 0d,
//        5d, 0d,
//        2.5d, 4.3d};
    // Viereck
//    private static final Double[] POLYSHAPE = new Double[]{
//        0d, 0d,
//        3d, 0d,
//        3d, 2d,
//        0d, 2d};
    // Spritzenform
//    private static final Double[] POLYSHAPE = new Double[]{
//        10d, 1d,
//        8d, 1d,
//        8d, 0d,
//        13d, 0d,
//        13d, 1d,
//        11d, 1d,
//        11d, 3d,
//        15d, 3d,
//        15d, 4d,
//        13d, 4d,
//        13d, 10d,
//        11d, 12d,
//        11d, 18d,
//        10d, 18d,
//        10d, 12d,
//        8d, 10d,
//        8d, 4d,
//        6d, 4d,
//        6d, 3d,
//        10d, 3d};
    // kleinere Spritzenform
//    private static final Double[] POLYSHAPE = new Double[]{
//        5d, 0.5d,
//        4d, 0.5d,
//        4d, 0d,
//        6.5d, 0d,
//        6.5d, 0.5d,
//        5.5d, 0.5d,
//        5.5d, 1.5d,
//        7.5d, 1.5d,
//        7.5d, 2d,
//        6.5d, 2d,
//        6.5d, 5d,
//        5.5d, 6d,
//        5.5d, 9d,
//        5d, 9d,
//        5d, 6d,
//        4d, 5d,
//        4d, 2d,
//        3d, 2d,
//        3d, 1.5d,
//        5d, 1.5d};
    // Pfeil
    private static final Double[] POLYSHAPE = new Double[]{
        0d, 4d,
        7d, 4d,
        7d, 0d,
        8d, 0d,
        15d, 7d,
        8d, 14d,
        7d, 14d,
        7d, 10d,
        0d, 10d,};

    private static final double SHAPEFACTOR = 0.75;
    private static final Double[] SUDDENINCIDENTSHAPE = new Double[]{
        0.0, 0.0,
        9.0, 0.0,
        7.0, 11.0,
        11.0, 9.0,
        13.0, 9.0,
        9.0, 27.0,
        7.0, 23.0,
        7.0, 16.0,
        3.0, 17.0,
        1.0, 8.0,
        0.0, 0.0,};

    public enum Pattern {
        LINEAR,
        LOGARITHMIC,
        CATEGORY,
        DIVERGING3CLASS,
        DIVERGING4CLASS,
        DIVERGING5CLASS,
        DIVERGING6CLASS;
    }

    public double getTimeBeginOffset() {
        return timeBeginOffset.get();
    }

    public void setTimeBeginOffset(double value) {
        timeBeginOffset.set(value);
    }

    public DoubleProperty timeBeginOffsetProperty() {
        return timeBeginOffset;
    }

    public double getTiemBeginOffsetSave() {
        return timeBeginOffsetSave.get();
    }

    public void setTiemBeginOffsetSave(double value) {
        timeBeginOffsetSave.set(value);
    }

    public DoubleProperty tiemBeginOffsetSaveProperty() {
        return timeBeginOffsetSave;
    }

    public int getTimeBeginYearOffset() {
        return timeBeginYearOffset.get();
    }

    public void setTimeBeginYearOffset(int value) {
        timeBeginYearOffset.set(value);
    }

    public IntegerProperty timeBeginYearOffsetProperty() {
        return timeBeginYearOffset;
    }

    public int getTimeBeginYearOffsetSave() {
        return timeBeginYearOffsetSave.get();
    }

    public void setTimeBeginYearOffsetSave(int value) {
        timeBeginYearOffsetSave.set(value);
    }

    public IntegerProperty timeBeginYearOffsetSaveProperty() {
        return timeBeginYearOffsetSave;
    }

    public void calculateDrawMatrix() {

        if (!visFirstCheckBox.isSelected()) {
            return;
        }

        LocalTime nowTime = LocalTime.now();
        //System.out.println("Calculate DrawMatrix Beginn: " + LocalTime.now());
        // TODO: Design a matrix, that has "realScreen" Data, which then only has to be drawn
        // Haben:
        // Definierte Anzahl an Patienten
        // Jeder Patient hat eine definierte Anzahl an 
        //
        // Visuswerten
        // Visusklassenwerten
        // VisusDeltawerten
        // Spitzen
        //
        // Hieraus ist eine Matrix zu bauen, die eine definierte Anzahl an Rechtecken enthält, diese ergeben sich allein aus der Anzahl der Daten
        //
        // Das heißt, die geoLocation ist allein abhängig von den Datumsangaben
        // Die mulitvariaten Daten werden pro Rechteck abgelegt

        // Matrix with all rectangles to be drawn
        //List<OrderRectangle> drawVisusValues = new ArrayList();
        //List<OrderPolygon> drawInjections = new ArrayList();
        // Initialize the minimum and maximum date in the dataset
        minDate = VA_topos.masterPatient.getBeginMonitoring();
        maxDate = VA_topos.masterPatient.getEndMonitoring();
//        System.out.println("minDate: " + minDate);
//        System.out.println("maxDate: " + maxDate);
// Initialize the minimum and maximum visual acuity
//        double minDezVisus = VA_topos.masterPatient.getMinVisus();
//        double maxDezVisus = VA_topos.masterPatient.getMaxVisus();

        // Derive the min- and maxdate from the data (to set the Range for x)
//        for (DataModel ereignis : VA_topos.sortedFilteredData) {
//            if (ereignis.getEreignisDatum().isBefore(minDate)) {
//                minDate = ereignis.getEreignisDatum();
//            }
//            if (ereignis.getEreignisDatum().isAfter(maxDate)) {
//                maxDate = ereignis.getEreignisDatum();
//            }
//            if (!Double.isNaN(ereignis.getDezimalVisusWert())) {
//                if (ereignis.getDezimalVisusWert() < minDezVisus) {
//                    minDezVisus = ereignis.getDezimalVisusWert();
//                }
//                if (ereignis.getDezimalVisusWert() > maxDezVisus) {
//                    maxDezVisus = ereignis.getDezimalVisusWert();
//                }
//            }
//        }
        // Set the x and y factor to fit the map onto the screen
        DoubleProperty windowFitFactorX = new SimpleDoubleProperty();
        DoubleProperty windowFitFactorY = new SimpleDoubleProperty();

        // WindowFitFactorX is width of screen divided by maximum range of data
        windowFitFactorX.bind(visusFirstDrawObjectAnchorPane.widthProperty().divide(DAYS.between(minDate, maxDate)));
        final double totalDays = DAYS.between(minDate, maxDate);
        // WindowFitFactorY is height of screen divided by maximum number of eyes to be displayed

        // Ziel: Senkrechte Striche zum jeden 1. Januar des Jahres in den Grafikbereich zeichnen
        int firstYear = minDate.getYear();
        int lastYear = maxDate.getYear();

        timeBeginOffsetSave.set(DAYS.between(minDate, LocalDate.of(firstYear, Month.JANUARY, 1)));
        timeBeginOffset.set(timeBeginn.isSelected() ? DAYS.between(minDate, LocalDate.of(firstYear, Month.JANUARY, 1)) : 0.0);

        DoubleProperty daysPerYear = new SimpleDoubleProperty(365);

        for (int i = firstYear; i <= lastYear; i++) {
            yearLegend.add(new OrderLine());
        }
        int yearIndex = 0;
        for (OrderLine line : yearLegend) {
            line.startXProperty().bind(timeBeginOffset.add(daysPerYear.multiply(yearIndex)).multiply(windowFitFactorX));
            line.endXProperty().bind(timeBeginOffset.add(daysPerYear.multiply(yearIndex)).multiply(windowFitFactorX));
            line.setStartY(0);
            line.endYProperty().bind(visusFirstDrawObjectAnchorPane.heightProperty());
            line.strokeProperty().bind(backColPaintObject);
            line.visibleProperty().bind(legendShowCheckBox.selectedProperty());
            yearIndex++;
        }
        // Die senkrechten Linien werden am Ende der Prozedur hinzugefügt

        // Ziel: Die Jahreszahl 3x auf dem Strich anzeigen, gleichmäßig über den view verteilt
        // List<Label> yearLegendLabel = new ArrayList();
        // System.out.println("first: " + firstYear + " last: " + lastYear);
        timeBeginYearOffsetSave.set(1);
        timeBeginYearOffset.set(timeBeginn.isSelected() ? 1 : 0);

        yearLegendLabel.clear();
        for (int i = 0; i <= (lastYear - firstYear); i++) {

            for (int j = 1; j < 4; j++) {
                Label lab = new Label();
                //lab.autosize();
                lab.textProperty().bind(timeBeginYearOffset.multiply(firstYear).add(i).asString());

                //lab.setPrefSize(300.0, 300.0);
                //lab.setMinSize(200.0, 200.0);
                //lab.setMaxSize(500.0, 500.0);
                //setAnchor(lab, 100, 100, Double.NaN, Double.NaN);
                // the 30 is for the vis regressionarrow offset
                lab.translateXProperty().bind(timeBeginOffset.add(daysPerYear.multiply(i).add(183 + 30)).multiply(windowFitFactorX)); //.add(daysPerYear.multiply(i).multiply(windowFitFactorX).subtract(lab.widthProperty().divide(2))));

                //lab.setTextFill(STD_BORDERCOLOR);
//                lab.setLayoutX(300.0);
//                lab.setLayoutY(300.0);
                //lab.setFont(new Font("Arial", 30));
                //lab.autosize();
                //lab.textFillProperty().bind(new SimpleObjectProperty<> (128, 128, 128));
                //lab.bind
                lab.textFillProperty().bind(backColPaintObject);
                lab.visibleProperty().bind(legendShowCheckBox.selectedProperty());

//                lab.textFillProperty().bind(Bindings.createObjectBinding(() -> {
//                    return Color.rgb((int) backColorSlider.getValue(), (int) backColorSlider.getValue(), (int) backColorSlider.getValue());
//                }));
                //lab.setBackground(MainWindowBuilder.setStyle(lab, MainWindowBuilder.NodeStyles.INVERTED_DYNAMIC_BACKCOLOR));
                lab.translateYProperty().bind(visusFirstDrawObjectAnchorPane.heightProperty().divide(4.0).multiply(j));
//                lab.layoutYProperty().addListener((listener, oldValue, newValue) -> {
//                    System.out.println("yProp - old: " + oldValue + " new: " + newValue);
//                });
//                lab.layoutXProperty().addListener((listener, oldValue, newValue) -> {
//                    System.out.println("xProp - old: " + oldValue + " new: " + newValue);
//                });
                yearLegendLabel.add(lab);
            }
        }
        // Die Label werden am Ende der Prozedur hinzugefügt

        // Create an array with all patients, to afterwards sort it, for different oderings on the screen
        Patient[] myPat = new Patient[VA_topos.observablePatients.values().size()];
        VA_topos.observablePatients.values().toArray(myPat);
        // TODO: insert more sorting possibilities
        changeSorting(myPat);

        // Derive all IDs for the patient (each patient has one ID for each eye)
        List<String> patientIDs = new ArrayList();

        // get all sorted patients and add a rectangle each, where the single visus rectangles are placed
        for (Patient pat : myPat) {
            if (pat.isSelected()) {
                patientIDs.add(pat.getPatientID());
//                OrderRectangle myRect = new OrderRectangle();
//                myRect.widthProperty().bind(visusFirstDrawObjectAnchorPane.widthProperty());
//                myRect.screenYPositionProperty().bind(new SimpleDoubleProperty(patientIDs.size() - 1).add(scrollPosition));
//                myRect.yProperty().bind(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty().multiply(myRect.screenYPositionProperty().multiply(10d))));
//                patRectList.add(myRect);
            }
        }

        // The days between the minimum Date and the maximum Date is the x-resolution
        final LocalDate minD = minDate;

        VA_topos.observablePatients.forEach((key, patient) -> {
            PatientBar patBar = new PatientBar();
            patBar.screenYPositionProperty().bind(new SimpleDoubleProperty(patientIDs.indexOf(key)).add(scrollPosition));
            patBar.yProperty().bind(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty().multiply(patBar.screenYPositionProperty().multiply(10d))));
            patient.setDrawPatientBar(patBar);
        });
        // For each incident create a rectangle, which later will be displayed on the screen
        VA_topos.sortedFilteredData.forEach(ereignis -> {

            String eyeID = ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet();
            PatientBar patBar = new PatientBar();
            try {
                patBar = VA_topos.observablePatients.get(eyeID).getDrawPatientBar();
            } catch (Exception e) {
            }

            if (ereignis.isArgosEreignis()) {
                String text = "";
                if (ereignis.getArgosEreignisText().toLowerCase().contains("phako")) {
                    text = "phako";
                }
                if (ereignis.getArgosEreignisText().toLowerCase().contains("makulablutung")) {
                    text = "makulablutung";
                }
                if (ereignis.getArgosEreignisText().toLowerCase().contains("ambulante chemo")) {
                    text = "ambulante chemo";
                }
                OrderPolygon sudden = new OrderPolygon(SUDDENINCIDENTSHAPE, SHAPEFACTOR, text);
                sudden.setUserData(ereignis);
                sudden.layoutXProperty().bind(windowFitFactorX.multiply(sudden.screenXPositionProperty().add(DAYS.between(minD, ereignis.getEreignisDatum()))));
                sudden.layoutYProperty().bind(patBar.yProperty());
                sudden.visibleProperty().bind(suddenIncidentShowCheckBox.selectedProperty().and((BooleanProperty) scenarioComboBox.getUserData()).or(ereignis.cleansedProperty().not().and(((BooleanProperty) scenarioComboBox.getUserData()).not())));
                sudden.strokeWidthProperty().bind(MainWindowBuilder.borderWidthSlider.valueProperty());
                if (ereignis.isArgosEreignisRelevanz()) {
                    setPolygonStandardDesign(sudden);
                } else {
                    setPolygonCleansedDesign(sudden);
                }

                //sudden.heightProperty().bind(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty().multiply(6d)));
                final String[] captions = {"Patient", "Lateralität", "Geburtsdatum", "Geschlecht", "Hauptdiagnose", "Ereignistext argos_tl", "Datum", "Nutzerkommentar"};
                final String[] values = {ereignis.getPatientenID(), ereignis.getUntersuchungLateralitaet().toString(), ereignis.getGeburtsdatum().toString(), ereignis.getGeschlecht().toString(), ereignis.getHauptDiagnose().toString(), ereignis.getArgosEreignisText(), ereignis.getEreignisDatum().toString(), ereignis.getAnnotationText()};

                sudden.setOnMouseEntered(e -> VisFirstViewEvents.onDrawObjectMouseEntered(e, captions, values));
                sudden.setOnMouseExited(e -> VisFirstViewEvents.onDrawObjectMouseExited());
                sudden.setOnMouseClicked(e -> VisFirstViewEvents.onSuddenMouseClicked(e, (DataModel) sudden.getUserData(), sudden));
                sudden.setOnMouseReleased(e -> VisFirstViewEvents.onDrawObjectMouseReleased(e, (DataModel) sudden.getUserData()));
                patBar.getSuddenIncidents().add(sudden);

            }
            // Only if the incident contains a valid value for visual acuity
            if (!Double.isNaN(ereignis.getDezimalVisusWert())) {
                // The eye-ID (name of patient + LEFT or RIGHT)

                // The rectangle to be drawn on the screen (as an object)
                OrderRectangle rect = new OrderRectangle();
                OrderLine line = new OrderLine();
                rect.setUserData(ereignis);  // save the incident in the rectangle object

                // The X value for the rectangle
//                System.out.println("hier:");
//                System.out.println(ereignis.getUntersuchungLateralitaet());
//                System.out.println(ereignis.getPatientenID());
//                System.out.println(VA_topos.observablePatients.keySet()); //.get(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet()));
                //rect.screenXPositionProperty().bind(new SimpleDoubleProperty(-DAYS.between(minD, VA_topos.observablePatients.get(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet()).getBeginTreatment())));
                rect.xProperty().bind(windowFitFactorX.multiply(rect.screenXPositionProperty().add(DAYS.between(minD, ereignis.getEreignisDatum()))));
                //rect.visibleProperty().bind(rect.yProperty().greaterThanOrEqualTo(0.0).and(rect.yProperty().lessThan(visusFirstDrawObjectAnchorPane.heightProperty())));
                rect.visibleProperty().bind(((BooleanProperty) scenarioComboBox.getUserData()).or(ereignis.cleansedProperty().not().and(((BooleanProperty) scenarioComboBox.getUserData()).not())));

                line.startXProperty().bind(windowFitFactorX.multiply(rect.screenXPositionProperty().add(DAYS.between(minD, ereignis.getEreignisDatum()))));
                line.endXProperty().bind(line.startXProperty());
                line.visibleProperty().bind(((BooleanProperty) scenarioComboBox.getUserData()).or(ereignis.cleansedProperty().not().and(((BooleanProperty) scenarioComboBox.getUserData()).not())));

//try {
//                rect.yProperty().bind(patRectList.get(patientIDs.indexOf(eyeID)).yProperty());
//    
//} catch (Exception e) {
//    System.out.println(eyeID);
//}
//                rect.screenYPositionProperty().bind(new SimpleDoubleProperty(patientIDs.indexOf(eyeID)).add(scrollPosition));
                rect.yProperty().bind(patBar.yProperty());
                line.startYProperty().bind(patBar.yProperty());

//                rect.yProperty().bind(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty().multiply(rect.screenYPositionProperty().multiply(10d))));
                rect.heightProperty().bind(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty().multiply(6d)));
                line.endYProperty().bind(line.startYProperty().add(rect.heightProperty()));

                // When the mouse is pressed for a specific rectangle
//                rect.setOnMousePressed(e -> VisFirstViewEvents.onDrawObjectMousePressed(rect, e.getX(), e.getY()));
                // When the Mouse Button has been released and the mouse has not been dragged, when the button was pressed
                rect.setOnMouseReleased(e -> VisFirstViewEvents.onDrawObjectMouseReleased(e, ereignis));
                // When the mouse is scrolled on the rectangle 
                rect.setOnScroll(e -> VisFirstViewEvents.onVisViewScrolled(e));
                // When the mouse is moved on the object, the 

                // Prepare the detail View
                final String visusValueKind;
                String yValueKey = MainWindowBuilder.visusDimensionComboBox.getSelectionModel().getSelectedItem().toString().toLowerCase();
                String visusLegendValue = yValueKey.length() > 2 ? yValueKey.substring(0, 1).toUpperCase() + yValueKey.substring(1, yValueKey.length()) : "";

                switch (yValueKey) {
                    case "visusklasse":
                        visusValueKind = "" + ereignis.getVisusKlasse();
                        break;
                    case "dezimalvisus":
                        visusValueKind = "" + ereignis.getDezimalVisusWert();
                        break;
                    case "logmar visus":
                        visusValueKind = "" + Math.round(ereignis.getLogMarVisusWert() * 1000.0) / 1000.0;
                        break;
                    case "visusdelta":
                        visusValueKind = "" + ereignis.getVisusDelta();
                        break;
                    case "visusdelta (logmar)":
                        visusValueKind = "" + ereignis.getVisusLogMarDelta();
                        break;
                    case "visusklassendelta":
                        visusValueKind = "" + ereignis.getVisusKlasseDelta();
                        break;
                    default:
                        System.out.println("Hm? " + yValueKey);
                        visusValueKind = "" + ereignis.getDezimalVisusWert();
                        visusLegendValue = "Dezimalvisuswert:";
                }
                final String[] captions = {"Patient", "Lateralität", "Geburtsdatum", "Geschlecht", "Hauptdiagnose aus System (aus Brief)", "Dezimalvisuswert aus System (aus Brief)", visusLegendValue, "Datum", "Nutzerkommentar"};
                final String[] values = {ereignis.getPatientenID(), ereignis.getUntersuchungLateralitaet().toString(), ereignis.getGeburtsdatum().toString(), ereignis.getGeschlecht().toString(), ereignis.getHauptDiagnose().toString() + " (" + ereignis.gethauptDiagnoseFromLetterExtract() + ")", "" + ereignis.getDezimalVisusWert() + " (" + ereignis.getDezimalVisusWertFromLetterExtract() + ")", visusValueKind, ereignis.getEreignisDatum().toString(), ereignis.getAnnotationText()};
                String[] summaryVis = {"-1", "-1", "-1", "-1"};
                try {
                    summaryVis[0] = "" + observablePatients.get(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet()).getFirstVisus();
                    summaryVis[1] = "" + observablePatients.get(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet()).getLastVisus();
                    summaryVis[2] = "" + observablePatients.get(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet()).getMeanVisus();
                    summaryVis[3] = "" + observablePatients.get(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet()).getMedianVisus();
                } catch (Exception e) {
                }

                //System.out.println(summaryVis[0]);
                rect.setOnMouseEntered(e -> VisFirstViewEvents.onDrawObjectMouseEntered(e, captions, values, summaryVis));
                // When the mouse leaves the drawing object
                rect.setOnMouseExited(e -> VisFirstViewEvents.onDrawObjectMouseExited());

//                int i = 0;
//                for (OrderPolygon myRect : patBar.getInjectionRectangles()) {
//                    if (myRect.getLayoutX() < rect.getLayoutX()) {
//                        i++;
//                    } else {
//                        break;
//                    }
//                }
                OrderRectangle anno = new OrderRectangle();
                anno.yProperty().bind(rect.yProperty());
                anno.translateYProperty().bind(rect.translateYProperty());
                anno.xProperty().bind(rect.xProperty());
                //anno.translateXProperty().bind(rect.translateXProperty());
                anno.widthProperty().bind(rect.widthProperty());
                anno.heightProperty().bind(rect.heightProperty());
                anno.visibleProperty().bind(ereignis.annotatedProperty().or(ereignis.cleansedProperty()));
                anno.setStroke(ANNO_BORDERCOLOR);
                anno.setStrokeWidth(2.0);
                anno.setFill(null);
                anno.setOnMouseReleased(e -> VisFirstViewEvents.onDrawObjectMouseReleased(e, ereignis));

                patBar.getVisenRectangles().add(rect);
                patBar.getAnnoRectangles().add(anno);
                patBar.getVisIncidents().add(line);

            }

            switch (ereignis.getMedikament()) {
                case AVASTIN:
                case LUCENTIS:
                case EYLEA:
                case OZURDEX:
                case TRIAMCINOLON:
                    // Add triangles in the respective color here
//                    OrderPolygon syringe = new OrderPolygon();
                    OrderRectangle syringe = new OrderRectangle();
//                    syringe.getPoints().addAll(POLYSHAPE);
//                    syringe.scaleXProperty().bind(windowFitFactorX.multiply(MainWindowBuilder.zoomSlider.valueProperty().divide(VA_topos.observablePatients.size()).multiply(200)));
//                    syringe.scaleYProperty().bind(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty()));
                    syringe.heightProperty().bind(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty().multiply(1.5d)));
                    syringe.setWidth(5d);

//                    syringe.screenXPositionProperty().bind(new SimpleDoubleProperty(MainWindowBuilder.normBegin.isSelected() ? 1000 : 0));
                    syringe.screenXPositionProperty().bind(new SimpleDoubleProperty(0));
//                    syringe.layoutXProperty().bind(windowFitFactorX.multiply(syringe.screenXPositionProperty().add(DAYS.between(minD, ereignis.getEreignisDatum()))));
                    syringe.xProperty().bind(windowFitFactorX.multiply(syringe.screenXPositionProperty().add(DAYS.between(minD, ereignis.getEreignisDatum()))));
                    syringe.visibleProperty().bind(((BooleanProperty) scenarioComboBox.getUserData()).or(ereignis.cleansedProperty().not().and(((BooleanProperty) scenarioComboBox.getUserData()).not())));

//                    syringe.screenYPositionProperty().bind(new SimpleDoubleProperty(patientIDs.indexOf(eyeID)).add(scrollPosition));
//                    syringe.layoutYProperty().bind(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty().multiply(syringe.screenYPositionProperty().multiply(10d))));
//                    syringe.layoutYProperty().bind(patBar.yProperty());
                    syringe.yProperty().bind(patBar.yProperty().subtract(syringe.heightProperty()));

                    syringe.setUserData(ereignis);
                    // Set the event handling for the injection objects
                    final String[] captions = {"Patient", "Lateralität", "Geburtsdatum", "Geschlecht", "Diagnose", "Medikament", "Datum", "Nutzerkommentar"};
                    final String[] values = {ereignis.getPatientenID(), ereignis.getUntersuchungLateralitaet().toString(), ereignis.getGeburtsdatum().toString(), ereignis.getGeschlecht().toString(), ereignis.getHauptDiagnose().toString(), ereignis.getMedikament().toString(), ereignis.getEreignisDatum().toString(), ereignis.getAnnotationText()};
                    String[] summaryVis = {"-1", "-1", "-1"};
                    try {
                        summaryVis[0] = "" + observablePatients.get(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet()).getFirstVisus();
                        summaryVis[1] = "" + observablePatients.get(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet()).getLastVisus();
                        summaryVis[2] = "" + observablePatients.get(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet()).getNumberOfInjections();
                    } catch (Exception e) {
                    }

                    // Move object or open detail view
//                    syringe.setOnMousePressed(e -> VisFirstViewEvents.onDrawObjectMousePressed(syringe, e.getX(), e.getY()));
                    // if mouse button is released
                    syringe.setOnMouseReleased(e -> VisFirstViewEvents.onDrawObjectMouseReleased(e, ereignis));
                    // zoom or scroll
                    syringe.setOnScroll(e -> VisFirstViewEvents.onVisViewScrolled(e));
                    // Show detailed information on enter
                    syringe.setOnMouseEntered(e -> VisFirstViewEvents.onDrawObjectMouseEntered(e, captions, values, summaryVis));
                    // remove the detailed information on exit
                    syringe.setOnMouseExited(e -> VisFirstViewEvents.onDrawObjectMouseExited());

                    // Insert the polygon in the right position in the polygon list for the patient
//                    int i = 0;
//                    for (OrderPolygon myPolygon : patBar.getInjectionRectangles()) {
//                        if (myPolygon.getLayoutX() < syringe.getLayoutX()) {
//                            i++;
//                        } else {
//                            break;
//                        }
//                    }
                    OrderRectangle anno = new OrderRectangle();
                    anno.yProperty().bind(syringe.yProperty());
                    anno.translateYProperty().bind(syringe.translateYProperty());
                    anno.xProperty().bind(syringe.xProperty());
                    anno.translateXProperty().bind(syringe.translateXProperty());
                    anno.widthProperty().bind(syringe.widthProperty());
                    anno.heightProperty().bind(syringe.heightProperty());
                    anno.visibleProperty().bind(ereignis.annotatedProperty().or(ereignis.cleansedProperty()));
                    anno.setStroke(ANNO_BORDERCOLOR);
                    anno.setStrokeWidth(1.0);
                    anno.setFill(null);
                    anno.setOnMouseReleased(e -> VisFirstViewEvents.onDrawObjectMouseReleased(e, ereignis));

                    patBar.getAnnoRectangles().add(anno);

                    patBar.getInjectionRectangles().add(syringe);
//                    List<OrderPolygon> myPolygonList = drawInjections.getOrDefault(eyeID, new ArrayList());
//                    myPolygonList.add(syringe);
//                    drawInjections.put(eyeID, myPolygonList);
            }
        }
        );

        // Jeden Patienten durchgehen 
        observablePatients.values()
                .forEach(patient -> {

                    // Für jeden Medikamentenwechsel ein Rechteck hinzufügen
                    patient.getMyMedicationChanges().values().forEach(change -> {

                        OrderRectangle medChangeRect = new OrderRectangle();
                        medChangeRect.setUserData(change);  // save the incident in the rectangle object

                        // The X value for the rectangle
                        medChangeRect.xProperty().bind(windowFitFactorX.multiply(medChangeRect.screenXPositionProperty().add(DAYS.between(minD, change.getOldVisDate()) - 10.0)));
//                medChangeRect.visibleProperty().bind(medChangeRect.yProperty().greaterThanOrEqualTo(0.0).and(medChangeRect.yProperty().lessThan(visusFirstDrawObjectAnchorPane.heightProperty())));
                        medChangeRect.visibleProperty().bind(showMedChangeCheckBox.selectedProperty());

                        medChangeRect.yProperty().bind(patient.getDrawPatientBar().yProperty().subtract(medChangeRect.heightProperty().divide(2)));

                        medChangeRect.heightProperty().bind(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty().multiply(6d)));
                        medChangeRect.widthProperty().bind(windowFitFactorX.multiply(20d));

                        // When the mouse is pressed for a specific rectangle
//                medChangeRect.setOnMousePressed(e -> VisFirstViewEvents.onDrawObjectMousePressed(medChangeRect, e.getX(), e.getY()));
                        // When the Mouse Button has been released and the mouse has not been dragged, when the button was pressed
//                medChangeRect.setOnMouseReleased(e -> VisFirstViewEvents.onDrawObjectMouseReleased(e, ereignis));
                        // When the mouse is scrolled on the rectangle 
                        medChangeRect.setOnScroll(e -> VisFirstViewEvents.onVisViewScrolled(e));

                        //System.out.println(summaryVis[0]);
//                medChangeRect.setOnMouseEntered(e -> VisFirstViewEvents.onDrawObjectMouseEntered(e, captions, values, summaryVis));
                        // When the mouse leaves the drawing object
//                medChangeRect.setOnMouseExited(e -> VisFirstViewEvents.onDrawObjectMouseExited());
                        patient.getDrawPatientBar().getVisChangeRectangles().add(medChangeRect);

                    });

                    // Insert the Regressioncurve for the patient
                    patient.getDrawPatientBar().getRegressionLine().visibleProperty().bind(regressionShowCheckBox.selectedProperty());
                    patient.getDrawPatientBar().getRegressionLine().setStroke(Color.RED);
                    patient.getDrawPatientBar().getRegressionLine().setStartX(20.0);
                    patient.getDrawPatientBar().getRegressionLine().endXProperty().bind(visusFirstDrawObjectAnchorPane.widthProperty());
                    patient.getDrawPatientBar().getRegressionLine().startYProperty().bind(patient.getDrawPatientBar().yProperty().subtract(windowFitFactorY.multiply(10 * patient.getLinearRegressionIntercept()))); //.add(windowFitFactorY.multiply(patient.getLinearRegressionIntercept())));
                    patient.getDrawPatientBar().getRegressionLine().endYProperty().bind(patient.getDrawPatientBar().yProperty().subtract(windowFitFactorY.multiply(-10 * (patient.getLinearRegressionSlope() * totalDays + patient.getLinearRegressionIntercept()))));
                    // Insert the regression arrow for the patient
                    patient.getDrawPatientBar().getRegressionArrow().setFill(regressionColorPicker(patient.getLinearRegressionSlope(), patient.getLinearRegressionIntercept(), patient.getNumberOfVisusMeasurements(), patient.getLengthOfMonitoring()));
                    patient.getDrawPatientBar().getRegressionArrow().setStroke(Color.BLACK);
                    patient.getDrawPatientBar().getRegressionArrow().setRotate(22.5 * Math.atan(patient.getLinearRegressionSlope() * 1000));
                    //System.out.println("Ergebnis: " + 45.0 *Math.atan(patient.getLinearRegressionSlope()*1000));
//            patient.getDrawPatientBar().getRegressionArrow().setRotate(365 * Math.asin(patient.getLinearRegressionSlope()));
                    patient.getDrawPatientBar().getRegressionArrow().getPoints().addAll(POLYSHAPE);
                    patient.getDrawPatientBar().getRegressionArrow().setLayoutX(-20.0);
                    patient.getDrawPatientBar().getRegressionArrow().layoutYProperty().bind(patient.getDrawPatientBar().yProperty().add(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty().multiply(2d))));

                    patient.getDrawPatientBar().getRegressionArrow().setUserData(patient);
                    // Set the event handling for the injection objects
                    final String[] captions = {"Patient", "Lateralität", "Geburtsdatum", "Geschlecht", "Regressionsanstieg", "Regressionseingangswert"};
                    final String[] values = {patient.getPatientID(), patient.getLateralitaet().toString(), patient.getDateOfBirth().toString(), patient.getSex().toString(), "" + 1 / Math.pow(10, patient.getLinearRegressionSlope()) * patient.getNumberOfVisusMeasurements(), "" + 1 / Math.round(Math.pow(10, patient.getLinearRegressionIntercept()) * 100.0) / 100.0};

                    // Show detailed information on enter
                    patient.getDrawPatientBar().getRegressionArrow().setOnMouseEntered(e -> VisFirstViewEvents.onDrawObjectMouseEntered(e, captions, values));
                    // remove the detailed information on exit
                    patient.getDrawPatientBar().getRegressionArrow().setOnMouseExited(e -> VisFirstViewEvents.onDrawObjectMouseExited());

                    try {
                        // die Visusrechtecke nach der Zeit sortieren
                        if (patient.getDrawPatientBar().getVisenRectangles().size() > 0) {
                            patient.getDrawPatientBar().getVisenRectangles().sort((OrderRectangle first, OrderRectangle second) -> {
                                return first.getX() < second.getX() ? -1 : first.getX() > second.getX() ? 1 : 0;
                            });
                        }
                        // die Spritzenrechtecke nach der Zeit sortieren
                        if (patient.getDrawPatientBar().getInjectionRectangles().size() > 0) {
                            patient.getDrawPatientBar().getInjectionRectangles().sort((OrderRectangle first, OrderRectangle second) -> {
                                return first.getX() < second.getX() ? -1 : first.getX() > second.getX() ? 1 : 0;
                            });
                        }

                        // Die Verschiebung des Rechtecks mit allen anderen Rechtecken des Auges verbinden
//                        if (patient.getDrawPatientBar().getVisenRectangles().size() > 1) {
//                            for (int i = 1; i < patient.getDrawPatientBar().getVisenRectangles().size(); i++) {
//                                patient.getDrawPatientBar().getVisenRectangles().get(i).translateXProperty().bindBidirectional(patient.getDrawPatientBar().getVisenRectangles().get(0).translateXProperty());
//                                patient.getDrawPatientBar().getVisenRectangles().get(i).translateYProperty().bindBidirectional(patient.getDrawPatientBar().getVisenRectangles().get(0).translateYProperty());
//                            }
//                        }
                        // Die Breite der Visussegmente festlegen
                        if (patient.getDrawPatientBar().getVisenRectangles().size() > 0) {
                            for (int i = 0; i < (patient.getDrawPatientBar().getVisenRectangles().size() - 1); i++) {
                                patient.getDrawPatientBar().getVisenRectangles().get(i).widthProperty().bind(patient.getDrawPatientBar().getVisenRectangles().get(i + 1).xProperty().subtract(patient.getDrawPatientBar().getVisenRectangles().get(i).xProperty()));
//                        patient.getDrawPatientBar().getVisenRectangles().get(i).widthProperty().bind(windowFitFactorX.multiply(20.0)); //patient.getDrawPatientBar().getVisenRectangles().get(i + 1).xProperty().subtract(patient.getDrawPatientBar().getVisenRectangles().get(i).xProperty()));
                            }
                        }
//                        if ((patient.getDrawPatientBar().getInjectionRectangles().size() > 0) && (patient.getDrawPatientBar().getVisenRectangles().size() > 0)) {
//                            for (int i = 0; i < patient.getDrawPatientBar().getInjectionRectangles().size(); i++) {
//                                patient.getDrawPatientBar().getInjectionRectangles().get(i).translateXProperty().bindBidirectional(patient.getDrawPatientBar().getVisenRectangles().get(0).translateXProperty());
//                                patient.getDrawPatientBar().getInjectionRectangles().get(i).translateYProperty().bindBidirectional(patient.getDrawPatientBar().getVisenRectangles().get(0).translateYProperty());
//                            }
//                        }
//                        if (patient.getDrawPatientBar().getVisenRectangles().size() > 0) {
//                            patient.getDrawPatientBar().getVisenRectangles().get(patient.getDrawPatientBar().getVisenRectangles().size() - 1).widthProperty().bind(windowFitFactorX.multiply(30d));
//                        }
                        //            rectList.get(rectList.size() - 1).widthProperty().bind(windowFitFactorX.multiply(30d));
                    } catch (Exception e) {
                        System.out.println("Fehler in der Klasse 'VisusFirstMaker'. Funktion 'calculateDrawMatrix' Fehlertext: " + e);
                    }

                }
                );
        windowFitFactorY.bind(visusFirstDrawObjectAnchorPane.heightProperty().divide(observablePatients.size() * 10));

        VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\nZeit für das Berechnen der Zeichnung: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0));
        //System.out.println("Calculate DrawMatrix Ende: " + LocalTime.now());

        visusFirstDrawObjectAnchorPane.getChildren()
                .clear();

        visusFirstDrawObjectAnchorPane.getChildren()
                .addAll(yearLegend);
        //visusFirstDrawObjectAnchorPane.getChildren().addAll(yearLegendLabel);
        legendAnchorPane.getChildren()
                .clear();
        try {
            legendAnchorPane.getChildren().addAll(yearLegendLabel);
        } catch (Exception e) {

        }
        observablePatients.forEach(
                (key, patient) -> {
                    if (patient.isSelected() && patient.isShow()) {
                        visusFirstDrawObjectAnchorPane.getChildren().addAll(patient.getDrawPatientBar().getVisenRectangles());
                        visusFirstDrawObjectAnchorPane.getChildren().addAll(patient.getDrawPatientBar().getAnnoRectangles());
                        visusFirstDrawObjectAnchorPane.getChildren().addAll(patient.getDrawPatientBar().getVisIncidents());
                        visusFirstDrawObjectAnchorPane.getChildren().addAll(patient.getDrawPatientBar().getInjectionRectangles());
                        visusFirstDrawObjectAnchorPane.getChildren().addAll(patient.getDrawPatientBar().getVisChangeRectangles());
                        visusFirstDrawObjectAnchorPane.getChildren().addAll(patient.getDrawPatientBar().getRegressionLine());
                        visusFirstDrawObjectAnchorPane.getChildren().addAll(patient.getDrawPatientBar().getRegressionArrow());
                        visusFirstDrawObjectAnchorPane.getChildren().addAll(patient.getDrawPatientBar().getSuddenIncidents());
                    }
                }
        );
        drawVisusFirst(visusFirstDrawObjectAnchorPane);

        //visusFirstDrawObjectAnchorPane.getChildren().add(myLab);
    }

    /**
     * Function to redraw the drawing objects on the visFirst screen
     *
     * @param myDrawPane - the panel where the drawing objects are drawn
     */
    public void drawVisusFirst(Node myDrawPane) {

        if (!visFirstCheckBox.isSelected()) {
            return;
        }

        LocalTime nowTime = LocalTime.now();

        //((DotGrid) myDrawPane).getChildren().removeAll(((DotGrid) myDrawPane).getChildren());
        //System.out.println(yearLegend.size());
        //((DotGrid) myDrawPane).getChildren().addAll(yearLegend);
        final Pattern myPat;
        String patternKey = MainWindowBuilder.scaleComboBox.getSelectionModel().getSelectedItem().toString().toLowerCase();
        //patternKey = patternKey.substring(patternKey.indexOf("'"));
        switch (patternKey) {
            case "linear":
                myPat = Pattern.LINEAR;
                break;
            case "logarithmic":
                myPat = Pattern.LOGARITHMIC;
                break;
            default:
                myPat = Pattern.DIVERGING3CLASS;
        }

        final String visYValue;
        String yValueKey = MainWindowBuilder.visusDimensionComboBox.getSelectionModel().getSelectedItem().toString().toLowerCase();
        //yValueKey = yValueKey.substring(yValueKey.indexOf("'"));
        final double minVal, maxVal;

        switch (yValueKey) {
            case "visusklasse":
                visYValue = "visusKlasse";
                minVal = VA_topos.masterPatient.getMinVisClass(); // 0d
                maxVal = VA_topos.masterPatient.getMaxVisClass(); // 6d
                break;
            case "dezimalvisus":
                visYValue = "dezimalVisusWert";
                minVal = VA_topos.masterPatient.getMinVisus(); // 0d
                maxVal = VA_topos.masterPatient.getMaxVisus(); // 2.5d
                break;
            case "logmar visus":
                visYValue = "logMarVisusWert";
                minVal = VA_topos.masterPatient.getMinLogMarVis(); // -2d
                maxVal = VA_topos.masterPatient.getMaxLogMarVis(); // LOGMAR_MAXIMUM;
                break;
            case "visusdelta":
                visYValue = "visusDelta";
                minVal = VA_topos.masterPatient.getMinVisDelta(); // -1.8d
                maxVal = VA_topos.masterPatient.getMaxVisDelta(); // 1.8d
                break;
            case "visusdelta (logmar)":
                visYValue = "visusLogMarDelta";
                minVal = VA_topos.masterPatient.getMinLogMarVisDelta();// -3d;
                maxVal = VA_topos.masterPatient.getMaxLogMarVisDelta();// 3d;
                break;
            default:
                visYValue = "visusKlasseDelta";
                minVal = VA_topos.masterPatient.getMinVisClassDelta(); // -5d;
                maxVal = VA_topos.masterPatient.getMaxVisClassDelta(); // 5d;
        }

        // Visuswerte hinzufügen
        observablePatients.forEach((key, patient) -> {
            patient.getDrawPatientBar().getSuddenIncidents().forEach(sudden -> {
                sudden.screenXPositionProperty().bind(new SimpleDoubleProperty(MainWindowBuilder.timeBeginn.isSelected() ? 0 : -DAYS.between(minDate, patient.getBeginMonitoring())));
            });

            patient.getDrawPatientBar().getVisenRectangles().forEach(rect -> {
                double attributValue;
                switch (visYValue) {
                    case "visusKlasse":
                        attributValue = ((DataModel) rect.getUserData()).getVisusKlasse();
                        invert = false;
                        break;
                    case "dezimalVisusWert":
                        attributValue = ((DataModel) rect.getUserData()).getDezimalVisusWert();
                        invert = false;
                        break;
                    case "logMarVisusWert":
                        attributValue = ((DataModel) rect.getUserData()).getLogMarVisusWert();
                        invert = true;
                        break;
                    case "visusDelta":
                        attributValue = ((DataModel) rect.getUserData()).getVisusDelta();
                        invert = false;
                        break;
                    case "visusLogMarDelta":
                        attributValue = ((DataModel) rect.getUserData()).getVisusLogMarDelta();
                        invert = true;
                        break;
                    case "visusKlasseDelta":
                        attributValue = ((DataModel) rect.getUserData()).getVisusKlasseDelta();
                        invert = false;
                        break;
                    default:
                        attributValue = ((DataModel) rect.getUserData()).getDezimalVisusWert();
                        invert = false;
                }
                rect.setStroke(STD_BORDERCOLOR);
                rect.setStrokeWidth(0.0); //.strokeWidthProperty().bind(MainWindowBuilder.borderWidthSlider.valueProperty());
                //rect.setFill(colorPicker(((DataModel) rect.getUserData()).getHauptDiagnose().ordinal()));
                rect.setFill(colorCalculator(minVal, maxVal, attributValue, myPat, invert, MainWindowBuilder.greyRadio.isSelected()));
                try {
                    rect.screenXPositionProperty().bind(new SimpleDoubleProperty(MainWindowBuilder.timeBeginn.isSelected() ? 0 : -DAYS.between(minDate, patient.getBeginMonitoring())));
                } catch (Exception e) {
                    System.out.println("Fehler Visus: " + e.getMessage() + " Patient: " + key + " x: " + rect.getX() + " y:" + rect.getY());

                }

            });
            patient.getDrawPatientBar().getVisIncidents().forEach(line -> {
                line.setStroke(STD_BORDERCOLOR);
                line.strokeWidthProperty().bind(MainWindowBuilder.borderWidthSlider.valueProperty());
            });
            try {
                //((DotGrid) myDrawPane).getChildren().addAll(patient.getDrawPatientBar().getVisenRectangles());
                //((DotGrid) myDrawPane).getChildren().addAll(patient.getDrawPatientBar().getAnnoRectangles());
                //((DotGrid) myDrawPane).getChildren().addAll(patient.getDrawPatientBar().getVisIncidents());
                //((DotGrid) myDrawPane).getChildren().add(patient.getDrawPatientBar().getRegressionLine());
            } catch (Exception e) {
            }
        });

        // Spritzen hinzufügen
        observablePatients.forEach((key, patient) -> {
            patient.getDrawPatientBar().getInjectionRectangles().forEach(polygon -> {
                polygon.setStroke(STD_BORDERCOLOR);
                polygon.strokeWidthProperty().bind(MainWindowBuilder.borderWidthSlider.valueProperty());
                polygon.setFill(colorPicker(((DataModel) polygon.getUserData()).isDiskrepanz() ? 6 : ((DataModel) polygon.getUserData()).getMedikament().ordinal()));

                try {
                    polygon.screenXPositionProperty().bind(new SimpleDoubleProperty(MainWindowBuilder.timeBeginn.isSelected() ? 0 : -DAYS.between(minDate, patient.getBeginMonitoring())));

                } catch (Exception e) {
                    System.out.println("Fehler Spritze: " + e.getMessage() + " Patient: " + key + " " + patient.getBeginMonitoring() + "x: " + polygon.getLayoutX() + " y:" + polygon.getLayoutY());

                }
            });
            try {
                //((DotGrid) myDrawPane).getChildren().addAll(patient.getDrawPatientBar().getInjectionRectangles());
            } catch (Exception e) {
            }

        });

        // Visusänderungen nach Medikamentenwechsel hinzufügen
        observablePatients.forEach((key, patient) -> {
            patient.getDrawPatientBar().getVisChangeRectangles().forEach(polygon -> {
//                double minVal = -2.0;
//                double maxVal = 2.0;
//                polygon.setFill(Color.BLACK);
                polygon.setStroke(STD_BORDERCOLOR);
                double changeVal = ((MedicationChange) polygon.getUserData()).getVisChange();
                Color col = changeVal < 0 ? Color.GREEN : changeVal > 0 ? Color.RED : Color.YELLOW;
                polygon.setFill(col);
//                polygon.setFill(colorCalculator(minVal, maxVal, ((MedicationChange) polygon.getUserData()).getVisChange(), myPat, STD_DIVIDEND, MainWindowBuilder.greyRadio.isSelected()));
                polygon.strokeWidthProperty().bind(MainWindowBuilder.borderWidthSlider.valueProperty());
                try {
                    polygon.screenXPositionProperty().bind(new SimpleDoubleProperty(MainWindowBuilder.timeBeginn.isSelected() ? 0 : -DAYS.between(minDate, patient.getBeginMonitoring())));

                } catch (Exception e) {
                    System.out.println("Fehler VischangeRectangle: " + e.getMessage() + " Patient: " + key + " " + patient.getBeginMonitoring() + "x: " + polygon.getLayoutX() + " y:" + polygon.getLayoutY());

                }
            });

            //PatientBar patBar = patient.getDrawPatientBar();
            //patBar.setFill(Color.BLACK);
            //patBar.setStroke(Color.BLACK);
            //patBar.setStrokeWidth(1.0);
            //patBar.setWidth(1000.0);
            //patBar.setWidth(300.0);
            //patBar.setHeight(20.0);
            //patBar.setVisible(true);
            //patBar.xProperty().bind(windowFitFactorX.multiply(50.0));
            //((DotGrid) myDrawPane).getChildren().addAll(patient.getDrawPatientBar());
            try {
                ((DotGrid) myDrawPane).getChildren().addAll(patient.getDrawPatientBar().getVisChangeRectangles());
            } catch (Exception e) {
            }

        });

        // What to do to create a functionioning legend:
        // 
        // To set the legend it is necessary to
        // 1. determine all colors currently used
        // 2. determine the color distribution on screen to reflect it on the legend
        // 3. determine the minimum, (center), and maximum values
        //
        // 4. Create color scheme for rectangle --> create as many stops as necessary
        // 5. assign stops to linear gradient
        // 6. setFill of legendScaleRectangle with linearGradient
//Stop[] stops = new Stop[]{new Stop(0, minCol), new Stop(1 - rectOpacity, Color.WHITE), new Stop(1 - rectOpacity, myCol), new Stop(1, maxCol)};
        switch (myPat) {
            case DIVERGING3CLASS:
                Color minCol = colorCalculator(minVal, maxVal, minVal, myPat, invert, MainWindowBuilder.greyRadio.isSelected());
                Color midCol = colorCalculator(minVal, maxVal, 0, myPat, invert, MainWindowBuilder.greyRadio.isSelected());
                Color maxCol = colorCalculator(minVal, maxVal, maxVal, myPat, invert, MainWindowBuilder.greyRadio.isSelected());
                Stop[] stops = new Stop[]{new Stop(0, minCol), new Stop(0.5, midCol), new Stop(1, maxCol)};
                LinearGradient lg1 = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
                legendScaleRectangle.setFill(lg1);
                legendMinValueLabel.setText("" + minVal);
                legendMidValueLabel.setText("0");
                legendMaxValueLabel.setText("" + maxVal);
                break;
            default:
                minCol = colorCalculator(minVal, maxVal, minVal, myPat, invert, MainWindowBuilder.greyRadio.isSelected());
                maxCol = colorCalculator(minVal, maxVal, maxVal, myPat, invert, MainWindowBuilder.greyRadio.isSelected());
                stops = new Stop[]{new Stop(0, minCol), new Stop(1, maxCol)};
                lg1 = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
                legendScaleRectangle.setFill(lg1);
                legendMinValueLabel.setText("" + minVal);
                legendMidValueLabel.setText("");
                legendMaxValueLabel.setText("" + maxVal);
        }

        VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\nZeit für das Zeichnen: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + "\n");
    }

    private double saturationCalculator(double min, double max, double value, Pattern pattern, boolean invert) {

        switch (pattern) {
            case LINEAR:
                double returnValue = (value - min) / (max - min);
                if ((returnValue < 0d) || (returnValue > 1d)) {
                    System.out.println("min: " + min + " max: " + max + " value (Linear): " + value);
                }

                return !invert ? (value - min) / (max - min) : 1 - (value - min) / (max - min);
            case LOGARITHMIC:
                return !invert ? logarithmic((value - min) / (max - min)) : 1 - logarithmic((value - min) / (max - min));

            case DIVERGING3CLASS:
                double middle = 0; //min + (max - min) / 2d;
                //double absValue = Math.abs(value);
                returnValue = (value < middle) ? 1 - ((value - min) / (middle - min)) : (1 - (max - value) / (max - middle));
                if ((returnValue < 0d) || (returnValue > 1d)) {
                    System.out.println("Error on saturationcalculation in VisusFirstMaker. returnValue not in Range: min: " + min + " max: " + max + " value (Diverging): " + value);
                }
                return !invert ? returnValue : 1 - returnValue;
            default:
                return 0;
        }

    }

    // Berechnet die Farbe je nach Datumswert und Art der Kodierung
    private Color colorCalculator(double min, double max, double value, Pattern pattern, boolean invert, boolean greyScale) {
        double middle = 0; //(min + max) / 2.0;
        int hue;
        double saturation;
        double brightness;

        if (Double.isNaN(value)) {
            return Color.rgb(0, 0, 0, 0.0);
        }

//        if (greyScale) {
//            // schwarz - weiß
//            saturation = 0;
//            brightness = 1 - saturationCalculator(min, max, value, pattern, invert);
//            switch (pattern) {
//                case DIVERGING3CLASS:
//                    if (value < middle) {
//                        hue = HSB_STDORANGE;
//                    } else {
//                        hue = HSB_STDBLUE;
//                    }
//                    break;
//                default:
//                    hue = HSB_STDBLUE;
//            }
//        } else {
        // Farbig
        saturation = saturationSlider.getValue() * saturationCalculator(min, max, value, pattern, invert);
        brightness = 1 - (brightnessSlider.getValue() * saturationCalculator(min, max, value, pattern, invert));
        switch (pattern) {
            case DIVERGING3CLASS:
                if (value < middle) {
                    hue = HSB_STDORANGE;
                } else {
                    hue = HSB_STDBLUE;
                }
                break;
            default:
                hue = HSB_STDBLUE;
        }

//        }
        if (saturation < 0 || saturation > 1) {
            saturation = 0.0;
        }
        if (brightness < 0 || brightness > 1) {
            brightness = 0.0;
        }
        return Color.hsb(hue, saturation, brightness); //opacityCalculator(min, max, value, pattern, invert));
    }

    // Wählt aus einer Liste vordefinierter Farben die entsprechende nach dem value aus
    public static Color colorPicker(int value) {
        int col = value % Math.min(BLUE.length, Math.min(RED.length, GREEN.length));
        return Color.rgb(RED[col], GREEN[col], BLUE[col]);
    }

    /**
     * Function to determine the color of the visual acuity development arrow
     *
     * @param slope - the slope of the regression curve for the visual acuity
     * value of one patient
     * @param intercept - the intercept with the y-axis -> represents the
     * starting visual acuity of one patient for the regression curve
     * @return a color which represents the category of the patient -> good
     * performer, indifferent, bad performer
     */
    private Color regressionColorPicker(double slope, double intercept, double injectionNum, double lengthOfMonitoring) {

        // Calculation of Vis development according to regression
        // y = m x + b;
        // y = difference in logMar
        // m = slope;
        // x = injectionNum or lengthOfMonitoring
        // b = intercept
        boolean timeDependent = false;
        double x = timeDependent ? lengthOfMonitoring : injectionNum;
        double startLogMarValue = intercept;
        double startLetter = -50.0 * startLogMarValue + 85;
        double endlogMarValue = slope * x + intercept;
        double endLetter = -50.0 * endlogMarValue + 85;
        double deltaLetter = endLetter - startLetter;

        //System.out.println("y=m*x+b y: " + deltaLetter + " = " + slope + " * " + injectionNum + " + " + intercept);
        if (deltaLetter > 15) {
            return Color.GREEN;
        }
        if (deltaLetter < -15) {
            return Color.RED;
        }
        return Color.GRAY;
    }

    // Visualize some summarized values for each patient into a summary Window (canvas)
    public static void makeSummaryVis(Canvas canvas) {

        List<HashMap<String, Circle>> circles = new ArrayList();
        // Get the drawing area
        GraphicsContext gc = canvas.getGraphicsContext2D();
        // Clear the drawing area
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        gc.setStroke(Color.BLACK);
        gc.strokeRect(0, 0, canvas.getWidth(), canvas.getHeight());

        // Define some standard variables for the drawing
        final double circleWidth = 10;
        final double circleHeight = 10;
        final double rowNum = 4;

        // The Space between the rows and the borders of the canvas
        final double canvasBackOffset = 40;
        final double canvasFrontOffset = 20;
        final double canvasTopOffset = 20;
        final double canvasBottomOffset = 20;

        // height of one row
        final double rowHeight = (canvas.getHeight() - canvasTopOffset - canvasBottomOffset) / rowNum;
        // width of one row (within all values are placed)
        final double rowWidth = canvas.getWidth() - canvasBackOffset - canvasFrontOffset;

//        for (int i = 0; i < rowNum; i++) {
//            gc.strokeRect(canvasFrontOffset, rowHeight * i + canvasTopOffset, rowWidth, rowHeight);
//        }
//        final double colOffset = canvasBackOffset + (rowHeight / 2d);
        // The normFactors ensure, that the values are drawn accordingly within the canvas
        //double injMax = VA_topos.masterPatient.getNumberOfInjections();
        double firstVisMax = VA_topos.masterPatient.getFirstVisus();
        double lastVisMax = VA_topos.masterPatient.getLastVisus();
        double meanMax = VA_topos.masterPatient.getMaxMeanVisus();
        double medianMax = VA_topos.masterPatient.getMaxMedianVisus();
        double legendVisMax = Math.round(Math.max(Math.max(meanMax, medianMax), Math.max(firstVisMax, lastVisMax)) * 100.0) / 100.0;

        final double normVisusFactor = rowWidth / legendVisMax;
//        final double normInjectionsFactor = rowWidth / injMax;

        double xPosition = canvasFrontOffset;
        double yPosition = canvasTopOffset + rowHeight / 2 + rowHeight * 0;
        List<Number> values = new ArrayList();
        VA_topos.observablePatients.values().forEach((patient) -> values.add(patient.getFirstVisus()));
        circles.add(drawSummaryValues(values, gc, normVisusFactor, xPosition, yPosition, circleWidth, circleHeight, false));
        drawSummaryAxes(gc, "Eingangsvisus", "" + "0", "" + legendVisMax, xPosition, yPosition + circleHeight, rowWidth, rowHeight);

        values.clear();
        VA_topos.observablePatients.values().forEach((patient) -> values.add(patient.getLastVisus()));
        yPosition = canvasTopOffset + rowHeight / 2 + rowHeight * 1;
        circles.add(drawSummaryValues(values, gc, normVisusFactor, xPosition, yPosition, circleWidth, circleHeight, false));
        drawSummaryAxes(gc, "Ausgangsvisus", "" + "0", "" + legendVisMax, xPosition, yPosition + circleHeight, rowWidth, rowHeight);

        values.clear();
        VA_topos.observablePatients.values().forEach((patient) -> values.add(patient.getMeanVisus()));
        yPosition = canvasTopOffset + rowHeight / 2 + rowHeight * 2;
        circles.add(drawSummaryValues(values, gc, normVisusFactor, xPosition, yPosition, circleWidth, circleHeight, false));
        drawSummaryAxes(gc, "Mean Visus", "" + "0", "" + legendVisMax, xPosition, yPosition + circleHeight, rowWidth, rowHeight);

        values.clear();
        VA_topos.observablePatients.values().forEach((patient) -> values.add(patient.getMedianVisus()));
        yPosition = canvasTopOffset + rowHeight / 2 + rowHeight * 3;
        circles.add(drawSummaryValues(values, gc, normVisusFactor, xPosition, yPosition, circleWidth, circleHeight, false));
        drawSummaryAxes(gc, "Median Visus", "" + "0", "" + legendVisMax, xPosition, yPosition + circleHeight, rowWidth, rowHeight);

//        VA_topos.observablePatients.values().forEach((patient) -> values.add(patient.getNumberOfInjections()));
//        yPosition = canvasTopOffset + rowHeight / 2 + rowHeight * 2;
//        circles.add(drawSummaryValues(values, gc, normInjectionsFactor, xPosition, yPosition, circleWidth, circleHeight, false));
//        drawSummaryAxes(gc, "Anz. Injektionen", "" + "0", "" + injMax, xPosition, yPosition + circleHeight, rowWidth, rowHeight);
        canvas.setUserData(circles);
        circles.forEach(circ -> {
            circ.values().forEach(c -> {
                c.setTranslateX(20);
                c.setTranslateY(20);
                c.setOpacity(1 - c.getOpacity());
                if (!summaryAnchorPane.getChildren().contains(c)) {
                    summaryAnchorPane.getChildren().add(c);
                }
            });
        });

        Circle[] highCircs = new Circle[4];

        for (int i = 0; i < 4; i++) {
            highCircs[i] = new Circle();
            highCircs[i].setFill(Color.RED);
        }
        summaryAnchorPane.getChildren().addAll(highCircs);
        summaryAnchorPane.setUserData(highCircs);
    }

    // function to draw a specific value range to a specific position in the summary window
    private static HashMap<String, Circle> drawSummaryValues(List<Number> values, GraphicsContext gc, double normFactor, double xStart, double yPos, double circleWidth, double circleHeight, boolean logarithmic) {

        // Aufgabe:
        //
        // Für jeden Wert nur ein Objekt erzeugen. Die Sättigung des Objektes vorher ermitteln
        // Eine Map mit den Objekten als userData ablegen
        // auf das Userdata zugreifen bei mouseover --> Umrandung setzen
        HashMap<String, Circle> result = new HashMap();
        result.clear();

        values.forEach(value -> {
            String key = value.toString();
            double normValue = logarithmic ? logarithmic((double) value) * normFactor : (double) value * normFactor;
            Circle circ = result.getOrDefault(key, new Circle(xStart + normValue - (circleWidth / 2d), yPos - (circleHeight / 2d), circleHeight / 2d));

            circ.setFill(Color.rgb(0, 100, 200));
            //circ.setOpacity(circ.getOpacity() + 1d / ((double) VA_topos.observablePatients.size()));
            circ.setOpacity(circ.getOpacity() - (2d / ((double) VA_topos.observablePatients.size())));
            //System.out.println(normValue / normFactor);
            //gc.setFill(Color.rgb(0, 100, 200, (10d / ((double) VA_topos.observablePatients.size()))));
            //gc.fillOval(xStart + normValue - (circleWidth / 2d), yPos - (circleHeight / 2d), circleWidth, circleHeight);
            result.put(key, circ);
            //System.out.println(result);
        });
        return result;
    }

    /**
     * function to draw the coordinate system for the values
     *
     * @param gc gc is the graphicscontext, on which the axis is drawn
     * @param title The title of the diagram, for which the axes are drawn
     * @param minValue The minValue on the X axis
     * @param maxValue The maximum vlaue on the x axis
     * @param xPos The x position, where the axes start
     * @param yPos The y position, where the axes start
     * @param width The width of the diagram
     * @param height the height of the diagram
     *
     */
    private static void drawSummaryAxes(GraphicsContext gc, String title, String minValue, String maxValue, double xPos, double yPos, double width, double height) {

        gc.setStroke(Color.BLACK);
        gc.strokeLine(xPos, yPos, xPos + width, yPos);
        gc.strokeLine(xPos, yPos, xPos, yPos - (height / 2d));
        gc.setFill(Color.BLACK);
        gc.fillText(title, xPos + width / 2d, yPos + 13d);
        gc.fillText(minValue, xPos - 3d, yPos + 13d);
        gc.fillText(maxValue, xPos + width - 7d, yPos + 13d);

    }

    // Funktion zur Ermittlung eines logarithmischen Wertes zu einem linearen Wert
    private static double logarithmic(double linear) {

        double wb = 1.25;
        double exp = 0.3;
        double loga = Math.pow(linear / wb, exp);

        return loga;
    }

    /**
     * Function to renew the visualization. Depending on the starting number,
     * either all objects are recalculated, the design is renewed and/or the
     * ordering is updated
     *
     * @param StartingNumber - The number indicating all necessary updates ->
     * (0) -> all updates are necessary (1) -> data is unchanged but the design
     * must be renewed (2) -> only the ordering on the screen must be updated.
     * @param myDrawPane - the panel on which the visualization is drawn
     */
    public void makeVisusFirst(int StartingNumber, Node myDrawPane) {

        System.out.println("Start makeVisusFirst: " + LocalTime.now());
        DoubleProperty windowFitFactorY = new SimpleDoubleProperty();
        DoubleProperty windowFitFactorX = new SimpleDoubleProperty();
        windowFitFactorY.bind(visusFirstDrawObjectAnchorPane.heightProperty().divide(observablePatients.size() * 10));

        switch (StartingNumber) {
            case 0: // Data has changed -> recalculate
                // Find the maximum duration for all patients for the screen fitting
                ((DotGrid) myDrawPane).getChildren().clear();
                LocalDate firstDate = LocalDate.MAX; // Datum des ersten Ereignisses in den Daten
                LocalDate lastDate = LocalDate.MIN; // Datum des ersten Ereignisses in den Daten

                for (Patient patient : VA_topos.observablePatients.values()) {
                    if (patient.getBeginMonitoring().isBefore(firstDate)) {
                        firstDate = patient.getBeginMonitoring();
                    }
                    if (patient.getEndMonitoring().isAfter(lastDate)) {
                        lastDate = patient.getEndMonitoring();
                    }
                }
                // Set the factor to fit the Timeline to the screen width
                windowFitFactorX.bind(visusFirstDrawObjectAnchorPane.widthProperty().divide(DAYS.between(firstDate, lastDate)));
                // Define the general drawing box for each patient
                for (Patient patient : VA_topos.observablePatients.values()) {

                    PatientBar patBar = patient.getDrawPatientBar();
                    if (patBar == null) {
                        patBar = new PatientBar();
                    }

                    Color myCol = Color.rgb(CustomColors.RGB_STDBLACK[0], CustomColors.RGB_STDBLACK[1], CustomColors.RGB_STDBLACK[2]);
                    Stop[] stops = new Stop[]{new Stop(0, Color.WHITE), new Stop(1 - 0.5, Color.WHITE), new Stop(1 - 0.5, myCol), new Stop(1, myCol)};
                    LinearGradient linearGradient = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
                    patBar.setFill(linearGradient);

                    // Position und Größe des Patientenbalkens
                    //
                    // Position
                    //
                    // X 
                    patBar.xProperty().bind(MainWindowBuilder.timeBeginn.isSelected() ? new SimpleDoubleProperty(0) : windowFitFactorX.multiply(patBar.screenXPositionProperty()));

                    // Größe
                    patBar.heightProperty().bind(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty().multiply(9d)));
                    patBar.widthProperty().bind(windowFitFactorX.multiply(patient.getDurationOfTreatment()));
                    ((DotGrid) myDrawPane).getChildren().add(patBar);
                }

            case 1: // Design has changed -> redesign
            //X,Y Positions and Color
            case 2: // Sorting has changed -> Y-Position
                Patient[] patients = new Patient[VA_topos.observablePatients.size()];
                VA_topos.observablePatients.values().toArray(patients);
                changeSorting(patients);
                int i = 0;
                for (Patient patient : patients) {
                    if (patient.isSelected()) {
                        PatientBar patBar = patients[i].getDrawPatientBar();
                        if (patBar == null) {
                            patBar = new PatientBar();
                        }
                        System.out.println("Hier geht's los!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        patBar.screenYPositionProperty().bind(new SimpleDoubleProperty(i).add(scrollPosition));
                        patBar.yProperty().bind(windowFitFactorY.multiply(MainWindowBuilder.zoomSlider.valueProperty().multiply(patBar.screenYPositionProperty().multiply(10d))));
                        patient.setDrawPatientBar(patBar);
                        i++;
                    }
                }
            default:
        }
        System.out.println("Ende makeVisusFirst: " + LocalTime.now());

    }

    // Sort a List of patients, dependend on the selection on the screen
    private void changeSorting(Patient[] patients) {

        // TODO: insert more sorting possibilities
        String sortingComboText = MainWindowBuilder.sortingComboBox.getSelectionModel().getSelectedItem().toString().toLowerCase();
        // Ob nach Augen oder ganzen Patienten sortiert werden soll
        if (patientRadio.isSelected()) {
            Arrays.sort(patients, (Patient datum1, Patient datum2) -> (datum1.getPatientID().compareTo(datum2.getPatientID())));
        } else {

            switch (sortingComboText) {
                // Sort by the lentgh of treatment ascending
                case "länge der behandlung":
                    Arrays.sort(patients, (Patient datum1, Patient datum2) -> (datum1.getLengthOfMonitoring() > datum2.getLengthOfMonitoring()) ? -1 : (datum1.getLengthOfMonitoring() < datum2.getLengthOfMonitoring()) ? 1 : 0);
                    break;
                case "anzahl der besonderen ereignisse":
                    Arrays.sort(patients, (Patient datum1, Patient datum2) -> (datum1.getNumberOfArgos_tlIncidents() > datum2.getNumberOfArgos_tlIncidents()) ? -1 : (datum1.getNumberOfArgos_tlIncidents() < datum2.getNumberOfArgos_tlIncidents()) ? 1 : 0);
                    break;
                case "anzahl der injektionen":
                    Arrays.sort(patients, (Patient datum1, Patient datum2) -> (datum1.getNumberOfInjections() > datum2.getNumberOfInjections()) ? -1 : (datum1.getNumberOfInjections() < datum2.getNumberOfInjections()) ? 1 : 0);
                    break;
                case "anzahl der verschiedenen medikamente":
                    Arrays.sort(patients, (Patient datum1, Patient datum2) -> (datum1.getNumberOfMedications() > datum2.getNumberOfMedications()) ? -1 : (datum1.getNumberOfMedications() < datum2.getNumberOfMedications()) ? 1 : 0);
                    break;
                case "visusentwicklung":
//                    try {
                        Arrays.sort(patients, (Patient datum1, Patient datum2) -> {
                            if (Double.isNaN(datum1.getNumberOfVisusMeasurements()) && Double.isNaN(datum2.getNumberOfVisusMeasurements())) {
                              return 0;   
                            }
                            if (Double.isNaN(datum1.getNumberOfVisusMeasurements())) {
                              return 1;   
                            }
                            if (Double.isNaN(datum2.getNumberOfVisusMeasurements())) {
                              return -1;   
                            }
                            return ((datum1.getLinearRegressionSlope() * datum1.getNumberOfVisusMeasurements()) < (datum2.getLinearRegressionSlope() * datum2.getNumberOfVisusMeasurements())) ? -1 : ((datum1.getLinearRegressionSlope() * datum1.getNumberOfVisusMeasurements()) > (datum2.getLinearRegressionSlope() * datum2.getNumberOfVisusMeasurements())) ? 1 : 0;
                        });
//                    } catch (Exception e) {
//                        System.out.println(e.getMessage());
//                    }

                    break;
                case "zeit stabil":
                    Arrays.sort(
                            patients, (Patient datum1, Patient datum2)
                            -> {
//                        if (                                || datum1.getNumberOfVisusMeasurements() < 2 && datum2.getNumberOfVisusMeasurements() > 1 return 1
//                                        
//                                || datum1.getFirstInjection().isBefore(datum1.getFirstVisusMeasurement())
//                                || datum1.getLastInjection().isAfter(datum1.getLastVisusMeasurement())
//                                || datum2.getNumberOfInjections() < 1
//                                || datum2.getNumberOfVisusMeasurements() < 2
//                                || datum2.getFirstInjection().isBefore(datum2.getFirstVisusMeasurement())
//                                || datum2.getLastInjection().isAfter(datum2.getLastVisusMeasurement())) {
//
//                            return -1;
//                        }
//                        double beforeDays1 = DAYS.between(datum1.getFirstVisusMeasurement(), datum1.getFirstInjection());
//                        double afterDays1 = DAYS.between(datum1.getLastInjection(), datum1.getLastVisusMeasurement());
//                        double beforeDays2 = DAYS.between(datum2.getFirstVisusMeasurement(), datum2.getFirstInjection());
//                        double afterDays2 = DAYS.between(datum2.getLastInjection(), datum2.getLastVisusMeasurement());
//                        int returnValue = 0;
//                        return Math.min(beforeDays1, afterDays1) < Math.min(beforeDays2, afterDays2) ? -1 : Math.min(beforeDays1, afterDays1) > Math.min(beforeDays2, afterDays2) ? 1 : 0;
                        return DAYS.between(datum1.getLastInjection(), datum1.getLastVisusMeasurement()) > DAYS.between(datum2.getLastInjection(), datum2.getLastVisusMeasurement()) ? -1
                                : DAYS.between(datum1.getLastInjection(), datum1.getLastVisusMeasurement()) < DAYS.between(datum2.getLastInjection(), datum2.getLastVisusMeasurement()) ? 1
                                : 0;
                    });
                    break;
                case "eingangsvisus":
                    Arrays.sort(patients, (Patient datum1, Patient datum2) -> (datum1.getFirstVisus() > datum2.getFirstVisus()) ? -1 : (datum1.getFirstVisus() < datum2.getFirstVisus()) ? 1 : 0);
                    break;
                case "ausgangsvisus":
                    Arrays.sort(patients, (Patient datum1, Patient datum2) -> (datum1.getLastVisus() > datum2.getLastVisus()) ? -1 : (datum1.getLastVisus() < datum2.getLastVisus()) ? 1 : 0);
                    break;
                case "durchschnittsvisus":
                    Arrays.sort(patients, (Patient datum1, Patient datum2) -> (datum1.getMeanVisus() > datum2.getMeanVisus()) ? -1 : (datum1.getMeanVisus() < datum2.getMeanVisus()) ? 1 : 0);
                    break;
                case "medianvisus":
                    Arrays.sort(patients, (Patient datum1, Patient datum2) -> (datum1.getMedianVisus() > datum2.getMedianVisus()) ? -1 : (datum1.getMedianVisus() < datum2.getMedianVisus()) ? 1 : 0);
                    break;
                case "anzahl diagnosen im system":
                    Arrays.sort(patients, (Patient datum1, Patient datum2) -> (datum1.getNumberOfDiagnoses() > datum2.getNumberOfDiagnoses()) ? -1 : (datum1.getNumberOfDiagnoses() == datum2.getNumberOfDiagnoses()) ? 0 : 1);
                    break;
                default:
                    //sortingKind = "visusImprovementAfterMedicationSwitchRadio";
                    Arrays.sort(patients, (Patient datum1, Patient datum2) -> {
                        if ((datum1.getMyMedicationChanges().size() < 1) || (datum2.getMyMedicationChanges().size() < 1)) {
                            return -1;
                        }
                        double max1 = -10;
                        for (MedicationChange value : datum1.getMyMedicationChanges().values()) {
                            if (value.getVisChange() > max1) {
                                max1 = value.getVisChange();
                            }
                        }
                        double max2 = -10;
                        for (MedicationChange value : datum2.getMyMedicationChanges().values()) {
                            if (value.getVisChange() > max2) {
                                max2 = value.getVisChange();
                            }
                        }
                        return max1 > max2 ? 1 : (max1 < max2) ? -1 : 0;
                    });
            }
        }
    }

    // Set the design of a Rectangle (e.g. Sudden incident) in respect to its state - normal, cleansed, or annotated
    public static void setRectangleStandardDesign(OrderRectangle rect) {
        rect.setOpacity(1.0);
        rect.setStroke(STD_BORDERCOLOR);
    }

    public static void setRectangleAnnotationDesign(OrderRectangle rect) {
        rect.setOpacity(1.0);
        rect.setStroke(ANNO_BORDERCOLOR);
    }

    public static void setRectangleCleansedDesign(OrderRectangle rect) {
        rect.setOpacity(0.4);
        rect.setStroke(ANNO_BORDERCOLOR);
    }

    // Set the design of a Polygon (e.g. Sudden incident) in respect to its state - normal, cleansed, or annotated
    public static void setPolygonStandardDesign(OrderPolygon polygon) {
        polygon.setOpacity(1.0);
        polygon.setStroke(STD_BORDERCOLOR);
    }

    public static void setPolygonAnnotationDesign(OrderPolygon polygon) {
        polygon.setOpacity(1.0);
        polygon.setStroke(ANNO_BORDERCOLOR);
    }

    public static void setPolygonCleansedDesign(OrderPolygon polygon) {
        polygon.setOpacity(0.4);
        polygon.setStroke(ANNO_BORDERCOLOR);
    }

}
