/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.visualization;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

/**
 *
 * @author lokal-admin
 */
public class OrderPolygon extends Polygon {

    private DoubleProperty screenXPosition = new SimpleDoubleProperty();
    private DoubleProperty screenYPosition = new SimpleDoubleProperty();

    public OrderPolygon() {
        this.screenXPosition = new SimpleDoubleProperty(0);
        this.screenYPosition = new SimpleDoubleProperty(0);
    }

    public OrderPolygon(Double[] shape, double shapeFactor, String text) {
        this.screenXPosition = new SimpleDoubleProperty(0);
        this.screenYPosition = new SimpleDoubleProperty(0);
        switch (text) {
            case "makulablutung":
                this.setFill(Color.DARKRED);
                break;
            case "phako":
                this.setFill(Color.YELLOW);
                break;
            case "ambulante chemo":
                this.setFill(Color.GRAY);
                break;
        }
        this.setStroke(Color.BLACK);
        Double[] shapeFac = new Double[shape.length];
        for (int i = 0; i < shape.length; i++) {
            shapeFac[i] = shape[i] * shapeFactor;
        }
        this.getPoints().addAll(shapeFac);
    }

    public double getScreenXPosition() {
        return screenXPosition.get();
    }

    public void setScreenXPosition(double value) {
        screenXPosition.set(value);
    }

    public DoubleProperty screenXPositionProperty() {
        return screenXPosition;
    }

    public double getScreenYPosition() {
        return screenYPosition.get();
    }

    public void setScreenYPosition(double value) {
        screenYPosition.set(value);
    }

    public DoubleProperty screenYPositionProperty() {
        return screenYPosition;
    }

}
