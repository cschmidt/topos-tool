/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.visualization;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

/**
 *
 * @author Christoph Schmidt
 */
public class InjPolygon extends Polygon {

    private static final Double[] POLYSHAPE = new Double[]{
        0d, 0d,
        3d, 0d,
        3d, 2d,
        0d, 2d};
    private DoubleProperty globalXTranslation = new SimpleDoubleProperty();
    private DoubleProperty globalYTranslation = new SimpleDoubleProperty();
    private DoubleProperty xOffset = new SimpleDoubleProperty();
    private DoubleProperty yOffset = new SimpleDoubleProperty();



    public InjPolygon() {
        this.getPoints().addAll(POLYSHAPE);
    }

    public InjPolygon(Double[] shapePoints) throws NullPointerException {
        this.getPoints().addAll(shapePoints);
    }

    public InjPolygon(Color fillColor) throws NullPointerException {
        this.getPoints().addAll(POLYSHAPE);
        this.setStroke(fillColor);
        this.setFill(fillColor);
    }

    public InjPolygon(Color fillColor, Color borderColor) throws NullPointerException {
        this.getPoints().addAll(POLYSHAPE);
        this.setFill(fillColor);
        this.setStroke(borderColor);
    }

    public InjPolygon(Color fillColor, Color borderColor, Double[] shapePoints) throws NullPointerException {
        this.getPoints().addAll(shapePoints);
        this.setFill(fillColor);
        this.setStroke(borderColor);
    }

    private void setLocationProperty(DoubleProperty xProperty, DoubleProperty yProperty) {
        this.layoutXProperty().bind(xProperty);
        this.layoutYProperty().bind(yProperty);
    }

    private void setSizeProperty(DoubleProperty xProperty, DoubleProperty yProperty) {
        this.scaleXProperty().bind(xProperty);
        this.scaleYProperty().bind(yProperty);
    }

}
