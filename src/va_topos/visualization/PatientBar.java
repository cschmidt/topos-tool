/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.visualization;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author varie
 */
public class PatientBar extends OrderRectangle {

    private List<OrderRectangle> visenRectangles = new ArrayList();
    private List<OrderPolygon> suddenIncidents = new ArrayList();
    private List<OrderRectangle> injectionRectangles = new ArrayList();
    private List<OrderRectangle> visChangeRectangles = new ArrayList();
    private List<OrderRectangle> annoRectangles = new ArrayList();
    private List<OrderLine> visIncidents = new ArrayList();
    private OrderLine regressionLine = new OrderLine();
    private OrderPolygon regressionArrow = new OrderPolygon();
    private String patientID;

    public void patientBar(String patientID) {
        this.patientID = patientID;
        this.visenRectangles = new ArrayList();
        this.injectionRectangles = new ArrayList();
        this.annoRectangles = new ArrayList();
        this.visIncidents = new ArrayList();
    }

    public List<OrderPolygon> getSuddenIncidents() {
        return suddenIncidents;
    }

    public void setSuddenIncidents(List<OrderPolygon> suddenIncidents) {
        this.suddenIncidents = suddenIncidents;
    }
    
    public OrderPolygon getRegressionArrow() {
        return regressionArrow;
    }

    public void setRegressionArrow(OrderPolygon regressionArrow) {
        this.regressionArrow = regressionArrow;
    }

    public OrderLine getRegressionLine() {
        return regressionLine;
    }

    public void setRegressionLine(OrderLine regressionLine) {
        this.regressionLine = regressionLine;
    }

    public List<OrderLine> getVisIncidents() {
        return visIncidents;
    }

    public void setVisIncidents(List<OrderLine> visIncidents) {
        this.visIncidents = visIncidents;
    }

    public List<OrderRectangle> getVisenRectangles() {
        return visenRectangles;
    }

    public void setVisenRectangles(List<OrderRectangle> visen) {
        this.visenRectangles = visen;
    }

    public List<OrderRectangle> getAnnoRectangles() {
        return annoRectangles;
    }

    public void setAnnoRectangles(List<OrderRectangle> annos) {
        this.annoRectangles = annos;
    }

    public List<OrderRectangle> getInjectionRectangles() {
        return injectionRectangles;
    }

    public void setInjectionRectangles(List<OrderRectangle> injections) {
        this.injectionRectangles = injections;
    }

    public List<OrderRectangle> getVisChangeRectangles() {
        return visChangeRectangles;
    }

    public void setVisChangeRectangles(List<OrderRectangle> visChangeRectangles) {
        this.visChangeRectangles = visChangeRectangles;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public void bindAllY(List<OrderRectangle> visen, List<OrderRectangle> injections, List<OrderRectangle> visChangeRectangles) {
        this.visenRectangles.forEach(visus -> visus.yProperty().bind(this.yProperty()));
        this.injectionRectangles.forEach(injection -> injection.layoutYProperty().bind(this.yProperty()));
        this.visChangeRectangles.forEach(visChangeRectangle -> visChangeRectangle.layoutYProperty().bind(this.yProperty()));
        //this.annoRectangles.forEach(annoRectangle -> annoRectangle.layoutYProperty().bind(this.yProperty()));
    }

}
