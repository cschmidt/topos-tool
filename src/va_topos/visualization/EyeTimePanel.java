/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.visualization;

import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import javafx.geometry.Insets;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import va_topos.color.CustomColors;
import va_topos.data.DataModel.AntiVEGFMedikament;
import va_topos.data.DataModel.AugeLateralitaet;
import va_topos.data.GlobalData;
import va_topos.data.Patient;

/**
 *
 * @author lokal-admin
 */
public final class EyeTimePanel extends StackPane {

    private AugeLateralitaet laterality = AugeLateralitaet.E;
    private final DoubleProperty maxDays = new SimpleDoubleProperty();
    private final DoubleProperty xOffset = new SimpleDoubleProperty();
    private final DoubleProperty yOffset = new SimpleDoubleProperty();

    private ObservableMap<LocalDate, Double> visualAcuities = FXCollections.observableHashMap();
    private ObservableMap<LocalDate, AntiVEGFMedikament> injections = FXCollections.observableHashMap();
    private ObservableMap<Double, InjPolygon> injDrawing = FXCollections.observableHashMap();
    private ObservableMap<Double, Rectangle> visDrawing = FXCollections.observableHashMap();

    public EyeTimePanel(GlobalData glob, Patient patient) {

        //System.out.println(glob + " : " + patient.getxOffset());
        this.xOffset.bind(glob.xFitFactorProperty().multiply(patient.xOffsetProperty()));
        this.maxDaysProperty().bind(new SimpleDoubleProperty(patient.getLengthOfMonitoring()));
        if (this.translateXProperty().isBound()) {
        } else {
            this.translateXProperty().bind(xOffset);
        }

        if (this.prefWidthProperty().isBound()) {
        } else {
            //System.out.println(glob.xFitFactorProperty().get());
            this.prefWidthProperty().bind(glob.xFitFactorProperty().multiply(patient.getLengthOfMonitoring()));
        }

        //this.xOffset.bind(maxDays.multiply(MainWindowBuilder.normBegin.selectedProperty()));
        this.laterality = patient.getLateralitaet();
        this.injections = patient.getSpritzen();
        this.visualAcuities = patient.getVisen();
        this.visualAcuities.addListener(new MapChangeListener() {
            @Override
            public void onChanged(MapChangeListener.Change change) {
                // recalculate polygons
                System.out.println("visual Acuities in EyeTimePanel have changed.");
            }
        });
        this.injections.addListener(new MapChangeListener() {
            @Override
            public void onChanged(MapChangeListener.Change change) {
                // recalculate polygons
                change.getMap().forEach((key, value) -> {
                    System.out.println("Injections in EyeTimePanel have changed. Key: " + key + " Value: " + value);
                });
            }
        });
        this.injDrawing.addListener(new MapChangeListener() {
            @Override
            public void onChanged(MapChangeListener.Change change) {
                // recalculate polygons
                System.out.println("InjectionDrawing Objects in EyeTimePanel have changed.");
            }
        });
        this.visDrawing.addListener(new MapChangeListener() {
            @Override
            public void onChanged(MapChangeListener.Change change) {
                // recalculate polygons
                System.out.println("visual Acuities Drawing Objects in EyeTimePanel have changed.");
            }
        });
        this.initDraw();
        this.borderSetter(Color.rgb(CustomColors.RGB_STDGREY[0], CustomColors.RGB_STDGREY[1], CustomColors.RGB_STDGREY[2]), 1.0);

    }

    public double getyOffset() {
        return yOffset.get();
    }

    public void setyOffset(double value) {
        yOffset.set(value);
    }

    public DoubleProperty yOffsetProperty() {
        return yOffset;
    }

    public double getxOffset() {
        return xOffset.get();
    }

    public void setxOffset(double value) {
        xOffset.set(value);
    }

    public DoubleProperty xOffsetProperty() {
        return xOffset;
    }

    public double getMaxDays() {
        return maxDays.get();
    }

    public void setMaxDays(double value) {
        maxDays.set(value);
    }

    public DoubleProperty maxDaysProperty() {
        return maxDays;
    }

    public AugeLateralitaet getLaterality() {
        return laterality;
    }

    public void setLaterality(AugeLateralitaet laterality) {
        this.laterality = laterality;
    }

    public ObservableMap<LocalDate, Double> getVisualAcuities() {
        return visualAcuities;
    }

    public void setVisualAcuities(ObservableMap<LocalDate, Double> visualAcuities) {
        this.visualAcuities = visualAcuities;
    }

    public ObservableMap<LocalDate, AntiVEGFMedikament> getInjections() {
        return injections;
    }

    public void setInjections(ObservableMap<LocalDate, AntiVEGFMedikament> injections) {
        this.injections = injections;
    }

    public ObservableMap<Double, InjPolygon> getInjDrawing() {
        return injDrawing;
    }

    public void setInjDrawing(ObservableMap<Double, InjPolygon> injDrawing) {
        this.injDrawing = injDrawing;
    }

    public ObservableMap<Double, Rectangle> getVisDrawing() {
        return visDrawing;
    }

    public void setVisDrawing(ObservableMap<Double, Rectangle> visDrawing) {
        this.visDrawing = visDrawing;
    }

    private void borderSetter(Color borderCol, double borderWidth) {
        this.setBorder(new Border(new BorderStroke(borderCol, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(borderWidth), Insets.EMPTY)));
    }

    private void initDraw() {
        Line myLine = new Line();
        myLine.setStroke(Color.rgb(CustomColors.RGB_STDGREY[0], CustomColors.RGB_STDGREY[1], CustomColors.RGB_STDGREY[2]));
        myLine.setStrokeWidth(1.0);
        myLine.setStartX(0.0);
        myLine.startYProperty().bind(this.heightProperty().divide(2.0));
        myLine.endXProperty().bind(this.widthProperty());
        myLine.endYProperty().bind(myLine.startYProperty());
        this.getChildren().add(myLine);
        this.getChildren().addAll(this.injDrawing.values());
        this.getChildren().addAll(this.visDrawing.values());
    }

    private double convertLocalDateToPercentage(LocalDate date, LocalDate minDate, double maxDays) {
        double daysPast = DAYS.between(minDate, date);
        return daysPast / maxDays;
    }

    private void injectionSetter(Patient patient) {
        this.injDrawing.clear();
        this.injections.forEach((date, injection) -> {
            //InjPolygon inj = new InjPolygon(Color.rgb(CustomColors.RGB_RED_VALUES[injection.ordinal()],CustomColors.RGB_GREEN_VALUES[injection.ordinal()],CustomColors.RGB_BLUE_VALUES[injection.ordinal()]));
            this.injDrawing.put(convertLocalDateToPercentage(date, patient.getBeginMonitoring(), maxDays.get()), new InjPolygon(Color.rgb(CustomColors.RGB_RED_VALUES[injection.ordinal()], CustomColors.RGB_GREEN_VALUES[injection.ordinal()], CustomColors.RGB_BLUE_VALUES[injection.ordinal()])));

        });
    }

    private void setLocation(DoubleProperty xLocation, DoubleProperty yLocation) {

    }
}
