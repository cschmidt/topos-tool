/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.visualization;

import va_topos.data.DataModel;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.EventHandler;
import javafx.scene.chart.Axis;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import va_topos.events.EventHandlers;
import va_topos.ui.MainWindowBuilder;
import va_topos.VA_topos;

/**
 *
 * @author varie
 */
@SuppressWarnings("unchecked")
public class StandardVisMaker {

    StringProperty groupBy = new SimpleStringProperty();
    StringProperty xDimLabel = new SimpleStringProperty();
    StringProperty yDimLabel = new SimpleStringProperty();
    ObservableList<String> groupItems = FXCollections.observableList(new ArrayList());

    public void makeControlPanel(AnchorPane myParent) {

        Label xDimBezeichner = new Label("X-Achse");
        AnchorPane.setLeftAnchor(xDimBezeichner, 20.0);
        AnchorPane.setTopAnchor(xDimBezeichner, 20.0);

        ComboBox myXDimension = new ComboBox();
        AnchorPane.setLeftAnchor(myXDimension, 20.0);
        AnchorPane.setRightAnchor(myXDimension, 20.0);
        AnchorPane.setTopAnchor(myXDimension, 40.0);
        myXDimension.setPromptText("Dimension der X-Achse");
        xDimLabel.bind(myXDimension.valueProperty());

        myXDimension.valueProperty().addListener((boxValues, oldSelected, newSelected) -> {
            makeStdVisScatterPlot(MainWindowBuilder.stdVisAnchorPane);
        });

        Label yDimBezeichner = new Label("Y-Achse");
        AnchorPane.setLeftAnchor(yDimBezeichner, 20.0);
        AnchorPane.setTopAnchor(yDimBezeichner, 80.0);

        ComboBox myYDimension = new ComboBox();
        myYDimension.setPromptText("Dimension der Y-Achse");
        AnchorPane.setLeftAnchor(myYDimension, 20.0);
        AnchorPane.setRightAnchor(myYDimension, 20.0);
        AnchorPane.setTopAnchor(myYDimension, 100.0);
        yDimLabel.bind(myYDimension.valueProperty());
        myYDimension.valueProperty().addListener((boxValues, oldSelected, newSelected) -> {
            makeStdVisScatterPlot(MainWindowBuilder.stdVisAnchorPane);
        });

        Label grupBezeichner = new Label("Gruppierung");
        AnchorPane.setLeftAnchor(grupBezeichner, 20.0);
        AnchorPane.setTopAnchor(grupBezeichner, 140.0);

        // ComboBox für die Gruppierung der Daten
//        ComboBox myGrouping = new ComboBox();
//        myGrouping.setPromptText("Wählen Sie eine Gruppierung");
//        AnchorPane.setLeftAnchor(myGrouping, 20.0);
//        AnchorPane.setRightAnchor(myGrouping, 20.0);
//        AnchorPane.setTopAnchor(myGrouping, 160.0);
//
//        groupBy.bind(myGrouping.valueProperty());
//        myGrouping.valueProperty().addListener((boxValues, oldSelected, newSelected) -> {
//            makeStdVisScatterPlot(MainWindowMaker.stdVisAnchorPane);
//        });
        // Zu den ComboBoxen werden alle Dimensionen hinzugefügt.
        VA_topos.myDimensions.forEach((dimension, dimType) -> {
            myXDimension.getItems().add(dimension);
            myYDimension.getItems().add(dimension);
            //myGrouping.getItems().add(dimension);
        });

        // Falls ausreichend Dimensionen vorhanden sind, werden standardDims ausgewählt
        if (VA_topos.myDimensions.size() > 2) {
            myXDimension.setValue(myXDimension.getItems().get(0));
            myYDimension.setValue(myYDimension.getItems().get(1));
            //myGrouping.setValue(myGrouping.getItems().get(2));
        }

        TreeView myCheck = new TreeView();
        AnchorPane.setLeftAnchor(myCheck, 20.0);
        AnchorPane.setRightAnchor(myCheck, 20.0);
        AnchorPane.setTopAnchor(myCheck, 200.0);

        myCheck.setRoot(new CheckBoxTreeItem("Dimensions"));
        myCheck.setCellFactory(CheckBoxTreeCell.<String>forTreeView());
        VA_topos.myDimensions.forEach((dimension, dimType) -> {
            CheckBoxTreeItem myItem = new CheckBoxTreeItem(dimension);
            myItem.selectedProperty().addListener((listen, oldValue, newValue) -> {
                if (newValue) {
                    groupItems.add(myItem.getValue().toString());
                } else {
                    groupItems.remove(myItem.getValue().toString());
                }
                makeStdVisScatterPlot(MainWindowBuilder.stdVisAnchorPane);
            });
            myCheck.getRoot().getChildren().add(myItem);
        });
//        myCheck.getCheckModel().getCheckedItems().addListener((Observable c) -> {
//        groupItems.add("patientenID"); // = myCheck.getCheckModel().getCheckedItems();
//            makeStdVisScatterPlot(MainWindowMaker.stdVisAnchorPane);
//            System.out.println(myCheck.getCheckModel().getCheckedItems());
//            });
//        groupBy.bind(myCheck.itemsProperty());
//        myGrouping.valueProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue boxValues, String oldSelected, String newSelected) {
//                makeVisusFirst(MakeMainWindow.stdVisAnchorPane);
//            }
//        });

//            myCheck.getItemBooleanProperty(this).itemsProperty().addListener(new ChangeListener<String>() {
//                @Override
//                public void changed(ObservableValue boxValues, String oldSelected, String newSelected) {
//                    makeVisusFirst(MainWindowMaker.stdVisAnchorPane);
//                    System.out.println("changed");
//                }
//            });
        // Die Controls zum Panel hinzufügen
        myParent.getChildren().addAll(myCheck);
        myParent.getChildren().addAll(myXDimension, myYDimension);
        myParent.getChildren().addAll(xDimBezeichner, yDimBezeichner, grupBezeichner);

    }
    // Prozedur, um das Visualisierungsobjekt zu erzeugen

    public void makeStdVisScatterPlot(AnchorPane myParent) {

        // ContainerObject, um auf die Prozeduren in der Datenmodellklasse zuzugreifen
        DataModel myMod = new DataModel();

        List<String> gruppierung = groupItems;

        String xAchsenDim = (xDimLabel.isEmpty().get()) ? "ereignisDatum" : xDimLabel.get();
        // Den ersten Buchstaben in Kleinbuchstaben umwandeln
        xAchsenDim = xAchsenDim.substring(0, 1).toLowerCase() + xAchsenDim.substring(1);

        String yAchsenDim = (yDimLabel.isEmpty().get()) ? "dezimalVisusWert" : yDimLabel.get();
        // Den ersten Buchstaben in Kleinbuchstaben umwandeln
        yAchsenDim = yAchsenDim.substring(0, 1).toLowerCase() + yAchsenDim.substring(1);

        // Alles für den Chart vorbereiten
        // Der Achsentyp ist abhängig von den gewählten Kategorien
        ScatterChart scatterPlot;

        switch (VA_topos.myDimensions.get(yAchsenDim.substring(0, 1).toUpperCase() + yAchsenDim.substring(1))) {
            case NUMBER:
                if (VA_topos.myDimensions.get(xAchsenDim.substring(0, 1).toUpperCase() + xAchsenDim.substring(1)) == DataModel.DataTypes.NUMBER) {
                    scatterPlot = new ScatterChart<>(new NumberAxis(), new NumberAxis());
                } else {
                    scatterPlot = new ScatterChart<>(new CategoryAxis(), new NumberAxis());
                    //xAchsenSort(scatterPlot.getXAxis(), gruppierung);
                }
                break;
            default: // OTHER
                if (VA_topos.myDimensions.get(xAchsenDim.substring(0, 1).toUpperCase() + xAchsenDim.substring(1)) == DataModel.DataTypes.NUMBER) {
                    scatterPlot = new ScatterChart<>(new NumberAxis(), new CategoryAxis());
                } else {
                    scatterPlot = new ScatterChart<>(new CategoryAxis(), new CategoryAxis());
                }

        } // switch myDimensions

        scatterPlot.getXAxis().setLabel(xAchsenDim);
        scatterPlot.getYAxis().setLabel(yAchsenDim);

        scatterPlot.getXAxis().setAutoRanging(true);
        scatterPlot.getYAxis().setAutoRanging(true);

        // Chart formatieren
        AnchorPane.setLeftAnchor(scatterPlot, 0.0);
        AnchorPane.setRightAnchor(scatterPlot, 0.0);
        AnchorPane.setTopAnchor(scatterPlot, 0.0);
        AnchorPane.setBottomAnchor(scatterPlot, 0.0);
        scatterPlot.setTitle("Darstellung von " + yAchsenDim + " über " + xAchsenDim + " gruppiert nach " + groupItems);
        scatterPlot.setHorizontalGridLinesVisible(false);
        scatterPlot.setVerticalGridLinesVisible(false);
        //scatterPlot.createSymbolsProperty().set(true);
        //scatterPlot.setLegendVisible(false);

        EventHandler<MouseEvent> onMouseEnteredListener
                = (MouseEvent event) -> {
                    EventHandlers eh = new EventHandlers();
                    eh.lineChartEntered(event);
                };
        EventHandler<MouseEvent> onMouseExitedListener
                = (MouseEvent event) -> {
                    EventHandlers eh = new EventHandlers();
                    eh.lineChartExited(event);
                };
        EventHandler<MouseEvent> onMouseMovedListener
                = (MouseEvent event) -> {
                    EventHandlers eh = new EventHandlers();
                    eh.lineChartMouseMoved(event);
                };
        scatterPlot.setOnMouseEntered(onMouseEnteredListener);
        scatterPlot.setOnMouseExited(onMouseExitedListener);
        scatterPlot.setOnMouseMoved(onMouseMovedListener);

        // jeden berechneten Datenpunkt zum Linechart hinzufügen
        // aus Performancegründen wird zunächst ein DataSet erzeugt, welches mit Daten gefüllt wird
        // Diese Dataset wird im Anschluss in einem Schritt zur Series hinzugefügt
        // Realize the grouping
        //
        // 1. Auswahl einer Dimension für die Gruppierung
        // Durch Combobox und das daran gebundene "groupBy" gegeben
        // 1a. Auswahl der xAchsen- und yAchsendimension
        // Durch Comboboxen und daran gebundene Variablen (xDimLabel, yDimLabel) gegeben
        //System.out.println("Hier geht's los: " + groupBy.get());
        // 2. Erstellung einer Liste der Werte für die Gruppierungsdimension sowie die zugehörigen Wertepaare        
        // myGroupMap enthält alle voneinander verschiedenen werte der dimension "groupBy" in DataModel und die für die xAchse und yAchse ausgwählten Wertepaare aus dem Datenmodell
        ObservableMap<String, XYChart.Series> myGroupMap;
        myGroupMap = myMod.getValueList(VA_topos.sortedFilteredData, gruppierung, xAchsenDim, yAchsenDim);
        // 3. Hinzufügen aller Chartserien zum Scatterplot für die Anzeige
        myGroupMap.forEach((groupValue, xyChartSeries) -> {

            try {
                xyChartSeries.setName(groupValue);
                scatterPlot.getData().add(xyChartSeries);

            } catch (Exception e) {
                System.out.println("Could not add ChartSeries to Scatterplot: " + e.getMessage());
            }
        });

        // evtl vorhandene Visualisierungen entfernen
        myParent.getChildren().clear();
        // neuen lineChart hinzufügen
        myParent.getChildren().add(scatterPlot);
    }

    private void xAchsenSort(Axis xAxis, String sortDimension) {

    }

}
