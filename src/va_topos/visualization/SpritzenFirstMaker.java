/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.visualization;

import javafx.collections.ObservableMap;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import va_topos.data.Patient;

/**
 *
 * @author lokal-admin
 */
public class SpritzenFirstMaker {

    /**
     * Function to show the visualization of the injections and visual acuity of
     * the patients' eyes over time
     *
     * @param drawPanel the AnchorPane in which the drawings shall be shown
     * @param patients the patients Eyes, which shall be drawn
     */
    public static void makeSpritzenFirst(StackPane drawPanel, ObservableMap<String, Patient> patients) {
        // Only if neither patients nor drawPanel is null

        if ((patients != null) && (drawPanel != null)) {
            // Remove old "eyes"
            drawPanel.getChildren().clear();
            va_topos.VA_topos.globalData.windowWidthProperty().bind(drawPanel.widthProperty());
            va_topos.VA_topos.globalData.windowHeightProperty().bind(drawPanel.heightProperty());

            int index = 1;
            // go through the patients and add the eyes for each patient to the drawing area
            for (Patient patient : patients.values()) {
                StackPane myPan = patient.getEyeTimePanel();
                //MainWindowBuilder.setAnchor(myPan, 10.0, Double.NaN, index*30.0, Double.NaN);
                myPan.translateYProperty().bind(drawPanel.heightProperty().divide(patients.size()).multiply(index));
//                myPan.prefHeightProperty().bind(drawPanel.heightProperty().divide(patients.size()));
//                myPan.setPrefSize(60.0, 20.0);

                //myPan.setPrefSize(200.0, 10.0);
                System.out.println("Adding panel: " + index + " TranslateX: " + myPan.getTranslateX() + " TranslateY: " + myPan.getTranslateY());
                drawPanel.getChildren().add(myPan);
                index++;
            }
        }
    }

}
