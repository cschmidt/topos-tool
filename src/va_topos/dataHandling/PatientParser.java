/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.dataHandling;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.commons.math3.stat.StatUtils;
import va_topos.VA_topos;
import va_topos.data.DataModel;
import va_topos.data.DataModel.HauptDiagnosen;
import va_topos.data.GlobalData;
import va_topos.data.MedicationChange;
import va_topos.data.Patient;
import va_topos.visualization.EyeTimePanel;
import org.apache.commons.math3.stat.regression.SimpleRegression;

/**
 *
 * @author lokal-admin
 */
public class PatientParser {

    @SuppressWarnings("unchecked")
    public List<String> parsePatientDataFromIncidentList(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {

        List<String> result = new ArrayList();

        // Initialize all Patients
        // -> Generate Patient objects and basic patient data (ID, sex, date of Birth)
        LocalTime nowTime = LocalTime.now();
        result.add("Ergebnis der Patienteninitialisierung: " + generatePatientBasics(data, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // Derive the basic patient data from incidentlist
        nowTime = LocalTime.now();
        result.add("Ergebnis Grunddatenermittlung: " + setVisenPressureMedicationIncidents(data, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // Derive the difference between the current and the previous visus (visit related)
        nowTime = LocalTime.now();
        result.add("Ergebnis VisusDelta: " + calculateVisusDelta(data, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // Derive the difference between the current and the previous visus in LogMar (visit related)
        nowTime = LocalTime.now();
        result.add("Ergebnis VisusLogMarDelta: " + calculateVisusLogMarDelta(data, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // Derive the difference between the current and the previous visusClass (visit related)
        nowTime = LocalTime.now();
        result.add("Ergebnis VisusKlassenDelta: " + calculateVisusClassDelta(data, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // Get/Set the minimum and maximum Visus for each patient
        nowTime = LocalTime.now();
        result.add("Ergebnis der Diagnosenermittlung: " + setDiagnoses(data, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // Get/Set the time between the injections (time since last injection)
        nowTime = LocalTime.now();
        result.add("Ergebnis Injektionszwischenzeit - Ermittlung: " + deriveTimeBetweenInjections(data, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // Get/Set the time between the visus measurements (time since last measutrement)
        nowTime = LocalTime.now();
        result.add("Ergebnis Visuszwischenzeit - Ermittlung: " + deriveTimeBetweenMeasurements(data, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // Get/Set the number of visus-measruements between two injections --> derive wanted and unwanted breaks
        nowTime = LocalTime.now();
        result.add("Ergebnis Anz. Visusmessungen in Injektionszwischenzeit - Ermittlung: " + deriveMeasurementsBetweenInjections(data, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // Get/Set the aggregated information for each patient
        nowTime = LocalTime.now();
        result.add("Aggregierte Informationen für jeden Patienten ermitteln: " + generatePatientSummaries(data, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // Get/Set the number of visus-measruements between two injections --> derive wanted and unwanted breaks
        nowTime = LocalTime.now();
        result.add("Ergebnis Visusentwicklung nach Medikamentenwechsel: " + getVisusForInjections(patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // initialize and set the global parameters
        nowTime = LocalTime.now();
        result.add("Ergebnis Initialisierung der globalen Parameter: " + setGlobalData(VA_topos.globalData, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");
        // initialize the drawing objects
        nowTime = LocalTime.now();
        result.add("Ergebnis Initialisierung der Visualisierungsobjekte: " + generateVisualizationObjects(VA_topos.globalData, patients) + " (Zeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0) + ")");


        /* This section is to calculate the number of incidents per patient and per year
        
        // Count the number of patient data points per year
        double maxNumberPerYear = 0.0;
        for (Patient patient : patients.values()) {
            double thisPatientIncidents = 0.0;
        }

        // Map with all patients and for each patient the incidents per Year
        HashMap<String, HashMap<String, Integer>> countMap = new HashMap();

        VA_topos.observableData.forEach(datum -> {
            HashMap<String, Integer> patMap = countMap.getOrDefault(datum.getPatientenID() + datum.getUntersuchungLateralitaet(), new HashMap());
            Integer count = patMap.getOrDefault("" + datum.getEreignisDatum().getYear(), 0);
            count++;
            patMap.put("" + datum.getEreignisDatum().getYear(), count);
            countMap.put(datum.getPatientenID() + datum.getUntersuchungLateralitaet(), patMap);
        });
        // Get/Set the number of visus-measruements between two injections --> derive wanted and unwanted breaks
        //result.add("Ergebnis PausenListe - Ermittlung: " + derivePausenListe(data, group.get().getObservableAnnotations()));

        HashMap<Integer, Integer> maxCount = new HashMap();

        countMap.forEach((key, patMap) -> {
            patMap.forEach((patKey, count) -> {
                maxCount.put(count, maxCount.getOrDefault(count, 0) + 1);
            });
        });
        System.out.println("Incidents per Year;Count");
        maxCount.forEach((key, value) -> System.out.println(key + ";" + value));
         */
        return result;

    }

    // Funktion, um einen Patienten mit Daten zu füllen
    public String generatePatientBasics(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {

        patients.clear();
        String result = "Basisdaten des Patienten erfolgreich geschrieben";
        // Initiate Variables
        data.forEach(datum -> {
            patients.putIfAbsent(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet(), new Patient(datum, true));
        });
        return result;

    }

    // Save all visual acuity values, tensio values and injections 
    public String setVisenPressureMedicationIncidents(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {
        addAllDataToSinglePatient(patients, null, data);
        return "Visus, Tensio und Injektionen erfolgreich für die Patienten hinzugefügt.";
    }

    // Save all visuses in timely order for each patient
    public String setDiagnoses(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {

//        final ObservableMap<LocalDate, Double> myVisen = FXCollections.observableHashMap();
        data.forEach(datum -> {
            Patient myPatient = patients.getOrDefault(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet(), new Patient(datum, true));
            if ((datum.getHauptDiagnose() != null) && (datum.getHauptDiagnose() != HauptDiagnosen.E)) {
                myPatient.getDiagnoses().put(datum.getEreignisDatum(), datum.getHauptDiagnose().toString());
            }
            patients.put(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet(), myPatient);
        });
        return "Diagnosen für Patienten erfolgreich erstellt.";
    }

    @SuppressWarnings("unchecked")
    public String calculateVisusDelta(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {

        if (patients.isEmpty()) {
            return "Keine Patienten vorhanden. Konnte keine Visusdelten ermitteln.";
        }

        final String DATE_FORMAT = "yyyy-MM-dd";
        final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

        String result = "Visusdelten erfolgreich ermittelt.";

        // BEGINN -> Ermittlung der Datengruppen für jeden Patienten, um die Visusdelten zu ermitteln
        DataModel dataMethods = new DataModel();
        List<String> groups = new ArrayList();
        groups.add("patientenID");
        groups.add("untersuchungLateralitaet");
        String xDimension = "ereignisDatum";
        String yDimension = "dezimalVisusWert";
        ObservableMap<String, HashMap<String, Double[]>> myGroupMap = FXCollections.observableHashMap();
        dataMethods.getDataGroups(myGroupMap, data, groups, xDimension, yDimension, 0, 1);
        // ENDE -> Ergebnis: myGroupMap

        // Aufgabe: die Datengruppen sortieren, durchgehen und zum jeweiligen Patienten das Visusdelta zum vorherigen Visuswert ermitteln
        ObservableMap<String, Double> deltaAllMap = FXCollections.observableHashMap();
        myGroupMap.forEach((patientIdentifier, mapOfValues) -> {

            // Datumsliste anlegen, um den Stringschlüssel in Datum umzuwandeln und zeitlich zu sortieren
            List<LocalDate> dataList = new ArrayList();
            // Alle Schlüssel der Datumsliste formatiert hinzufügen
            mapOfValues.forEach((dateKey, dataPoints) -> {
                dataList.add(LocalDate.parse(dateKey, FORMATTER));
            });
            // Die Datumsliste zeitlich aufsteigend sortieren
            dataList.sort(Comparator.naturalOrder());
            // Der vorherige Visus wird in "previousVis" abgelegt
            DoubleProperty previousVis = new SimpleDoubleProperty(Double.NaN);
            ObjectProperty<LocalDate> previousDate = new SimpleObjectProperty(LocalDate.MIN);
            // Jedes Datum durchgehen, und das Delta zum vorherigen Visus ermitteln und in einer Map (deltaMap) ablegen
            HashMap<String, Double> deltaMap = new HashMap();
            dataList.forEach((singleDate) -> {
                double currentVis = mapOfValues.get(singleDate.toString())[0];
                if (!Double.isNaN(currentVis)) { // Nur falls es sich um einen gültigen Visuwert handelt
                    if (!Double.isNaN(previousVis.get())) { // Nur falls bereits ein vorheriger Visuswert existiert
                        // Delta ermitteln, runden und ablegen
                        double deltaVis = (double) Math.round(10000 * (currentVis - previousVis.get())) / 10000;
                        //System.out.println(patientIdentifier + singleDate.toString());
                        deltaMap.putIfAbsent(patientIdentifier + singleDate.toString(), deltaVis);
                        // TODO: Correction of MapKey --> Period is not unique
                        patients.get(patientIdentifier).getVisusDelten().putIfAbsent(Period.between(previousDate.get(), singleDate), deltaVis);
                    } // previousVis != nan
                    previousVis.set(currentVis); // Der aktuelle wird nun zum vorherigen
                    previousDate.set(singleDate);
                } // currentVis != nan
            }); // forEach dataList
            deltaAllMap.putAll(deltaMap);
        }); // forEach myGroupMap

        // Nun alle Datensätze durchgehen und die Delten zuordnen und ablegen
        final IntegerProperty counter = new SimpleIntegerProperty();
        data.forEach((datum) -> {
            //System.out.println(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
            try {
                double visusDelta = deltaAllMap.get(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
                datum.setVisusDelta(visusDelta);
            } catch (NullPointerException e) {
                //System.out.println("NullPointer No. :" + counter.get() + " Datensätze: " + data.size());
                counter.set(counter.get() + 1);
            }
            //        datum.setVisusDelta(datum.getPatientenID() + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
        });

        return result;
    }

    @SuppressWarnings("unchecked")
    public String calculateVisusLogMarDelta(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {

        if (patients.isEmpty()) {
            return "Keine Patienten vorhanden. Konnte keine Visusdelten ermitteln.";
        }

        final String DATE_FORMAT = "yyyy-MM-dd";
        final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

        String result = "Visusdelten erfolgreich ermittelt.";

        // BEGINN -> Ermittlung der Datengruppen für jeden Patienten, um die Visusdelten zu ermitteln
        DataModel dataMethods = new DataModel();
        List<String> groups = new ArrayList();
        groups.add("patientenID");
        groups.add("untersuchungLateralitaet");
        String xDimension = "ereignisDatum";
        String yDimension = "logMarVisusWert";
        ObservableMap<String, HashMap<String, Double[]>> myGroupMap = FXCollections.observableHashMap();
        dataMethods.getDataGroups(myGroupMap, data, groups, xDimension, yDimension, 0, 1);
        // ENDE -> Ergebnis: myGroupMap

        // Aufgabe: die Datengruppen sortieren, durchgehen und zum jeweiligen Patienten das Visusdelta zum vorherigen Visuswert ermitteln
        ObservableMap<String, Double> deltaAllMap = FXCollections.observableHashMap();
        myGroupMap.forEach((patientIdentifier, mapOfValues) -> {

            // Datumsliste anlegen, um den Stringschlüssel in Datum umzuwandeln und zeitlich zu sortieren
            List<LocalDate> dataList = new ArrayList();
            // Alle Schlüssel der Datumsliste formatiert hinzufügen
            mapOfValues.forEach((dateKey, dataPoints) -> {
                dataList.add(LocalDate.parse(dateKey, FORMATTER));
            });
            // Die Datumsliste zeitlich aufsteigend sortieren
            dataList.sort(Comparator.naturalOrder());
            // Der vorherige Visus wird in "previousVis" abgelegt
            DoubleProperty previousVis = new SimpleDoubleProperty(Double.NaN);
            ObjectProperty<LocalDate> previousDate = new SimpleObjectProperty(LocalDate.MIN);
            // Jedes Datum durchgehen, und das Delta zum vorherigen Visus ermitteln und in einer Map (deltaMap) ablegen
            HashMap<String, Double> deltaMap = new HashMap();
            dataList.forEach((singleDate) -> {
                double currentVis = mapOfValues.get(singleDate.toString())[0];
                if (!Double.isNaN(currentVis)) { // Nur falls es sich um einen gültigen Visuwert handelt
                    if (!Double.isNaN(previousVis.get())) { // Nur falls bereits ein vorheriger Visuswert existiert
                        // Delta ermitteln, runden und ablegen
                        double deltaVis = (double) Math.round(10000 * (currentVis - previousVis.get())) / 10000;
                        //System.out.println(patientIdentifier + singleDate.toString());
                        deltaMap.putIfAbsent(patientIdentifier + singleDate.toString(), deltaVis);
                        // TODO: Correction of MapKey --> Period is not unique
                        patients.get(patientIdentifier).getVisusLogMarDelten().putIfAbsent(Period.between(previousDate.get(), singleDate), deltaVis);
                    } // previousVis != nan
                    previousVis.set(currentVis); // Der aktuelle wird nun zum vorherigen
                    previousDate.set(singleDate);
                } // currentVis != nan
            }); // forEach dataList
            deltaAllMap.putAll(deltaMap);
        }); // forEach myGroupMap

        // Nun alle Datensätze durchgehen und die Delten zuordnen und ablegen
        final IntegerProperty counter = new SimpleIntegerProperty();
        data.forEach((datum) -> {
            //System.out.println(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
            try {
                double visusLogMarDelta = deltaAllMap.get(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
                datum.setVisusLogMarDelta(visusLogMarDelta);
            } catch (NullPointerException e) {
                //System.out.println("NullPointer No. :" + counter.get() + " Datensätze: " + data.size());
                counter.set(counter.get() + 1);
            }
            //        datum.setVisusDelta(datum.getPatientenID() + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
        });

        return result;
    }

    @SuppressWarnings("unchecked")
    public String calculateVisusClassDelta(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {

        final String DATE_FORMAT = "yyyy-MM-dd";
        final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

        if (patients.isEmpty()) {
            return "Keine Patienten vorhanden. Konnte keine VisusClassdelten ermitteln.";
        }

        String result = "VisusClassdelten erfolgreich ermittelt.";

        // BEGINN -> Ermittlung der Datengruppen für jeden Patienten, um die Visusdelten zu ermitteln
        DataModel dataMethods = new DataModel();
        List<String> groups = new ArrayList();
        groups.add("patientenID");
        groups.add("untersuchungLateralitaet");
        String xDimension = "ereignisDatum";
        String yDimension = "visusKlasse";
        ObservableMap<String, HashMap<String, Double[]>> myGroupMap = FXCollections.observableHashMap();
        dataMethods.getDataGroups(myGroupMap, data, groups, xDimension, yDimension, 0, 1);
        // ENDE -> Ergebnis: myGroupMap

        // Aufgabe: die Datengruppen sortieren, durchgehen und zum jeweiligen Patienten das Visusdelta zum vorherigen Visuswert ermitteln
        ObservableMap<String, Double> deltaAllMap = FXCollections.observableHashMap();
        myGroupMap.forEach((patientIdentifier, mapOfValues) -> {

            // Datumsliste anlegen, um den Stringschlüssel in Datum umzuwandeln und zeitlich zu sortieren
            List<LocalDate> dataList = new ArrayList();
            // Alle Schlüssel der Datumsliste formatiert hinzufügen
            mapOfValues.forEach((dateKey, dataPoints) -> {
                dataList.add(LocalDate.parse(dateKey, FORMATTER));
            });
            // Die Datumsliste zeitlich aufsteigend sortieren
            dataList.sort(Comparator.naturalOrder());

            // Der vorherige Visus wird in "previousVis" abgelegt
            DoubleProperty previousVisClass = new SimpleDoubleProperty(Double.NaN);
            ObjectProperty<LocalDate> previousDate = new SimpleObjectProperty(LocalDate.MIN);
            // Jedes Datum durchgehen, und das Delta zum vorherigen Visus ermitteln und in einer Map (deltaMap) ablegen
            HashMap<String, Double> deltaMap = new HashMap();
            dataList.forEach((singleDate) -> {
                double currentVisClass = mapOfValues.get(singleDate.toString())[0];
                patients.get(patientIdentifier).getVisClasses().put(singleDate, currentVisClass);
                if (!Double.isNaN(currentVisClass)) { // Nur falls es sich um einen gültigen Visuwert handelt
                    if (!Double.isNaN(previousVisClass.get())) { // Nur falls bereits ein vorheriger Visuswert existiert
                        // Delta ermitteln, runden und ablegen
                        double deltaVisClass = (double) Math.round(10000 * (currentVisClass - previousVisClass.get())) / 10000;
                        //System.out.println(patientIdentifier + singleDate.toString());
                        deltaMap.putIfAbsent(patientIdentifier + singleDate.toString(), deltaVisClass);
                        patients.get(patientIdentifier).getVisusClassDelten().putIfAbsent(Period.between(previousDate.get(), singleDate), deltaVisClass);
                    } // previousVis != nan
                    previousVisClass.set(currentVisClass); // Der aktuelle wird nun zum vorherigen
                    previousDate.set(singleDate);
                } // currentVis != nan
            }); // forEach dataList
            deltaAllMap.putAll(deltaMap);
        }); // forEach myGroupMap

        // Nun alle Datensätze durchgehen und die Delten zuordnen und ablegen
        final IntegerProperty counter = new SimpleIntegerProperty();
        data.forEach((datum) -> {
            //System.out.println(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
            try {
                double visusClassDelta = deltaAllMap.get(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
                datum.setVisusKlasseDelta(visusClassDelta);
            } catch (NullPointerException e) {
                //System.out.println("NullPointer No. :" + counter.get() + " Datensätze: " + data.size());
                counter.set(counter.get() + 1);
            }
            //        datum.setVisusDelta(datum.getPatientenID() + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
        });

        return result;
    }

    // Get and set the time between two injections for each patient
    public String deriveTimeBetweenInjections(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {

        if (patients.isEmpty()) {
            return "Keine Patienten vorhanden. Konnte keine Zeiten zwischen den Injektionen ermitteln.";
        }
        String result = "";
        for (Patient patient : patients.values()) {
            result = result + "\n" + deriveTimeBetweenInjections(data, patient);
        }

        return "Zeit zwischen den Injektionen erfolgreich ermittelt.";
    }

    @SuppressWarnings("unchecked")
    public String deriveTimeBetweenInjections(ObservableList<DataModel> data, Patient patient) {

        if (patient == null) {
            return "Der Patient ist null";
        }
        String result = "erfolgreich";

        List<LocalDate> myDates = new ArrayList();
        patient.getSpritzen().keySet().forEach(sKey -> {
            myDates.add(sKey);
        });
        myDates.sort(Comparator.naturalOrder());

        LocalDate oldDate = LocalDate.MIN;
        for (LocalDate myDate : myDates) {
            if (oldDate != LocalDate.MIN) {
                patient.getSpritzPausen().putIfAbsent(myDate, Period.between(oldDate, myDate));
            }
            oldDate = myDate;
        }

        return result;
    }

    public String generatePatientSummaries(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {
        String result = "Aggregierte Informationen für jeden Patienten erfolgreich ermittelt.";

//ok    String patientID;
//ok    double ageAtBeginning;
//    double durationOfTreatment;
//    double differenceToCohortAverageAge;
//ok    String sex;
//    double numberOfDoctors;
//
//    String firstDiagnosis;
//    String lastDiagnosis;
//
//ok    double firstVisus;
//ok    double lastVisus;
//ok    double minVisus;
//ok    double maxVisus;
//ok    double averageVisus;
//ok    double firstVisusDeltaFromPersonalAverage;
//ok    double lastVisusDeltaFromPersonalAverage;
//    double firstVisusDeltaFromCohortAverage;
//    double lastVisusDeltaFromCohortAverage;
//ok    double numberOfVisusMeasurements;
//ok    double minTimeBetweenVisusMeasurements;
//ok    double maxTimeBetweenVisusMeasurements;
//ok    double averageTimeBetweenVisusMeasurements;
//
//    double firstEyePressure;
//    double lastEyePressure;
//    double minEyePressure;
//    double maxEyePressure;
//    double averageEyePressure;
//    double firstEyePressureDeltaFromPersonalAverage;
//    double lastEyePressureDeltaFromPersonalAverage;
//    double firstEyePressureDeltaFromCohortAverage;
//    double lastEyePressureDeltaFromCohortAverage;
//    double NumberOfEyePressureMeasurements;
//    double minTimeBetweenEyePressureMeasurements;
//    double maxTimeBetweenEyePressureMeasurements;
//    double averageTimeBetweenEyePressureMeasurements;
//
//ok    double numberOfInjections;
//ok    double averageTimeBetweenInjections;
//    double differenceToAverageInjectionNumber;
//ok   double lengthOftreatment;
//ok    double numberOfMedications;
//    String firstMedication;
//    String lastMedication;
//
//    double averageVisusImprovementAfterInjection;
//    double minVisusImprovementAfterInjection;
//    double maxVisusImprovementAfterInjection;
//    double visusImprovementsRateAfterInjection;
//        List<String> namesList = new ArrayList();
        VA_topos.masterPatient.setMinVisus(Double.MAX_VALUE);
        VA_topos.masterPatient.setMaxVisus(Double.MIN_VALUE);
        VA_topos.masterPatient.setMinMeanVisus(Double.MAX_VALUE);
        VA_topos.masterPatient.setMaxMeanVisus(Double.MIN_VALUE);
        VA_topos.masterPatient.setMinMedianVisus(Double.MAX_VALUE);
        VA_topos.masterPatient.setMaxMedianVisus(Double.MIN_VALUE);
        VA_topos.masterPatient.setMinVisDelta(Double.MAX_VALUE);
        VA_topos.masterPatient.setMaxVisDelta(Double.MIN_VALUE);
        VA_topos.masterPatient.setMinLogMarVis(Double.MAX_VALUE);
        VA_topos.masterPatient.setMaxLogMarVis(Double.MIN_VALUE);
        VA_topos.masterPatient.setMinLogMarVisDelta(Double.MAX_VALUE);
        VA_topos.masterPatient.setMaxLogMarVisDelta(Double.MIN_VALUE);
        VA_topos.masterPatient.setMinVisClass(Double.MAX_VALUE);
        VA_topos.masterPatient.setMaxVisClass(Double.MIN_VALUE);

        VA_topos.masterPatient.setMinVisClassDelta(Double.MAX_VALUE);
        VA_topos.masterPatient.setMaxVisClassDelta(Double.MIN_VALUE);
        VA_topos.masterPatient.setMinLinearRegressionSlope(Double.MAX_VALUE);
        VA_topos.masterPatient.setMaxLinearRegressionSlope(Double.MIN_VALUE);

        VA_topos.masterPatient.setBeginMonitoring(LocalDate.MAX);
        VA_topos.masterPatient.setEndMonitoring(LocalDate.MIN);
        VA_topos.masterPatient.setDurationOfTreatment(Double.MIN_VALUE);
        VA_topos.masterPatient.setNumberOfVisusMeasurements(Double.MIN_VALUE);
        VA_topos.masterPatient.setNumberOfInjections(Double.MIN_VALUE);
        VA_topos.masterPatient.setFirstVisus(Double.MIN_VALUE);
        VA_topos.masterPatient.setLastVisus(Double.MIN_VALUE);

        patients.forEach((key, myPatient) -> {

            myPatient.setNumberOfArgos_tlIncidents(myPatient.getArgos_tlIncidents().size());

            myPatient.setNumberOfDiagnoses(0);
            if (myPatient.getDiagnoses().size() > 0) {
                HashMap<String, Integer> countMap = new HashMap();
                myPatient.getDiagnoses().values().forEach(diag -> {
                    if (!diag.equals("E") && !diag.equals("UNKNOWN")) {
                        //System.out.println(diag);
                        countMap.put(diag, 0);
                    }
                });
                myPatient.setNumberOfDiagnoses(countMap.size());
                Set<LocalDate> daten = myPatient.getDiagnoses().keySet();
                myPatient.setFirstDiagnosis(myPatient.getDiagnoses().get(Collections.min(daten)));
                myPatient.setLastDiagnosis(myPatient.getDiagnoses().get(Collections.max(daten)));
            }

            if (myPatient.getVisen().size() > 0) {
                myPatient.setMinVisus(Collections.min(myPatient.getVisen().values()));
                myPatient.setMaxVisus(Collections.max(myPatient.getVisen().values()));
                myPatient.setMinLogMarVis(Collections.min(myPatient.getVisenLogMar().values()));
                myPatient.setMaxLogMarVis(Collections.max(myPatient.getVisenLogMar().values()));
                myPatient.setMinVisClass(Collections.min(myPatient.getVisClasses().values()));
                myPatient.setMaxVisClass(Collections.max(myPatient.getVisClasses().values()));
            }
            myPatient.setNumberOfVisusMeasurements(myPatient.getVisen().size());

            if (myPatient.getAugendruecke().size() > 0) {
                myPatient.setMinEyePressure(Collections.min(myPatient.getAugendruecke().values()));
                myPatient.setMaxEyePressure(Collections.max(myPatient.getAugendruecke().values()));
                Set<LocalDate> daten = myPatient.getAugendruecke().keySet();
                myPatient.setFirstEyePressure(myPatient.getAugendruecke().get(Collections.min(daten)));
                myPatient.setLastEyePressure(myPatient.getAugendruecke().get(Collections.max(daten)));

                double[] myDoub = new double[myPatient.getAugendruecke().values().size()];
                int i = 0;
                for (Double druckValue : myPatient.getAugendruecke().values()) {
                    myDoub[i] = druckValue;
                    i++;
                }
                myPatient.setMeanEyePressure(StatUtils.mean(myDoub));
                myPatient.setMedianEyePressure(Utility.median(myPatient.getAugendruecke().values(), Comparator.naturalOrder()));
            }
            myPatient.setNumberOfEyePressureMeasurements(myPatient.getAugendruecke().size());
//            Set visus related values

            if (myPatient.getVisen().size() > 1) {
                //System.out.println(myAnno.getVisen().size());
//                myPatient.setMinVisus(myPatient.getMinVisus());
//                myPatient.setMaxVisus(myPatient.getMaxVisus());

                Set<LocalDate> daten = myPatient.getVisen().keySet();
                myPatient.setFirstVisus(myPatient.getVisen().get(Collections.min(daten)));
                myPatient.setLastVisus(myPatient.getVisen().get(Collections.max(daten)));
                double[] myDoub = new double[myPatient.getVisen().values().size()];
                int i = 0;
                for (Double visusValue : myPatient.getVisen().values()) {
                    myDoub[i] = visusValue;
                    i++;
                }
                myPatient.setMeanVisus(StatUtils.mean(myDoub));
                myPatient.setMedianVisus(Utility.median(myPatient.getVisen().values(), Comparator.naturalOrder()));
                //myPatient.getVisen().values().forEach(visus -> System.out.println("vis: " + visus));
                //System.out.println("Median: " + myPatient.getMedianVisus());
                //System.out.println("-------------------------------------------------------------");

                myPatient.setVisusDelta(myPatient.getLastVisus() - myPatient.getFirstVisus());

                myPatient.setMinVisDelta(Collections.min(myPatient.getVisusDelten().values()));
                myPatient.setMaxVisDelta(Collections.max(myPatient.getVisusDelten().values()));

                myPatient.setMinLogMarVisDelta(Collections.min(myPatient.getVisusLogMarDelten().values()));
                myPatient.setMaxLogMarVisDelta(Collections.max(myPatient.getVisusLogMarDelten().values()));

                myPatient.setMinVisClassDelta(Collections.min(myPatient.getVisusClassDelten().values()));
                myPatient.setMaxVisClassDelta(Collections.max(myPatient.getVisusClassDelten().values()));

                myPatient.setFirstVisusDeltaFromPersonalAverage(myPatient.getFirstVisus() - myPatient.getMeanVisus());
                myPatient.setLastVisusDeltaFromPersonalAverage(myPatient.getLastVisus() - myPatient.getMeanVisus());

                if (myPatient.getVisen().size() > 1) {
                    double min = 0d;
                    double max = 0d;
                    double average = 0d;
                    getMinMaxTime(myPatient.getVisen(), min, max, average);
                    myPatient.setMinTimeBetweenVisusMeasurements(min);
                    myPatient.setMaxTimeBetweenVisusMeasurements(max);
                    myPatient.setAverageTimeBetweenVisusMeasurements(average);
                }
            } else {
                myPatient.setVisusDelta(0);
            }

            if (myPatient.getSpritzen().size() > 0) {

                myPatient.setNumberOfInjections(myPatient.getSpritzen().size());
                boolean[] medi = new boolean[DataModel.AntiVEGFMedikament.values().length];
                for (int i = 0; i < medi.length; i++) {
                    medi[i] = false;
                }
                myPatient.getSpritzen().values().forEach(med -> medi[med.ordinal()] = true);
                //myPatient.setNumberOfMedications(0);
                for (int i = 0; i < medi.length; i++) {
                    if (medi[i]) {
                        myPatient.setNumberOfMedications(myPatient.getNumberOfMedications() + 1);
                    }
                }

                double min = 0d;
                double max = 0d;
                double average = 0d;
                getMinMaxTime(myPatient.getSpritzen(), min, max, average);
                myPatient.setMinTimeBetweenInjections(min);
                myPatient.setMaxTimeBetweenInjections(max);
                myPatient.setAverageTimeBetweenInjections(average);
            } else {
                myPatient.setNumberOfMedications(0);
                myPatient.setNumberOfInjections(0);
            }

            if (myPatient.getNumberOfInjections() > 0 || myPatient.getNumberOfVisusMeasurements() > 0) {

                myPatient.setAgeAtBeginning(calculateAge(myPatient));
                LocalDate spritzMinDate;
                LocalDate spritzMaxDate;
                LocalDate visMinDate;
                LocalDate visMaxDate;
                LocalDate absMinDate = LocalDate.MAX;
                LocalDate absMaxDate = LocalDate.MIN;

                // Hier geht es um die Ermittlung des ersten und letzten Visus- und Spritzendatums sowie um die Gesamtlänge der Betreuung des Patienten
                // Wenn sowohl Spritzen, als auch Visuswerte vorhanden sind muss das Gesamtdatum aus beiden ermittelt werden
                if (myPatient.getNumberOfInjections() > 0 && myPatient.getNumberOfVisusMeasurements() > 0) {

                    Set<LocalDate> spritzDaten = myPatient.getSpritzen().keySet();
                    spritzMinDate = Collections.min(spritzDaten);
                    spritzMaxDate = Collections.max(spritzDaten);
                    myPatient.setFirstInjection(spritzMinDate);
                    myPatient.setLastInjection(spritzMaxDate);

                    Set<LocalDate> visDaten = myPatient.getVisen().keySet();
                    visMinDate = Collections.min(visDaten);
                    visMaxDate = Collections.max(visDaten);
                    myPatient.setFirstVisusMeasurement(visMinDate);
                    myPatient.setLastVisusMeasurement(visMaxDate);

                    absMinDate = spritzMinDate.isBefore(visMinDate) ? spritzMinDate : visMinDate;
                    absMaxDate = spritzMaxDate.isAfter(visMaxDate) ? spritzMaxDate : visMaxDate;

                } else { // Es sind entweder Spritzen oder Visuswerte vorhanden
                    if (myPatient.getNumberOfInjections() > 0) {
                        Set<LocalDate> spritzDaten = myPatient.getSpritzen().keySet();
                        spritzMinDate = Collections.min(spritzDaten);
                        spritzMaxDate = Collections.max(spritzDaten);
                        myPatient.setFirstInjection(spritzMinDate);
                        myPatient.setLastInjection(spritzMaxDate);

                        absMinDate = spritzMinDate;
                        absMaxDate = spritzMaxDate;
                    }
                    if (myPatient.getNumberOfVisusMeasurements() > 0) {
                        Set<LocalDate> visDaten = myPatient.getVisen().keySet();
                        visMinDate = Collections.min(visDaten);
                        visMaxDate = Collections.max(visDaten);
                        myPatient.setFirstVisusMeasurement(visMinDate);
                        myPatient.setLastVisusMeasurement(visMaxDate);

                        absMinDate = visMinDate;
                        absMaxDate = visMaxDate;
                    }
                }

                myPatient.setBeginMonitoring(absMinDate);
                myPatient.setEndMonitoring(absMaxDate);
                myPatient.setLengthOfMonitoring(DAYS.between(absMinDate, absMaxDate));

                // set the visus values near injection dates
                //ObservableMap<String, HashMap<String, Double[]>> myMap = FXCollections.observableHashMap();
                //List<String> groups = new ArrayList();
                //dataModelMethods.getDataGroups(myMap, data, groups, xDim, yDim, 0, 1);
                //patients.put(patientName, myPatient);
                //namesList.add(patientName);
                if (myPatient.getVisenLogMar().size() > 1) {
                    deriveLinearRegressionValues(myPatient);
                }

                // Set the minima and maxima of the cohort to one masterPatient for later use            
                if (myPatient.getLinearRegressionSlope() < VA_topos.masterPatient.getMinLinearRegressionSlope()) {
                    VA_topos.masterPatient.setMinLinearRegressionSlope(myPatient.getLinearRegressionSlope());
                }

                if (myPatient.getLinearRegressionSlope() > VA_topos.masterPatient.getMaxLinearRegressionSlope()) {
                    VA_topos.masterPatient.setMaxLinearRegressionSlope(myPatient.getLinearRegressionSlope());
                }

                if (myPatient.getMinVisus() < VA_topos.masterPatient.getMinVisus()) {
                    VA_topos.masterPatient.setMinVisus(myPatient.getMinVisus());
                }
                //System.out.println("Min Visus: " + myPatient.getMinVisus());
                //System.out.println("Min Visus Master Patient: " + VA_topos.masterPatient.getMinVisus());

                if (myPatient.getMaxVisus() > VA_topos.masterPatient.getMaxVisus()) {
                    VA_topos.masterPatient.setMaxVisus(myPatient.getMaxVisus());
                }

                if (myPatient.getMeanVisus() < VA_topos.masterPatient.getMinMeanVisus()) {
                    VA_topos.masterPatient.setMinMeanVisus(myPatient.getMeanVisus());
                }

                if (myPatient.getMeanVisus() > VA_topos.masterPatient.getMaxMeanVisus()) {
                    VA_topos.masterPatient.setMaxMeanVisus(myPatient.getMeanVisus());
                }

                if (myPatient.getMedianVisus() < VA_topos.masterPatient.getMinMedianVisus()) {
                    VA_topos.masterPatient.setMinMedianVisus(myPatient.getMedianVisus());
                }

                if (myPatient.getMedianVisus() > VA_topos.masterPatient.getMaxMedianVisus()) {
                    VA_topos.masterPatient.setMaxMedianVisus(myPatient.getMedianVisus());
                }

                if (myPatient.getMinVisDelta() < VA_topos.masterPatient.getMinVisDelta()) {
                    VA_topos.masterPatient.setMinVisDelta(myPatient.getMinVisDelta());
                }
                if (myPatient.getMaxVisDelta() > VA_topos.masterPatient.getMaxVisDelta()) {
                    VA_topos.masterPatient.setMaxVisDelta(myPatient.getMaxVisDelta());
                }

                if (myPatient.getMinLogMarVis() < VA_topos.masterPatient.getMinLogMarVis()) {
                    VA_topos.masterPatient.setMinLogMarVis(myPatient.getMinLogMarVis());
                }
                if (myPatient.getMaxLogMarVis() > VA_topos.masterPatient.getMaxLogMarVis()) {
                    VA_topos.masterPatient.setMaxLogMarVis(myPatient.getMaxLogMarVis());
                }
                if (myPatient.getMinLogMarVisDelta() < VA_topos.masterPatient.getMinLogMarVisDelta()) {
                    VA_topos.masterPatient.setMinLogMarVisDelta(myPatient.getMinLogMarVisDelta());
                }
                if (myPatient.getMaxLogMarVisDelta() > VA_topos.masterPatient.getMaxLogMarVisDelta()) {
                    VA_topos.masterPatient.setMaxLogMarVisDelta(myPatient.getMaxLogMarVisDelta());
                }

                if (myPatient.getMinVisClass() < VA_topos.masterPatient.getMinVisClass()) {
                    VA_topos.masterPatient.setMinVisClass(myPatient.getMinVisClass());
                }
                if (myPatient.getMaxVisClass() > VA_topos.masterPatient.getMaxVisClass()) {
                    VA_topos.masterPatient.setMaxVisClass(myPatient.getMaxVisClass());
                }
                if (myPatient.getMinVisClassDelta() < VA_topos.masterPatient.getMinVisClassDelta()) {
                    VA_topos.masterPatient.setMinVisClassDelta(myPatient.getMinVisClassDelta());
                }
                if (myPatient.getMaxVisClassDelta() > VA_topos.masterPatient.getMaxVisClassDelta()) {
                    VA_topos.masterPatient.setMaxVisClassDelta(myPatient.getMaxVisClassDelta());
                }

                if (myPatient.getBeginMonitoring().isBefore(VA_topos.masterPatient.getBeginMonitoring())) {
                    VA_topos.masterPatient.setBeginMonitoring(myPatient.getBeginMonitoring());
                }
                if (myPatient.getEndMonitoring().isAfter(VA_topos.masterPatient.getEndMonitoring())) {
                    VA_topos.masterPatient.setEndMonitoring(myPatient.getEndMonitoring());
                }
                if (myPatient.getDurationOfTreatment() > VA_topos.masterPatient.getDurationOfTreatment()) {
                    VA_topos.masterPatient.setDurationOfTreatment(myPatient.getDurationOfTreatment());
                }
                if (myPatient.getNumberOfVisusMeasurements() > VA_topos.masterPatient.getNumberOfVisusMeasurements()) {
                    VA_topos.masterPatient.setNumberOfVisusMeasurements(myPatient.getNumberOfVisusMeasurements());
                }
                if (myPatient.getNumberOfInjections() > VA_topos.masterPatient.getNumberOfInjections()) {
                    VA_topos.masterPatient.setNumberOfInjections(myPatient.getNumberOfInjections());
                }
                if (myPatient.getFirstVisus() > VA_topos.masterPatient.getFirstVisus()) {
                    VA_topos.masterPatient.setFirstVisus(myPatient.getFirstVisus());
                }
                if (myPatient.getLastVisus() > VA_topos.masterPatient.getLastVisus()) {
                    VA_topos.masterPatient.setLastVisus(myPatient.getLastVisus());
                }

            } else {
                myPatient.setLengthOfMonitoring(0.0);
                System.out.println("Keine Injektionen oder keine Visusmessungen für den Patienten: " + myPatient.getPatientID());
            }

            // Zeige nur Patienten mit mehr als einer Diagnose
            myPatient.setShow(true);
        });
//        String firstDiagnosis;
//        String lastDiagnosis;
//        double firstVisusDeltaFromCohortAverage;
//        double lastVisusDeltaFromCohortAverage;
//        double differenceToCohortAverageAge;
//        double numberOfDoctors;

        //namesList.sort(Comparator.naturalOrder());
        //System.out.println(patients.size());
        return result;
    }

    // Das Alter eines Patienten zu Beginn der Behandlung ermitteln
    private double calculateAge(Patient patient) {
        double result = 0d;
        ObjectProperty<LocalDate> startDatum = new SimpleObjectProperty(LocalDate.MAX);
        // Den ersten Termin in der Klinik zur Visusmessung ermitteln
        patient.getVisen().forEach((key, visus) -> {
            if (startDatum.get().isAfter(key)) {
                startDatum.set(key);
            }
        });
        // Zeit zwischen erstem Termin und Geburtstag ermitteln
        if (patient.getDateOfBirth() != null && startDatum.get() != LocalDate.MAX) {

            Period myPeriod = Period.between(patient.getDateOfBirth(), startDatum.get());
            // Das Alter in Jahren + Monate / 12 + Tage / 365 = Alter in Jahren mit Nachkommastelle
            result = Math.round((myPeriod.getYears() + myPeriod.getMonths() / 12.0d + myPeriod.getDays() / 365.0d) * 100d) / 100d;
        }
        return result;
    }

//    private double getAverageVisus(Patient patient) {
//
//        double visSum = 0d;
//        Collection<Double> myVisen = patient.getVisen().values();
//        visSum = myVisen.stream().map((visus) -> visus).reduce(visSum, (accumulator, _item) -> accumulator + _item);
//        double result = Math.round(visSum / patient.getVisen().size() * 10000d) / 10000d;
//        return result;
//    }
    /**
     * Function to derive the minimum and maximum and average time between
     * incidents. The incidents are stored in myMap
     *
     * @param myMap contains a list of incidents. Between the incidents time has
     * past
     * @param min variable within which the minimum time between two incidents
     * is stored
     * @param max variable within which the maximum time between two incidents
     * is stored
     * @param average variable within which the average time elapsed between all
     * incidents is stored
     */
    private void getMinMaxTime(ObservableMap<LocalDate, ?> myMap, double min, double max, double average) {

        List<LocalDate> mydata = new ArrayList();
        myMap.keySet().forEach((datum) -> {
            mydata.add(datum);
        });
        mydata.sort(Comparator.naturalOrder());

        LocalDate oldDate = LocalDate.MIN;
        List<Double> zeit = new ArrayList();
        double sum = 0d;
        for (LocalDate datum : mydata) {
            if (oldDate != LocalDate.MIN) {
                zeit.add((double) DAYS.between(oldDate, datum));
                sum += (double) DAYS.between(oldDate, datum);
            }
            oldDate = datum;
        }
        if (zeit.size() > 0) {
            min = Collections.min(zeit);
            max = Collections.max(zeit);
            average = sum / ((double) zeit.size());
        }
        //System.out.println("min: " + min + " max: " + max + " average: " + average);
    }

    // Get and set the time between two visus measurements for each patient
    @SuppressWarnings("unchecked")
    public String deriveTimeBetweenMeasurements(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {

        if (patients.isEmpty()) {
            return "Keine Patienten vorhanden. Konnte keine Zeiten zwischen den Visusmessungen ermitteln.";
        }
        String result = "erfolgreich";

        patients.values().forEach(patient -> {
            List<LocalDate> myDates = new ArrayList();
            patient.getVisen().keySet().forEach(sKey -> {
                myDates.add(sKey);
            });
            myDates.sort(Comparator.naturalOrder());

            LocalDate oldDate = LocalDate.MIN;
            for (LocalDate myDate : myDates) {
                if (oldDate != LocalDate.MIN) {
                    patient.getVisPausen().putIfAbsent(myDate, Period.between(oldDate, myDate));
                }
                oldDate = myDate;
            }
        });

//        final IntegerProperty counter = new SimpleIntegerProperty();
        data.forEach((datum) -> {
            //System.out.println(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
            try {
                Period zeitSeitVisus = patients.get(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet()).getVisPausen().get(datum.getEreignisDatum());
                if (zeitSeitVisus != null) {
                    datum.setZeitSeitLetztemVisus(zeitSeitVisus);
                }
            } catch (Exception e) {
//                System.out.println("NullPointer No. :" + counter.get() + " Datensätze: " + data.size());
//                counter.set(counter.get() + 1);
            }
            //        datum.setVisusDelta(datum.getPatientenID() + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
        });

        return result;
    }

    // Get and set the time between two visus measurements for each patient
    @SuppressWarnings("unchecked")
    public String deriveMeasurementsBetweenInjections(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {

        if (patients.isEmpty()) {
            return "Keine Patienten vorhanden. Konnte keine Visusmessungen zwischen den Injektionen ermitteln.";
        }
        String result = "erfolgreich";

        // Alle Annotationen durchgehen (hier ein anno = ein Auge eines Patienten
        patients.values().forEach(patient -> {
            // Alle Visusdaten ermitteln und sortieren
            List<LocalDate> myVisDates = new ArrayList();
            patient.getVisen().keySet().forEach(sKey -> {
                myVisDates.add(sKey);
            });
            myVisDates.sort(Comparator.naturalOrder());
            // Alle Spritzendaten durchgehen und sortieren
            List<LocalDate> myInjDates = new ArrayList();
            patient.getSpritzen().keySet().forEach(sKey -> {
                myInjDates.add(sKey);
            });
            myInjDates.sort(Comparator.naturalOrder());

            // Nun die Visusdaten zwischen den spritzendaten zählen
            LocalDate oldDate = LocalDate.MIN;
            int i = 0;
            int oldi = 0;
            for (LocalDate myInjDate : myInjDates) {
                while ((i < myVisDates.size()) && myVisDates.get(i).isBefore(myInjDate)) {
                    i++;
                }
                patient.getVisMeasurementsSinceLastInjection().putIfAbsent(myInjDate, (double) (i - oldi));
                oldi = i;
            }
        });

//        final IntegerProperty counter = new SimpleIntegerProperty();
        data.forEach((datum) -> {
            //System.out.println(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
            try {
                double spritzenZahl = patients.get(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet()).getVisMeasurementsSinceLastInjection().get(datum.getEreignisDatum());
                datum.setVergangeneTage((int) Math.round(spritzenZahl));

            } catch (Exception e) {
                //System.out.println("NullPointer No. :" + counter.get() + " Datensätze: " + data.size());
                //              counter.set(counter.get() + 1);
            }
            //        datum.setVisusDelta(datum.getPatientenID() + datum.getUntersuchungLateralitaet() + datum.getEreignisDatum());
        });

        return result;
    }

    @SuppressWarnings("unchecked")
    public String derivePausenListe(ObservableList<DataModel> data, ObservableMap<String, Patient> patients) {

        patients.values().forEach(patient -> {
            // Jede Spritzepause (Zeit zwischen zwei Spritzen) durchgehen
            patient.getSpritzPausen().forEach((pauseKey, pauseValue) -> {
                // Falls die Pause mindestens zwei Monate dauerte
                if (pauseValue.toTotalMonths() >= 2.0d) {
                    // Falls mindestens ein gemessener Visuwert zwischen den zwei Spritzen liegt
                    boolean gewollt = (patient.getVisMeasurementsSinceLastInjection().get(pauseKey) > 0.0d);
                    final DoubleProperty anfangsVisus = new SimpleDoubleProperty(0.0d), endVisus = new SimpleDoubleProperty(0.0d);
                    final DoubleProperty vorVisus = new SimpleDoubleProperty(0.0d), nachVisus = new SimpleDoubleProperty(0.0d);
                    final BooleanProperty firstAfter = new SimpleBooleanProperty(true);
                    ObjectProperty<LocalDate> before = new SimpleObjectProperty(), after = new SimpleObjectProperty();

                    // Den dichtesten Visus vor und nach der Spritze am ENDE der Pause ermitteln
                    patient.getVisen().forEach((key, visus) -> {
                        if (key.isBefore(pauseKey)) {
                            before.set(key);
                            vorVisus.set(visus);
                        } else if (firstAfter.get()) {
                            after.set(key);
                            nachVisus.set(visus);
                            firstAfter.set(false);
                        }
                    });

                    // Identifizieren des dichteren der beiden zuvor ermittelten Visen
                    firstAfter.set(true);
                    if (after.get() != null) {
                        if (before.get() != null) {
                            // Falls die Anzahl der Tage zwischen dem letzten Visus und der Spritze geringer ist, als die Anzahl der Tage zwischen Spritze und dem FolgeVisus
                            if (DAYS.between(pauseKey, after.get()) > DAYS.between(before.get(), pauseKey)) {
                                endVisus.set(vorVisus.get());
                            } else {
                                endVisus.set(nachVisus.get());
                            }
                        } else {
                            endVisus.set(nachVisus.get());
                        }
                    } else if (before.get() != null) {
                        endVisus.set(vorVisus.get());
                    }

                    // Ermitteln des Datums der Spritze am Anfang der Pause
                    ObjectProperty<LocalDate> pausenAnfangsKey = new SimpleObjectProperty();
                    patient.getSpritzen().keySet().forEach(key -> {
                        if (key.isBefore(pauseKey)) {
                            pausenAnfangsKey.set(key);
                        }
                    });

                    // Den dichtesten Visus vor und nach der Spritze am ANFANG der Pause ermitteln
                    firstAfter.set(true);
                    patient.getVisen().forEach((key, visus) -> {
                        if (key.isBefore(pausenAnfangsKey.get())) {
                            before.set(key);
                            vorVisus.set(visus);
                        } else if (firstAfter.get()) {
                            after.set(key);
                            nachVisus.set(visus);
                            firstAfter.set(false);
                        }
                    });

                    // Identifizieren des dichteren der beiden zuvor ermittelten Visen
                    firstAfter.set(true);
                    if (after.get() != null) {
                        if (before.get() != null) {
                            // Falls die Anzahl der Tage zwischen dem letzten Visus und der Spritze geringer ist, als die Anzahl der Tage zwischen Spritze und dem FolgeVisus
                            if (DAYS.between(pausenAnfangsKey.get(), after.get()) > DAYS.between(before.get(), pausenAnfangsKey.get())) {
                                anfangsVisus.set(vorVisus.get());
                            } else {
                                anfangsVisus.set(nachVisus.get());
                            }
                        } else {
                            anfangsVisus.set(nachVisus.get());
                        }
                    } else if (before.get() != null) {
                        anfangsVisus.set(vorVisus.get());
                    }
                    double pausenLaenge = DAYS.between(pausenAnfangsKey.get(), pauseKey);
//                    pausenListe.add(new PausenScatterData(gewollt, pausenLaenge, anfangsVisus.get(), endVisus.get()));
                }
            });
        });

//        pausenListe.forEach((pause) -> {
//            int isGewollt = pause.isGewollt() ? 1 : 0;
//            //System.out.println(isGewollt + ";" + pause.getPausenLaenge() + ";" + pause.getAnfangsVisus() + ";" + pause.getEndVisus());
//        });
        String result = "erfolgreich";

        return result;
    }

    @SuppressWarnings("unchecked")
    public String getVisusForInjections(ObservableMap<String, Patient> patients) {
        // TODO: Prozedur entwickeln, die Visusmessungen beim Medikamentenwechsel identifiziert und das Delta herausfindet
        //
        // 1. Für alle Patienten die Medikamentenwechsel identifizieren
        System.out.println("MedicationChange active");
        final double TIMESPAN = 120;
        patients.values()
                .forEach(patient -> {

                    LocalDate oldInjDate;
                    LocalDate newInjDate;
                    DataModel.AntiVEGFMedikament oldMed;
                    DataModel.AntiVEGFMedikament newMed;

                    LocalDate oldVisDate;
                    LocalDate newVisDate;

                    double oldVisualAcuity;
                    double newVisualAcuity;

                    double visChange;

                    double oldVisualAcuityLetter;
                    double newVisualAcuityLetter;

                    double visChangeLetter;

                    String patientID = patient.getPatientID();
                    String diagnosis = "";
                    if (patient.getNumberOfMedications() > 1) {

                        LocalDate[] injDates = new LocalDate[patient.getSpritzen().size()];
                        patient.getSpritzen().keySet().toArray(injDates);
                        Arrays.sort(injDates);

                        LocalDate oldDate = LocalDate.MIN;
                        for (LocalDate injDate : injDates) {
                            if (oldDate != LocalDate.MIN) {
                                if (patient.getSpritzen().get(oldDate) != patient.getSpritzen().get(injDate)) {
                                    // Das alte Medikament identifizieren (inkl. Datum)
                                    // Das neue Medikament identifizieren (inkl. Datum)
                                    oldInjDate = oldDate;
                                    newInjDate = injDate;
                                    oldMed = patient.getSpritzen().get(oldDate);
                                    newMed = patient.getSpritzen().get(injDate);

                                    // Den letzten visus vor dem Wechsel ermitteln (inkl. Datum)
                                    // Den ersten Visus nach dem Wechsel ermitteln (inkl. Datum)
                                    // Das Visusdelta ermitteln
                                    oldVisDate = findNearestDate(new ArrayList<>(patient.getVisenLogMar().keySet()), newInjDate, true);
                                    String oldDiagnosis = patient.getDiagnoses().get(findNearestDate(new ArrayList<>(patient.getDiagnoses().keySet()), oldVisDate, true));

                                    // vis-change is only valid, if the visus measurement is between the medication change
                                    if (oldVisDate.isBefore(newInjDate) && oldVisDate.isAfter(oldInjDate)) {
                                        oldVisualAcuity = patient.getVisenLogMar().get(oldVisDate);
                                        oldVisualAcuityLetter = -50.0 * oldVisualAcuity + 85;
                                        newVisDate = findNearestDate(new ArrayList<>(patient.getVisenLogMar().keySet()), newInjDate, false);
                                        String newDiagnosis = patient.getDiagnoses().get(findNearestDate(new ArrayList<>(patient.getDiagnoses().keySet()), newVisDate, true));
                                        if (oldDiagnosis == null) {
                                            newDiagnosis = "old";
                                        }
                                        if (newDiagnosis == null) {
                                            newDiagnosis = "new";
                                        }
                                        if (newDiagnosis.equals(oldDiagnosis)) {
                                            diagnosis = newDiagnosis;
                                        }
                                        //System.out.println("Diagnose: " + diagnosis);
                                        if ((DAYS.between(newInjDate, newVisDate) < TIMESPAN) && (DAYS.between(oldInjDate, newInjDate) < TIMESPAN) && !diagnosis.equals("")) {
                                            //System.out.println("Positiv - Patient: " + patient.getPatientID() + " OldInjDate: " + oldInjDate + " OldVisDate: " + oldVisDate+ " NewInjDate: " + newInjDate + " NewVisDate: " + newVisDate);
                                            newVisualAcuity = patient.getVisenLogMar().get(newVisDate);
                                            newVisualAcuityLetter = -50.0 * newVisualAcuity + 85;
                                            visChange = newVisualAcuity - oldVisualAcuity;
                                            visChangeLetter = newVisualAcuityLetter - oldVisualAcuityLetter;

                                            patient.getMyMedicationChanges().put("" + patient.getMyMedicationChanges().size(), new MedicationChange(
                                                    oldInjDate, newInjDate,
                                                    oldMed, newMed,
                                                    oldVisDate, newVisDate,
                                                    oldVisualAcuity, newVisualAcuity, visChange,
                                                    oldVisualAcuityLetter, newVisualAcuityLetter, visChangeLetter,
                                                    patient.getPatientID(), diagnosis
                                            ));
                                        }
                                    }

                                }
                            }
                            oldDate = injDate;
                        } // for injDates
                    } // if more than one medication
                }); // forEach patient

        return "Visusermittlung zu Medikamentenwechsel erfolgreich";
    }

    /**
     * Function to set some global Parameters, that have to be used in other
     * functions throughout the program
     *
     * @param glob structure with all global variables
     * @param patients data for all patients
     * @return a string with the result of the setting of the global parameters
     */
    private String setGlobalData(GlobalData glob, ObservableMap<String, Patient> patients) {

        DoubleProperty maxDays = new SimpleDoubleProperty();
        LocalDate minDate = LocalDate.MAX;
        LocalDate maxDate = LocalDate.MIN;
        for (Patient patient : patients.values()) {
            if (minDate.isAfter(patient.getBeginMonitoring()) & patient.getBeginMonitoring().isAfter(LocalDate.MIN)) {
                minDate = patient.getBeginMonitoring();
            }
            if (maxDate.isBefore(patient.getEndMonitoring()) & patient.getEndMonitoring().isBefore(LocalDate.MAX)) {
                maxDate = patient.getEndMonitoring();
            }
        }

        maxDays.set(DAYS.between(minDate, maxDate));
        glob.setDaysCount(maxDays.get());
        //glob.setXFitFactorProperty();
        glob.xFitFactorProperty().bind(glob.windowWidthProperty().divide(glob.daysCountProperty()));
        final LocalDate offSetDate = minDate;
        // Set the XOffset for each patient in Days
        patients.values().forEach(patient -> patient.setxOffset(DAYS.between(offSetDate, patient.getBeginMonitoring())));
        return "Setzen der globalen Parameter erfolgreich.";
    }

    /**
     * Function to initialize all ScreenObjects for each patient
     *
     * @param patients all patients
     * @return String that contains the result of the function
     */
    private String generateVisualizationObjects(GlobalData glob, ObservableMap<String, Patient> patients) {
        patients.values().forEach(patient -> {
            if (patient.getEyeTimePanel() == null) {
                patient.setEyeTimePanel(new EyeTimePanel(glob, patient));
            }
        });
        return "Generierung der Grafikobjekte erfolgreich.";
    }

    /**
     * Function to find a nearest date within a list of dates to another given
     * date
     *
     * @param dates - List of dates, where a nearest date shall to be found
     * @param searchDate - The given date to which the nearest date shall be
     * found
     * @param findBefore - Boolean parameter; true if the date to be found has
     * to be before searchDate, otherwise after
     * @return the nearest date
     */
    private LocalDate findNearestDate(List<LocalDate> dates, LocalDate searchDate, boolean findBefore) {

        LocalDate tempDate = LocalDate.MIN;
        if (dates.size() > 0) {
            dates.sort(Comparator.naturalOrder());
            for (LocalDate date : dates) {
                if (date.isAfter(searchDate)) {
                    if (!findBefore) {
                        tempDate = date;
                    }
                    return tempDate;
                }
                tempDate = date;
            }
        }
        return LocalDate.MIN;
    }

    /**
     * Function to calculate a linear regression slope and intercept to indicate
     * the development of one patients' visual acuity
     *
     * @param patient - the patient for whom the visual acuity linear regression
     * shall be calculated
     */
    private void deriveLinearRegressionValues(Patient patient) {

        if (patient.getVisenLogMar().size() > 1) {

            List<Double> visLogMarList = new ArrayList();
            List<Double> dateList = new ArrayList();
            List<LocalDate> sortedDateList = new ArrayList();
            sortedDateList.addAll(patient.getVisenLogMar().keySet());
            sortedDateList.sort(Comparator.naturalOrder());
            sortedDateList.forEach((curDate) -> {
                dateList.add((double) DAYS.between(patient.getFirstVisusMeasurement(), curDate));
                visLogMarList.add(patient.getVisenLogMar().get(curDate));
            });

            Double[] visLogMar = new Double[patient.getVisenLogMar().size()];
            visLogMarList.toArray(visLogMar);
            Double[] daysLogMar = new Double[dateList.size()];
            dateList.toArray(daysLogMar);
            double[][] regressionData = new double[dateList.size()][];
            for (int i = 0; i < dateList.size(); i++) {
                regressionData[i] = new double[2];
            }

            // TODO: Insert option on screen to choose
            boolean timeDependent = false;
            for (int i = 0; i < dateList.size(); i++) {
                regressionData[i][0] = timeDependent ? daysLogMar[i] : i;
                regressionData[i][1] = visLogMar[i];
            }
            SimpleRegression simpleRegression = new SimpleRegression(true);
            simpleRegression.addData(regressionData);
            patient.setLinearRegressionSlope(simpleRegression.getSlope());
            patient.setLinearRegressionIntercept(simpleRegression.getIntercept());
        }

    }

    // TODO: Add functions, to add single patient data without having to calculate all other
    /**
     * Function to add a single incident (ereignis) to a single patient. If the
     * patient does not exist yet, the patient will be created.
     *
     * @param patients - list of all patients
     * @param patient - a single patient to whom the incident has to be added
     * @param ereignis - the incident that should be added
     * @return information on the result of the function
     */
    public static String addSingleDataToSinglePatient(ObservableMap<String, Patient> patients, Patient patient, DataModel ereignis) {
        // If a new patient has to be created
        if (ereignis.getEreignisDatum() == LocalDate.MIN) {
            return "Ereignis nicht hinzugefüg, da das Ereignisdatum nicht gesetzt ist.";
        }
        if (ereignis.getEreignisDatum() == LocalDate.MAX) {
            return "Ereignis nicht hinzugefüg, da das Ereignisdatum nicht gesetzt ist.";
        }
        //System.out.println("Ereignisdatum: " + ereignis.getEreignisDatum());
        if (patient == null) {
            patient = patients.getOrDefault(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet(), new Patient(ereignis, true));
        }
        // Add a visual acuity value to the patient if applicable
        if (!Double.isNaN(ereignis.getDezimalVisusWert())) {
            patient.getVisen().put(ereignis.getEreignisDatum(), ereignis.getDezimalVisusWert());
        }
        // Add a logMar visual acuity value if applicable
        if (!Double.isNaN(ereignis.getLogMarVisusWert())) {
            patient.getVisenLogMar().put(ereignis.getEreignisDatum(), ereignis.getLogMarVisusWert());
        }
        // Add an extraordinary incident to the patient if applicable
        if (ereignis.isArgosEreignis()) {
            patient.getArgos_tlIncidents().put(ereignis.getEreignisDatum(), ereignis.getArgosEreignisText());
        }
        // Add a tensio value to the patient if applicable
        if (!Double.isNaN(ereignis.getAugendruck())) {
            patient.getAugendruecke().put(ereignis.getEreignisDatum(), ereignis.getAugendruck());
        }
        // Add an injection with the medication to the patient if applicable
        switch (ereignis.getMedikament()) {
            case UNKNOWN:
            case E:
                break;
            default:
                if (patient.getSpritzen().isEmpty()) {
                    patient.setSpritzen(FXCollections.observableHashMap());
                }
                patient.getSpritzen().put(ereignis.getEreignisDatum(), ereignis.getMedikament());
        }
        // Add/Replace the patient to the patients list
        patients.put(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet(), patient);
        return "Patient " + ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet() + " erfolgreich hinzugefügt.";
    }

    /**
     * Function to add data from several incidents to patient information
     *
     * @param patients
     * @param patient
     * @param ereignisse
     * @return information on the success of the function
     */
    public static String addAllDataToSinglePatient(ObservableMap<String, Patient> patients, Patient patient, ObservableList<DataModel> ereignisse) {
        String returnString = "";
        for (DataModel ereignis : ereignisse) {
            if (patient != null) {
                patient = patients.getOrDefault(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet(), new Patient(ereignis, true));
            } else {

            }
            returnString = returnString + "\n" + addSingleDataToSinglePatient(patients, patient, ereignis);
        }
        return returnString;
    }
}
