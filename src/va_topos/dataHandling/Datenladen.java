/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.dataHandling;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import va_topos.data.JsonArgos_tl;
import va_topos.data.JsonPdv_verschluesselung;
import va_topos.data.JsonStammdaten;
import va_topos.VA_topos;

import va_topos.data.OPSList;
import va_topos.data.ICD10List;
import va_topos.data.DataModel;
import va_topos.data.DataModelNoProperty;
import va_topos.data.JsonOp_bericht_extract;
import va_topos.data.JsonTextMining_letter_extract;
import va_topos.data.Patient;
import va_topos.data.SerializableList;

/**
 *
 * @author Christph Schmidt 2018
 */
@SuppressWarnings("unchecked")
public class Datenladen {

    public List<DataModel> loadDataFromCSV(List<File> files) {

        // Liste aller Datensätze, welche durch die Funktion gefüllt und zurückgegeben wird
        List<DataModel> myData = new ArrayList();

        // Alle übermittelten Dateien durchgehen und die Daten aus diesen Dateien laden
        IntegerProperty lfdNr = new SimpleIntegerProperty();         // Zähler für alle Datensätze
        lfdNr.set(0);

        files.forEach((file) -> {
            // Nur, falls es sich um eine CSV Datei handelt
            if (file.getName().toUpperCase().endsWith(".CSV")) {

                String line; // Eine einzelne Zeile in der Datei
                BufferedReader brData = null; // einen bufferedreader, um die daten aus der Datei zu lesen
                try { // Um Ausnahmen abzufangen

                    brData = new BufferedReader(new FileReader(file)); // CSV File einlesen
                    // Erste Zeile einlesen
                    line = brData.readLine();
                    // Die erste Zeile in Spalten aufteilen und in einem Array ablegen
                    String myFirstLine[] = line.split(";");
                    // Den Array in eine Liste aus Strings nur aus Kleinbuchstaben umwandeln (um Fehler durch Groß-/Kleinschreibung zu vermeiden)
                    List<String> myFirstList = new ArrayList();
                    for (String s : myFirstLine) {
                        myFirstList.add(s.toLowerCase());
                    }

                    // Alle Attribute des Datenmodells einlesen
                    final Field[] fields = DataModel.class.getDeclaredFields();

                    // Feld für die Zuordnung der Spalten in der CSV Datei zu den Attributen im Datenmodell;
                    HashMap csvToModelMap = new HashMap();

                    // Die Ausgabe auf dem Bildschirm zum Stand des Ladens aktualisieren
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nDatei: " + file.getName());

                    for (Field field : fields) {
                        try {

                            //System.out.println("field.getName: " + field.getName().charAt(0) + " myFirstList.get(0): " + String.format("%04x", (int) myFirstList.get(0).charAt(0)) + " equalsIgnoreCase: " + field.getName().equalsIgnoreCase(myFirstList.get(0)));
                            String fieldName = field.getName().toLowerCase();

                            // Das Feld für die Zuordnung befüllen, um die richtige Spalte dem richtigen Attribut zuzuordnen
                            csvToModelMap.put(fieldName, myFirstList.indexOf(fieldName));

                            // Die Ausgabe auf dem Bildschirm zum Stand des Ladens aktualisieren                            
                            VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n     " + fieldName + " ist " + csvToModelMap.get(fieldName));

                        } catch (NullPointerException e) { // Im Fehlerfall einen Text ausgeben
                            // Die Ausgabe auf dem Bildschirm zum Stand des Ladens aktualisieren                            
                            VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n     " + e.getMessage());
                            System.out.println(e.getMessage());
                        }
                    }

                    int datensatzZahl = 0; // zählt die gelsenenen Datensätze einer Datei
                    int fehlerhaft = 0; // zählt die fehlerhaften/unvollständigen Datensätze
                    while ((line = brData.readLine()) != null) {
                        //    Umsatzauswertung.ladeText.set(Umsatzauswertung.ladeText.get() + "\n     " + line);
                        line = line.replaceAll("[^a-zäöüA-ZÄÖÜ0-9.,;-]/", ""); // Sonderzeichen entfernen
                        String[] myLines = line.split(";"); // Trennungszeichen, separiert die Attribute
                        DataModel myDatum = new DataModel(); // Einzelner Datensatz

                        // Prüfen, mit welchem Resultat die Datenfüllroutine ausgeführt wird
                        String fehlerText = myDatum.setAll(csvToModelMap, myLines, file.getName(), lfdNr.get());
                        switch (fehlerText) {
                            case "": // alles i.O.
                                myData.add(myDatum); // Einzeldatensatz zu allen Datensätzen hinzufügen
                                datensatzZahl++; // Ein Datensatz wurde gelesen...
                                lfdNr.set(lfdNr.get() + 1); //
                                break;
                            default: // alle anderen Ergebnisse = Fehler
                                fehlerhaft++;
                                VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n     Fehlertext: " + fehlerText);
                                VA_topos.ladeText.set(VA_topos.ladeText.get() + " --> Fehlerhafter Datensatz: " + line);
                        } // Datensatz befüllen
                    } // Solange noch Zeilen in der Datendatei sind

                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n     Gesamtzahl der Datensätze in der Datei: " + (datensatzZahl + fehlerhaft) + ". Hiervon erfolgreich: " + datensatzZahl + ". Hiervon fehlerhaft: " + fehlerhaft + ".");
                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n     " + e.getMessage());
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n     " + e.getMessage());
                } finally {
                    if (brData != null) {
                        try {
                            brData.close();
                        } catch (IOException e) {
                            System.out.println(e.getMessage());
                            VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n     " + e.getMessage());
                        }
                    }
                }

            } else {
                if (!file.isDirectory()) {
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n     " + file.getName() + " ist keine 'csv'-Datei -> wird für den csv-Import ignoriert.");

                }
            }
        });
        return myData;
    }

    // Load Data from JSon Files
    public List<DataModel> loadDataFromJSon(List<File> files) {

//        HashMap<String, Integer> argos_list = new HashMap();
//        HashMap<String, Integer> pdv_list = new HashMap();
        List<DataModel> myData = new ArrayList();
        IntegerProperty patientenZahl = new SimpleIntegerProperty(0);         // Zähler für alle Datensätze
        IntegerProperty jsonZahl = new SimpleIntegerProperty(0);         // Zähler für alle Datensätze
        final String DATE_FORMAT = "yyyyMMdd";
        final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

        files.forEach((file) -> {
            // Nur, falls es sich um ein Verzeichnis handelt
            if (file.isDirectory()) {
                patientenZahl.set(patientenZahl.get() + 1);
                // System.out.println("Verzeichnis: " + file.getName());
                // Im Ordner alle Dateien auslesen

                List<File> jsonFiles = Arrays.asList(file.listFiles());

                JsonStammdaten stammdaten = new JsonStammdaten();
                List<JsonPdv_verschluesselung> pdv_verschluesselungen = new ArrayList();
                List<JsonArgos_tl> argos_tls = new ArrayList();
                List<JsonOp_bericht_extract> op_bericht_extracte = new ArrayList();
                Patient fromJsonPatient = new Patient(null, false);
                HashMap<LocalDate, JsonTextMining_letter_extract> textMining_letter_extract = new HashMap<>(); //JsonTextMining_letter_extract();

                // Nur wenn es sich um eine json Datei handelt
                //System.out.println(jsonFile);
                for (File jsonFile : jsonFiles) {
                    if (jsonFile.getName().toUpperCase().endsWith(".JSON")) {
                        Gson myGson = new Gson();
                        try {

                            FileReader myFileReader = new FileReader(jsonFile);
                            JsonReader myJsonReader = new JsonReader(myFileReader);
                            if (jsonFile.getName().toUpperCase().equals("STAMMDATEN.JSON")) {
                                stammdaten = myGson.fromJson(myJsonReader, JsonStammdaten.class);
                                jsonZahl.set(jsonZahl.get() + 1);
//                                Topos.ladeText.set(Topos.ladeText.get() + "\n     " + jsonFile.getName() + " für Patienten " + file.getName() + " erfolgreich importiert.");
                            } // if Stammdaten
                            if (jsonFile.getName().toUpperCase().equals("PDV_VERSCHLUESSELUNG.JSON")) {
                                pdv_verschluesselungen = myGson.fromJson(myJsonReader, new TypeToken<List<JsonPdv_verschluesselung>>() {
                                }.getType());
                                jsonZahl.set(jsonZahl.get() + 1);
//                                Topos.ladeText.set(Topos.ladeText.get() + "\n     " + jsonFile.getName() + " für Patienten " + file.getName() + " erfolgreich importiert.");
                            } // if PDV_VERSCHLUESSELUNG
                            if (jsonFile.getName().toUpperCase().equals("ARGOS_TL.JSON")) {
                                argos_tls = myGson.fromJson(myJsonReader, new TypeToken<List<JsonArgos_tl>>() {
                                }.getType());
                                jsonZahl.set(jsonZahl.get() + 1);
//                                Topos.ladeText.set(Topos.ladeText.get() + "\n     " + jsonFile.getName() + " für Patienten " + file.getName() + " erfolgreich importiert.");
                            } // if ARGOS_TL

                            if (jsonFile.getName().toUpperCase().equals("OP_BERICHT_EXTRACT.JSON")) {
                                op_bericht_extracte = myGson.fromJson(myJsonReader, new TypeToken<List<JsonOp_bericht_extract>>() {
                                }.getType());
                                jsonZahl.set(jsonZahl.get() + 1);
//                                Topos.ladeText.set(Topos.ladeText.get() + "\n     " + jsonFile.getName() + " für Patienten " + file.getName() + " erfolgreich importiert.");
                            } // if OP_BERICHT_EXTRACT
                            if (jsonFile.getName().toUpperCase().equals("PATIENT.JSON")) {
                                fromJsonPatient = myGson.fromJson(myJsonReader, Patient.class);
                                jsonZahl.set(jsonZahl.get() + 1);
//                                Topos.ladeText.set(Topos.ladeText.get() + "\n     " + jsonFile.getName() + " für Patienten " + file.getName() + " erfolgreich importiert.");
                                if (fromJsonPatient != null) {
                                    System.out.println("Patient-Json: " + fromJsonPatient.getPatientID());
                                }
                            } // if ARGOS_TL
                            // Read the text mining extracts
                            if (jsonFile.getName().toUpperCase().startsWith(file.getName())) {
                                //System.out.println(file.getName());
                                LocalDate currentDate = LocalDate.parse(jsonFile.getName().substring(jsonFile.getName().indexOf("_") + 1, jsonFile.getName().lastIndexOf(".")), FORMATTER);
                                //System.out.println(currentDate);
                                textMining_letter_extract.put(currentDate, myGson.fromJson(myJsonReader, new TypeToken<JsonTextMining_letter_extract>() {
                                }.getType()));
                                //textMining_letter_extract = myGson.fromJson(myJsonReader, new TypeToken<JsonTextMining_letter_extract>() {
                                //}.getType());
                                //System.out.println(jsonFile.getName().substring(jsonFile.getName().indexOf("_") + 1, jsonFile.getName().lastIndexOf(".")));
                                jsonZahl.set(jsonZahl.get() + 1);
//                                Topos.ladeText.set(Topos.ladeText.get() + "\n     " + jsonFile.getName() + " für Patienten " + file.getName() + " erfolgreich importiert.");
                            } // if OP_BERICHT_EXTRACT

                        } catch (FileNotFoundException e) {
                            System.out.println("Error: " + e.getMessage());

                        } // try filenotfound
                    } // endswith JSON
                    else { // endswith Json
                    } // filename endswith json

                } // forEach jsonFiles 
                stammdaten.setId(file.getName());
                if (stammdaten.getGeburtsdatum() == null || stammdaten.getGeschlecht() == null) {
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nDie Stammdaten für Patient " + file.getName() + " sind unvollständig: Geburtsdatum: " + stammdaten.getGeburtsdatum() + " Geschlecht: " + stammdaten.getGeschlecht() + ".");
                }
                JsonDataModelParser jsonParser = new JsonDataModelParser();
                List<DataModel> patientData = jsonParser.dataModelMapper(stammdaten, pdv_verschluesselungen, argos_tls, op_bericht_extracte, textMining_letter_extract);
                myData.addAll(patientData);
            } // isDirectory
        }); // forEach file

//        VA_topos.ladeText.set("");
//        argos_list.forEach((key, value) -> {
//            VA_topos.ladeText.set(VA_topos.ladeText.get() + key + "<" + value + "\n");
//        });
//        VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\n");
//        pdv_list.forEach((key, value) -> {
//            VA_topos.ladeText.set(VA_topos.ladeText.get() + key + "<" + value + "\n");
//        });
        //System.out.println(myData);
        VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n" + patientenZahl.get() + " Patienten aus insgesamt " + jsonZahl.get() + " Json-Dateien erfolgreich importiert.");

        return myData;
    }

    // Prozedur, um die OPS Liste zu laden
    public List<OPSList> loadOPSList(String filePath) {
        List<OPSList> myOPSList = new ArrayList();

        File file = new File(filePath + "\\2016ops.csv");
        if (file.getName().toUpperCase().endsWith("OPS.CSV")) {

            String line; // Eine einzelne Zeile in der Datei
            BufferedReader brData = null; // einen bufferedreader, um die daten aus der Datei zu lesen
            try { // Um Ausnahmen abzufangen

                brData = new BufferedReader(new FileReader(file)); // CSV File einlesen
                // Erste Zeile einlesen
                line = brData.readLine();
                // Die erste Zeile in Spalten aufteilen und in einem Array ablegen
                String myFirstLine[] = line.split(";");
                // Den Array in eine Liste aus Strings nur aus Kleinbuchstaben umwandeln (um Fehler durch Groß-/Kleinschreibung zu vermeiden)
                List<String> myFirstList = new ArrayList();
                for (String s : myFirstLine) {
                    myFirstList.add(s.toLowerCase());
                }

                // Alle Attribute des Datenmodells einlesen
                final Field[] fields = OPSList.class.getDeclaredFields();

                // Feld für die Zuordnung der Spalten in der CSV Datei zu den Attributen im Datenmodell;
                HashMap csvToOPSMap = new HashMap();

                // Jedes Feld der OPSList-Klasse durchgehen und prüfen, ob in der CSV Datei eine Spalte vorhanden ist, dessen Name identisch ist.
                // Falls ja, dann den Index des Attributs merken, falls nicht, dann -1
                for (Field field : fields) {
                    try {

                        // Das Feld für die Zuordnung befüllen, um die richtige Spalte dem richtigen Attribut zuzuordnen
                        //System.out.println(field.getName().toLowerCase() + "   " + myFirstList);
                        csvToOPSMap.put(field.getName().toLowerCase(), myFirstList.indexOf(field.getName().toLowerCase()));

                    } catch (NullPointerException e) { // Im Fehlerfall einen Text ausgeben
                        // Die Ausgabe auf dem Bildschirm zum Stand des Ladens aktualisieren                            
                        System.out.println(e.getMessage());
                    }
                }

                int datensatzZahl = 0; // zählt die gelsenenen Datensätze einer Datei
                int fehlerhaft = 0; // zählt die fehlerhaften/unvollständigen Datensätze
                while ((line = brData.readLine()) != null) {

                    line = line.replaceAll("[^a-zäöüA-ZÄÖÜ0-9.,;-]/", ""); // Sonderzeichen entfernen
                    String[] myLines = line.split(";"); // Trennungszeichen, separiert die Attribute
                    OPSList myOPS = new OPSList(); // Einzelner Datensatz

                    // Prüfen, mit welchem Resultat die Datenfüllroutine ausgeführt wird
                    if (myOPS.setAll(csvToOPSMap, myLines, file.getName())) {
                        myOPSList.add(myOPS); // Einzeldatensatz zu allen Datensätzen hinzufügen
                        datensatzZahl++; // Ein Datensatz wurde gelesen...
                        break;
                    } else {
                        fehlerhaft++;
                    } // Datensatz befüllen
                } // Solange noch Zeilen in der Datendatei sind

                String meldeString = (fehlerhaft == 0)
                        ? "\n OPS-Liste vollständig geladen."
                        : ("\n OPS-Liste NICHT vollständig geladen. Gesamtzahl der Datensätze in der OPS-Datei: " + (datensatzZahl + fehlerhaft) + ". Hiervon erfolgreich geladen: " + datensatzZahl + ". Hiervon fehlerhaft: " + fehlerhaft + ".");
                VA_topos.ladeText.set(VA_topos.ladeText.get() + meldeString);

            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            } finally {
                if (brData != null) {
                    try {
                        brData.close();
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }

        } else {
            System.out.println("Die angegebene Datei: " + file.getName() + " ist keine gültige .csv Datei mit OPS Daten.");
        }

        return myOPSList;
    }

    // Prozedur, um die ICD10 Liste zu laden
    public HashMap<String, List<ICD10List>> loadICD10List(String filePath) {

        HashMap<String, List<ICD10List>> myICD10List = new HashMap();

        File file = new File(filePath + "\\2016icd10.csv");
        //System.out.println(file.getName());
        if (file.getName().toUpperCase().endsWith("ICD10.CSV")) {

            String line; // Eine einzelne Zeile in der Datei
            BufferedReader brData = null; // einen bufferedreader, um die daten aus der Datei zu lesen
            try { // Um Ausnahmen abzufangen

                brData = new BufferedReader(new FileReader(file)); // CSV File einlesen
                // Erste Zeile einlesen
                line = brData.readLine();
                // Die erste Zeile in Spalten aufteilen und in einem Array ablegen
                String myFirstLine[] = line.split(";");
                // Den Array in eine Liste aus Strings nur aus Kleinbuchstaben umwandeln (um Fehler durch Groß-/Kleinschreibung zu vermeiden)
                List<String> myFirstList = new ArrayList();
                for (String s : myFirstLine) {
                    myFirstList.add(s.toLowerCase());
                }

                // Alle Attribute des Datenmodells einlesen
                final Field[] fields = ICD10List.class.getDeclaredFields();

                // Feld für die Zuordnung der Spalten in der CSV Datei zu den Attributen im Datenmodell;
                HashMap csvToICD10Map = new HashMap();

                for (Field field : fields) {
                    try {
                        // Das Feld für die Zuordnung befüllen, um die richtige Spalte dem richtigen Attribut zuzuordnen
                        csvToICD10Map.put(field.getName().toLowerCase(), myFirstList.indexOf(field.getName().toLowerCase()));
                    } catch (NullPointerException e) { // Im Fehlerfall einen Text ausgeben
                        // Die Ausgabe auf dem Bildschirm zum Stand des Ladens aktualisieren                            
                        System.out.println(e.getMessage());
                    }
                }

                int datensatzZahl = 0; // zählt die gelsenenen Datensätze einer Datei
                int fehlerhaft = 0; // zählt die fehlerhaften/unvollständigen Datensätze
                while ((line = brData.readLine()) != null) {

                    line = line.replaceAll("[^a-zäöüA-ZÄÖÜ0-9.,;-]/", ""); // Sonderzeichen entfernen
                    String[] myLines = line.split(";"); // Trennungszeichen, separiert die Attribute
                    ICD10List myICD10 = new ICD10List(); // Einzelner Datensatz

                    // Prüfen, mit welchem Resultat die Datenfüllroutine ausgeführt wird
                    if (myICD10.setAll(csvToICD10Map, myLines, file.getName())) { // true -> alles ok
                        String key = myICD10.getPrimaerschluessel();
                        key = myICD10.getSternschluessel().equals("") ? key : key + "_" + myICD10.getSternschluessel();
                        key = myICD10.getZusatzschluessel().equals("") ? key : key + "_" + myICD10.getZusatzschluessel();
                        myICD10List.putIfAbsent(key, new ArrayList<>()); // Einzeldatensatz zu allen Datensätzen hinzufügen
                        myICD10List.get(key).add(myICD10);
                        datensatzZahl++; // Ein Datensatz wurde gelesen...
                    } else { // false --> Fehler
                        fehlerhaft++;
                    } // Datensatz befüllen
                } // Solange noch Zeilen in der Datendatei sind

                //System.out.println("Gesamtzahl der Datensätze in der ICD10-Datei: " + (datensatzZahl + fehlerhaft) + ". Hiervon erfolgreich geladen: " + datensatzZahl + ". Hiervon fehlerhaft: " + fehlerhaft + ".");
                String meldeString = ((fehlerhaft == 0) && (myICD10List.size() > 0))
                        ? "\n ICD10-Liste vollständig geladen."
                        : ("\n ICD10-Liste NICHT vollständig geladen. Gesamtzahl der Datensätze in der ICD10-Datei: " + (datensatzZahl + fehlerhaft) + ". Hiervon erfolgreich geladen: " + datensatzZahl + ". Hiervon fehlerhaft: " + fehlerhaft + ".");
                VA_topos.ladeText.set(VA_topos.ladeText.get() + meldeString);

            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            } finally {
                if (brData != null) {
                    try {
                        brData.close();
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }

        } else {
            System.out.println("Die angegebene Datei: " + file.getName() + " ist keine gültige .csv Datei mit ICD10 Daten.");
        }
        return myICD10List;
    }

//    public void createAnnoPatients() {
//        VA_topos.observableData.forEach((datum) -> {
//            VA_topos.groupData.get().getObservableAnnotations().putIfAbsent(
//                    datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet(),
//                    new AnnotationModel(datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet(), datum.getGeburtsdatum(), datum.getGeschlecht().toString()));
//        });
//    }
    public ObservableList<DataModel> loadDataFromObject(List<File> files) {

        DataModel methods = new DataModel();
        ObservableList<DataModel> result = FXCollections.observableArrayList();

        //VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nStartzeit: " + LocalDateTime.now());
        files.forEach((file) -> {
            List<DataModelNoProperty> noPropList = new ArrayList(); //methods.convertToDataModelNoProperty(data);
            // Nur, falls es sich um eine CSV Datei handelt
            if (file.getName().toUpperCase().endsWith(".SAV")) {

                FileInputStream fis;
                try {
                    fis = new FileInputStream(file);
                    System.out.println(fis);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    SerializableList readObj = new SerializableList(noPropList);
                    readObj = (SerializableList) ois.readObject();
                    noPropList = readObj.getData();
                    VA_topos.observableData.clear();
                    VA_topos.observableData.addAll(methods.convertFromDataModelNoProperty(noPropList));

                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(Datenladen.class.getName()).log(Level.SEVERE, null, ex);
                } // try - catch // try - catch - catch
            } // If endswith .sav 
        }); // forEach file
        //VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nEndzeit: " + LocalDateTime.now());

        return result;
    }

    /**
     * Read the content of a doctor's letter
     *
     * @param patientID: Id of the patient, used as the folder name on the
     * hard-drive in this context.
     * @param ereignisDatum: Date of the incident for the patient. This will be
     * compared to the filename.
     *
     * The function searches for a file in the folder with the name of the
     * patient ID the name of the file must match the date of the incident given
     * in ereignisDatum. If the date is not matched, the function returns the
     * content of the nearest letter in the future of the incident.
     *
     * @return The function returns a string with the content of the file, that
     * closest matches the date.
     *
     */
    public static String loadLetter(String patientID, LocalDate ereignisDatum) {

        String dataPath = VA_topos.data_directory_custom + "\\" + patientID;
        File directory = new File(dataPath);
        String result = "no content";
        List<File> files = Arrays.asList(directory.listFiles());
        final String DATE_FORMAT = "yyyyMMdd";
        final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
        for (File file : files) {
            String dateStringFromFileName = file.getName().substring(0, file.getName().lastIndexOf("."));
            result = dateStringFromFileName;
//            System.out.println("Filename: " + result + " Ereignisdatum: " + ereignisDatum.toString());
            try {
                LocalDate fileDate = LocalDate.parse(dateStringFromFileName, FORMATTER);
                if (fileDate.isEqual(ereignisDatum) || fileDate.isAfter(ereignisDatum)) {
                    // load text from file here

                    String line; // Eine einzelne Zeile in der Datei
                    BufferedReader brData = null; // einen bufferedreader, um die daten aus der Datei zu lesen
                    try { // Um Ausnahmen abzufangen
                        brData = new BufferedReader(new FileReader(file)); // CSV File einlesen
                        while ((line = brData.readLine()) != null) {
                            result = result + "\n" + line;
                        } // Solange noch Zeilen in der Datendatei sind
                    } catch (FileNotFoundException e) {
                        System.out.println(e.getMessage());
                        result = "Datei " + file.getName() + " nicht  gefunden.";
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                        result = "Fehler beim einlesen der Datei: " + file.getName();
                    } finally {
                        if (brData != null) {
                            try {
                                brData.close();
                            } catch (IOException e) {
                                result = "Fehler beim einlesen der Datei: " + file.getName();
                            }
                        }
                    }
                    return result;
                }
            } catch (DateTimeParseException e) {
            }

        }
        return result;
    }

    /**
     * function to load the translations from the retinal physicians
     *
     * @param filePath -> Path on hard-drive for the code file
     * @return a HashMap with the codes listed in the file and its translation
     */
    public HashMap<String, String> loadCodeTranslator(String filePath) {
        HashMap<String, String> result = new HashMap();

        File file = new File(filePath + "\\2018CodesBasti.csv");
        //System.out.println(file.getName());
        if (file.getName().toUpperCase().endsWith("CODESBASTI.CSV")) {

            String line; // Eine einzelne Zeile in der Datei
            BufferedReader brData = null; // einen bufferedreader, um die daten aus der Datei zu lesen
            try { // Um Ausnahmen abzufangen
                int fehlerhaft = 0;
                int datensatzZahl = 0;
                brData = new BufferedReader(new FileReader(file)); // CSV File einlesen
                while ((line = brData.readLine()) != null) {

                    line = line.replaceAll("[^a-zäöüA-ZÄÖÜ0-9.,;-]/", ""); // Sonderzeichen entfernen
                    String[] myLines = line.split(";"); // Trennungszeichen, separiert die Attribute
                    if (myLines.length == 2) {
                        result.putIfAbsent(myLines[0], myLines[1]);
                        datensatzZahl++; // Ein Datensatz wurde gelesen...
                    } else {
                        fehlerhaft++;
                    }
                } // Solange noch Zeilen in der Datendatei sind

                //System.out.println("Gesamtzahl der Datensätze in der ICD10-Datei: " + (datensatzZahl + fehlerhaft) + ". Hiervon erfolgreich geladen: " + datensatzZahl + ". Hiervon fehlerhaft: " + fehlerhaft + ".");
                String meldeString
                        = (fehlerhaft == 0)
                                ? "\n Übersetzungsliste vollständig geladen."
                                : ("\n Übersetzungsliste NICHT vollständig geladen. Gesamtzahl der Datensätze in der ICD10-Datei: " + (datensatzZahl + fehlerhaft) + ". Hiervon erfolgreich geladen: " + datensatzZahl + ". Hiervon fehlerhaft: " + fehlerhaft + ".");
                VA_topos.ladeText.set(VA_topos.ladeText.get() + meldeString);

            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            } finally {
                if (brData != null) {
                    try {
                        brData.close();
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }

        } else {
            System.out.println("Die angegebene Datei: " + file.getName() + " ist keine gültige .csv Datei mit Code-Übersetzungsdaten.");
        }

        return result;
    }
}
