/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.dataHandling;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

/**
 *
 * @author lokal-admin
 */
public class Utility {

    /**
     *
     * @param <T>
     * @param coll an ArrayList of Comparable objects
     * @param comp
     * @return the median of coll
     */
    public static <T extends Number> double median(Collection<T> coll, Comparator<T> comp) {
        double result;
        int n = coll.size() / 2;

        if (coll.size() % 2 == 0) // even number of items; find the middle two and average them
        {
            result = (nth(coll, n - 1, comp).doubleValue() + nth(coll, n, comp).doubleValue()) / 2.0;
        } else // odd number of items; return the one in the middle
        {
            result = nth(coll, n, comp).doubleValue();
        }

        return result;
    } // median(coll)

    /**
     *
     * @param <T>
     * @param coll a collection of Comparable objects
     * @param n the position of the desired object, using the ordering defined
     * on the list elements
     * @param comp
     * @return the nth smallest object
     */
    public static <T> T nth(Collection<T> coll, int n, Comparator<T> comp) {
        T result, pivot;
        ArrayList<T> underPivot = new ArrayList<>(), overPivot = new ArrayList<>(), equalPivot = new ArrayList<>();

        // choosing a pivot is a whole topic in itself.
        // this implementation uses the simple strategy of grabbing something from the middle of the ArrayList.
        pivot = coll.stream().findFirst().get();

        // split coll into 3 lists based on comparison with the pivot
        coll.forEach((obj) -> {
            int order = comp.compare(obj, pivot);

            if (order < 0) // obj < pivot
            {
                underPivot.add(obj);
            } else if (order > 0) // obj > pivot
            {
                overPivot.add(obj);
            } else // obj = pivot
            {
                equalPivot.add(obj);
            }
        }); // for each obj in coll

        // recurse on the appropriate list
        if (n < underPivot.size()) {
            result = nth(underPivot, n, comp);
        } else if (n < underPivot.size() + equalPivot.size()) // equal to pivot; just return it
        {
            result = pivot;
        } else // everything in underPivot and equalPivot is too small.  Adjust n accordingly in the recursion.
        {
            result = nth(overPivot, n - underPivot.size() - equalPivot.size(), comp);
        }

        return result;
    } // nth(coll, n)

} // Utility
