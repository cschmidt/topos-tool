/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.dataHandling;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableMap;
import va_topos.VA_topos;
import va_topos.data.DataModel.HauptDiagnosen;
import va_topos.data.Patient;

/**
 *
 * @author Christoph Schmidt
 */
public class ICACalculation {

    public static double[][] generateDataFromPatients(ObservableMap<String, Patient> patients) {
        List<List<Double>> myICADataList = new ArrayList();
        patients.values().forEach(patient -> {
            try {
                List<Double> patList = new ArrayList();
                try {
                    patList.add(Double.parseDouble(patient.getPatientID().substring(0, patient.getPatientID().indexOf("_"))));
                } catch (NumberFormatException e) {
                    patList.add(-1.0);
                }
                patList.add((double) patient.getLateralitaet().ordinal());
                patList.add(patient.getAgeAtBeginning());
                patList.add((double) patient.getSex().ordinal());
                patList.add((double) patient.getBeginMonitoring().getYear());
                patList.add(patient.getLengthOfMonitoring());
                patList.add(patient.getNumberOfDiagnoses());
                patList.add(patient.getFirstDiagnosis().isEmpty() ? -1.0 : (double) HauptDiagnosen.valueOf(patient.getFirstDiagnosis()).ordinal());
                patList.add(patient.getLastDiagnosis().isEmpty() ? -1.0 : (double) HauptDiagnosen.valueOf(patient.getLastDiagnosis()).ordinal());

                patList.add(patient.getNumberOfInjections());
                patList.add(patient.getNumberOfMedications());

                patList.add(patient.getNumberOfVisusMeasurements());
                patList.add(patient.getFirstVisus());
                patList.add(patient.getLastVisus());
                patList.add(patient.getMeanVisus());
                patList.add(patient.getMinLogMarVis());
                patList.add(patient.getMaxLogMarVis());

                patList.add(patient.getLinearRegressionIntercept());
                patList.add(patient.getLinearRegressionSlope());

                patList.add(patient.getNumberOfEyePressureMeasurements());
                patList.add(patient.getFirstEyePressure());
                patList.add(patient.getLastEyePressure());
                patList.add(patient.getMeanEyePressure());
                patList.add(patient.getMinEyePressure());
                patList.add(patient.getMaxEyePressure());

                myICADataList.add(patList);

            } catch (Exception e) {
                System.out.println(e.getMessage() + " for patient: " + patient.getPatientID());
            }
        });
        List<List<Double>> removeList = new ArrayList();
        myICADataList.forEach((listElement) -> {
            listElement.forEach(singleElement -> {
                if (Double.isNaN(singleElement)) {
                    removeList.add(listElement);
                }
            });
        });
        myICADataList.removeAll(removeList);

        double[][] returnArray = new double[myICADataList.size()][];
        int fehlerNum = 0;
        int fehler = 0;
        for (int i = 0; i < myICADataList.size(); i++) {
            returnArray[i] = new double[myICADataList.get(i).size()];
            for (int j = 0; j < myICADataList.get(i).size(); j++) {
                if (myICADataList.get(i).get(j) != null) {
                    returnArray[i - fehler][j] = myICADataList.get(i).get(j);
                } else {
                    returnArray[i][j] = 0;
                    fehlerNum++;
                    System.out.println("Fehler Nr.: " + fehlerNum);
                }
            }
        }
        calculateICAData(returnArray);
        return returnArray;
    }

    public static void calculateICAData(double[][] myData) {

        String nameLine = "Patient ID;"
                + "Laterality;"
                + "Age (begin);"
                + "Sex;"
                + "Year First Monitoring;"
                + "Monitoring length;"
                + "Main diagnoses no.;"
                + "first diagnosis;"
                + "last diagnosis;"
                + "Injection no.;"
                + "Medication no.;"
                + "Vis Measurements no.;"
                + "Visus (first);"
                + "Visus (last);"
                + "Visus (avg);"
                + "LogMar (min);"
                + "LogMar (max);"
                + "Intercept (lin reg);"
                + "Slope (lin reg);"
                + "Pressure Measurements no.;"
                + "Pressure (first);"
                + "Pressure (last);"
                + "Pressure (avg);"
                + "Pressure (min);"
                + "Pressure (max)";

        DatenExportieren export = new DatenExportieren();

        export.csvExportICAData(VA_topos.SAVE_DIRECTORY_DEFAULT + "ICAData_" + LocalDate.now() + "_" + LocalTime.now().getHour() + "." + LocalTime.now().getMinute() + "." + LocalTime.now().getSecond() + ".csv", nameLine, myData);
        //patList.add(patient.getAgeAtBeginning());
        //patList.add(patient.getAverageEyePressure());
        //patList.add(patient.getAverageVisus());
        //patList.add(patient.getLengthOfMonitoring());
        //patList.add(patient.getFirstEyePressure());
        //patList.add(patient.getFirstVisus());
        //patList.add(patient.getLastEyePressure());
        //patList.add(patient.getLastVisus());
        //patList.add(patient.getLinearRegressionIntercept());
        //patList.add(patient.getLinearRegressionSlope());
//                patList.add(patient.getMaxEyePressure());
//                patList.add(patient.getMaxLogMarVis());
//                patList.add(patient.getMinEyePressure());
//                patList.add(patient.getMinLogMarVis());
//                patList.add(patient.getNumberOfInjections());
//                patList.add(patient.getNumberOfMedications());
//                patList.add(patient.getNumberOfVisusMeasurements());
//                patList.add(patient.getNumberOfEyePressureMeasurements());
//                patList.add((double) patient.getSex().ordinal());
//                patList.add((double) patient.getBeginMonitoring().getYear());        
        //double[][] myData = {{1.0, 10.2}, {2.1, 2.5}, {3.0, 31.7}};
//        try {
//            FastICA myICA = new FastICA(myData, 1);
//            System.out.println(myICA.getICVectors()[0][0]);
//        } catch (FastICAException ex) {
//            Logger.getLogger(ICATabBuilder.class.getName()).log(Level.SEVERE, null, ex);
//            System.out.println("fail");
//        }
    }
}
