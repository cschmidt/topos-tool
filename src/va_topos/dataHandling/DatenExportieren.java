/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.dataHandling;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import va_topos.data.DataModel;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableMap;
import javafx.stage.FileChooser;
import va_topos.VA_topos;
import va_topos.data.DataModelNoProperty;
import va_topos.data.MedicationChange;
import va_topos.data.Patient;
import va_topos.data.SerializableList;

/**
 *
 * @author lokal-admin
 */
@SuppressWarnings("unchecked")
public class DatenExportieren {

    public List<String> csvExportDataModel(String dateiname, List<DataModel> exportDaten, boolean saveToFile) {

        // Das Resultat initialisieren (true = export erfolgreich)
        List<String> result = new ArrayList();
        if (!saveToFile) { // Falls die Exportdaten nur in der Konsole angezeigt werden sollen
            //Field[] fields = DataModel.class.getDeclaredFields();
            // Die Feldnamen in der Datei ablegen
            String fieldNames = "Patient ID;Visus from System; Visus from Letter; Pressure from System; Pressure from Letter;Diagnosis from System; Diagnosis from Letter";
            //for (Field field : fields) {
            //    if (!field.getName().contains("CLASSNAME")) {
            //fieldNames = (fieldNames.equals("")) ? "\n" + field.getName() : fieldNames + ";" + field.getName();
//                }
//            }
            System.out.println(fieldNames);
//            VA_topos.ladeText.set(fieldNames);

            // Jeden Datensatz durchgehen
            exportDaten.forEach((datum) -> {
                //System.out.println("Exportiere Datensatz " + (exportDaten.indexOf(datum) + 1) + " von " + exportDaten.size());
                // Methodenobjekt für den Datenzugriff auf die einzelnen Felder
                DataModel data = new DataModel();
                String line = "";
//                for (Field field : fields) {
//                    try {
//                        line += (line.equals("")) ? "" : ";";
//                        line += data.getValueFromFields(field, datum);
//                    } catch (IllegalArgumentException ex) {
//                        Logger.getLogger(DatenExportieren.class.getName()).log(Level.SEVERE, null, ex);
//                        result.add("\nFehler: " + ex.getMessage());
//                    } // IllegalArgument
//                } // for fields
                System.out.println(datum.getPatientenID() + ";" + datum.getDezimalVisusWert() + ";" + datum.getDezimalVisusWertFromLetterExtract() + ";" + datum.getAugendruck() + ";" + datum.getAugendruckFromLetterExtract() + ";" + datum.getHauptDiagnose() + ";" + datum.gethauptDiagnoseFromLetterExtract());
                //VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n" + datum.getPatientenID() + ";" + datum.getDezimalVisusWert() + ";" + datum.getDezimalVisusWertFromLetterExtract() + ";" + datum.getAugendruck() + ";" + datum.getAugendruckFromLetterExtract() + ";" + datum.getHauptDiagnose() + ";" + datum.gethauptDiagnoseFromLetterExtract());
            }); // ExportDatum

            if (result.size() < 1) {
                result.add("erfolgreich.");
            }
            return result;
        }

        FileWriter fw = null;

        try {
            // Die Datei zum schreiben öffnen
            fw = new FileWriter(dateiname);
            // Alle Felder des Datenmodells einlesen
            try (BufferedWriter bw = new BufferedWriter(fw)) {
                // Alle Felder des Datenmodells einlesen
                Field[] fields = DataModel.class.getDeclaredFields();
                // Die Feldnamen in der Datei ablegen
                String fieldNames = "";
                for (Field field : fields) {
                    if (!field.getName().contains("CLASSNAME")) {
                        fieldNames = (fieldNames.equals("")) ? field.getName() : fieldNames + ";" + field.getName();
                    }
                }
                bw.write(fieldNames);

                List<String> lines = new ArrayList();

                // Jeden Datensatz durchgehen
                exportDaten.forEach((datum) -> {
                    System.out.println("Exportiere Datensatz " + (exportDaten.indexOf(datum) + 1) + " von " + exportDaten.size());
                    // Methodenobjekt für den Datenzugriff auf die einzelnen Felder
                    DataModel data = new DataModel();
                    String line = "";
                    for (Field field : fields) {
                        try {
                            line += (line.equals("")) ? "" : ";";
                            line += data.getValueFromFields(field, datum);
                        } catch (IllegalArgumentException ex) {
                            Logger.getLogger(DatenExportieren.class.getName()).log(Level.SEVERE, null, ex);
                            result.add("\nFehler: " + ex.getMessage());
                        } // IllegalArgument
                    } // for fields
//                    System.out.println("Line added. Line: " + lines.size() + " of " + exportDaten.size() + " Lines.");
                    lines.add(line);
//                try {
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nExportiere Datensatz: " + line);
                }); // ExportDatum
//                    bw..write(lines);

                lines.forEach(line -> {
                    try {
                        bw.newLine();
                        bw.write(line);
//                        System.out.println("Writing line: " + lines.indexOf(line));
                    } catch (IOException ex) {
                        Logger.getLogger(DatenExportieren.class.getName()).log(Level.SEVERE, null, ex);
                        result.add("\nFehler: " + ex.getMessage());
                    }
                });

            }
        } catch (IOException ex) {
            result.add("\nFehler: " + ex.getMessage());
        } finally {
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                result.add("\nFehler: " + ex.getMessage());
            }
        }
        return result;

    }

    public List<String> csvExportAnnoPatients(String dateiname, ObservableMap<String, Patient> exportMap) {
        List<String> result = new ArrayList();
        result.add("erfolgreich.");

        List<Patient> exportDaten = new ArrayList();
        exportMap.forEach((key, element) -> exportDaten.add(element));

        FileWriter fw = null;

        try {
            // Die Datei zum schreiben öffnen
            fw = new FileWriter(dateiname);
            // Alle Felder des Datenmodells einlesen
            try (BufferedWriter bw = new BufferedWriter(fw)) {
                // Alle Felder des Datenmodells einlesen
                Field[] fields = Patient.class.getDeclaredFields();
                // Die Feldnamen in der Datei ablegen
                String fieldNames = "";
                for (Field field : fields) {
                    fieldNames = (fieldNames.equals("")) ? field.getName() : fieldNames + ";" + field.getName();
                }
                bw.write(fieldNames);
                // Jeden Datensatz durchgehen
                List<String> lines = new ArrayList();
                exportDaten.forEach((datum) -> {
                    // Methodenobjekt für den Datenzugriff auf die einzelnen Felder
                    Patient data = new Patient(null, false);
                    String line = "";
                    for (Field field : fields) {
                        try {
                            line += (line.equals("")) ? "" : ";";
                            line += data.getValueFromFields(field, datum);
                        } catch (IllegalArgumentException ex) {
                            Logger.getLogger(DatenExportieren.class.getName()).log(Level.SEVERE, null, ex);
                            result.add("\nFehler: " + ex.getMessage());
                        } // IllegalArgument
                    } // for fields
                    lines.add(line);
//                try {
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nExportiere Datensatz: " + line);
                }); // ExportDatum
//                    bw..write(lines);
                lines.forEach(line -> {
                    try {
                        bw.newLine();
                        bw.write(line);
                    } catch (IOException ex) {
                        Logger.getLogger(DatenExportieren.class.getName()).log(Level.SEVERE, null, ex);
                        result.add("\nFehler: " + ex.getMessage());

                    }
                });
            }
        } catch (IOException ex) {
            result.add("\nFehler: " + ex.getMessage());
        } finally {
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                result.add("\nFehler: " + ex.getMessage());
            }
        }

        return result;
    }

    public List<String> csvExportVisChange(String dateiname, List<MedicationChange> exportDaten) {
        List<String> result = new ArrayList();
        result.add("erfolgreich.");

        String fieldNames = "getOldInjDate;getOldInjDate;getOldMed;getNewMed;getOldVisDate;getNewVisDate;getOldVisualAcuity;getNewVisualAcuity;getVisChange;getOldVisualAcuityLetter;getNewVisualAcuityLetter;getVisChangeLetter;getPatientID;getDiagnosis";
        System.out.println(fieldNames);
        //List<String> lines = new ArrayList();
        exportDaten.forEach((datum) -> {

            String line = "";
            line += datum.getOldInjDate().toString() + ";";
            line += datum.getNewInjDate().toString() + ";";
            line += datum.getOldMed().toString() + ";";
            line += datum.getNewMed().toString() + ";";
            line += datum.getOldVisDate().toString() + ";";
            line += datum.getNewVisDate().toString() + ";";
            line += datum.getOldVisualAcuity() + ";";
            line += datum.getNewVisualAcuity() + ";";
            line += datum.getVisChange() + ";";
            line += datum.getOldVisualAcuityLetter() + ";";
            line += datum.getNewVisualAcuityLetter() + ";";
            line += datum.getVisChangeLetter() + ";";
            line += datum.getPatientID() + ";";
            line += datum.getDiagnosis();
            //lines.add(line);
//                try {
            VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nExportiere Datensatz: " + line);
            System.out.println(line);
        }); // ExportDatum

//        FileWriter fw = null;
//
//        try {
//            // Die Datei zum schreiben öffnen
//            fw = new FileWriter(dateiname);
//            // Alle Felder des Datenmodells einlesen
//            try (BufferedWriter bw = new BufferedWriter(fw)) {
//                // Alle Felder des Datenmodells einlesen
////                Field[] fields = MedicationChange.class.getDeclaredFields();
//                // Die Feldnamen in der Datei ablegen
//                String fieldNames = "getOldInjDate;getOldInjDate;getOldMed;getNewMed;getOldVisDate;getNewVisDate;getOldVisualAcuity;getNewVisualAcuity;getVisChange;getPatientID;getDiagnosis";
////                for (Field field : fields) {
////                    fieldNames = (fieldNames.equals("")) ? field.getName() : fieldNames + ";" + field.getName();
////                }
//                bw.write(fieldNames);
//                // Jeden Datensatz durchgehen
//                List<String> lines = new ArrayList();
//                exportDaten.forEach((datum) -> {
//
//                    String line = "";
//                    line += datum.getOldInjDate().toString() + ";";
//                    line += datum.getNewInjDate().toString() + ";";
//                    line += datum.getOldMed().toString() + ";";
//                    line += datum.getNewMed().toString() + ";";
//                    line += datum.getOldVisDate().toString() + ";";
//                    line += datum.getNewVisDate().toString() + ";";
//                    line += datum.getOldVisualAcuity() + ";";
//                    line += datum.getNewVisualAcuity() + ";";
//                    line += datum.getVisChange() + ";";
//                    line += datum.getPatientID() + ";";
//                    line += datum.getDiagnosis();
//                    lines.add(line);
////                try {
//                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nExportiere Datensatz: " + line);
//                }); // ExportDatum
////                    bw..write(lines);
//                lines.forEach(line -> {
//                    try {
//                        bw.newLine();
//                        bw.write(line);
//                    } catch (IOException ex) {
//                        Logger.getLogger(DatenExportieren.class.getName()).log(Level.SEVERE, null, ex);
//                        result.add("\nFehler: " + ex.getMessage());
//
//                    }
//                });
//            }
//        } catch (IOException ex) {
//            result.add("\nFehler: " + ex.getMessage());
//        } finally {
//            try {
//                if (fw != null) {
//                    fw.close();
//                }
//            } catch (IOException ex) {
//                result.add("\nFehler: " + ex.getMessage());
//            }
//        }
        return result;
    }

    public List<String> csvExportICAData(String dateiname, String nameLine, double[][] exportDaten) {
        List<String> result = new ArrayList();
        result.add("erfolgreich.");

        FileWriter fw = null;

        try {
            // Die Datei zum schreiben öffnen
            fw = new FileWriter(dateiname);
            // Alle Felder des Datenmodells einlesen
            try (BufferedWriter bw = new BufferedWriter(fw)) {
                bw.write(nameLine);
                // Jeden Datensatz durchgehen
                List<String> lines = new ArrayList();
                for (double[] exportDatum : exportDaten) {
                    String line = "";
                    for (double exportValue : exportDatum) {
                        line += exportValue + ";";
                    }
                    lines.add(line);
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nExportiere Datensatz: " + line);
                }
                lines.forEach(line -> {
                    try {
                        bw.newLine();
                        bw.write(line);
                    } catch (IOException ex) {
                        Logger.getLogger(DatenExportieren.class.getName()).log(Level.SEVERE, null, ex);
                        result.add("\nFehler: " + ex.getMessage());

                    }
                });
            }
        } catch (IOException ex) {
            result.add("\nFehler: " + ex.getMessage());
        } finally {
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                result.add("\nFehler: " + ex.getMessage());
            }
        }

        return result;
    }

    public void objectSave(List<DataModel> data) {

        DataModel methods = new DataModel();
        List<DataModelNoProperty> noPropList = methods.convertToDataModelNoProperty(data);

        FileChooser dc = new FileChooser();
        File initDir = new File(VA_topos.SAVE_DIRECTORY_DEFAULT);
        dc.setInitialDirectory(initDir);
        File selectedFile = dc.showSaveDialog(null);

        if (selectedFile != null) {
            FileOutputStream fis;
            try {
                fis = new FileOutputStream(selectedFile);
                ObjectOutputStream ois;
                ois = new ObjectOutputStream(fis);
                SerializableList writeObj = new SerializableList(noPropList);
                ois.writeObject(writeObj);

            } catch (FileNotFoundException ex) {
                Logger.getLogger(DatenExportieren.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DatenExportieren.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Function to save the patients as Json structure
     *
     * @param patients
     */
    public void jsonSave(ObservableMap<String, Patient> patients) {
        Gson myGson = new Gson();
        patients.forEach((key, patient) -> {
            File jsonFile = new File(va_topos.VA_topos.data_directory_custom + "/" + patient.getPatientID().substring(0,8) + "/" + patient.getLateralitaet() + "_patient.json");

            System.out.println("blub");
            try {
                //jsonFile.createNewFile();
                FileWriter myFileWriter = new FileWriter(jsonFile);
                JsonWriter myJsonWriter = new JsonWriter(myFileWriter);
                myGson.toJson(patient, Patient.class, myJsonWriter);
            } catch (IOException ex) {
                Logger.getLogger(AnnotationToJsonParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
}
