/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.dataHandling;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import va_topos.data.JsonAnnotation;

/**
 *
 * @author Christoph Schmidt
 */
public class AnnotationToJsonParser {

    public static boolean parseAnnotation(JsonAnnotation anno) {

        if (anno == null || anno.getPatientID().equals("")) {
            return false;
        }

        Gson myGson = new Gson();
        File jsonFile = new File(va_topos.VA_topos.data_directory_custom + "/" + anno.getPatientNo() + "/" + anno.getLateralitaet() + "_annotation_" + LocalTime.now().getHour() + "_" + LocalTime.now().getMinute() + "_" + LocalTime.now().getSecond() + "." + LocalTime.now().getNano() + ".json");
        try {
            jsonFile.createNewFile();
            try (FileWriter myFileWriter = new FileWriter(jsonFile)) {
                myGson.toJson(anno, myFileWriter);
                myFileWriter.flush();
            }
        } catch (IOException ex) {
            Logger.getLogger(AnnotationToJsonParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
}
