/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.dataHandling;

import java.time.Duration;
import va_topos.data.JsonArgos_tl;
import va_topos.data.DataModel;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import va_topos.data.JsonPdv_verschluesselung;
import va_topos.data.JsonStammdaten;
import va_topos.VA_topos;
import static va_topos.VA_topos.LOGMAR_MAXIMUM;
import va_topos.data.DataModel.AntiVEGFMedikament;
import va_topos.data.DataModel.HauptDiagnosen;
import va_topos.data.DataModel.Visuswerte;
import va_topos.data.JsonOp_bericht_extract;
import va_topos.data.JsonTextMining_letter_extract;

/**
 *
 * @author varie
 */
@SuppressWarnings("unchecked")
public class JsonDataModelParser {

    // function, to sort the different argos_tl values from the data into a map and count the occurences
    public void countArgosValues(HashMap<String, Integer> argos_list, List<JsonArgos_tl> argos_tls) {
        argos_tls.forEach(argos_tl -> {
            String key = argos_tl.getContent() + "<" + argos_tl.getLane();
            if (argos_list.containsKey(key)) {
                argos_list.put(key, argos_list.get(key) + 1);
            } else {
                argos_list.put(key, 1);
            }
        });

    }

    // function, to sort the different pdv_verschluesselungen values from the data into a map and count the occurences
    public void countPDVValues(HashMap<String, Integer> pdv_list, List<JsonPdv_verschluesselung> pdv_verschluesselungen) {
        pdv_verschluesselungen.forEach(pdv -> {
//            String key = pdv.getLoc() + "<" + pdv.getCode();
            String key = pdv.getCode();
            if (pdv_list.containsKey(key)) {
                pdv_list.put(key, pdv_list.get(key) + 1);
            } else {
                pdv_list.put(key, 1);
            }
        });

    }

    // function, to sort the different pdv_verschluesselungen values from the data into a map and count the occurences
    public void countOPBerichte(HashMap<String, HashMap<String, Integer>> result, List<JsonOp_bericht_extract> op_bericht_extracte) {
        //HashMap<String, HashMap<String, Integer>> result = new HashMap();

        result.putIfAbsent("Auge", new HashMap<>());
        result.putIfAbsent("Datum", new HashMap<>());
        result.putIfAbsent("Diag", new HashMap<>());
        result.putIfAbsent("Medi", new HashMap<>());

        final String[] vierer = {"Auge", "Datum", "Diag", "Medi"};

        for (int i = 0; i < 4; i++) {
            HashMap<String, Integer> attribut = result.get(vierer[i]);
            for (JsonOp_bericht_extract op : op_bericht_extracte) {
                String value = "";
                switch (vierer[i]) {
                    case "Auge":
                        value = op.getAuge();
                        break;
                    case "Datum":
                        value = op.getDatum();
                        break;
                    case "Diag":
                        value = op.getDiag();
                        break;
                    case "Medi":
                        value = op.getMedi();
                        break;
                }
                Integer anzahl = attribut.get(value);
                if (anzahl != null) {
                    attribut.put(value, anzahl + 1);
                } else {
                    attribut.put(value, 1);
                }
            }
            result.put(vierer[i], attribut);
        }
//        result.forEach((column, map) -> {
//        System.out.println(column);
//            map.forEach((row, count) -> System.out.println(row + ";" + count));
//        });
        //return result;
    }

    // Function to sort the JSon Data from the clinic into the dataModel. 
    // The function receives all json Data from one patient (the whole timeline)
    public List<DataModel> dataModelMapper(JsonStammdaten stammdaten, List<JsonPdv_verschluesselung> pdv_verschluesselungen, List<JsonArgos_tl> argos_tls, List<JsonOp_bericht_extract> op_bericht_extracte, HashMap<LocalDate, JsonTextMining_letter_extract> textMining_letter_extract) {

        //countOPBerichte(VA_topos.opData, op_bericht_extracte);
        // Ziel: Erstellen aller relevanten Datensätze aus den vorhandenen Daten
        //
        // zu diesem Zeitpunkt sind folgende Daten verfügbar:
        //
        // Vergleichsdaten:
        // VA_topos.observableICD10List --> ICD10 Liste
        // VA_topos.observableOPSList --> OPS Liste
        //
        // Freiburger Daten zu einem Patienten (die ganze Historie):
        //
        // stammdaten: patientenID, geburtsdatum & geschlecht
        //
        // argos_tls: 
        //      start: Datum des Ereignisses; 
        //      end: leer; 
        //      lane: codierung des Ereignisses & Auge; interne codierung
        //              0_RA, 0_LA              --> Visus
        //              1_RA, 1_LA              --> Augendruck
        //              2_BA                    --> ICD10 code?
        //              2_OPRA, 2_OPLA, 02_OPBA --> Spritze, bzw. andere OP
        //              30                      --> behandelnder Arzt
        //              1_DocsCal		--> "Sudie:..." (not misspelled!)
        //      content: beschreibung des ereignisses, teilweise ICD10 codes
        //
        // pdv_verschluesselung: 
        //      datum: Datum & Uhrzeit des Ereignisses; 
        //      loc: "R", "L", "B", ""; 
        //      code: Code des Ereignisses (ICD10 & OPS)
        //
        // op_bericht_extract:
        //
        //      auge: Lateralität
        //      datum: OP Datum
        //      diag: die Diagnose für das Auge
        //      medi: verwendetes Medikament
        //
        // textMining_letter_extract
        //
        //  enthält die strukturiert ausgelesenen Informationen von Averbis
        //
        //      Struktur sie class JsonTextMining_letter_extract
        //
        //
        // Aufgabe: 
        //
        //  Info: in den Datenlisten argos_tl & pdv_verschluesselung sind eine Reihe von Datumsangaben, zu denen Ereignisse stattgefunden haben.
        //  1. Es muss identifiziert werden, zu welchem Datum welches Ereignis stattgefunden hat, und inwieweit sich daraus ein Datensatz für das Datenmodell erstellen lässt
        //      1.a. Die identifizierung bzw. decodierung des Ereignisses muss mit Hilfe der Vergleichsdaten erfolgen
        //  2. Es muss sichergestellt werden, dass zum gleichen Datum nur ein entsprechender Datensatz erstellt wird, um Redundanzen zu vermeiden
        //
        // Zu 1.
        // Gehe alle argos_tl & pdv_verschluesselungsdaten durch
        //
        // Ein einzelner Datensatz wird charakterisiert durch:
        // 1. Patient
        // 2. Datum
        // 3. Lateralitaet
        //
        LocalTime nowTime = LocalTime.now();
        VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n NICHT interpretiert für Patient: " + stammdaten.getId() + ": ");
        HashMap<String, DataModel> argosData = argos_tlMapper(stammdaten, argos_tls);
        List<DataModel> op_bericht_extractData = op_bericht_extractMapper(stammdaten, argosData, op_bericht_extracte);
        List<DataModel> allData = pdv_verschluesselungenMapper(op_bericht_extractData, stammdaten, pdv_verschluesselungen);
        allData = textMining_letter_extractMapper(allData, stammdaten, pdv_verschluesselungen, textMining_letter_extract);
        List<DataModel> removeList = new ArrayList();

        allData.forEach((datum) -> {
            if (datum.getAugenID() != null && !datum.getAugenID().equals("")) {
                if ((datum.getMedikament().equals(DataModel.AntiVEGFMedikament.E) || datum.getMedikament().equals(DataModel.AntiVEGFMedikament.UNKNOWN))
                        //&& (datum.getSpritzenID().equals("") || datum.getSpritzenID().equals("argos_tl") )
                        && !datum.isArgosEreignis()
                        && Double.isNaN(datum.getAugendruck())
                        && Double.isNaN(datum.getDezimalVisusWert())) {
                    removeList.add(datum);
                }
            } else {
                removeList.add(datum);
            }
            if (datum.getAchse() < 1) {
                removeList.add(datum);
            }
        });
        allData.removeAll(removeList);
        VA_topos.ladeText.set(VA_topos.ladeText.get() + " Analysezeit: " + (double) (Duration.between(nowTime, LocalTime.now()).toMillis() / 1000.0));
        return allData;
    }

    private HashMap<String, DataModel> argos_tlMapper(JsonStammdaten stammdaten, List<JsonArgos_tl> argos_tls) {

        final String DATE_FORMAT = "yyyy-MM-dd";
        final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

        // Erstelle eine HashMap, mit dem Datum & der Lateralitaet als Schluessel und einem Datensatz als value
        HashMap<String, DataModel> ereignisse = new HashMap();

        IntegerProperty ereignisZaehler = new SimpleIntegerProperty(argos_tls.size());
        IntegerProperty ereignisFehler = new SimpleIntegerProperty(0);
        //System.out.println("1");

        argos_tls.forEach((argos_tl) -> {
            try {
                LocalDate myDate = LocalDate.parse(argos_tl.getStart(), FORMATTER);

                List<String> keys = new ArrayList();
                //System.out.println("2");

                switch (argos_tl.getLane()) {

                    case "0_RA":
                    case "1_RA":
                    case "2_OPRA":
                        keys.add("_r_" + myDate);
                        break;

                    case "0_LA":
                    case "1_LA":
                    case "2_OPLA":
                        keys.add("_l_" + myDate);
                        break;

                    case "2_BA":
                    case "2_OPBA":
                    case "30":
                        //case "30":
                        keys.add("_r_" + myDate);
                        keys.add("_l_" + myDate);
                        break;

                    case "1_DocsCal":
                        break;
                    default:
                        System.out.println("Patient: " + stammdaten.getId() + " argos_tl.getLane(): " + argos_tl.getLane());
                        ereignisFehler.set(ereignisFehler.get() + 1);
                } // switch lane
                //System.out.println("3");
                keys.forEach(key -> {

                    // Lateralitaet zwischenspeichern
                    DataModel.AugeLateralitaet lat
                            = key.startsWith("_l")
                            ? DataModel.AugeLateralitaet.LEFT
                            : DataModel.AugeLateralitaet.RIGHT;
                    if (!ereignisse.containsKey(key)) {
                        ereignisse.put(key, new DataModel());
                        ereignisse.get(key).initializeAll(ereignisse.get(key));
                    }
                    DataModel temp = ereignisse.get(key);
                    // Die Basisdaten für den Datensatz schreiben
                    //System.out.println("4");
                    temp.setEreignisDatum(myDate);
                    try { // falls sie vorhanden sind
                        temp.setPatientenID(stammdaten.getId());
                        temp.setDatenquelle("Ordner " + stammdaten.getId());
                        temp.setGeburtsdatum(LocalDate.parse(stammdaten.getGeburtsdatum(), FORMATTER));
                        temp.setGeschlecht(DataModel.Geschlecht.valueOf(stammdaten.getGeschlecht()));
                    } catch (Exception e) {
                        System.out.println("fehler beim parsen der stammdaten. Stammdaten: ID: " + stammdaten.getId() + " Geb.datum: " + stammdaten.getGeburtsdatum() + " Geschlecht: " + stammdaten.getGeschlecht());
                    }
                    temp.setAugenID(key);

                    // Initialisierung, dass es sich zunächst weder um eine Spritze, noch um eine Untersuchung handelt
                    //System.out.println("5");
                    switch (argos_tl.getLane()) {
                        case "0_RA":
                        case "0_LA":
                            //System.out.println("5.1a");
                            temp.setUntersuchungLateralitaet(lat);
                            temp.setRohWert(argos_tl.getContent());
                            //temp.parseVisusWert(temp.getRohWert());
                            temp.setVisuswerte(temp.convertRohWertToVisusWert(temp.getRohWert()));
                            temp.setDezimalVisusWert(temp.calculateDezimalVisusWert(temp.getVisuswerte(), temp.getRohWert()));
                            temp.setLogMarVisusWert(temp.calculateLogMarVisusWert(LOGMAR_MAXIMUM, temp.getDezimalVisusWert()));
                            temp.setVisusKlasse(temp.calculateVisusClass(temp.getDezimalVisusWert()));
                            temp.setUntersuchungsID("1"); // Es handelt sich um eine Untersuchung
                            temp.setAchse(1);
//                            System.out.println("Rohwert: " + argos_tl.getContent() + " Dez-VisusWert: " + temp.getVisusKlasse());
                            //System.out.println("5.1b");
                            break;
                        case "1_RA":
                        case "1_LA":
                            //System.out.println("5.2a");

                            // Flag setzen, dass Daten tatsächlich eingetragen wurden
                            temp.setAchse(1);

                            temp.setUntersuchungLateralitaet(lat);
                            try {
                                temp.setAugendruck(Double.parseDouble(argos_tl.getContent()));
//                                System.out.println("Augendruck-Rohwert: " + argos_tl.getContent() + " erfolgreich in " + temp.getAugendruck() + " umgewandelt.");

                            } catch (NumberFormatException e) {
                                System.out.println("Augendruck-Rohwert: " + argos_tl.getContent() + " konnte nicht in eine Zahl umgewandelt werden.");
                            }
                            temp.setUntersuchungsID("1"); // Es handelt sich um eine Untersuchung
                            //System.out.println("5.2b");
                            break;
                        case "2_BA":
                            //System.out.println("5.3a");
                            try {
                                String[] mainDiagnoses = {"H34", "H35", "H36"};
                                String diagKey = "neben";

                                for (String possType : mainDiagnoses) {
                                    if (argos_tl.getContent().toUpperCase().contains(possType)) {
                                        diagKey = possType;
                                        //break;
                                    }
                                }

                                temp.setDiagnoseQuelle("argos_tl");
                                temp.setArgos_tl_Diagnose(argos_tl.getContent());

                                switch (diagKey.toUpperCase()) {
                                    case "H34":
                                        temp.setHauptDiagnose(DataModel.HauptDiagnosen.GEFAESSVERSCHLUSS);
                                        temp.setDiagnoseID(temp.getDiagnoseID().equals("") ? argos_tl.getContent() : temp.getDiagnoseID() + ", " + argos_tl.getContent());
                                        break;
                                    case "H35":
                                        temp.setHauptDiagnose(DataModel.HauptDiagnosen.AMD_ALLGEMEIN);
                                        temp.setDiagnoseID(temp.getDiagnoseID().equals("") ? argos_tl.getContent() : temp.getDiagnoseID() + ", " + argos_tl.getContent());
                                        break;
                                    case "H36":
                                        temp.setHauptDiagnose(DataModel.HauptDiagnosen.DMOE);
                                        temp.setDiagnoseID(temp.getDiagnoseID().equals("") ? argos_tl.getContent() : temp.getDiagnoseID() + ", " + argos_tl.getContent());
                                        break;
                                    default:
                                        temp.setNebenDiagnose(temp.getNebenDiagnose() + argos_tl.getContent().toUpperCase());
                                        temp.setDiagnoseID(temp.getDiagnoseID().equals("") ? argos_tl.getContent() : temp.getDiagnoseID() + ", " + argos_tl.getContent());
                                } // switch key

//                                temp.setDiagnoseID(argos_tl.getContent());
                                //System.out.println("5.3a1");
                                // Hier ICD10 Prüfung auf content machen und im Erfolgsfall auf Diagnose schreiben
//                                ICD10List myICD = new ICD10List();
                                //System.out.println("5.3a2");
//                                String icd10diag = myICD.getValueFromKey(VA_topos.observableICD10Map, argos_tl.getContent());
                                //System.out.println("5.3a3");
//                                temp.setNebenDiagnose(icd10diag == null ? "" : icd10diag);
                                //System.out.println("5.3a4");
                            } catch (Exception e) {
                                System.out.println("Content: " + argos_tl.getContent() + " Fehlertext: " + e.getMessage());
                            }
                            //System.out.println("5.3b");
                            break;

                        case "2_OPRA":
                        case "2_OPLA":
                        case "2_OPBA":
                            //System.out.println("5.4a");
                            //System.out.println(argos_tl.getContent());

                            temp.setAchse(1);
                            if (argos_tl.getContent().toLowerCase().contains("phako")) {
                                temp.setArgosEreignis(true);
                                temp.setArgosEreignisText(argos_tl.getContent());
                            } else {
                                temp.setMedikament(DataModel.convertToAntiVEGFMedikament(argos_tl.getContent()));
                                temp.setSpritzenQuelle("argos_tl"); // Es handelt sich um eine Spritze
                            }
                            temp.setEreignisDatum(myDate);
                            temp.setUntersuchungLateralitaet(lat);
                            //System.out.println("5.4b");
                            break;
                        case "30":
                            //System.out.println("5.5a");
                            temp.setAchse(1);
                            temp.setName(argos_tl.getContent());
                            //System.out.println("5.5b");
                            break;
                        default:
                    } // switch lane
                }); // forEach key
                //System.out.println("6");
//                String[] myLines = temp.split(";"); // Trennungszeichen, separiert die Attribute
//                String fehlerText = ereignisse.get(myDate).setAll(jsonToModelMap, myLines, "Folder " + stammdaten.getId(), -1000);
//                System.out.println("Erfolg -- start: " + argos_tl.start + " myDate: " + myDate);
            } catch (Exception e) {
                System.out.println("Fehler in argos_tl: " + e.getLocalizedMessage() + " Content: " + argos_tl.getContent() + " Datum: " + argos_tl.getStart() + " Lane: " + argos_tl.getLane());
            } // Exception

        }
        );
        if (ereignisFehler.get() > 0) {
            VA_topos.ladeText.set(VA_topos.ladeText.get() + " argos_tl.json: " + ereignisFehler.get() + " / " + ereignisZaehler.get());
        }

        //System.out.println("Patient: " + stammdaten.getId() + " Anzahl ereignisse in argos_tl: " + ereignisse.size());
//        List<DataModel> returnList = new ArrayList();
//
//        ereignisse.forEach(
//                (key, value) -> {
//                    returnList.add(value);
//                }
//        );
        return ereignisse;
    }

    private List<DataModel> op_bericht_extractMapper(JsonStammdaten stammdaten, HashMap<String, DataModel> ereignisse, List<JsonOp_bericht_extract> op_bericht_extractData) {

        final String DATE_FORMAT = "yyyy-MM-dd";
        final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
        final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
        final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);

        IntegerProperty ereignisZaehler = new SimpleIntegerProperty(op_bericht_extractData.size());
        IntegerProperty ereignisFehler = new SimpleIntegerProperty(0);

        op_bericht_extractData.forEach((op) -> {
            try {
                LocalDate myDate = LocalDate.parse(op.getDatum(), DATE_TIME_FORMATTER);

                List<String> keys = new ArrayList();

                switch (op.getAuge()) {

                    case "RA":
                        keys.add("_r_" + myDate);
                        break;
                    case "LA":
                        keys.add("_l_" + myDate);
                        break;
                    case "BA":
                        keys.add("_r_" + myDate);
                        keys.add("_l_" + myDate);
                        break;

                    default:
                        ereignisFehler.set(ereignisFehler.get() + 1);
                } // switch lane
                keys.forEach(key -> {

                    // Lateralitaet zwischenspeichern
                    DataModel.AugeLateralitaet lat
                            = key.startsWith("_l")
                            ? DataModel.AugeLateralitaet.LEFT
                            : DataModel.AugeLateralitaet.RIGHT;
                    if (!ereignisse.containsKey(key)) {
                        ereignisse.put(key, new DataModel());
                        ereignisse.get(key).initializeAll(ereignisse.get(key));
                    }
                    DataModel temp = ereignisse.get(key);
                    // Die Basisdaten für den Datensatz schreiben
                    temp.setEreignisDatum(myDate);
                    try { // falls sie vorhanden sind
                        temp.setPatientenID(stammdaten.getId());
                        temp.setDatenquelle("Ordner " + stammdaten.getId());
                        temp.setGeburtsdatum(LocalDate.parse(stammdaten.getGeburtsdatum(), DATE_FORMATTER));
                        temp.setGeschlecht(DataModel.Geschlecht.valueOf(stammdaten.getGeschlecht()));
                    } catch (Exception e) {
                        System.out.println("fehler beim parsen der stammdaten. Stammdaten: ID: " + stammdaten.getId() + " Geb.datum: " + stammdaten.getGeburtsdatum() + " Geschlecht: " + stammdaten.getGeschlecht());
                    }
                    temp.setAugenID(key);
                    switch (op.getDiag()) {
                        case "AMD":
                            temp.setHauptDiagnose(HauptDiagnosen.AMD_ALLGEMEIN);
                            break;
                        case "ZVV":
                        case "VAV":
                            temp.setHauptDiagnose(HauptDiagnosen.GEFAESSVERSCHLUSS);
                            break;
                        case "DMÖ":
                            //System.out.println("5.1a");
                            temp.setHauptDiagnose(HauptDiagnosen.DMOE);
                            break;
                        default:
                            temp.setDiagnoseID(temp.getDiagnoseID().isEmpty() ? op.getDiag() : temp.getDiagnoseID() + ", " + op.getDiag());
                    } // switch lane

                    temp.setOp_bericht_extract_Diagnose(op.getDiag());
                    temp.setDiagnoseQuelle(temp.getDiagnoseQuelle().isEmpty() ? "op_bericht_extract" : temp.getDiagnoseQuelle() + ", " + "op_bericht_extract");
                    temp.setSpritzenQuelle(temp.getSpritzenQuelle().isEmpty() ? "op_bericht_extract" : temp.getSpritzenQuelle() + ", " + "op_bericht_extract");
                    AntiVEGFMedikament med = DataModel.convertToAntiVEGFMedikament(op.getMedi());
//                    System.out.println("op: " + op.getMedi() + " med: " + med);
                    temp.setDiskrepanz((med != temp.getMedikament()) && (temp.getMedikament() != AntiVEGFMedikament.E));
                    temp.setMedikament(DataModel.convertToAntiVEGFMedikament(op.getMedi()));

                }); // forEach key
                //System.out.println("6");
//                String[] myLines = temp.split(";"); // Trennungszeichen, separiert die Attribute
//                String fehlerText = ereignisse.get(myDate).setAll(jsonToModelMap, myLines, "Folder " + stammdaten.getId(), -1000);
//                System.out.println("Erfolg -- start: " + argos_tl.start + " myDate: " + myDate);
            } catch (Exception e) {
                System.out.println("Fehler in op_bericht_extractData: " + e.getLocalizedMessage() + "Patient: " + stammdaten.getId() + " Auge: " + op.getAuge() + " Datum: " + op.getDatum() + " Diag: " + op.getDiag() + " Medi: " + op.getMedi());
                ereignisFehler.set(ereignisFehler.get() + 1);
            } // Exception

        });

        if (ereignisFehler.get() > 0) {
            VA_topos.ladeText.set(VA_topos.ladeText.get() + " op_bericht_extract.json: " + ereignisFehler.get() + " / " + ereignisZaehler.get());
        }
        //System.out.println("Patient: " + stammdaten.getId() + " Anzahl ereignisse in argos_tl: " + ereignisse.size());
        List<DataModel> returnList = new ArrayList();

        ereignisse.forEach(
                (key, value) -> {
                    returnList.add(value);
                }
        );

        return returnList;
    }

    private List<DataModel> pdv_verschluesselungenMapper(List<DataModel> op_bericht_extractData, JsonStammdaten stammdaten, List<JsonPdv_verschluesselung> pdv_verschluesselungen) {

        final String DATE_FORMAT = "yyyy-MM-dd";
        final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
        final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
        final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);

        // Erstelle eine HashMap, mit dem Datum & der Lateralitaet als Schluessel und einem Datensatz als value
        HashMap<String, DataModel> ereignisse = new HashMap();

        op_bericht_extractData.forEach((datum) -> {
            ereignisse.put(datum.getAugenID(), datum);
        });

        IntegerProperty ereignisZaehler = new SimpleIntegerProperty();
        IntegerProperty ereignisFehler = new SimpleIntegerProperty(0);
        if (pdv_verschluesselungen != null) {
            ereignisZaehler.set(pdv_verschluesselungen.size());

            pdv_verschluesselungen.forEach((pdv) -> {
                try {

                    LocalDate myDate = LocalDate.parse(pdv.getDatum(), DATE_TIME_FORMATTER);

                    List<String> keys = new ArrayList();

                    switch (pdv.getLoc()) {

                        case "R":
                            keys.add("_r_" + myDate);
                            break;

                        case "L":
                            keys.add("_l_" + myDate);
                            break;

                        case "B":
                        case "":
                            keys.add("_r_" + myDate);
                            keys.add("_l_" + myDate);
                            break;
                        default:
                            System.out.println("nicht interpretiert: " + pdv.getLoc());
                            ereignisFehler.set(ereignisFehler.get() + 1);
                    } // switch lane

                    keys.forEach(key -> {

                        // Lateralitaet zwischenspeichern
                        DataModel.AugeLateralitaet lat
                                = key.startsWith("_l")
                                ? DataModel.AugeLateralitaet.LEFT
                                : DataModel.AugeLateralitaet.RIGHT;

                        if (!ereignisse.containsKey(key)) {

                            ereignisse.put(key, new DataModel());
                            ereignisse.get(key).initializeAll(ereignisse.get(key));
                        }
                        DataModel temp = ereignisse.get(key);

                        // Die Basisdaten für den Datensatz schreiben
                        temp.setDatenquelle("Ordner " + stammdaten.getId());
                        temp.setGeburtsdatum(LocalDate.parse(stammdaten.getGeburtsdatum(), DATE_FORMATTER));
                        temp.setGeschlecht(DataModel.Geschlecht.valueOf(stammdaten.getGeschlecht()));
                        temp.setPatientenID(stammdaten.getId());
                        temp.setAugenID(key);
                        temp.setPdv_verschluesselungen_Diagnose(pdv.getCode());
                        // Initialisierung, dass es sich zunächst weder um eine Spritze, noch um eine Untersuchung handelt
                        //System.out.println(pdv.getCode());
                        switch (pdv.getCode()) {
                            case "5-156.9":
                            case "5-156.9x":
                                temp.setAchse(temp.getAchse() + 1);
                                temp.setEreignisDatum(myDate);
                                temp.setUntersuchungLateralitaet(lat);
                                temp.setSpritzenQuelle(temp.getSpritzenQuelle().isEmpty() ? "pdv_verschluesselung" : temp.getSpritzenQuelle() + ", " + "pdv_verschluesselung");
                                break;
                            case "0-854.1": //	ambulante Chemotherapie
                                temp.setAchse(temp.getAchse() + 1);
                                temp.setEreignisDatum(myDate);
                                temp.setArgosEreignis(true); //System.out.println("Ambulante Chemo");
                                temp.setArgosEreignisRelevanz(true);
                                temp.setArgosEreignisText("Ambulante Chemo");
                                break;
//0-300.7	Sonographie der Mammae
//0-910.1	Aufklärung im Funktionsbereich
//0-302.2	Duplexsonographie der Halsgefäße
//	
//0-100.3	Blutabnahme im Funktionsbereich
//	
//0-110.1	körperliche Untersuchung im Funktionsbereich
//0-122.3	Autorefratormeter, Skiaskopie, Objektive Refraktion
//0-300.40	Transthorakale Echokardiographie in Ruhe
//0-301.00	Eindimensionale Dopplersonographie der Hirngefäße ohne funktionelle Testung
//0-122.1	Applanationstonometrie, Tonometrie einfache Messung
//0-903	Durchführung einer IVOM-Nachsorge intern
//0-854	Applikation von Chemotherapeutika, oral
//0-122.91	Funduskopie, Spiegelung des Augenhintergrundes
//0-999.2	Behandlung Sehbehinderter Augenklinik Ambulanz
//0-855	Erstvorstellung Onkologiepatient
//0-110.0	Anamnese im Funktionsbereich
//0-122.90	Fundusfotografie
//0-302.30	TTE mit Duplexsonographie in Ruhe
//0-812.0	Stomatherapie
//0-126.0	EKG
                            case "0-902": //Vergabe eines Nachsorgeauftrages an externe
                                //System.out.println(" ---------------------------------- extern");
                                break;
//0-131.0	digital rektale Untersuchung
//	
//0-122.22	subjektive Refraktion: sphärische Gläser
//0-302.0	Duplexsonographie der Hirngefäße
//0-301.4	Eindimensionale Dopplersonographie der Halsgefäße
//0-854.0	Applikation von Chemotherapeutika, oral
//0-910.0	Beratung im Funktionsbereich
//0-100.0	Abstrichentnahme im Funktionsbereich
                            case "H35.3":
                                break;
                            default:
                                String preFix = temp.getNebenDiagnose().isEmpty() ? "" : temp.getNebenDiagnose() + ", ";
                                temp.setNebenDiagnose(preFix + VA_topos.codeTranslator.getOrDefault(pdv.getCode(), "code: " + pdv.getCode()));
                            //System.out.println(ICD10List.getValueFromKey(VA_topos.observableICD10Map,pdv.getCode()));
                            //System.out.println(VA_topos.codeTranslator.getOrDefault(pdv.getCode().substring(0,pdv.getCode().indexOf(".")-1), "code: " + pdv.getCode()));
                        } // switch lane
                    }); // keys.forEach

//                String[] myLines = temp.split(";"); // Trennungszeichen, separiert die Attribute
//                String fehlerText = ereignisse.get(myDate).setAll(jsonToModelMap, myLines, "Folder " + stammdaten.getId(), -1000);
//                System.out.println("Erfolg -- start: " + argos_tl.start + " myDate: " + myDate);
                } catch (Exception e) {
                    System.out.println("Fehler in pdv_verschluesselung: " + e.getLocalizedMessage() + " Code: " + pdv.getCode() + " Datum: " + pdv.getDatum() + " Loc: " + pdv.getLoc());
                } // Exception
//
//
            });
        }

        if (ereignisFehler.get() > 0) {
            VA_topos.ladeText.set(VA_topos.ladeText.get() + " pdv_verschluesselungen.json: " + ereignisFehler.get() + " / " + ereignisZaehler.get());
        }

        ereignisse.forEach((key, value) -> {
            if (op_bericht_extractData.contains(value)) {
                op_bericht_extractData.remove(value);
            }
            op_bericht_extractData.add(value);
        });

        return op_bericht_extractData;
    }

    /**
     * Die Funktion soll die aus den Briefen strukturiert extrahierten Daten in
     * Ereignisse umwandeln und in die Ereignisstruktur überführen
     *
     * @param allData // ereignisse aus der op_bericht_extract.json
     * @param stammdaten // die stammdateninformationen
     * @param pdv_verschluesselungen // ereignisse aus den
     * pdv_verschluesselungen
     * @param textMining_letter_extracts // Patientendaten aus den Briefen
     */
    private List<DataModel> textMining_letter_extractMapper(List<DataModel> allData, JsonStammdaten stammdaten, List<JsonPdv_verschluesselung> pdv_verschluesselungen, HashMap<LocalDate, JsonTextMining_letter_extract> textMining_letter_extracts) {

        IntegerProperty ereignisZaehler = new SimpleIntegerProperty(0); // Number of found incidents
        IntegerProperty ereignisFehler = new SimpleIntegerProperty(0); // number of erroneous incidents
        LocalTime before = LocalTime.now(); // indicator for the time consumed by this function
        final String DATE_FORMAT = "yyyy-MM-dd";
        final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

        HashMap<String, DataModel> ereignisse = new HashMap();

        allData.forEach((datum) -> {
            ereignisse.put(datum.getAugenID(), datum);
        });
        // for each extracted letter go through the data, extract the information and store it in the incident structure
        textMining_letter_extracts.forEach((letterDate, textMining_letter_extract) -> {

            // initialize incidents for right and left eye
            ObjectProperty<DataModel> rightTemp = new SimpleObjectProperty(new DataModel());
            ObjectProperty<DataModel> leftTemp = new SimpleObjectProperty(new DataModel());

            if (ereignisse.containsKey("_r_" + letterDate)) {
                rightTemp.set(ereignisse.get("_r_" + letterDate));
                //if (rightTemp.get().getEreignisDatum().isEqual(LocalDate.MIN)) System.out.println("Righttemp vorhanden. AugenID: " + rightTemp.get().getAugenID());
                //System.out.println(ereignisse.get("_r_" + letterDate).getEreignisDatum());
            } else {
                rightTemp.get().initializeAll();
                rightTemp.get().setUntersuchungLateralitaet(DataModel.AugeLateralitaet.RIGHT);
                //if (rightTemp.get().getEreignisDatum().isEqual(LocalDate.MIN)) System.out.println("Righttemp NICHT vorhanden. AugenID: " + rightTemp.get().getAugenID());

            }
            rightTemp.get().setPatientenID(stammdaten.getId());
            rightTemp.get().setAugenID("_r_" + letterDate);
            rightTemp.get().setDatenquelle("Ordner " + stammdaten.getId());
            rightTemp.get().setGeburtsdatum(LocalDate.parse(stammdaten.getGeburtsdatum(), FORMATTER));
            rightTemp.get().setGeschlecht(DataModel.Geschlecht.valueOf(stammdaten.getGeschlecht()));
            rightTemp.get().setEreignisDatum(letterDate);

            if (ereignisse.containsKey("_l_" + letterDate)) {
                leftTemp.set(ereignisse.get("_l_" + letterDate));
            } else {
                leftTemp.get().initializeAll();
                leftTemp.get().setUntersuchungLateralitaet(DataModel.AugeLateralitaet.LEFT);
            }
            leftTemp.get().setPatientenID(stammdaten.getId());
            leftTemp.get().setAugenID("_l_" + letterDate);
            leftTemp.get().setDatenquelle("Ordner " + stammdaten.getId());
            leftTemp.get().setGeburtsdatum(LocalDate.parse(stammdaten.getGeburtsdatum(), FORMATTER));
            leftTemp.get().setGeschlecht(DataModel.Geschlecht.valueOf(stammdaten.getGeschlecht()));
            leftTemp.get().setEreignisDatum(letterDate);

            //System.out.println("######################### Beginn von letterExtract: " + letterDate);
            textMining_letter_extract.getAnnotationDtos().forEach(extract -> {

                switch (extract.getType()) {

                    case "de.averbis.types.health.VisualAcuity":
                        if (extract.getRightEye() != null) {
                            switch (extract.getRightEye().getType()) {
                                case "de.averbis.types.health.VisualAcuityValue":
                                    rightTemp.get().setRohWertFromLetterExtract(extract.getRightEye().getFact());
//                                    rightTemp.get().parseVisusWert(rightTemp.get().getRohWertFromLetterExtract());
                                    rightTemp.get().setVisuswerteFromLetterExtract(rightTemp.get().convertRohWertToVisusWert(extract.getRightEye().getFact()));
                                    rightTemp.get().setDezimalVisusWertFromLetterExtract(rightTemp.get().calculateDezimalVisusWert(rightTemp.get().getVisuswerteFromLetterExtract(), rightTemp.get().getRohWertFromLetterExtract()));
                                    rightTemp.get().setLogMarVisusWertFromLetterExtract(rightTemp.get().calculateLogMarVisusWert(LOGMAR_MAXIMUM, rightTemp.get().getDezimalVisusWertFromLetterExtract()));
                                    ereignisse.put(rightTemp.get().getAugenID(), rightTemp.get());
                                    break;
                            }
                        }
                        if (extract.getLeftEye() != null) {
                            switch (extract.getLeftEye().getType()) {
                                case "de.averbis.types.health.VisualAcuityValue":
                                    leftTemp.get().setRohWertFromLetterExtract(extract.getLeftEye().getFact());
//                                    leftTemp.get().parseVisusWert(leftTemp.get().getRohWertFromLetterExtract());
                                    leftTemp.get().setVisuswerteFromLetterExtract(leftTemp.get().convertRohWertToVisusWert(extract.getLeftEye().getFact()));
                                    leftTemp.get().setDezimalVisusWertFromLetterExtract(leftTemp.get().calculateDezimalVisusWert(leftTemp.get().getVisuswerteFromLetterExtract(), leftTemp.get().getRohWertFromLetterExtract()));
                                    leftTemp.get().setLogMarVisusWertFromLetterExtract(leftTemp.get().calculateLogMarVisusWert(LOGMAR_MAXIMUM, leftTemp.get().getDezimalVisusWertFromLetterExtract()));
                                    if (leftTemp.get().getVisuswerte() == Visuswerte.UNKNOWN) {
                                        System.out.println("Start: >" + extract.getLeftEye().getFact() + "< Ende");
                                    } else {

                                    }
                                    //System.out.println(leftTemp.get().getRohWert());
                                    ereignisse.put(leftTemp.get().getAugenID(), leftTemp.get());
                                    break;
                            }
                        }
                        break;

                    case "de.averbis.types.health.Tensio":
                        if (extract.getRightEye() != null) {
                            switch (extract.getRightEye().getType()) {
                                case "de.averbis.types.health.Measurement":
                                    rightTemp.get().setAugendruckFromLetterExtract(extract.getRightEye().getValue());
                                    ereignisse.put(rightTemp.get().getAugenID(), rightTemp.get());
                                    break;
                            }
                        }
                        if (extract.getLeftEye() != null) {
                            switch (extract.getLeftEye().getType()) {
                                case "de.averbis.types.health.Measurement":
                                    leftTemp.get().setAugendruckFromLetterExtract(extract.getLeftEye().getValue());
                                    ereignisse.put(leftTemp.get().getAugenID(), leftTemp.get());
                                    break;
                            }
                        }
                        break;

                    case "de.averbis.types.health.Diagnosis":
                    case "de.averbis.types.health.Ophthalmology":
                        if (extract.getSide() != null && !extract.getSide().isEmpty()) {
                            //System.out.println(extract.getDictCanon());
                            HauptDiagnosen haupt = DataModel.convertToHauptDiagnosen(extract.getDictCanon());
                            if (haupt != HauptDiagnosen.E && haupt != HauptDiagnosen.UNKNOWN) {
                                if (extract.getSide().toUpperCase().equals("RIGHT")) {
                                    rightTemp.get().setHauptDiagnoseFromLetterExtract(haupt);
                                    ereignisse.put(rightTemp.get().getAugenID(), rightTemp.get());
                                }
                                if (extract.getSide().toUpperCase().equals("LEFT")) {
                                    leftTemp.get().setHauptDiagnoseFromLetterExtract(haupt);
                                    ereignisse.put(leftTemp.get().getAugenID(), leftTemp.get());
                                }
                            }
                            //System.out.println(extract.getDictCanon());
                            if (extract.getDictCanon().toLowerCase().contains("makulablutung") && extract.getNegatedBy() == null) {
                                //System.out.println(extract.getNegatedBy());
                                //System.out.println(letterDate + " " + extract.getDictCanon());
                                if (extract.getSide().toUpperCase().equals("RIGHT")) {
                                    rightTemp.get().setArgosEreignis(true);
                                    rightTemp.get().setArgosEreignisText(extract.getDictCanon());
                                    //System.out.println("Right " + letterDate);
                                    ereignisse.put(rightTemp.get().getAugenID(), rightTemp.get());
                                }
                                if (extract.getSide().toUpperCase().equals("LEFT")) {
                                    leftTemp.get().setArgosEreignis(true);
                                    leftTemp.get().setArgosEreignisText(extract.getDictCanon());
                                    //System.out.println("Left " + letterDate);
                                    ereignisse.put(leftTemp.get().getAugenID(), leftTemp.get());
                                }
                            }
                        }
                        break;

                    default:
                    // System.out.println(anno.getType());
                }

                //System.out.println("RightEye: " + anno.getRightEye());
                //System.out.println("LeftEye: " + anno.getLeftEye());
                if (extract.getLaterality() != null) {
                    switch (extract.getLaterality()) {
                        default:
                        //System.out.println("\n+++++++++++++++++++++++++++\n" + anno.getLaterality());
                        //System.out.println("\n" + anno.getCoveredText());
                    }
                } else { // anno.getlaterality isNull
                    //System.out.println("\n--------------------------------\n" + anno.getCoveredText());

                }
                //System.out.println(anno.getCoveredText());

            });
        });

        ereignisse.forEach((key, value) -> {
            if (allData.contains(value)) {
                allData.remove(value);
            }
            allData.add(value);
        });

//        VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n Patient: " + stammdaten.getId() + " Anzahl Ereignisse in pdv_verschluesselungen.json: " + ereignisZaehler.get() + " Anzahl nicht interpretierter Ereignisse: " + ereignisFehler.get());
        //VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n Zeitbedarf für die Briefextrakte: " + (double) (Duration.between(before, LocalTime.now()).toMillis() / 1000.0));
        return allData;
    }
}
