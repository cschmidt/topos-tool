/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.color;

import javafx.scene.paint.Color;

/**
 *
 * @author lokal-admin
 */
public interface ColorPalette {

    public ColorPaletteType getPaletteType();

    public int getMaximumColorCount();

    public boolean hasNrOfClasses(int numberOfClasses);

    public Color[] getColors(int numberOfClasses);

    public Color[] getMaximumColors();

}
