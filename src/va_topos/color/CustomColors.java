/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.color;

import javafx.scene.paint.Color;

/**
 *
 * @author lokal-admin
 */
public class CustomColors {

    public static final Color STD_BORDERCOLOR = Color.BLACK;
    public static final Color ANNO_BORDERCOLOR = Color.RED;
    public static final Color STD_FILLCOLOR = Color.WHITE;    

    public static final int[] RGB_RED_VALUES = {228, 55, 77, 152, 255, 255}; //d / 255d;
    public static final int[] RGB_GREEN_VALUES = {26, 126, 175, 78, 127, 255}; //d / 255d;
    public static final int[] RGB_BLUE_VALUES = {28, 184, 74, 163, 0, 51}; //d / 255d;

    public static final int[] RGB_STDBLACK = {0, 0, 0};
    public static final int[] RGB_STDGREY = {103, 103, 103};
    public static final int[] RGB_STDWHITE = {255, 255, 255};
    public static final int[] RGB_STDBLUE = {103, 169, 207};
    public static final int[] RGB_STDRED = {239, 138, 98};
    public static final int[] RGB_STDNODECOLOR = {240, 240, 240};
    
    public static final int HSB_STDBLUE = 210;
    public static final int HSB_STDORANGE = 10;
    
}
