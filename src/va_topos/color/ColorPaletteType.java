/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.color;

/**
 *
 * @author Christoph Schmidt
 */
public enum ColorPaletteType {

    SEQUENTIAL,
    DIVERGING,
    QUALITATIVE

}
