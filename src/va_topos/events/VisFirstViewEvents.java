/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.events;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.shape.Circle;
import static va_topos.ui.MainWindowBuilder.spareFive;
import static va_topos.ui.MainWindowBuilder.spareFour;
import static va_topos.ui.MainWindowBuilder.spareOne;
import static va_topos.ui.MainWindowBuilder.spareSeven;
import static va_topos.ui.MainWindowBuilder.spareSix;
import static va_topos.ui.MainWindowBuilder.spareThree;
import static va_topos.ui.MainWindowBuilder.spareTwo;
import static va_topos.ui.SubWindowsBuilder.letterTabPane;
import va_topos.VA_topos;
import static va_topos.VA_topos.observableData;
import va_topos.annotationHandling.AnnoBuffer;
import va_topos.annotationHandling.DrawInfo;
import va_topos.annotationHandling.DrawingOperations;
import va_topos.data.DataModel;
import va_topos.data.JsonAnnotation;
import va_topos.dataHandling.AnnotationToJsonParser;
import va_topos.dataHandling.Datenladen;
import va_topos.ui.DialogPanelBuilder;
import va_topos.ui.MainWindowBuilder;
import va_topos.ui.MainWindowBuilder.DIALOGPANEL;
import static va_topos.ui.MainWindowBuilder.DIALOGPANEL.ANNOPANEL;
import static va_topos.ui.MainWindowBuilder.DIALOGPANEL.CLEANSINGPANEL;
import static va_topos.ui.MainWindowBuilder.cleansingCurrentDate;
import static va_topos.ui.MainWindowBuilder.cleansingCurrentDateLabel;
import static va_topos.ui.MainWindowBuilder.cleansingDataAttributField;
import static va_topos.ui.MainWindowBuilder.cleansingDataAttributFieldLabel;
import static va_topos.ui.MainWindowBuilder.cleansingDataTypeField;
import static va_topos.ui.MainWindowBuilder.cleansingDataTypeFieldLabel;
import static va_topos.ui.MainWindowBuilder.cleansingDateField;
import static va_topos.ui.MainWindowBuilder.cleansingDateFieldLabel;
import static va_topos.ui.MainWindowBuilder.cleansingLaterality;
import static va_topos.ui.MainWindowBuilder.cleansingLateralityLabel;
import static va_topos.ui.MainWindowBuilder.cleansingPatient;
import static va_topos.ui.MainWindowBuilder.cleansingPatientLabel;
import static va_topos.ui.MainWindowBuilder.cleansingUser;
import static va_topos.ui.MainWindowBuilder.cleansingUserLabel;
import static va_topos.ui.MainWindowBuilder.cleansingValueField;
import static va_topos.ui.MainWindowBuilder.cleansingValueFieldLabel;
import static va_topos.ui.MainWindowBuilder.scenarioComboBox;
import static va_topos.ui.MainWindowBuilder.spareEight;
import static va_topos.ui.MainWindowBuilder.spareNine;
import static va_topos.ui.MainWindowBuilder.summaryAnchorPane;
import static va_topos.ui.MainWindowBuilder.summaryVisusCanvas;
import static va_topos.ui.SubWindowsBuilder.dialogPanelStage;
import static va_topos.ui.SubWindowsBuilder.doctoralLetterStage;
import va_topos.visualization.OrderPolygon;
import va_topos.visualization.VisusFirstMaker;

/**
 *
 * @author lokal-admin
 */
public class VisFirstViewEvents {

    // MakeVisusFirst --> auf der Zeichenfläche das Mausrad bewegt
    public static void onVisViewScrolled(ScrollEvent e) {

        // If the ctrl key is NOT pressed -> zoom in/out
        if (e.isControlDown()) {
            if (e.getDeltaY() > 0) { // Zoom in
                if (MainWindowBuilder.zoomSlider.getValue() < VisusFirstMaker.MAX_ZOOM_FACTOR) {
                    MainWindowBuilder.zoomSlider.setValue(MainWindowBuilder.zoomSlider.getValue() + VisusFirstMaker.growZoomFactor.get());
                }
            } else { // Zoom out
                if (MainWindowBuilder.zoomSlider.getValue() > VisusFirstMaker.MIN_ZOOM_FACTOR) {
                    MainWindowBuilder.zoomSlider.setValue(MainWindowBuilder.zoomSlider.getValue() - VisusFirstMaker.growZoomFactor.get());
                }
            }
        } else { // if the ctrl key is pressed -> scroll the whole view
            if (e.getDeltaY() < 0) { // Scroll up
                VisusFirstMaker.scrollPosition.set(VisusFirstMaker.scrollPosition.get() - 2); //(VA_topos.sortedFilteredData.size() / 200));
            } else { // Scroll down
                VisusFirstMaker.scrollPosition.set(VisusFirstMaker.scrollPosition.get() + 2); // (VA_topos.sortedFilteredData.size() / 200 * ));
            }
        }
    }

    // MakeVisusFirst --> auf der Zeichenfläche die Maustaste gedrückt und die Maus bewegt (gezogen).
    public static void onVisMouseDragged(MouseEvent e) {
        if (e.isPrimaryButtonDown()) {
            if (MainWindowBuilder.visusFirstAnnotationAnchorPane.getChildren().size() > 1) {
                try {
                    AnnoBuffer annBuff = VA_topos.drawBuffer.get(VA_topos.currentAnnoIndex.get() - 1);
                    ObservableList<DrawInfo> drawItems = annBuff.getDrawInfos();

                    DrawInfo currentAnno = drawItems.get(annBuff.getDrawIndex());
                    currentAnno.setWidth(Math.abs(e.getX() - currentAnno.getStartX()));
                    currentAnno.setHeight(Math.abs(e.getY() - currentAnno.getStartY()));
//                    currentAnno.setStartX(Math.min(currentAnno.getStartX(), e.getX()));
//                    currentAnno.setStartY(Math.min(currentAnno.getStartY(), e.getY()));
                    annBuff.getCanvas().getGraphicsContext2D().clearRect(0d, 0d, annBuff.getCanvas().getWidth(), annBuff.getCanvas().getHeight());
                    DrawingOperations drawOps = new DrawingOperations(annBuff);
                    boolean Xnegative = (e.getX() - currentAnno.getStartX()) < 0;
                    boolean Ynegative = (e.getY() - currentAnno.getStartY()) < 0;
                    for (int i = 0; i <= annBuff.getDrawIndex() - 1; i++) {
                        drawOps.draw(annBuff.getCanvas(), drawItems.get(i), false, false);
                    }
                    drawOps.draw(annBuff.getCanvas(), drawItems.get(drawItems.size() - 1), Xnegative, Ynegative);
                } catch (ClassCastException er) {
                }
            }

        } else {
        }
    }

    // procedure to be called, when the mouse is pressed on the visualization canvas
    public static void onVisMousePressed(MouseEvent e) {
        // if the right mouse button is pressed -> start the dragging
        if (e.getButton() == MouseButton.SECONDARY && e.isDragDetect()) {
            // if the button is pressed and the mouse is dragged
            ((MainWindowBuilder.DotGrid) e.getSource()).setOnMouseDragged(dragEvent -> {
                // take every child
                ((MainWindowBuilder.DotGrid) e.getSource()).getChildren().forEach(myPane -> {
                    if (!(myPane instanceof Canvas)) {
                        final int xTransValue = ((dragEvent.getX() - e.getX()) > 0) ? 1 : -1;
                        myPane.setTranslateX(myPane.getTranslateX() + xTransValue);
                        final int yTransValue = ((dragEvent.getY() - e.getY()) > 0) ? 1 : -1;
                        myPane.setTranslateY(myPane.getTranslateY() + yTransValue);
                    }
                });
            });
        }
    }

//    public static void onDrawObjectMousePressed(Node drawObject, double deltaX, double deltaY) {
//        drawObject.setOnMouseDragged(e -> {
//            if (e.getButton() == MouseButton.PRIMARY) {
//                drawObject.setTranslateX(drawObject.getTranslateX() + e.getX() - deltaX);
//                drawObject.setTranslateY(drawObject.getTranslateY() + e.getY() - deltaY);
//            } else {
//
//            }
//        });
//    }

    /**
     * Function to be called, if a mouse button was released on a drawing object
     *
     * @param e the event object
     * @param ereignis the current incident -> this is used to either show the
     * correct letter or remove the correct drawing objects from the view
     */
    public static void onDrawObjectMouseReleased(MouseEvent e, DataModel ereignis) {

        if (ereignis == null) {
            System.out.println(e.getPickResult());
            openCommentwindow(CLEANSINGPANEL, e);
            e.consume();
            return;
        }
        if (e.isDragDetect() && (e.getButton() == MouseButton.PRIMARY)) {
            TextArea letterTextField = new TextArea();
            MainWindowBuilder.setAnchor(letterTextField, 0, 0, 0, 0);
            Tab letterTab = new Tab(ereignis.getPatientenID() + " Lateralität: " + ereignis.getUntersuchungLateralitaet() + " Datum: " + ereignis.getEreignisDatum());
            letterTab.setContent(letterTextField);
            letterTabPane.getTabs().add(0, letterTab);
            letterTabPane.getSelectionModel().select(letterTab);
            letterTextField.setText(Datenladen.loadLetter(ereignis.getPatientenID(), ereignis.getEreignisDatum()));
            doctoralLetterStage.show();
            doctoralLetterStage.requestFocus();
            e.consume();
        }

        // If the right mousebutton is pressed and not dragged
        if (e.isDragDetect() && (e.getButton() == MouseButton.SECONDARY)) {

            switch (scenarioComboBox.getValue().toString()) {
                case "Datenbereinigung":
                    ereignis.setCleansed(!ereignis.isCleansed());
                    JsonAnnotation anno = new JsonAnnotation();
                    anno.setAnnoDate(LocalDate.now().toString());
                    anno.setAnnoField("ereignis.isCleansed");
                    anno.setComment("removed ereignis from visualization");
                    anno.setConfirmations("0");
                    anno.setLateralitaet(ereignis.getUntersuchungLateralitaet().toString());
                    anno.setNewValue("" + ereignis.isCleansed());
                    anno.setOldValue("" + !ereignis.isCleansed());
                    anno.setPatientID(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet());
                    anno.setPatientNo(ereignis.getPatientenID());
                    anno.setUser(va_topos.VA_topos.USER_NAME.get());
                    AnnotationToJsonParser.parseAnnotation(anno);
                    //va_topos.VA_topos.observablePatients.get(ereignis.getPatientenID() + "_" ereignis.getUntersuchungLateralitaet())
                    e.consume();
                    break;
                default:
                    ereignis.setAnnotated(!ereignis.isAnnotated());
                    openCommentwindow(ANNOPANEL, e);
                    e.consume();
            }
            System.out.println("Just annotated ereignis: " + ereignis.getPatientenID() + ereignis.getUntersuchungLateralitaet() + " Datum: " + ereignis.getEreignisDatum() + " Result: " + ereignis.isAnnotated());
            e.consume();
            /* Temporary commented to support other ation on right mouse button
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Bestätigung");
            alert.setHeaderText("Einen Patienten aus der Visualisierung entfernen.");
            alert.setContentText("Wollen Sie den Patienten aus der Visualisierung entfernen?");
            alert.setX(e.getScreenX());
            alert.setY(e.getScreenY());

            
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                tableFilterTreeView.getRoot().getChildren().forEach((child) -> {
                    //System.out.println(((TreeItem) child).getValue().toString());
                    if (((TreeItem) child).getValue().toString().equals("patientenID")) {
                        ((TreeItem) child).getChildren().forEach(patient -> {
                            //System.out.println(patient.getClass());
                            //System.out.println(((CheckBoxTreeItem) patient).getValue().toString().substring(0, ((CheckBoxTreeItem) patient).getValue().toString().indexOf(" ")));
                            if (((CheckBoxTreeItem) patient).getValue().toString().substring(0, ((CheckBoxTreeItem) patient).getValue().toString().indexOf(" ")).equals(ereignis.getPatientenID())) {
                                ((CheckBoxTreeItem) patient).setSelected(false);
                            }
                        });
                        //System.out.println(((TreeItem) child).getValue().toString());
                    }
                });
            }
             */
        }

    }

    public static void onDrawObjectMouseEntered(MouseEvent e, String captions[], String[] details) {
        onDrawObjectMouseEntered(e, captions, details, null);
    }

    /**
     * Function to set the detail information on the detail window, when the
     * mouse hovers over a drawing object
     *
     * @param e - the MouseEvent object
     * @param captions captions is a string array, that contains the name of the
     * parameters, for which the values are shown
     * @param details the value content for each dimension
     * @param summaryHighlight three keys, that indicate, which circles have to
     * be highlighted in the summary visualization
     */
    public static void onDrawObjectMouseEntered(MouseEvent e, String captions[], String[] details, String[] summaryHighlight) {

        // If the number of captions is not equal to the number of information
        if (captions.length != details.length) {
            System.out.println("Fehler in VisFirstViewEvents.onDrawObjectMouseEntered: die anzahl der Felder (" + captions.length + ") stimmt nicht mit der Anzahl der Inhalte (" + details.length + ") überein.");
            return;
        }
        if ((summaryHighlight != null) && (summaryHighlight.length == 4)) {
            try {
                List<HashMap<String, Circle>> myList = ((List<HashMap<String, Circle>>) summaryVisusCanvas.getUserData());
                if (myList == null) {
                    System.out.println("no list no fun");
                } else {
                    for (int i = 0; i < 4; i++) {
                        Circle circ = myList.get(i).get(summaryHighlight[i]);
                        Circle highCirc = ((Circle[]) summaryAnchorPane.getUserData())[i];
                        highCirc.setCenterX(circ.getCenterX());
                        highCirc.setCenterY(circ.getCenterY());
                        highCirc.setTranslateX(circ.getTranslateX());
                        highCirc.setTranslateY(circ.getTranslateY());
                        highCirc.setRadius(circ.getRadius());
                        highCirc.setVisible(true);
                    }
                }

            } catch (Exception ex) {
            }
        }

        // Depending on the number of details, only some of the Strings are filled
        switch (details.length) {
            case 9:
                spareNine.textProperty().bind(new SimpleStringProperty(captions[8] + ": " + details[8]));
            case 8:
                spareEight.textProperty().bind(new SimpleStringProperty(captions[7] + ": " + details[7]));
            case 7:
                spareSeven.textProperty().bind(new SimpleStringProperty(captions[6] + ": " + details[6]));
            case 6:
                spareSix.textProperty().bind(new SimpleStringProperty(captions[5] + ": " + details[5]));
            case 5:
                spareFive.textProperty().bind(new SimpleStringProperty(captions[4] + ": " + details[4]));
            case 4:
                spareFour.textProperty().bind(new SimpleStringProperty(captions[3] + ": " + details[3]));
            case 3:
                spareThree.textProperty().bind(new SimpleStringProperty(captions[2] + ": " + details[2]));
            case 2:
                spareTwo.textProperty().bind(new SimpleStringProperty(captions[1] + ": " + details[1]));
            case 1:
                spareOne.textProperty().bind(new SimpleStringProperty(captions[0] + ": " + details[0]));
                break;
            default:
        }
    }

    /**
     * Function to reset the detail information, when a drawing object is not
     * hovered by the mouse any more
     */
    public static void onDrawObjectMouseExited() {
        spareOne.textProperty().bind(new SimpleStringProperty(""));
        spareTwo.textProperty().bind(new SimpleStringProperty(""));
        spareThree.textProperty().bind(new SimpleStringProperty(""));
        spareFour.textProperty().bind(new SimpleStringProperty(""));
        spareFive.textProperty().bind(new SimpleStringProperty(""));
        spareSix.textProperty().bind(new SimpleStringProperty(""));
        spareSeven.textProperty().bind(new SimpleStringProperty(""));
        spareEight.textProperty().bind(new SimpleStringProperty(""));
        spareNine.textProperty().bind(new SimpleStringProperty(""));
        ((Circle[]) summaryAnchorPane.getUserData())[0].setVisible(false);
        ((Circle[]) summaryAnchorPane.getUserData())[1].setVisible(false);
        ((Circle[]) summaryAnchorPane.getUserData())[2].setVisible(false);
        ((Circle[]) summaryAnchorPane.getUserData())[3].setVisible(false);

    }

    public static void onSuddenMouseClicked(MouseEvent e, DataModel ereignis, OrderPolygon sudden) {
        if (e.getButton() == MouseButton.SECONDARY) {
            ereignis.setArgosEreignisRelevanz(!ereignis.isArgosEreignisRelevanz());
            if (ereignis.isArgosEreignisRelevanz()) {
                VisusFirstMaker.setPolygonStandardDesign(sudden);
            } else {
                VisusFirstMaker.setPolygonCleansedDesign(sudden);
            }
            e.consume();
        }

    }

    // Function to open a commentWindow, where the user can enter a comment, which is stored in the data
    private static void openCommentwindow(DIALOGPANEL panel, MouseEvent e) {
        DialogPanelBuilder.buildDialogPanel(0, panel);
        dialogPanelStage.showAndWait();
        if (panel == DIALOGPANEL.CLEANSINGPANEL) {
            Map<String, String> ereignisData = new HashMap();
            ereignisData.put(cleansingPatientLabel.getText(), cleansingPatient.getText());
            ereignisData.put(cleansingUserLabel.getText(), cleansingUser.getText());
            ereignisData.put(cleansingLateralityLabel.getText(), cleansingLaterality.getText());
            ereignisData.put(cleansingCurrentDateLabel.getText(), cleansingCurrentDate.getText());
            ereignisData.put(cleansingDateFieldLabel.getText(), cleansingDateField.getValue().toString());
            ereignisData.put(cleansingDataAttributFieldLabel.getText(), cleansingDataAttributField.getSelectionModel().getSelectedItem().toString());
            ereignisData.put(cleansingDataTypeFieldLabel.getText(), cleansingDataTypeField.getSelectionModel().getSelectedItem().toString());
            ereignisData.put(cleansingValueFieldLabel.getText(), cleansingValueField.getText());
            DataModel.createSingleEreignis(ereignisData, observableData);
        }
        if (panel == DIALOGPANEL.ANNOPANEL) {
            System.out.println("Test");
            try {
                ((DataModel) e.getPickResult().getIntersectedNode().getUserData()).setAnnotationText(((DataModel) e.getPickResult().getIntersectedNode().getUserData()).getAnnotationText() + "Muhaha");
                ((DataModel) e.getPickResult().getIntersectedNode().getUserData()).setAnnotated(true);
            } catch (Exception ex) {

            }

        }

    }
}
