/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.events;

import va_topos.visualization.VisusFirstMaker;
import va_topos.visualization.StandardVisMaker;
import va_topos.dataHandling.Datenladen;
import va_topos.dataHandling.DatenExportieren;
import java.io.File;
import java.lang.reflect.Field;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.stage.DirectoryChooser;
import va_topos.ui.MainWindowBuilder;

//import va_topos.data.AnnotationModel.FieldClass;
import va_topos.data.DataModel;
import va_topos.annotationHandling.DrawInfo;
import va_topos.annotationHandling.AnnoBuffer;
import va_topos.ui.MainWindowBuilder.DotGrid;
import va_topos.VA_topos;
import static va_topos.VA_topos.observablePatients;

import va_topos.annotationHandling.DrawingOperations;
import va_topos.color.CustomColors;
import va_topos.data.MedicationChange;
import va_topos.data.Patient;
import va_topos.dataHandling.PatientParser;
import static va_topos.ui.MainWindowBuilder.sampleCountSlider;
import static va_topos.ui.MainWindowBuilder.table;
import va_topos.ui.DialogPanelBuilder;
import static va_topos.ui.MainWindowBuilder.DIALOGPANEL.SAMPLEPANEL;
import static va_topos.ui.SubWindowsBuilder.dialogPanelStage;

/**
 *
 * @author varie
 */
@SuppressWarnings("unchecked")
public class EventHandlers {

    // Wenn ein Button zum Laden der Daten geklickt wird
    public void menuLoadClicked(ActionEvent event, int ladeArt) {

        String defaultDir;
        switch (ladeArt) {
            case 0:
            case 1:
                defaultDir = VA_topos.data_directory_custom;
                break;
            case 2:
                defaultDir = VA_topos.SAVE_DIRECTORY_DEFAULT;
                break;
            case 3:
                defaultDir = VA_topos.DATA_DIRECTORY_FULL;
                break;
                
            default:
                defaultDir = VA_topos.data_directory_custom;
        }

        DirectoryChooser dc = new DirectoryChooser();
        File initDir = new File(defaultDir);
        dc.setInitialDirectory(initDir);
        File selectedDirectory = dc.showDialog(null);
        LocalTime gesamtLadeZeit = LocalTime.now();
        VA_topos.observableData.clear();
        VA_topos.observableICD10Map.clear();
        VA_topos.observableOPSList.clear();
        //VA_topos.ladeText.set("");
        // Falls ein Verzeichnis ausgewählt wurde
        if (selectedDirectory != null) {
            // Alle Dateien identifizieren
            List<File> myfiles = Arrays.asList(selectedDirectory.listFiles());

            Datenladen daten = new Datenladen();

            // Die Übersetzungstabelle für die Abrechnungscodes laden
            VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\n OPS-Import: \n");
            LocalTime beginTime = LocalTime.now();
            VA_topos.observableOPSList.addAll(daten.loadOPSList(VA_topos.CONFIG_DIRECTORY_DEFAULT));
            VA_topos.ladeText.set(VA_topos.ladeText.get() + " (Dauer: " + (double) (Duration.between(beginTime, LocalTime.now()).toMillis() / 1000.0) + " Sekunden)");
            // Die Übersetzungstabelle für ICD10 laden
            beginTime = LocalTime.now();
            VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\n ICD10-Import: \n");
            VA_topos.observableICD10Map.putAll(daten.loadICD10List(VA_topos.CONFIG_DIRECTORY_DEFAULT));
            VA_topos.ladeText.set(VA_topos.ladeText.get() + " (Dauer: " + (double) (Duration.between(beginTime, LocalTime.now()).toMillis() / 1000.0) + " Sekunden)");

            // Übersetzung der Codes von Bastian laden
            beginTime = LocalTime.now();
            VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\n Code-Übersetzungen der Augenärzte laden: \n");
            VA_topos.codeTranslator.putAll(daten.loadCodeTranslator(VA_topos.CONFIG_DIRECTORY_DEFAULT));
            VA_topos.ladeText.set(VA_topos.ladeText.get() + " (Dauer: " + (double) (Duration.between(beginTime, LocalTime.now()).toMillis() / 1000.0) + " Sekunden)");

            switch (ladeArt) {
                case 0:
                    // Daten aus evtl. vorhandenen JSon Dateien laden (Voraussetzung: Patientendaten liegen in Ordnern im Datenverzeichnis "\data")
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\n Json-Import: \n");
                    beginTime = LocalTime.now();
                    VA_topos.observableData.addAll(daten.loadDataFromJSon(myfiles));
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "Daten aus den Json-Dateien geladen. (Dauer: " + (double) (Duration.between(beginTime, LocalTime.now()).toMillis() / 1000.0) + " Sekunden)");
                    VA_topos.data_directory_custom = selectedDirectory.getPath();
                    break;
                case 1:
                    // Die Daten aus evtl. vorhandenen CSV Files laden
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\n CSV-Import: \n");
                    beginTime = LocalTime.now();
                    VA_topos.observableData.addAll(daten.loadDataFromCSV(myfiles));
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nDaten aus der CSV-Datei geladen. (Dauer: " + (double) (Duration.between(beginTime, LocalTime.now()).toMillis() / 1000.0) + " Sekunden)");
                    VA_topos.data_directory_custom = selectedDirectory.getPath();
                    break;
                case 2:
                    // Die Daten aus einer zuvor gespeicherten Objektstruktur laden
                    beginTime = LocalTime.now();
                    VA_topos.observableData.addAll(daten.loadDataFromObject(myfiles));
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nDaten aus der Objektstruktur geladen. (Dauer: " + (double) (Duration.between(beginTime, LocalTime.now()).toMillis() / 1000.0) + " Sekunden)");
                    break;
                case 3:
                    // Teildaten aus einem Ordner laden
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\n Json-Import: \n");
                    beginTime = LocalTime.now();
                    List<File> mySampleFiles = sampleFiles(myfiles);
                    VA_topos.observableData.addAll(daten.loadDataFromJSon(mySampleFiles));
                    VA_topos.ladeText.set(VA_topos.ladeText.get() + "Daten aus den Json-Dateien geladen. (Dauer: " + (double) (Duration.between(beginTime, LocalTime.now()).toMillis() / 1000.0) + " Sekunden)");
                    VA_topos.data_directory_custom = selectedDirectory.getPath();
                    break;

                default:
            }
            VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\nGesamtdauer des Ladevorgangs: " + (double) (Duration.between(gesamtLadeZeit, LocalTime.now()).toMillis() / 1000.0) + " Sekunden");
            // Die AnnotationMap initialisieren
            // daten.createAnnoPatients();
//            annotationChanged();
        } // if selectedDirectory not Null
    } // buttonLoadBtnClicked

    public void menuCalculateClicked(ActionEvent event) {
        // Auf den Daten die ersten automatischen Analysen durchführen
        PatientParser analytics = new PatientParser();
        LocalTime beginTime = LocalTime.now();
        //VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nBegin to parse patients: " + LocalTime.now() + "\n");
        List<String> result = analytics.parsePatientDataFromIncidentList(VA_topos.sortedFilteredData, observablePatients);
        VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\nBerechnungsdauer: " + (double) (Duration.between(beginTime, LocalTime.now()).toMillis() / 1000.0) + " Sekunden.");
        result.forEach(line -> VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n" + line));
//        annotationChanged();
//        DoubleProperty minTime = new SimpleDoubleProperty(30000d);
//        DoubleProperty maxTime = new SimpleDoubleProperty(0000d);
//        DoubleProperty minNoVisits = new SimpleDoubleProperty(30000d);
//        DoubleProperty maxNoVisits = new SimpleDoubleProperty(0000d);
//        observablePatients.values().forEach(patient -> {
//            if (minTime.get() > patient.getLengthOfTreatment()) {
//                minTime.set(patient.getLengthOfTreatment());
//            }
//            if (maxTime.get() < patient.getLengthOfTreatment()) {
//                maxTime.set(patient.getLengthOfTreatment());
//            }
//
//            if (minNoVisits.get() > (patient.getNumberOfVisusMeasurements() + patient.getNumberOfInjections())) {
//                minNoVisits.set(patient.getNumberOfVisusMeasurements() + patient.getNumberOfInjections());
//            }
//            if (maxNoVisits.get() < (patient.getNumberOfVisusMeasurements() + patient.getNumberOfInjections())) {
//                maxNoVisits.set(patient.getNumberOfVisusMeasurements() + patient.getNumberOfInjections());
//            }
//        });
//        System.out.println("MinTime: " + minTime.get());
//        System.out.println("MaxTime: " + maxTime.get());
//
//        System.out.println("minNoVisits: " + minNoVisits.get());
//        System.out.println("maxNoVisits: " + maxNoVisits.get());
        dataChanged(null);
    }

    // Sobald auf "Daten als CSV exportieren geklickt wird
    public void menuSaveClicked(ActionEvent event, boolean saveToFile) {
        DatenExportieren export = new DatenExportieren();
        VA_topos.ladeText.set(VA_topos.ladeText.get() + "Beginne Export...\n" + VA_topos.SAVE_DIRECTORY_DEFAULT + "test.csv");
        List<String> result = export.csvExportDataModel(VA_topos.SAVE_DIRECTORY_DEFAULT + "test.csv", VA_topos.observableData, saveToFile);
        if (result.isEmpty()) {
            VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nExport des Datenmodells erfolgreich.");
        } else {
            result.forEach(line -> VA_topos.ladeText.set(VA_topos.ladeText.get() + line));
        }
        VA_topos.ladeText.set(VA_topos.ladeText.get() + "\n\nBeginne Export der Annotationsstruktur");

        // TODO: Support the export of annotations
//        result = export.csvExportGroupDataModel(VA_topos.SAVE_DIRECTORY_DEFAULT + "annotest.csv", VA_topos.groupData.get());
//        if (result.isEmpty()) {
//            VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nExport der Annotationsstruktur erfolgreich.");
//        } else {
//            result.forEach(line -> VA_topos.ladeText.set(VA_topos.ladeText.get() + line));
//        }
//        result = export.csvExportAnnoPatients(VA_topos.SAVE_DIRECTORY_DEFAULT + "annoPatients.csv", VA_topos.groupData.get().getObservableAnnotations());
//        if (result.isEmpty()) {
//            VA_topos.ladeText.set(VA_topos.ladeText.get() + "\nExport der Annotationsstruktur erfolgreich.");
//        } else {
//            result.forEach(line -> VA_topos.ladeText.set(VA_topos.ladeText.get() + line));
//        }
    }

    /**
     * This function exports all elements of VA_topos.sortedFilteredData to an
     * object file
     *
     * @param event
     */
    public void menuSaveObjClicked(ActionEvent event) {
        VA_topos.ladeText.set(VA_topos.ladeText.get() + "Beginne Export der gefilterten Daten...\n");
        DatenExportieren export = new DatenExportieren();
        List<DataModel> exportList = new ArrayList();
        VA_topos.sortedFilteredData.forEach((element) -> exportList.add(element));
        export.objectSave(exportList);
        VA_topos.ladeText.set(VA_topos.ladeText.get() + "Export beendet.\n");

    }

    /**
     * This function exports all patients as a JSON structure
     * @param event 
     */
    public void menuSaveJsonClicked(ActionEvent event) {
        VA_topos.ladeText.set(VA_topos.ladeText.get() + "Beginne Export der Patienten...\n");
        DatenExportieren export = new DatenExportieren();
        List<DataModel> exportList = new ArrayList();
        VA_topos.sortedFilteredData.forEach((element) -> exportList.add(element));
        export.jsonSave(observablePatients);
        VA_topos.ladeText.set(VA_topos.ladeText.get() + "Export beendet.\n");
    }

    /**
     * This function exports all elements of VA_topos.sortedFilteredData to an
     * object file
     *
     * @param event
     */
    public void menuSaveVisChangeClicked(ActionEvent event) {
        VA_topos.ladeText.set("Beginne Export der Visusänderungen nach Medikamentenwechsel...\n");
        DatenExportieren export = new DatenExportieren();
        List<MedicationChange> exportList = new ArrayList();
        VA_topos.observablePatients.values().forEach((element) -> {
            element.getMyMedicationChanges().values().forEach(visChange -> {
                exportList.add(visChange);
            });
        });
        export.csvExportVisChange(VA_topos.SAVE_DIRECTORY_DEFAULT + "VisChange_" + LocalDate.now() + "_" + LocalTime.now().getHour() + "." + LocalTime.now().getMinute() + "." + LocalTime.now().getSecond() + ".csv", exportList);
        VA_topos.ladeText.set(VA_topos.ladeText.get() + "Export beendet.\n");

    }

    // Sobald sich etwas am Datensatz ändert
    public void dataFilterChanged(ListChangeListener.Change change) {
        System.out.println("DataFilter changed.");
        PatientParser analytics = new PatientParser();
        List<String> result = analytics.parsePatientDataFromIncidentList(VA_topos.sortedFilteredData, observablePatients);
        VisusFirstMaker visMaker = new VisusFirstMaker();
        visMaker.calculateDrawMatrix();
        VisusFirstMaker.makeSummaryVis(MainWindowBuilder.summaryVisusCanvas);
//        Patient patientMethods = new Patient();
//        VA_topos.observablePatients = null;
//        VA_topos.observablePatients = FXCollections.observableMap(new HashMap<>());
//        VA_topos.groupData = null;
//        VA_topos.groupData = new SimpleObjectProperty(new GroupDataModel());

//        AnnotationModel analytics = new AnnotationModel();
//        analytics.runAnalyticMethods(VA_topos.sortedFilteredData, VA_topos.groupData);
//        patientMethods.setAll(VA_topos.observablePatients, VA_topos.sortedFilteredData, VA_topos.groupData);
//        System.out.println("Patientenzahl: " + VA_topos.observablePatients.size());
    }

    public void dataChanged(ListChangeListener.Change change) {

        // Änderungen abstrakt in der Konsole ausgeben
        System.out.println("Data has changed. ChangeLog: ");

        // geänderten Häufigkeiten der Daten laden
        DataModel model = new DataModel();
        // Zunächst eine Map füllen, mit den Attributen als Schlüssel und einer Liste mit den konsolidierten Werten und ihren Häufigkeiten als value
        model.getValueQuantities(VA_topos.filterTreeViewData, VA_topos.observableData);

        // Zu jedem Column die Maxima und Minima hinzufügen
        table.getColumns().forEach(column -> {
            String searchText = ((TableColumn) column).getText();
            try {
                searchText = searchText.substring(0, 1).toLowerCase() + searchText.substring(1);
                String toolTip = "";
                List<String> myMinMax = VA_topos.filterTreeViewData.get(searchText);
                for (String min : myMinMax) {
                    toolTip = toolTip + min + "\n";
                }
                ((TableColumn) column).setUserData(toolTip);

            } catch (Exception e) {
                System.out.println("Fehler in der Funktion dataChanged in der Klasse EventHandlers. Finden der Maxima und Minima für die Spalte '" + searchText + "' nicht möglich.");
            }
        });
        // Die Werte aus der Map zum FilterTreeView hinzufügen
        //
        // Für jedes Attribut im TreeView
        MainWindowBuilder.tableFilterTreeView.getRoot().getChildren().forEach((child) -> {
            try {

                //System.out.println(child.toString());
                // Eine neue Liste von items anlegen, welche alle Werte für das Attribut sowie deren Häufigkeiten (zu einem String verkettet) enthalten soll
                List<CheckBoxTreeItem> subItems = new ArrayList();
                // Den Datensatz des aktuellen Attributs heraussuchen und für diesen alle Werte durchgehen
                VA_topos.filterTreeViewData.get(((CheckBoxTreeItem) child).getValue().toString()).forEach((item) -> {
                    // Das einzelne TreeItem generieren, welches den Wert und seine Häufigkeit enthält
                    CheckBoxTreeItem subItem = new CheckBoxTreeItem(item);
                    subItem.setSelected(true);

                    // Show only selected patients in Vis
                    if (((CheckBoxTreeItem) child).getValue().toString().equals("patientenID")) {
                        try {
                            String itemName = item.substring(0, item.lastIndexOf("(") - 1);
                            //System.out.println(VA_topos.observablePatients.get(itemName));

                            VA_topos.observablePatients.get(itemName + "_LEFT").selectedProperty().bindBidirectional(subItem.selectedProperty());
                            VA_topos.observablePatients.get(itemName + "_RIGHT").selectedProperty().bindBidirectional(subItem.selectedProperty());
                        } catch (Exception e) {
                            //System.out.println("item: " + item);
                            //VA_topos.observablePatients.keySet().forEach(key -> System.out.println(key));
                        }

                    }

                    // Add a rectangle to each value, to indicate its relative frequency
                    Rectangle rect = new Rectangle(16, 16);
                    // Get the quantity in Stringform from the current value
                    // TODO: hier tritt ein Fehler auf, bei der Farbberechnung, wenn ein neus Item hinzugefügt wird
                    String quantity = item.substring(item.lastIndexOf("(") + 1, item.lastIndexOf(")"));
                    double rectOpacity = quantity.equals("") ? 0.0 : Double.parseDouble(quantity) / (double) VA_topos.sortedFilteredData.size();
                    //System.out.println("Size:" + VA_topos.sortedFilteredData.size() + " Quantity: " + rectOpacity);

                    //rect.setStroke(Color.BLACK);
                    //rect.setStrokeWidth(0.5);
                    Color myCol = Color.rgb(CustomColors.RGB_STDBLUE[0], CustomColors.RGB_STDBLUE[1], CustomColors.RGB_STDBLUE[2]);
                    Stop[] stops = new Stop[]{new Stop(0, Color.WHITE), new Stop(1 - rectOpacity, Color.WHITE), new Stop(1 - rectOpacity, myCol), new Stop(1, myCol)};
                    LinearGradient lg1 = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
                    rect.setFill(lg1);

                    //rect.setFill(Color.rgb(CustomColors.RGB_STDBLUE[0], CustomColors.RGB_STDBLUE[1], CustomColors.RGB_STDBLUE[2], rectOpacity));
                    subItem.setGraphic(rect);

                    // Einen Listener hinzufügen, welcher dafür sorgt, dass die Datentabelle angepasst wird, sobald man klickt
                    subItem.selectedProperty().addListener((selectedChange, oldValue, newValue) -> {

                        //System.out.println("subItemListener executed for subItem: " + subItem + " Parent Node State: " + subItem.getParent().getValue());
                        // Einen String mit dem Spaltennamen erstellen
                        StringProperty fieldValue = new SimpleStringProperty(subItem.getValue().toString());
                        fieldValue.set(fieldValue.get().substring(0, fieldValue.get().lastIndexOf("(") - 1));

                        // Das passende Feld aus dem Datenmodell ermitteln
                        try {
                            Field field = DataModel.class.getDeclaredField(((CheckBoxTreeItem) child).getValue().toString());

                            // Festlegen, ob der Sichtbarkeitsindex erhöht oder verringert werden soll
                            //
                            // erhöht --> falls dieser Datensatz nicht mehr sichtbar sein soll
                            // verringert --> falls dieser Datensatz aufgrund des konkreten Feldes sichtbar sein soll
                            int zaehler = (newValue) ? -1 : 1;
                            VA_topos.observableData.forEach((datum) -> {
                                String compareValue = ((String) datum.getValueFromFields(field, datum));
                                if (!compareValue.isEmpty()) {
                                    compareValue = compareValue.toLowerCase();
                                    if (compareValue.equals(fieldValue.get().toLowerCase())) {
                                        datum.setDaysPast(datum.getDaysPast() + zaehler);
                                    }
                                }
                            });
                            VA_topos.filteredData.setPredicate(t -> true);
                            VA_topos.filteredData.setPredicate((DataModel t) -> {
                                return (t.getDaysPast() < 0);
                            });

                        } catch (NoSuchFieldException | SecurityException ex) {
                            Logger.getLogger(EventHandlers.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }); // subItem addListener

                    subItem.setIndependent(true);
                    subItems.add(subItem);

                });
                ((CheckBoxTreeItem) child).getChildren().clear();
                ((CheckBoxTreeItem) child).getChildren().addAll(subItems);
//                ((CheckBoxTreeItem) child).getChildren().addAll(VA_topos.filterTreeViewData.get(((CheckBoxTreeItem) child).getValue().toString()));
            } catch (ClassCastException e) {
                System.out.println("Fehler beim befüllen der FilterListe. Item: " + child.toString());
            }
            if (((CheckBoxTreeItem) child).getChildren().size() < 2) {
                ((CheckBoxTreeItem) child).selectedProperty().set(false);
            }
        }
        ); // tabelFilterTreeView.forEach

        VA_topos.observableData.forEach(datum -> datum.setLfdNr(VA_topos.observableData.indexOf(datum)));

        // Reload Visualizations 
        if (MainWindowBuilder.stdVisCheckBox.isSelected()) {
            StandardVisMaker stdVis = new StandardVisMaker();
            stdVis.makeStdVisScatterPlot(MainWindowBuilder.stdVisAnchorPane);
            stdVis.makeControlPanel(MainWindowBuilder.controlStdVisAnchorPane);

        }

        // 5. Aufgabe VisusFirst aktualisieren
        VisusFirstMaker VisFirstVis = new VisusFirstMaker();
        VisFirstVis.calculateDrawMatrix();
        VisusFirstMaker.makeSummaryVis(MainWindowBuilder.summaryVisusCanvas);
        //SpritzenFirstMaker.makeSpritzenFirst(identifySimilarStackPane, VA_topos.observablePatients);
//        annotationChanged();

        // Add the items to the Patient TreeView
        List<CheckBoxTreeItem> myItems = new ArrayList();
        List<String> mySortedList = new ArrayList();
        mySortedList.addAll(VA_topos.observablePatients.keySet());
        mySortedList.sort(Comparator.naturalOrder());
        // Add all patients
        mySortedList.forEach(datum -> {
            ImageView checkIcon = new ImageView(new Image(getClass().getResourceAsStream("/images/check.png"), 16, 16, false, false));
            CheckBoxTreeItem myItem = new CheckBoxTreeItem(datum, checkIcon);
            myItem.selectedProperty().bindBidirectional(VA_topos.observablePatients.get(datum).selectedProperty());
            myItem.setSelected(true);
            myItem.selectedProperty().addListener((myChange, oldVal, newVal) -> {
                checkIcon.setImage(newVal ? new Image(getClass().getResourceAsStream("/images/check.png"), 16, 16, false, false) : new Image(getClass().getResourceAsStream("/images/cross.png"), 16, 16, false, false));
                myItem.setGraphic(checkIcon);
            });
            // Add all items, relevant for the current patient
            myItem.getChildren().addAll(Patient.TreeViewHelper(VA_topos.observablePatients.get(datum)));
            myItems.add(myItem);
        });

        MainWindowBuilder.patientTreeView.getRoot().getChildren().addAll(myItems);

    }

//    Field[] fields = Patient.class.getDeclaredFields();
//    List<CheckBoxTreeItem> myItems = new ArrayList();
//    Patient myDatum = new Patient(null, false);
//    for (Field field : fields
//
//    
//        ) {
//            try {
//            myItems.add(new CheckBoxTreeItem(field.getName() + ": " + myDatum.getValueFromFields(field, VA_topos.observablePatients.get())));
//        } catch (IllegalArgumentException ex) {
//            myItems.add(new CheckBoxTreeItem(field.getName() + ": "));
//            Logger.getLogger(EventHandlers.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        // Falls es sich um eine Liste von Variablen handelt, wird der Knoten selektiert, um später detaillierter ausgewertet zu werden
//        if (field.getType().toString().toLowerCase().contains("observablemap") || field.getType().toString().toLowerCase().contains("util.list")) {
//            myItems.get(myItems.size() - 1).setSelected(true);
//        }
//    }
    //TODO: Integrate annotation actions and eventhandlers
//    public void annotationChanged() {
//        annotationChanged(null);
//    }
//
//    public void annotationChanged(GroupDataModel change) {
//
//        System.out.println("Annotations have changed.");
//        // Fill the Patient Treeview
//        Patient annoMethods = new Patient();
//        GroupDataModel groupMod = new GroupDataModel();
//
//        // Fill all global automatic derived annotations (eg. average visus)
//        Field[] fields = GroupDataModel.class.getDeclaredFields();
//        List<CheckBoxTreeItem> myItems = new ArrayList();
//        for (Field field : fields) {
//            try {
//                myItems.add(new CheckBoxTreeItem(field.getName() + ": " + groupMod.getValueFromFields(field, VA_topos.groupData.get())));
//            } catch (IllegalArgumentException ex) {
//                myItems.add(new CheckBoxTreeItem(field.getName() + ": "));
//                Logger.getLogger(EventHandlers.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            // Falls es sich um eine Liste von Variablen handelt, wird der Knoten selektiert, um später detaillierter ausgewertet zu werden
//            if (field.getType().toString().toLowerCase().contains("observablemap") || field.getType().toString().toLowerCase().contains("util.list")) {
//                myItems.get(myItems.size() - 1).setSelected(true);
//            }
//        }
//        MainWindowBuilder.patientTreeView.getRoot().getChildren().clear();
//        // Add all global values to treeview
//        MainWindowBuilder.patientTreeView.getRoot().getChildren().addAll(myItems);
//        // Add all patients
//        MainWindowBuilder.patientTreeView.getRoot().getChildren().forEach((child) -> {
//            if (((CheckBoxTreeItem) child).isSelected()) { // if selected, then there is a map behind (here it is the patients map)
//                ((CheckBoxTreeItem) child).getChildren().addAll(annoMethods.getTreeItemsfromField(((CheckBoxTreeItem) child).getValue().toString().substring(0, ((CheckBoxTreeItem) child).getValue().toString().indexOf(":")), "", "", VA_topos.groupData.get(), FieldClass.GROUPDATAMODEL));
//                // For each patient put the attributes
//                ((CheckBoxTreeItem) child).getChildren().forEach((subChild) -> {
//                    Field[] annoFields = Patient.class.getDeclaredFields();
//                    List<CheckBoxTreeItem> myAnnoItems = new ArrayList();
//                    for (Field field : annoFields) {
//                        if (!field.getName().contains("CLASSNAME")) {
//                            myAnnoItems.add(
//                                    new CheckBoxTreeItem(
//                                            field.getName() + ": " + annoMethods.getValueFromFields(
//                                            field, (Patient) VA_topos.groupData.get().getObservableAnnotations().get(
//                                                    ((CheckBoxTreeItem) subChild).getValue().toString()))));
//                            // Falls es sich um eine Liste von Variablen handelt, wird der Knoten selektiert, um später detaillierter ausgewertet zu werden
//                            if (field.getType().toString().toLowerCase().contains("observablemap") || field.getType().toString().toLowerCase().contains("util.list")) {
//                                myAnnoItems.get(myAnnoItems.size() - 1).setSelected(true);
//                            }
//                        } // if not CLASSNAME
//                    } // for annoFields
//                    ((CheckBoxTreeItem) subChild).getChildren().addAll(myAnnoItems);
//                    // Now put the map values for each patient
//                    ((CheckBoxTreeItem) subChild).getChildren().forEach((subSubChild) -> {
//                        // if selected, then there is a map behind (here they are the patients injections and visuses etc.)
//                        if (((CheckBoxTreeItem) subSubChild).isSelected()) {
//                            ((CheckBoxTreeItem) subSubChild).getChildren().addAll(annoMethods.getTreeItemsfromField(((CheckBoxTreeItem) subSubChild).getValue().toString().substring(0, ((CheckBoxTreeItem) subSubChild).getValue().toString().indexOf(":")), ((CheckBoxTreeItem) subChild).getValue().toString(), "", VA_topos.groupData.get(), FieldClass.ANNOTATIONMODEL));
//                            // If the element is the annotation element
//                            if (((CheckBoxTreeItem) subSubChild).getValue().toString().toLowerCase().startsWith("myAnnotations")) {
//                                Field[] myAnnoFields = Annotations.class.getDeclaredFields();
//                                List<CheckBoxTreeItem> myAnnosItems = new ArrayList();
//                                for (Field field : myAnnoFields) {
//// TODO: Add Annotations to TreeView (Korrektur!!)                                    
//                                    myAnnosItems.add(
//                                            new CheckBoxTreeItem(
//                                                    field.getName() + ": " + annoMethods.getValueFromFields(
//                                                    field, (Annotations) VA_topos.groupData.get().getObservableAnnotations().get(
//                                                            ((CheckBoxTreeItem) subSubChild).getValue().toString()))));
//                                    // Falls es sich um eine Liste von Variablen handelt, wird der Knoten selektiert, um später detaillierter ausgewertet zu werden
//                                    if (field.getType().toString().toLowerCase().contains("observablemap") || field.getType().toString().toLowerCase().contains("util.list")) {
//                                        myAnnosItems.get(myAnnosItems.size() - 1).setSelected(true);
//                                    }
//                                } // for myAnnoFields
//                                ((CheckBoxTreeItem) subSubChild).getChildren().addAll(myAnnosItems);
//
//                            }
//
//                        } // subsubChild isSelected
//                    }); // forEach subSubChild
//
//                }); // forEach SubChild (Patients)
//            } // if DataGroup isSelected
//        }); // ForEach DataGroup Attribut
//
//    }
    // Wenn die Sortierung der Liste geändert wird
    public void dataSortingChanged(ListChangeListener.Change change) {
        System.out.println("Sortierung wurde geändert!");
        try {
            if (MainWindowBuilder.stdVisCheckBox.isSelected()) {
                StandardVisMaker stdVis = new StandardVisMaker();
                stdVis.makeStdVisScatterPlot(MainWindowBuilder.stdVisAnchorPane);
                stdVis.makeControlPanel(MainWindowBuilder.controlStdVisAnchorPane);
            }
//            VisusFirstMaker VisFirstVis = new VisusFirstMaker();
//            VisFirstVis.drawVisusFirst(visusFirstDrawObjectAnchorPane, drawVisusValues, drawInjections);
        } catch (Exception e) {

        }
    }

    public void lineChartEntered(MouseEvent event) {

    }

    public void lineChartExited(MouseEvent event) {

    }

    public void lineChartMouseMoved(MouseEvent event) {
        //System.out.println( event.getX());//.setText(
        //String.format(
        // "(%.2f, %.2f)"
        //xAxis.getValueForDisplay(mouseEvent.getX()),
        //yAxis.getValueForDisplay(mouseEvent.getY())
//          )
//);
    }

    // MakeVisusFirst --> auf der Zeichenfläche das Mausrad bewegt
    public static void canvasScrolled(ScrollEvent e) {

        // If the ctrl key is NOT pressed
        if (!e.isControlDown()) {
//            VisusFirstMaker.oldLeftOffset = VisusFirstMaker.leftOffset;
//            VisusFirstMaker.oldTopOffset = VisusFirstMaker.topOffset;
//            VisusFirstMaker.oldZoomFactor = VisusFirstMaker.zoomFactor;
//            double realX = (e.getX() - VisusFirstMaker.oldLeftOffset) / VisusFirstMaker.zoomFactor;
//            double realY = (e.getY() - VisusFirstMaker.oldTopOffset) / VisusFirstMaker.zoomFactor;
            System.out.println("zoomed");
            if (e.getDeltaY() > 0) { // Zoom in
                if (MainWindowBuilder.zoomSlider.getValue() < VisusFirstMaker.MAX_ZOOM_FACTOR) {
                    MainWindowBuilder.zoomSlider.setValue(MainWindowBuilder.zoomSlider.getValue() + VisusFirstMaker.growZoomFactor.get());
                }
//                VisusFirstMaker.zoomFactor = VisusFirstMaker.zoomFactor < VisusFirstMaker.MAX_ZOOM_FACTOR ? VisusFirstMaker.zoomFactor + VisusFirstMaker.growZoomFactor.get() : VisusFirstMaker.MAX_ZOOM_FACTOR;
            } else { // Zoom out
                if (MainWindowBuilder.zoomSlider.getValue() > VisusFirstMaker.MIN_ZOOM_FACTOR) {
                    MainWindowBuilder.zoomSlider.setValue(MainWindowBuilder.zoomSlider.getValue() - VisusFirstMaker.growZoomFactor.get());
                }
//                VisusFirstMaker.zoomFactor = VisusFirstMaker.zoomFactor > VisusFirstMaker.MIN_ZOOM_FACTOR ? VisusFirstMaker.zoomFactor - VisusFirstMaker.growZoomFactor.get() : VisusFirstMaker.MIN_ZOOM_FACTOR;
            }
//            realX *= VisusFirstMaker.zoomFactor;
//            realY *= VisusFirstMaker.zoomFactor;
//            VisusFirstMaker.leftOffset = (e.getX()) - realX;
//            VisusFirstMaker.topOffset = e.getY() - realY;
//            MainWindowBuilder.zoomSlider.setValue(VisusFirstMaker.zoomFactor);
//            VisusFirstMaker visFirst = new VisusFirstMaker();
//            visFirst.makeVisusFirst();

        } else {
//            if (e.getSource() instanceof Canvas) {
//                System.out.println(IMAGE_LOC);
//                ((Canvas) e.getSource()).getGraphicsContext2D().drawImage(new Image(IMAGE_LOC), 0, 0);
//            }
        }

        //eh.canvasMouseMoved(e, canvas);
    }

    // MakeVisusFirst --> auf der Zeichenfläche die Maustaste gedrückt und die Maus bewegt (gezogen).
    public void canvasMouseDragged(MouseEvent e) {
        if (e.isPrimaryButtonDown()) {
            if (MainWindowBuilder.visusFirstAnnotationAnchorPane.getChildren().size() > 1) {
                try {
                    AnnoBuffer annBuff = VA_topos.drawBuffer.get(VA_topos.currentAnnoIndex.get() - 1);
                    ObservableList<DrawInfo> drawItems = annBuff.getDrawInfos();

                    DrawInfo currentAnno = drawItems.get(annBuff.getDrawIndex());
                    currentAnno.setWidth(Math.abs(e.getX() - currentAnno.getStartX()));
                    currentAnno.setHeight(Math.abs(e.getY() - currentAnno.getStartY()));
//                    currentAnno.setStartX(Math.min(currentAnno.getStartX(), e.getX()));
//                    currentAnno.setStartY(Math.min(currentAnno.getStartY(), e.getY()));
                    annBuff.getCanvas().getGraphicsContext2D().clearRect(0d, 0d, annBuff.getCanvas().getWidth(), annBuff.getCanvas().getHeight());
                    DrawingOperations drawOps = new DrawingOperations(annBuff);
                    boolean Xnegative = (e.getX() - currentAnno.getStartX()) < 0;
                    boolean Ynegative = (e.getY() - currentAnno.getStartY()) < 0;
                    for (int i = 0; i <= annBuff.getDrawIndex() - 1; i++) {
                        drawOps.draw(annBuff.getCanvas(), drawItems.get(i), false, false);
                    }
                    drawOps.draw(annBuff.getCanvas(), drawItems.get(drawItems.size() - 1), Xnegative, Ynegative);
                } catch (ClassCastException er) {
                }
            }

        } else {
//            System.out.println("dragged");

//            VisusFirstMaker.oldLeftOffset = VisusFirstMaker.leftOffset;
//            VisusFirstMaker.oldTopOffset = VisusFirstMaker.topOffset;
//            VisusFirstMaker.leftOffset += e.getX() - VisusFirstMaker.mouseXinit;
//            VisusFirstMaker.topOffset += e.getY() - VisusFirstMaker.mouseYinit;
//            VisusFirstMaker.mouseXinit = e.getX();
//            VisusFirstMaker.mouseYinit = e.getY();
//            VisusFirstMaker visFirst = new VisusFirstMaker();
//            visFirst.makeVisusFirst();
        }
    }

    // Procedure to be called, when the mouse is pressed on the visualization canvas
//    public void visMouseClicked(MouseEvent e) {
//        if (e.isDragDetect()) {
//            try {
//                ((LookUpTable) MainWindowBuilder.visusFirstAnchorPane.getUserData()).setPatientName("other Patient Name");
//                System.out.println("Patient Name changed");
//            } catch (Exception ex) {
//            }
//        }
//    }
    // procedure to be called, when the mouse is pressed on the visualization canvas
    public void visMousePressed(MouseEvent e) {
        // if the right mouse button is pressed -> start the dragging
        if (e.getButton() == MouseButton.SECONDARY) {
            // if the button is pressed and the mouse is dragged
            ((DotGrid) e.getSource()).setOnMouseDragged(dragEvent -> {
                // take every child
                ((DotGrid) e.getSource()).getChildren().forEach(myPane -> {
                    if (!(myPane instanceof Canvas)) {
//                        System.out.println("dragY: " + dragEvent.getY() + " e.Y: " + e.getY());
                        final int xTransValue = ((dragEvent.getX() - e.getX()) > 0) ? 1 : -1;
                        myPane.setTranslateX(myPane.getTranslateX() + xTransValue);
                        final int yTransValue = ((dragEvent.getY() - e.getY()) > 0) ? 1 : -1;
                        myPane.setTranslateY(myPane.getTranslateY() + yTransValue);
                    }
                });
            });
        }

        //((DotGrid) e.getSource()).getChildren().forEach(child -> moveAllPanes(child, e.getX(), e.getY()));
    }

    // Derive a sample of files from a list of files
    private List<File> sampleFiles(List<File> files) {

        List<File> fileList = new ArrayList();
        List<File> returnList = new ArrayList();
        int fileCount = files.size();
        List<Integer> fileNums = new ArrayList();

        files.forEach(file -> fileList.add(file));
        
        files.forEach(file -> {
            try {
                fileNums.add(Integer.parseInt(file.getName()));
                //System.out.println("Success: " + file.getName() + " Integer: " + fileNums.get(fileNums.size()-1));
            } catch (NumberFormatException e) {
                System.out.println("Failed to convert patient ID to Integer for directory: " + file.getName());
            }
        });

        DialogPanelBuilder.buildDialogPanel(fileCount, SAMPLEPANEL);
        dialogPanelStage.showAndWait();

        int sampleFileCount = (int) sampleCountSlider.getValue();
        // randomly get files from the complete fileList
        for (int i = 0; i < sampleFileCount; i++) {
            int index = (int) (Math.random() * (fileList.size()));
            returnList.add(fileList.get(index));
            //System.out.println("File: " + fileList.get(index).getName());
            fileList.remove(index);
        }

        return returnList;
    }
    
}
