/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.events;

import static va_topos.ui.MainWindowBuilder.visusFirstDrawObjectAnchorPane;
import va_topos.visualization.VisusFirstMaker;

/**
 *
 * @author lokal-admin
 */
public class VisFirstControlEvents {

    // redraw the objects on the Visus First View (due to change to unbound elements)
    public static void refreshScreen() {
        VisusFirstMaker visFirst = new VisusFirstMaker();
        visFirst.drawVisusFirst(visusFirstDrawObjectAnchorPane);
//        visFirst.makeVisusFirst(0, visusFirstDrawObjectAnchorPane);
        
    }
    
    public static void reloadVisualization() {
        VisusFirstMaker visFirst = new VisusFirstMaker();
        visFirst.calculateDrawMatrix();
//        visFirst.makeVisusFirst(0, visusFirstDrawObjectAnchorPane);
    }

}
