/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.events;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import va_topos.VA_topos;
import va_topos.annotationHandling.AnnoBuffer;
import va_topos.annotationHandling.DrawInfo;
import va_topos.annotationHandling.DrawingOperations;
import va_topos.color.CustomColors;
import va_topos.ui.MainWindowBuilder;
import static va_topos.ui.MainWindowBuilder.annoRedoButton;
import static va_topos.ui.MainWindowBuilder.annoUndoButton;
import static va_topos.ui.SubWindowsBuilder.annoTileAnchorPane;
import static va_topos.ui.SubWindowsBuilder.annoTilePane;
import static va_topos.ui.SubWindowsBuilder.annotationStage;
import va_topos.visualization.VisusFirstMaker;

/**
 *
 * @author varie
 */
public class AnnotationEvents {

    public static Canvas startAnnotation() {
        return startAnnotation(null);
    }

    // Create an annotation on the visualization (markings)
    public static Canvas startAnnotation(AnnoBuffer annBuff) {

        if (annBuff == null) {
            annBuff = new AnnoBuffer();
        }
        Canvas canvas = annBuff.getCanvas();

        if (canvas == null) {
            canvas = new Canvas();
        }
        //EventHandlers myEh = new EventHandlers();
        // Define, what to do, when the mousebutton is pressed
        canvas.setOnMousePressed(e -> canvasMouseDown(e));
        // Define, what to do, when the mousebutton is released
        canvas.setOnMouseReleased(e -> canvasMouseUp(e));

        // Define, what to do, when the mouse scrolls
        //canvas.setOnScroll(e -> canvasScrolled(e));
        // Define, what to do, when the mouse is dragged
        //canvas.setOnMouseDragged(e -> canvasMouseDragged(e));

        // Create the Structure to store the annotation actions
        annBuff.setCanvas(canvas); // Canvas is the drawing area for this annotation

        annBuff.setDrawIndex(0);

        annBuff.setAnnoColor(Color.rgb(CustomColors.RGB_RED_VALUES[VA_topos.currentAnnoIndex.get() % CustomColors.RGB_RED_VALUES.length], CustomColors.RGB_GREEN_VALUES[VA_topos.currentAnnoIndex.get() % CustomColors.RGB_GREEN_VALUES.length], CustomColors.RGB_BLUE_VALUES[VA_topos.currentAnnoIndex.get() % CustomColors.RGB_BLUE_VALUES.length]));
        // drawBuff is the structure to store all graphical annotations
        VA_topos.drawBuffer.add(VA_topos.currentAnnoIndex.get(), annBuff);
        VA_topos.currentAnnoIndex.set(VA_topos.currentAnnoIndex.get() + 1);

        // CopyCanvas is the Thumbnail of the Vis-Screen shown on the right side
        MainWindowBuilder.DotGrid copyCanvas = new MainWindowBuilder.DotGrid(200, 200);

        copyCanvas.setBackground(MainWindowBuilder.setStyle(copyCanvas, MainWindowBuilder.NodeStyles.ANNOTHUMBNAIL, annBuff.getAnnoColor().desaturate().desaturate().desaturate(), annBuff.getAnnoColor()));
        MainWindowBuilder.setAnchor(copyCanvas,
                10d, 0d, 0d, 0d);
        GraphicsContext gc2 = ((Canvas) copyCanvas.getChildren().get(0)).getGraphicsContext2D();
        SnapshotParameters params = new SnapshotParameters();

        params.setFill(Color.TRANSPARENT);
        WritableImage image = MainWindowBuilder.visusFirstDrawObjectAnchorPane.snapshot(params, null);

        gc2.setFill(Color.WHITE);
        gc2.fillRect(5, 5, 100, 100);
        gc2.setFill(annBuff.getAnnoColor());
        gc2.drawImage(image,
                6, 6, 98, 98);
        DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        Label timeStamp = new Label("Zeitstempel: " + LocalDateTime.now().format(timeFormat));
        MainWindowBuilder.setAnchor(timeStamp, 125d, 10d, 10d, Double.NaN);
        Label author = new Label("Autor: " + VA_topos.USER_NAME.get());
        MainWindowBuilder.setAnchor(author, 125d, 10d, 30d, Double.NaN);

        CheckBox visAnno = new CheckBox();
        visAnno.setText("Annotation in Visualisierung zeigen.");
        MainWindowBuilder.setAnchor(visAnno, 125d, 10d, 50d, Double.NaN);        
        
        AnchorPane annoThumbNail = new AnchorPane();
        annoThumbNail.setPrefHeight(200);
        MainWindowBuilder.setAnchor(annoThumbNail, 10d, 10d, annoThumbNail.getPrefHeight() * annoTileAnchorPane.getChildren().size(), Double.NaN);

        // Free Text annotation
        TextArea commentTextField = new TextArea();
        commentTextField.setMinSize(50, 50);
        MainWindowBuilder.setAnchor(commentTextField, 20d, 10d, 115d, 10d);

        // Categorie for the purpose of the annotation
        ComboBox annoPurpose = new ComboBox();
        MainWindowBuilder.setAnchor(annoPurpose, 125d, 10d, 85d, Double.NaN);
        annoPurpose.getItems().add("data annotation");
        annoPurpose.getItems().add("user annotation");
        annoPurpose.getItems().add("outcome annotation");

        annoThumbNail.getChildren().addAll(copyCanvas, author, timeStamp, visAnno, commentTextField, annoPurpose);

//        annoThumbNail.setBackground(MainWindowMaker.setStyle(annoThumbNail, MainWindowMaker.NodeStyles.ANNOTHUMBNAIL));
        annBuff.setAnnoThumbNail(annoThumbNail);

//        MainWindowBuilder.annoTilePane.getChildren()
//                .add(VA_topos.currentAnnoIndex.get() - 1, annoThumbNail);
        annoTileAnchorPane.getChildren().add(annoThumbNail);

        if (!annotationStage.isShowing()) {
            annotationStage.show();
        }
        return canvas;
    }

// Eine Annotation soll gelöscht werden
    public static void deleteAnnotation() {

        int current = VA_topos.currentAnnoIndex.get();

        // Das Canvas vom Bildschirm löschen
//        System.out.println("Number of Canvas prior deletion: " + MainWindowMaker.visusFirstAnchorPane.getChildren().size());
        MainWindowBuilder.visusFirstAnnotationAnchorPane.deleteCanvas(current);
//        System.out.println("Number of Canvas after deletion: " + MainWindowMaker.visusFirstAnchorPane.getChildren().size() + "\n");
        // Den ThumbNail löschen
        annoTilePane.getChildren().remove(VA_topos.drawBuffer.get(current - 1).getAnnoThumbNail());

        // Die Strukturierte Ablage löschen
        VA_topos.drawBuffer.remove(VA_topos.drawBuffer.get(current - 1));

//        System.out.println("current: " + current);
        current = current > 1 ? current - 1 : VA_topos.maxAnnoIndex.get() > 0 ? current : current - 1;
//        System.out.println("current: " + current);

        // Falls noch mindestens eine Annotation übrig ist, müssen die redo und undo buttons neu gesetzt werden (enable oder disable)
        if (VA_topos.maxAnnoIndex.get() > 1) {
            AnnoBuffer annBuff = VA_topos.drawBuffer.get(current - 1);
            annoUndoButton.disableProperty().bind(annBuff.drawIndexProperty().lessThanOrEqualTo(0));
            annoRedoButton.disableProperty().bind(annBuff.drawIndexProperty().greaterThanOrEqualTo(annBuff.maxIndexProperty()));
        } else { // wenn keine Annotation mehr übrig ist, müssen die buttons disabled werden
            annoUndoButton.disableProperty().bind(new SimpleBooleanProperty(true));
            annoRedoButton.disableProperty().bind(new SimpleBooleanProperty(true));
        }
        VA_topos.currentAnnoIndex.set(current);
    }

    public static void previousAnnotation() {

        if (VA_topos.currentAnnoIndex.get() > 1) {
            VA_topos.currentAnnoIndex.set(VA_topos.currentAnnoIndex.get() - 1);
            AnnoBuffer annBuff = VA_topos.drawBuffer.get(VA_topos.currentAnnoIndex.get() - 1);
            annoUndoButton.disableProperty().bind(annBuff.drawIndexProperty().lessThanOrEqualTo(0));
            annoRedoButton.disableProperty().bind(annBuff.drawIndexProperty().greaterThanOrEqualTo(annBuff.maxIndexProperty()));
        }
    }

    public static void nextAnnotation() {
        if (VA_topos.currentAnnoIndex.get() < (VA_topos.maxAnnoIndex.get())) {
            VA_topos.currentAnnoIndex.set(VA_topos.currentAnnoIndex.get() + 1);
            AnnoBuffer annBuff = VA_topos.drawBuffer.get(VA_topos.currentAnnoIndex.get() - 1);
            annoUndoButton.disableProperty().bind(annBuff.drawIndexProperty().lessThanOrEqualTo(0));
            annoRedoButton.disableProperty().bind(annBuff.drawIndexProperty().greaterThanOrEqualTo(annBuff.maxIndexProperty()));
        }
    }

    // Action to be performed, if the undo Button is pressed on screen "visus first"
    public static void undoAnnotation() {

        // Load the list of annotation actions taken for this annotation
        AnnoBuffer annBuff = VA_topos.drawBuffer.get(VA_topos.currentAnnoIndex.get() - 1);
        DrawingOperations drawOps = new DrawingOperations(annBuff);
        ObservableList<DrawInfo> tempList = annBuff.drawInfos;
        Canvas canvas = annBuff.getCanvas();
        if (tempList != null) { // if there is a list of annotations
            int currentIndex = annBuff.getDrawIndex();
            GraphicsContext gc = canvas.getGraphicsContext2D();

            // Clear the screen
            gc.clearRect(0d, 0d, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
            // if the number of annotation actions to be taken is more than zero
            if (currentIndex > 0) {
                // Decrease the index (undo = repaint all but the last action)
                currentIndex--;
                // For all actions, except the last one
                for (int i = 0; i < currentIndex; i++) {
                    drawOps.draw(canvas, tempList.get(i), false, false);
                }
            } // if currentindex > 0
            annBuff.setDrawIndex(currentIndex);
        } // if tempList != 0
    } // undo

    public static void redoAnnotation() {

        AnnoBuffer annBuff = VA_topos.drawBuffer.get(VA_topos.currentAnnoIndex.get() - 1);
        DrawingOperations drawOps = new DrawingOperations(annBuff);

        ObservableList<DrawInfo> tempList = annBuff.drawInfos;
        if (tempList != null) {

            int currentIndex = annBuff.getDrawIndex();
            int maxIndex = annBuff.getMaxIndex();
            Canvas canvas = annBuff.getCanvas();
            GraphicsContext gc = canvas.getGraphicsContext2D();
            // Clear the screen
            gc.clearRect(0d, 0d, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
            if (currentIndex <= maxIndex) {
                currentIndex++;
                for (int i = 0; i < currentIndex; i++) {
                    drawOps.draw(canvas, tempList.get(i), false, false);
                }
            }
            annBuff.setDrawIndex(currentIndex);
        }
    }
    
    
        public static void canvasMouseDown(MouseEvent e) {

        // If it is the secondary (!primary) button, the visualisation shall be moved
        if (e.getButton() != MouseButton.PRIMARY) {
            VisusFirstMaker.mouseXinit = e.getX();
            VisusFirstMaker.mouseYinit = e.getY();
        } else { // If it is the primary button, an annotation element shall be drawn
            // If the mouse is down and dragged
            if (!e.isDragDetect()) {
//                System.out.println("MouseDown");
                AnnoBuffer annBuff = VA_topos.drawBuffer.get(VA_topos.currentAnnoIndex.get() - 1);
                ObservableList<DrawInfo> drawItems = annBuff.drawInfos;
                drawItems.add(annBuff.getDrawIndex(), new DrawInfo(VA_topos.observableShape, annBuff.getAnnoColor(), Color.TRANSPARENT, e.getX(), e.getY(), 0, 0));
                //System.out.println(annBuff.getMaxIndex());
            }

        }
    }

    // procedure to be called, when the mouse is released on an annotation canvas
    public static void canvasMouseUp(MouseEvent e) {
        if (!e.isDragDetect() && (e.getButton() == MouseButton.PRIMARY)) {
//            System.out.println("MouseUp");
            AnnoBuffer annBuff = VA_topos.drawBuffer.get(VA_topos.currentAnnoIndex.get() - 1);
            ObservableList<DrawInfo> drawItems = annBuff.getDrawInfos();
            if (!drawItems.isEmpty()) {
                DrawInfo currentAnno = drawItems.get(annBuff.getDrawIndex());
                currentAnno.setWidth(Math.abs(e.getX() - currentAnno.getStartX()));
                currentAnno.setHeight(Math.abs(e.getY() - currentAnno.getStartY()));
                currentAnno.setStartX(Math.min(currentAnno.getStartX(), e.getX()));
                currentAnno.setStartY(Math.min(currentAnno.getStartY(), e.getY()));
                annBuff.getCanvas().getGraphicsContext2D().clearRect(0d, 0d, annBuff.getCanvas().getWidth(), annBuff.getCanvas().getHeight());
                DrawingOperations drawOps = new DrawingOperations(annBuff);
                for (int i = 0; i <= annBuff.getDrawIndex(); i++) {
                    drawOps.draw(annBuff.getCanvas(), drawItems.get(i), false, false);
                }
                annBuff.setDrawIndex(annBuff.getDrawIndex() + 1);

            }
        }
    }

    

}
