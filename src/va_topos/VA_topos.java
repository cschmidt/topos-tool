/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos;

import va_topos.ui.MainWindowBuilder;
import va_topos.events.EventHandlers;
import va_topos.data.OPSList;
import va_topos.data.ICD10List;
import va_topos.data.Patient;
import va_topos.data.DataModel;
import va_topos.annotationHandling.AnnoBuffer;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import va_topos.annotationHandling.DrawInfo;
import va_topos.data.GlobalData;
import va_topos.data.MasterPatient;

/**
 *
 * @author lokal-admin
 */
@SuppressWarnings("unchecked")
public class VA_topos extends Application {

    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
     */
    private static final String WORKING_DIRECTORY;
    public static final String IMAGE_DIRECTORY_DEFAULT;
    public static final String DATA_DIRECTORY_DEFAULT;
    public static final String DATA_DIRECTORY_FULL;
    public static final String SAVE_DIRECTORY_DEFAULT;
    public static final String CONFIG_DIRECTORY_DEFAULT;
    public static Optional<String> USER_NAME;
    public static final double LOGMAR_MAXIMUM = 3.0;

    //public static final double[] VISUSKLASSEN = {0.01, 0.05, 0.16, 0.3, 0.5};
    static {
        WORKING_DIRECTORY = System.getProperty("user.dir");
        IMAGE_DIRECTORY_DEFAULT = WORKING_DIRECTORY + "\\images\\test.png";
        DATA_DIRECTORY_DEFAULT = WORKING_DIRECTORY + "/data 100/";
        DATA_DIRECTORY_FULL = WORKING_DIRECTORY + "/data_full/";
        SAVE_DIRECTORY_DEFAULT = WORKING_DIRECTORY + "/save/";
        CONFIG_DIRECTORY_DEFAULT = WORKING_DIRECTORY + "/config data/";
    }

    //public static HashMap<String, HashMap<String, Integer>> opData = new HashMap();
    // Provenance Information
    public static Map<LocalDateTime, Canvas> provenance = new HashMap();

    // AnnotationProvenance
    public static List<AnnoBuffer> drawBuffer = new ArrayList();
//    public static IntegerProperty currentDrawIndex = new SimpleIntegerProperty(0);
//    public static IntegerProperty maxDrawIndex = new SimpleIntegerProperty(0);

    // Switch between annotations
    public static IntegerProperty currentAnnoIndex = new SimpleIntegerProperty(0);
    public static IntegerProperty maxAnnoIndex = new SimpleIntegerProperty(0);
    public static DrawInfo.Shape observableShape = DrawInfo.Shape.RECTANGLE;

    // Table for the meta Data of the items on the screen
    // public static List<LookUpTable> myLookUpTable = new ArrayList();
    // Ablage für die OPS Vergleichsliste
    public static ObservableList<OPSList> observableOPSList = FXCollections.observableList(new ArrayList());
    // Ablage für die ICD10 Vergleichsliste
    public static ObservableMap<String, List<ICD10List>> observableICD10Map = FXCollections.observableMap(new HashMap());
    // Ablage für die Codeübersetzung von Bastian
    public static HashMap<String, String> codeTranslator = new HashMap();

    //Ablage für die Dimensionen
    public static ObservableMap<String, DataModel.DataTypes> myDimensions = FXCollections.observableMap(new HashMap<>());

    // Ablage für die Datenbasis
    public static ObservableList<DataModel> observableData = FXCollections.observableList(new ArrayList());
    public static final FilteredList<DataModel> filteredData = new FilteredList<>(VA_topos.observableData);
    public static SortedList<DataModel> sortedFilteredData = new SortedList<>(filteredData);

    //public static final ObservableList<PatientBar> drawPatientsList = FXCollections.observableArrayList();
    //public static SortedList<PatientBar> sortedDrawPatientsList = new SortedList<>(drawPatientsList);
    //public static HashMap<String, List<OrderRectangle>> drawVisusValues;
    //public static HashMap<String, List<OrderPolygon>> drawInjections;
    // Ablage für die Annotationen (alle)
//    public static ObjectProperty<GroupDataModel> groupData = new SimpleObjectProperty(new GroupDataModel());
    // Storage for all parameters, that need to be globally available
    public static GlobalData globalData = new GlobalData();

    // Ablage für alle Patientendimensionen (Dimensionsreduktion etc.)
    public static ObservableMap<String, Patient> observablePatients = FXCollections.observableMap(new HashMap<>());
    public static MasterPatient masterPatient = new MasterPatient(null, true);

    // Data for the Treeview in the tableView
    public static ObservableMap<String, List<String>> filterTreeViewData = FXCollections.observableHashMap();

    // Ablage für zeitliche Daten (zweidimensional mit Zeitachse)
    public static ObservableMap<LocalDate, Number> timeObsMap = FXCollections.observableMap(new HashMap<>());

    // Text, welcher im Informationsfenster im DatenladeTab angezeigt wird
    public static StringProperty ladeText = new SimpleStringProperty();

    // String für den Dateipfad, falls er vom User geändert wird.
    public static String data_directory_custom = DATA_DIRECTORY_DEFAULT;

    @Override
    public void start(Stage primaryStage) {

        TextInputDialog dialog = new TextInputDialog("Christoph");
        dialog.setTitle("Username");
        dialog.setHeaderText("Please enter your name");
        USER_NAME = dialog.showAndWait();

        if (USER_NAME.isPresent()) {

            myDimensions = DataModel.getDimensions();

            try {
                // Falls sich die Datenbasis ändert
//                observableData.addListener(new ListChangeListener() {
//                    @Override
//                    public void onChanged(ListChangeListener.Change change) {
//                        EventHandlers myHandler = new EventHandlers();
//                        myHandler.dataChanged(change);
//                    }
//                });
                filteredData.addListener(new ListChangeListener() {
                    @Override
                    public void onChanged(ListChangeListener.Change change) {
                        EventHandlers myHandler = new EventHandlers();
                        myHandler.dataFilterChanged(change);
                        ladeText.set(ladeText.get() + " \n\nDie Daten wurden geändert.\n");
                    }
                });
//                sortedFilteredData.addListener(new ListChangeListener() {
//                    @Override
//                    public void onChanged(ListChangeListener.Change change) {
//                        EventHandlers myHandler = new EventHandlers();
//                        myHandler.dataSortingChanged(change);
//                    }
//                });

            } catch (Exception e) {
                System.out.println("Could not add ChangeListener.");
                System.out.println(e.getMessage());
            }
            primaryStage.setTitle("Vis-A-Vis");
            primaryStage.setScene(MainWindowBuilder.makeMainWindow());
            primaryStage.show();
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("Cannot start without a user!");
            alert.showAndWait();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
