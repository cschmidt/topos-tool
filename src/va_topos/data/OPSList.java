/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import java.util.HashMap;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Christoph Schmidt
 */
@SuppressWarnings("unchecked")
public class OPSList {

    private static final String CLASSNAME = OPSList.class.toString();

    String kodierung = "";
    String id = "";
    String erstKey = "";
    String zweitKey = "";
    String text = "";

    public String getKodierung() {
        return kodierung;
    }

    public void setKodierung(String kodierung) {
        this.kodierung = kodierung;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getErstKey() {
        return erstKey;
    }

    public void setErstKey(String erstKey) {
        this.erstKey = erstKey;
    }

    public String getZweitKey() {
        return zweitKey;
    }

    public void setZweitKey(String zweitKey) {
        this.zweitKey = zweitKey;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean setAll(HashMap myMap, String[] myDataLine, String datenQuelle) {

        BooleanProperty result = new SimpleBooleanProperty(true);

        // Zunächst alles initialisieren
        this.kodierung = "";
        this.id = "";
        this.erstKey = "";
        this.zweitKey = "";
        this.text = "";

        myMap.forEach((mapKey, mapValue) -> {
            try {
//                System.out.println("mapKey: " + mapKey + " mapValue: " + mapValue);
                if ((int) mapValue != -1) {
                    if ((int) mapValue < myDataLine.length) {
                        String thisData = myDataLine[(int) mapValue];
                        thisData = !thisData.isEmpty() ? thisData : "";

                        switch (mapKey.toString()) {

                            case "kodierung":
                                this.setKodierung(thisData);
                                break;

                            case "id":
                                this.setId(thisData);
                                break;

                            case "erstkey":
                                this.setErstKey(thisData);
                                break;

                            case "zweitkey":
                                this.setZweitKey(thisData);
                                break;

                            case "text":
                                this.setText(thisData);
                                break;

                            default:
                                System.out.println(CLASSNAME + " - Kann keine Spalte zuordnen:     MapKey: " + mapKey + " MapValue: " + (int) mapValue + " FeldInhalt: " + thisData);
                                result.set(false);

                        } // switch mapKey.toString()
                    } else {
                        StringProperty myBindString = new SimpleStringProperty("Der aktuelle Datensatz ist inkonsistent. Eventuell kann das entfernen von fehlerhaften Zeilenumbrüchen das Problem beheben.");
                        result.set(false);
                    } // Länge des Array nicht überschritten
                } // mapValue != -1

            } catch (NumberFormatException e) {
                System.out.println(CLASSNAME + " - NumberFormatException: MapKey: " + mapKey + " MapValue: " + (int) mapValue + " -- Fehlermeldung: " + e.getMessage());
                result.set(false);
            }
        });

        return result.get();
    }

}
