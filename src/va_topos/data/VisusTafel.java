/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import va_topos.data.DataModel.Visuswerte;

/**
 *
 * @author lokal-admin
 */
public enum VisusTafel {

    D0K0("", "", 0.0, "", 3.0, "", Visuswerte.NULLA_LUX),
    D0K002("", "", 0.002, "", 2.7, "", Visuswerte.LICHTSCHEIN),
    D0K005("", "", 0.005, "", 2.3, "", Visuswerte.HANDBEWEGUNGEN),
    D0K01("", "", 0.01, "", 2.0, "", Visuswerte.FINGERZAEHLEN),
    D0K012("", "", 0.012, "", 1.921, "", Visuswerte.DEZIMALVISUS),
    D0K013("", "", 0.013, "", 1.886, "", Visuswerte.DEZIMALVISUS),
    D0K018("", "", 0.018, "", 1.744, "", Visuswerte.DEZIMALVISUS),
    D0K02("6/120", "20/400", 0.02, "", 1.7, "1/50", Visuswerte.TAFELVISUS),
    D0K025("", "", 0.025, "", 1.6, "", Visuswerte.DEZIMALVISUS),
    D0K029("6/120", "20/400", 0.029, "", 1.544, "1/35", Visuswerte.TAFELVISUS),
    D0K03("", "", 0.03, "", 1.523, "", Visuswerte.DEZIMALVISUS),
    D0K033("", "", 0.033, "", 1.48, "", Visuswerte.DEZIMALVISUS),
    D0K04("6/120", "20/400", 0.04, "", 1.34, "1/25", Visuswerte.TAFELVISUS),
    D0K05("6/120", "20/400", 0.05, "", 1.3, "1/20", Visuswerte.TAFELVISUS),
    D0K06("", "", 0.06, "", 1.222, "", Visuswerte.DEZIMALVISUS),
    D0K067("", "", 0.067, "", 1.176, "1/15", Visuswerte.TAFELVISUS),
    D0K071("", "", 0.071, "", 1.149, "1/15", Visuswerte.TAFELVISUS),
    D0K08("6/75", "20/250", 0.08, "", 1.1, "", Visuswerte.DEZIMALVISUS),
    D0K083("", "", 0.083, "", 1.081, "", Visuswerte.DEZIMALVISUS),
    D0K09("", "", 0.09, "", 1.046, "", Visuswerte.DEZIMALVISUS),
    D0K1("6/60", "20/200", 0.1, "1/10", 1.0, "1/10", Visuswerte.DEZIMALVISUS),
    D0K107("", "", 0.107, "", 0.971, "", Visuswerte.DEZIMALVISUS),
    D0K12("", "", 0.12, "", 0.92, "", Visuswerte.DEZIMALVISUS),
    D0K125("", "", 0.125, "", 0.9, "", Visuswerte.DEZIMALVISUS),
    D0K13("", "", 0.13, "", 0.886, "", Visuswerte.DEZIMALVISUS),
    D0K133("", "", 0.133, "", 0.876, "", Visuswerte.DEZIMALVISUS),
    D0K14("", "", 0.14, "", 0.854, "", Visuswerte.DEZIMALVISUS),
    D0K143("", "", 0.143, "", 0.845, "", Visuswerte.DEZIMALVISUS),
    D0K16("", "", 0.16, "", 0.8, "", Visuswerte.DEZIMALVISUS),
    D0K2("6/30", "20/100", 0.2, "2/10", 0.7, "", Visuswerte.DEZIMALVISUS),
    D0K203("", "", 0.203, "", 0.693, "", Visuswerte.DEZIMALVISUS),
    D0K23("", "", 0.23, "", 0.638, "", Visuswerte.DEZIMALVISUS),
    D0K25("6/24", "20/80", 0.25, "", 0.6, "", Visuswerte.DEZIMALVISUS),
    D0K251("", "", 0.251, "", 0.6, "", Visuswerte.DEZIMALVISUS),
    D0K252("", "", 0.252, "", 0.599, "", Visuswerte.DEZIMALVISUS),
    D0K258("", "", 0.258, "", 0.588, "", Visuswerte.DEZIMALVISUS),
    D0K3("", "", 0.3, "", 0.523, "", Visuswerte.DEZIMALVISUS),
    D0K32("6/18", "20/60", 0.32, "", 0.5, "", Visuswerte.DEZIMALVISUS),
    D0K35("", "", 0.35, "", 0.456, "", Visuswerte.DEZIMALVISUS),
    D0K4("6/15", "20/50", 0.4, "4/10", 0.4, "", Visuswerte.DEZIMALVISUS),
    D0K411("", "", 0.411, "", 0.386, "", Visuswerte.DEZIMALVISUS),
    D0K42("", "", 0.42, "", 0.377, "", Visuswerte.DEZIMALVISUS),
    D0K425("", "", 0.425, "", 0.372, "", Visuswerte.DEZIMALVISUS),
    D0K45("", "", 0.45, "", 0.347, "", Visuswerte.DEZIMALVISUS),
    D0K5("6/12", "20/40", 0.5, "5/10", 0.3, "", Visuswerte.DEZIMALVISUS),
    D0K53("", "", 0.53, "", 0.276, "", Visuswerte.DEZIMALVISUS),
    D0K6("","",0.6,"",0.222,"",Visuswerte.DEZIMALVISUS),
    D0K62("", "", 0.62, "", 0.208, "", Visuswerte.DEZIMALVISUS),
    D0K63("", "", 0.63, "", 0.2, "", Visuswerte.DEZIMALVISUS),
    D0K633("", "", 0.633, "", 0.199, "", Visuswerte.DEZIMALVISUS),
    D0K639("", "", 0.639, "", 0.194, "", Visuswerte.DEZIMALVISUS),
    D0K64("", "", 0.64, "", 0.194, "", Visuswerte.DEZIMALVISUS),
    D0K65("", "", 0.65, "", 0.187, "", Visuswerte.DEZIMALVISUS),
    D0K67("6/9", "20/30", 0.67, "", 0.176, "", Visuswerte.DEZIMALVISUS),
    D0K7("", "", 0.7, "7/10", 0.155, "", Visuswerte.DEZIMALVISUS),
    D0K8("6/7.5", "20/25", 0.8, "8/10", 0.1, "", Visuswerte.DEZIMALVISUS),
    D0K85("", "", 0.85, "", 0.071, "", Visuswerte.DEZIMALVISUS),
    D0K9("", "", 0.9, "9/10", 0.046, "", Visuswerte.DEZIMALVISUS),
    D1K0("6/6", "20/20", 1.0, "10/10", 0.0, "", Visuswerte.DEZIMALVISUS),
    D1K007("", "", 1.007, "", -0.003, "", Visuswerte.DEZIMALVISUS),
    D1K028("", "", 1.028, "", -0.012, "", Visuswerte.DEZIMALVISUS),
    D1K16("", "", 1.16, "", -0.064, "", Visuswerte.DEZIMALVISUS),
    D1K2("", "", 1.2, "", -0.079, "", Visuswerte.DEZIMALVISUS),
    D1K25("", "", 1.25, "", -0.1, "", Visuswerte.DEZIMALVISUS),
    D1K33("6/4.5", "20/15", 1.33, "", -0.125, "", Visuswerte.DEZIMALVISUS),
    D1K5("", "", 1.5, "", -0.176, "", Visuswerte.DEZIMALVISUS),
    D1K6("", "", 1.6, "", -0.2, "", Visuswerte.DEZIMALVISUS),
    D1K75("", "", 1.75, "", -0.243, "", Visuswerte.DEZIMALVISUS),
    D1K8("", "", 1.8, "", -0.255, "", Visuswerte.DEZIMALVISUS),
    D1K9("", "", 1.9, "", -0.279, "", Visuswerte.DEZIMALVISUS),
    D2K0("6/3", "20/10", 2.0, "20/10", -0.3, "", Visuswerte.DEZIMALVISUS),
    DUNKNOWN("", "", Double.NaN, "", Double.NaN, "", Visuswerte.UNKNOWN);

    private final String snellenMeter;
    private final String snellenFeet;
    private final double decimal;
    private final String tenth;
    private final double logMar;
    private final String meterVisus;
    private final Visuswerte perception;

    private VisusTafel(String snellenMeter, String snellenFeet, double decimal, String tenth, double logMar, String meterVisus, Visuswerte perception) {
        this.snellenMeter = snellenMeter;
        this.snellenFeet = snellenFeet;
        this.decimal = decimal;
        this.tenth = tenth;
        this.logMar = logMar;
        this.meterVisus = meterVisus;
        this.perception = perception;
    }

    public String getSnellenMeter() {
        return snellenMeter;
    }

    public String getSnellenFeet() {
        return snellenFeet;
    }

    public double getDecimal() {
        return decimal;
    }

    public String getTenth() {
        return tenth;
    }

    public double getLogMar() {
        return logMar;
    }

    public String getMeterVisus() {
        return meterVisus;
    }

    public Visuswerte getLowPerception() {
        return perception;
    }
}
