/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.scene.control.CheckBoxTreeItem;
import va_topos.data.DataModel.AugeLateralitaet;
import va_topos.data.DataModel.Geschlecht;
import va_topos.visualization.EyeTimePanel;
import va_topos.visualization.PatientBar;

/**
 *
 * @author lokal-admin
 */
@SuppressWarnings("unchecked")
public class Patient {

    /**
     * selected indicates, if the patient is selected for visualization
     */
    private final BooleanProperty selected = new SimpleBooleanProperty();
    private final BooleanProperty show = new SimpleBooleanProperty();

    /**
     * Represents the visualized bar on the screen
     */
    private PatientBar drawPatientBar;
    //jsat.
    private String patientID;
    private AugeLateralitaet lateralitaet;
    private double ageAtBeginning;
    private final DoubleProperty durationOfTreatment = new SimpleDoubleProperty();
    private double differenceToCohortAverageAge;
    private Geschlecht sex;
    private double numberOfDoctors;
    private LocalDate dateOfBirth;

    private String firstDiagnosis;
    private String lastDiagnosis;
    private double numberOfDiagnoses;

    private double numberOfArgos_tlIncidents;

    private LocalDate firstVisusMeasurement;
    private LocalDate lastVisusMeasurement;
    private LocalDate firstInjection;
    private LocalDate lastInjection;
    private LocalDate beginMonitoring;
    private LocalDate endMonitoring;

    private double firstVisus;
    private double lastVisus;
    private double minVisus;
    private double maxVisus;
    private double meanVisus;
    private double medianVisus;
    private double visusDelta;
    private double visBeforeFirstInj;
    private double visAfterLastInjection;
    private double firstVisusDeltaFromPersonalAverage;
    private double lastVisusDeltaFromPersonalAverage;
    private double firstVisusDeltaFromCohortAverage;
    private double lastVisusDeltaFromCohortAverage;
    private double numberOfVisusMeasurements;
    private double minTimeBetweenVisusMeasurements;
    private double maxTimeBetweenVisusMeasurements;
    private double averageTimeBetweenVisusMeasurements;

    private double minVisClass;
    private double maxVisClass;
    private double minLogMarVis;
    private double maxLogMarVis;
    private double minVisDelta;
    private double maxVisDelta;
    private double minVisClassDelta;
    private double maxVisClassDelta;
    private double minLogMarVisDelta;
    private double maxLogMarVisDelta;

    private double firstEyePressure;
    private double lastEyePressure;
    private double minEyePressure;
    private double maxEyePressure;
    private double meanEyePressure;
    private double medianEyePressure;
    private double firstEyePressureDeltaFromPersonalAverage;
    private double lastEyePressureDeltaFromPersonalAverage;
    private double firstEyePressureDeltaFromCohortAverage;
    private double lastEyePressureDeltaFromCohortAverage;
    private double NumberOfEyePressureMeasurements;
    private double minTimeBetweenEyePressureMeasurements;
    private double maxTimeBetweenEyePressureMeasurements;
    private double averageTimeBetweenEyePressureMeasurements;

    private double numberOfInjections;
    private double minTimeBetweenInjections;
    private double maxTimeBetweenInjections;
    private double averageTimeBetweenInjections;
    private double differenceToAverageInjectionNumber;
    private double lengthOfMonitoring;

    private double numberOfMedications;
    private String firstMedication;
    private String lastMedication;

    private double averageVisusImprovementAfterInjection;
    private double minVisusImprovementAfterInjection;
    private double maxVisusImprovementAfterInjection;
    private double visusImprovementsRateAfterInjection;

    private double linearRegressionSlope;
    private double linearRegressionIntercept;

    private EyeTimePanel eyeTimePanel;
    private final DoubleProperty xOffset = new SimpleDoubleProperty();

    private final ObservableMap<LocalDate, Double> visen = FXCollections.observableHashMap();
    private final ObservableMap<LocalDate, Double> visenFromLetterExtract = FXCollections.observableHashMap();
    private final ObservableMap<LocalDate, Double> visenLogMar = FXCollections.observableHashMap();
    private final ObservableMap<LocalDate, Double> augendruecke = FXCollections.observableHashMap();
    private final ObservableMap<LocalDate, DataModel.AntiVEGFMedikament> spritzen = FXCollections.observableHashMap();
    private final ObservableMap<Period, Double> visusDelten = FXCollections.observableHashMap();
    private final ObservableMap<LocalDate, Double> visClasses = FXCollections.observableHashMap();
    private final ObservableMap<Period, Double> visusLogMarDelten = FXCollections.observableHashMap();
    private final ObservableMap<Period, Double> visusClassDelten = FXCollections.observableHashMap();
    private final ObservableMap<LocalDate, String> argos_tlIncidents = FXCollections.observableHashMap();

    private final ObservableMap<LocalDate, Period> visPausen = FXCollections.observableHashMap();
    private final ObservableMap<LocalDate, Double> visMeasurementsSinceLastInjection = FXCollections.observableHashMap();

    private final ObservableMap<LocalDate, Period> spritzPausen = FXCollections.observableHashMap();
    private final ObservableMap<String, Annotations> myAnnotations = FXCollections.observableHashMap();

    private final ObservableMap<LocalDate, String> diagnoses = FXCollections.observableHashMap();
    private final ObservableMap<String, MedicationChange> myMedicationChanges = FXCollections.observableHashMap();

    public Patient(DataModel datum, boolean initialize) {

        if (initialize) {

            this.selected.set(false);
            this.show.set(true);
            this.drawPatientBar = new PatientBar();
            //jsat.
            this.patientID = "";
            this.lateralitaet = AugeLateralitaet.UNKNOWN;
            this.ageAtBeginning = Double.NaN;
            this.durationOfTreatment.set(Double.NaN);
            this.differenceToCohortAverageAge = Double.NaN;
            this.sex = Geschlecht.u;
            this.numberOfDoctors = Double.NaN;
            this.dateOfBirth = LocalDate.MIN;

            this.firstDiagnosis = "";
            this.lastDiagnosis = "";
            this.numberOfDiagnoses = 0.0;

            this.numberOfArgos_tlIncidents = 0.0;

            this.beginMonitoring = LocalDate.MIN;
            this.endMonitoring = LocalDate.MIN;
            this.firstVisusMeasurement = LocalDate.MIN;
            this.lastVisusMeasurement = LocalDate.MIN;
            this.firstInjection = LocalDate.MIN;
            this.lastInjection = LocalDate.MIN;

            this.firstVisus = Double.NaN;
            this.lastVisus = Double.NaN;
            this.minVisus = Double.NaN;
            this.maxVisus = Double.NaN;
            this.visusDelta = Double.NaN;
            this.meanVisus = Double.NaN;
            this.medianVisus = Double.NaN;
            this.visBeforeFirstInj = Double.NaN;
            this.visAfterLastInjection = Double.NaN;

            this.firstVisusDeltaFromPersonalAverage = Double.NaN;
            this.lastVisusDeltaFromPersonalAverage = Double.NaN;
            this.firstVisusDeltaFromCohortAverage = Double.NaN;
            this.lastVisusDeltaFromCohortAverage = Double.NaN;
            this.numberOfVisusMeasurements = Double.NaN;
            this.minTimeBetweenVisusMeasurements = Double.NaN;
            this.maxTimeBetweenVisusMeasurements = Double.NaN;
            this.averageTimeBetweenVisusMeasurements = Double.NaN;

            this.minVisClass = Double.NaN;
            this.maxVisClass = Double.NaN;
            this.minLogMarVis = Double.NaN;
            this.maxLogMarVis = Double.NaN;
            this.minVisDelta = Double.NaN;
            this.maxVisDelta = Double.NaN;
            this.minVisClassDelta = Double.NaN;
            this.maxVisClassDelta = Double.NaN;
            this.minLogMarVisDelta = Double.NaN;
            this.maxLogMarVisDelta = Double.NaN;

            this.firstEyePressure = Double.NaN;
            this.lastEyePressure = Double.NaN;
            this.minEyePressure = Double.NaN;
            this.maxEyePressure = Double.NaN;
            this.meanEyePressure = Double.NaN;
            this.medianEyePressure = Double.NaN;
            this.firstEyePressureDeltaFromPersonalAverage = Double.NaN;
            this.lastEyePressureDeltaFromPersonalAverage = Double.NaN;
            this.firstEyePressureDeltaFromCohortAverage = Double.NaN;
            this.lastEyePressureDeltaFromCohortAverage = Double.NaN;
            this.NumberOfEyePressureMeasurements = Double.NaN;
            this.minTimeBetweenEyePressureMeasurements = Double.NaN;
            this.maxTimeBetweenEyePressureMeasurements = Double.NaN;
            this.averageTimeBetweenEyePressureMeasurements = Double.NaN;

            this.numberOfInjections = Double.NaN;
            this.minTimeBetweenInjections = Double.NaN;
            this.maxTimeBetweenInjections = Double.NaN;
            this.averageTimeBetweenInjections = Double.NaN;
            this.differenceToAverageInjectionNumber = Double.NaN;

            this.numberOfMedications = Double.NaN;
            this.firstMedication = "";
            this.lastMedication = "";

            this.lengthOfMonitoring = Double.NaN;

            this.averageVisusImprovementAfterInjection = Double.NaN;
            this.minVisusImprovementAfterInjection = Double.NaN;
            this.maxVisusImprovementAfterInjection = Double.NaN;
            this.visusImprovementsRateAfterInjection = Double.NaN;

            this.linearRegressionIntercept = 0.0;
            this.linearRegressionSlope = 0.0;
            

        }
        if (datum != null) {
            this.selected.set(true);
            this.dateOfBirth = datum.getGeburtsdatum();
            this.patientID = datum.getPatientenID() + "_" + datum.getUntersuchungLateralitaet();
            this.lateralitaet = datum.getUntersuchungLateralitaet();
            this.sex = datum.getGeschlecht();
        }

    }

    
    public boolean isShow() {
        return show.get();
    }

    public void setShow(boolean value) {
        show.set(value);
    }

    public BooleanProperty showProperty() {
        return show;
    }
    
    public double getMedianVisus() {
        return medianVisus;
    }

    public void setMedianVisus(double medianVisus) {
        this.medianVisus = medianVisus;
    }

    public double getMedianEyePressure() {
        return medianEyePressure;
    }

    public void setMedianEyePressure(double medianEyePressure) {
        this.medianEyePressure = medianEyePressure;
    }
    
    

    public double getNumberOfArgos_tlIncidents() {
        return numberOfArgos_tlIncidents;
    }

    public void setNumberOfArgos_tlIncidents(double numberOfArgos_tlIncidents) {
        this.numberOfArgos_tlIncidents = numberOfArgos_tlIncidents;
    }

    public double getNumberOfDiagnoses() {
        return numberOfDiagnoses;
    }

    public void setNumberOfDiagnoses(double numberOfDiagnoses) {
        this.numberOfDiagnoses = numberOfDiagnoses;
    }

    public double getMinVisClass() {
        return minVisClass;
    }

    public void setMinVisClass(double minVisClass) {
        this.minVisClass = minVisClass;
    }

    public double getMaxVisClass() {
        return maxVisClass;
    }

    public void setMaxVisClass(double maxVisClass) {
        this.maxVisClass = maxVisClass;
    }

    public double getMinLogMarVis() {
        return minLogMarVis;
    }

    public void setMinLogMarVis(double minLogMarVis) {
        this.minLogMarVis = minLogMarVis;
    }

    public double getMaxLogMarVis() {
        return maxLogMarVis;
    }

    public void setMaxLogMarVis(double maxLogMarVis) {
        this.maxLogMarVis = maxLogMarVis;
    }

    public double getMinVisDelta() {
        return minVisDelta;
    }

    public void setMinVisDelta(double minVisDelta) {
        this.minVisDelta = minVisDelta;
    }

    public double getMaxVisDelta() {
        return maxVisDelta;
    }

    public void setMaxVisDelta(double maxVisDelta) {
        this.maxVisDelta = maxVisDelta;
    }

    public double getMinVisClassDelta() {
        return minVisClassDelta;
    }

    public void setMinVisClassDelta(double minVisClassDelta) {
        this.minVisClassDelta = minVisClassDelta;
    }

    public double getMaxVisClassDelta() {
        return maxVisClassDelta;
    }

    public void setMaxVisClassDelta(double maxVisClassDelta) {
        this.maxVisClassDelta = maxVisClassDelta;
    }

    public double getMinLogMarVisDelta() {
        return minLogMarVisDelta;
    }

    public void setMinLogMarVisDelta(double minLogMarVisDelta) {
        this.minLogMarVisDelta = minLogMarVisDelta;
    }

    public double getMaxLogMarVisDelta() {
        return maxLogMarVisDelta;
    }

    public void setMaxLogMarVisDelta(double maxLogMarVisDelta) {
        this.maxLogMarVisDelta = maxLogMarVisDelta;
    }

    public double getLinearRegressionSlope() {
        return linearRegressionSlope;
    }

    public void setLinearRegressionSlope(double linearRegressionSlope) {
        this.linearRegressionSlope = linearRegressionSlope;
    }

    public double getLinearRegressionIntercept() {
        return linearRegressionIntercept;
    }

    public void setLinearRegressionIntercept(double linearRegressionIntercept) {
        this.linearRegressionIntercept = linearRegressionIntercept;
    }

    public double getxOffset() {
        return xOffset.get();
    }

    public void setxOffset(double value) {
        xOffset.set(value);
    }

    public DoubleProperty xOffsetProperty() {
        return xOffset;
    }

    public AugeLateralitaet getLateralitaet() {
        return lateralitaet;
    }

    public void setLateralitaet(AugeLateralitaet lateralitaet) {
        this.lateralitaet = lateralitaet;
    }

    public EyeTimePanel getEyeTimePanel() {
        return eyeTimePanel;
    }

    public void setEyeTimePanel(EyeTimePanel eyeTimePanel) {
        this.eyeTimePanel = eyeTimePanel;
    }

    public double getVisBeforeFirstInj() {
        return visBeforeFirstInj;
    }

    public void setVisBeforeFirstInj(double visBeforeFirstInj) {
        this.visBeforeFirstInj = visBeforeFirstInj;
    }

    public double getVisAfterLastInjection() {
        return visAfterLastInjection;
    }

    public void setVisAfterLastInjection(double visAfterLastInjection) {
        this.visAfterLastInjection = visAfterLastInjection;
    }

    public LocalDate getFirstVisusMeasurement() {
        return firstVisusMeasurement;
    }

    public void setFirstVisusMeasurement(LocalDate firstVisusMeasurement) {
        this.firstVisusMeasurement = firstVisusMeasurement;
    }

    public LocalDate getLastVisusMeasurement() {
        return lastVisusMeasurement;
    }

    public void setLastVisusMeasurement(LocalDate lastVisusMeasurement) {
        this.lastVisusMeasurement = lastVisusMeasurement;
    }

    public LocalDate getFirstInjection() {
        return firstInjection;
    }

    public void setFirstInjection(LocalDate firstInjection) {
        this.firstInjection = firstInjection;
    }

    public LocalDate getLastInjection() {
        return lastInjection;
    }

    public void setLastInjection(LocalDate lastInjection) {
        this.lastInjection = lastInjection;
    }

    public double getVisusDelta() {
        return visusDelta;
    }

    public void setVisusDelta(double visusDelta) {
        this.visusDelta = visusDelta;
    }

    public double getDurationOfTreatment() {
        return durationOfTreatment.get();
    }

    public void setDurationOfTreatment(double value) {
        durationOfTreatment.set(value);
    }

    public DoubleProperty durationOfTreatmentProperty() {
        return durationOfTreatment;
    }

    public double getLengthOfMonitoring() {
        return lengthOfMonitoring;
    }

    public void setLengthOfMonitoring(double lengthOfMonitoring) {
        this.lengthOfMonitoring = lengthOfMonitoring;
    }

    public final ObservableMap<LocalDate, Double> getVisClasses() {
        return visClasses;
    }

    public final ObservableMap<LocalDate, Double> getVisen() {
        return visen;
    }

    public final ObservableMap<LocalDate, Double> getVisenFromLetterExtract() {
        return visenFromLetterExtract;
    }

    public final ObservableMap<LocalDate, Double> getVisenLogMar() {
        return visenLogMar;
    }

    public final ObservableMap<LocalDate, Double> getAugendruecke() {
        return augendruecke;
    }

    public final ObservableMap<LocalDate, Period> getVisPausen() {
        return this.visPausen;
    }

    public final ObservableMap<Period, Double> getVisusDelten() {
        return this.visusDelten;
    }

    public final ObservableMap<Period, Double> getVisusLogMarDelten() {
        return this.visusLogMarDelten;
    }

    public final ObservableMap<Period, Double> getVisusClassDelten() {
        return this.visusClassDelten;
    }

    public final ObservableMap<LocalDate, String> getArgos_tlIncidents() {
        return argos_tlIncidents;
    }

    public final ObservableMap<LocalDate, Double> getVisMeasurementsSinceLastInjection() {
        return visMeasurementsSinceLastInjection;
    }

    public final ObservableMap<LocalDate, String> getDiagnoses() {
        return diagnoses;
    }

    public final ObservableMap<LocalDate, DataModel.AntiVEGFMedikament> getSpritzen() {
        return this.spritzen;
    }

    public final ObservableMap<LocalDate, Period> getSpritzPausen() {
        return this.spritzPausen;
    }

    public final ObservableMap<String, Annotations> getMyAnnotations() {
        return myAnnotations;
    }

    public ObservableMap<String, MedicationChange> getMyMedicationChanges() {
        return myMedicationChanges;
    }

    public final void setVisen(ObservableMap<LocalDate, Double> visen) {
        this.visen.clear();
        this.visen.putAll(visen);
    }

    public final void setVisClasses(ObservableMap<LocalDate, Double> visClasses) {
        this.visClasses.clear();
        this.visClasses.putAll(visClasses);
    }

    public final void setVisenFromLetterExtract(ObservableMap<LocalDate, Double> visenFromLetterExtract) {
        this.visenFromLetterExtract.clear();
        this.visenFromLetterExtract.putAll(visenFromLetterExtract);
    }

    public final void setVisenLogMar(ObservableMap<LocalDate, Double> visen) {
        this.visenLogMar.clear();
        this.visenLogMar.putAll(visen);
    }

    public final void setAugendruecke(ObservableMap<LocalDate, Double> augendruecke) {
        this.augendruecke.clear();
        this.augendruecke.putAll(augendruecke);
    }

    public final void setVisPausen(ObservableMap<LocalDate, Period> visPausen) {
        this.visPausen.clear();
        this.visPausen.putAll(visPausen);
    }

    public final void setVisusDelten(ObservableMap<Period, Double> visusDelten) {
        this.visusDelten.clear();
        this.visusDelten.putAll(visusDelten);
    }

    public final void setVisusLogMarDelten(ObservableMap<Period, Double> visusLogMarDelten) {
        this.visusLogMarDelten.clear();
        this.visusLogMarDelten.putAll(visusLogMarDelten);
    }

    public final void setVisusClassDelten(ObservableMap<Period, Double> visusClassDelten) {
        this.visusClassDelten.clear();
        this.visusClassDelten.putAll(visusClassDelten);
    }

    public final void setArgos_tlIncidents(ObservableMap<LocalDate, String> argos_tlIncidents) {
        this.argos_tlIncidents.clear();
        this.argos_tlIncidents.putAll(argos_tlIncidents);
    }

    public final void setVisMeasurementsSinceLastInjection(ObservableMap<LocalDate, Double> visMeasurementsSinceLastInjection) {
        this.visMeasurementsSinceLastInjection.clear();
        this.visMeasurementsSinceLastInjection.putAll(visMeasurementsSinceLastInjection);
    }

    public final void setDiagnoses(ObservableMap<LocalDate, String> diagnoses) {
        this.diagnoses.clear();
        this.diagnoses.putAll(diagnoses);
    }

    public final void setSpritzen(ObservableMap<LocalDate, DataModel.AntiVEGFMedikament> spritzen) {
        this.spritzen.clear();
        this.spritzen.putAll(spritzen);
    }

    public final void setSpritzPausen(ObservableMap<LocalDate, Period> spritzPausen) {
        this.spritzPausen.clear();
        this.spritzPausen.putAll(spritzPausen);
    }

    public final void setMyAnnotations(ObservableMap<String, Annotations> myAnnotations) {
        this.myAnnotations.clear();
        this.myAnnotations.putAll(myAnnotations);
    }

    public final void setMyMedicationChanges(ObservableMap<String, MedicationChange> myMedicationChanges) {
        this.myMedicationChanges.clear();
        this.myMedicationChanges.putAll(myMedicationChanges);
    }

    public PatientBar getDrawPatientBar() {
        return drawPatientBar;
    }

    public void setDrawPatientBar(PatientBar drawPatientBar) {
        this.drawPatientBar = drawPatientBar;
    }

    public LocalDate getEndMonitoring() {
        return endMonitoring;
    }

    public void setEndMonitoring(LocalDate endMonitoring) {
        this.endMonitoring = endMonitoring;
    }

    public LocalDate getBeginMonitoring() {
        return beginMonitoring;
    }

    public void setBeginMonitoring(LocalDate beginMonitoring) {
        this.beginMonitoring = beginMonitoring;
    }

    public boolean isSelected() {
        return selected.get();
    }

    public void setSelected(boolean value) {
        selected.set(value);
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public double getMinTimeBetweenInjections() {
        return minTimeBetweenInjections;
    }

    public void setMinTimeBetweenInjections(double minTimeBetweenInjections) {
        this.minTimeBetweenInjections = minTimeBetweenInjections;
    }

    public double getMaxTimeBetweenInjections() {
        return maxTimeBetweenInjections;
    }

    public void setMaxTimeBetweenInjections(double maxTimeBetweenInjections) {
        this.maxTimeBetweenInjections = maxTimeBetweenInjections;
    }

    public final String getPatientID() {
        return patientID;
    }

    public final void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public double getAgeAtBeginning() {
        return ageAtBeginning;
    }

    public void setAgeAtBeginning(double ageAtBeginning) {
        this.ageAtBeginning = ageAtBeginning;
    }

    public double getDifferenceToCohortAverageAge() {
        return differenceToCohortAverageAge;
    }

    public void setDifferenceToCohortAverageAge(double differenceToCohortAverageAge) {
        this.differenceToCohortAverageAge = differenceToCohortAverageAge;
    }

    public final Geschlecht getSex() {
        return sex;
    }

    public final void setSex(Geschlecht sex) {
        this.sex = sex;
    }

    public double getNumberOfDoctors() {
        return numberOfDoctors;
    }

    public void setNumberOfDoctors(double numberOfDoctors) {
        this.numberOfDoctors = numberOfDoctors;
    }

    public String getFirstDiagnosis() {
        return firstDiagnosis;
    }

    public void setFirstDiagnosis(String firstDiagnosis) {
        this.firstDiagnosis = firstDiagnosis;
    }

    public String getLastDiagnosis() {
        return lastDiagnosis;
    }

    public void setLastDiagnosis(String lastDiagnosis) {
        this.lastDiagnosis = lastDiagnosis;
    }

    public double getFirstVisus() {
        return firstVisus;
    }

    public void setFirstVisus(double firstVisus) {
        this.firstVisus = firstVisus;
    }

    public double getLastVisus() {
        return lastVisus;
    }

    public void setLastVisus(double lastVisus) {
        this.lastVisus = lastVisus;
    }

    public double getMinVisus() {
        return minVisus;
    }

    public void setMinVisus(double minVisus) {
        this.minVisus = minVisus;
    }

    public double getMaxVisus() {
        return maxVisus;
    }

    public void setMaxVisus(double maxVisus) {
        this.maxVisus = maxVisus;
    }

    public double getMeanVisus() {
        return meanVisus;
    }

    public void setMeanVisus(double meanVisus) {
        this.meanVisus = meanVisus;
    }

    public double getFirstVisusDeltaFromPersonalAverage() {
        return firstVisusDeltaFromPersonalAverage;
    }

    public void setFirstVisusDeltaFromPersonalAverage(double firstVisusDeltaFromPersonalAverage) {
        this.firstVisusDeltaFromPersonalAverage = firstVisusDeltaFromPersonalAverage;
    }

    public double getLastVisusDeltaFromPersonalAverage() {
        return lastVisusDeltaFromPersonalAverage;
    }

    public void setLastVisusDeltaFromPersonalAverage(double lastVisusDeltaFromPersonalAverage) {
        this.lastVisusDeltaFromPersonalAverage = lastVisusDeltaFromPersonalAverage;
    }

    public double getFirstVisusDeltaFromCohortAverage() {
        return firstVisusDeltaFromCohortAverage;
    }

    public void setFirstVisusDeltaFromCohortAverage(double firstVisusDeltaFromCohortAverage) {
        this.firstVisusDeltaFromCohortAverage = firstVisusDeltaFromCohortAverage;
    }

    public double getLastVisusDeltaFromCohortAverage() {
        return lastVisusDeltaFromCohortAverage;
    }

    public void setLastVisusDeltaFromCohortAverage(double lastVisusDeltaFromCohortAverage) {
        this.lastVisusDeltaFromCohortAverage = lastVisusDeltaFromCohortAverage;
    }

    public double getNumberOfVisusMeasurements() {
        return numberOfVisusMeasurements;
    }

    public void setNumberOfVisusMeasurements(double numberOfVisusMeasurements) {
        this.numberOfVisusMeasurements = numberOfVisusMeasurements;
    }

    public double getMinTimeBetweenVisusMeasurements() {
        return minTimeBetweenVisusMeasurements;
    }

    public void setMinTimeBetweenVisusMeasurements(double minTimeBetweenVisusMeasurements) {
        this.minTimeBetweenVisusMeasurements = minTimeBetweenVisusMeasurements;
    }

    public double getMaxTimeBetweenVisusMeasurements() {
        return maxTimeBetweenVisusMeasurements;
    }

    public void setMaxTimeBetweenVisusMeasurements(double maxTimeBetweenVisusMeasurements) {
        this.maxTimeBetweenVisusMeasurements = maxTimeBetweenVisusMeasurements;
    }

    public double getAverageTimeBetweenVisusMeasurements() {
        return averageTimeBetweenVisusMeasurements;
    }

    public void setAverageTimeBetweenVisusMeasurements(double averageTimeBetweenVisusMeasurements) {
        this.averageTimeBetweenVisusMeasurements = averageTimeBetweenVisusMeasurements;
    }

    public double getFirstEyePressure() {
        return firstEyePressure;
    }

    public void setFirstEyePressure(double firstEyePressure) {
        this.firstEyePressure = firstEyePressure;
    }

    public double getLastEyePressure() {
        return lastEyePressure;
    }

    public void setLastEyePressure(double lastEyePressure) {
        this.lastEyePressure = lastEyePressure;
    }

    public double getMinEyePressure() {
        return minEyePressure;
    }

    public void setMinEyePressure(double minEyePressure) {
        this.minEyePressure = minEyePressure;
    }

    public double getMaxEyePressure() {
        return maxEyePressure;
    }

    public void setMaxEyePressure(double maxEyePressure) {
        this.maxEyePressure = maxEyePressure;
    }

    public double getMeanEyePressure() {
        return meanEyePressure;
    }

    public void setMeanEyePressure(double meanEyePressure) {
        this.meanEyePressure = meanEyePressure;
    }

    public double getFirstEyePressureDeltaFromPersonalAverage() {
        return firstEyePressureDeltaFromPersonalAverage;
    }

    public void setFirstEyePressureDeltaFromPersonalAverage(double firstEyePressureDeltaFromPersonalAverage) {
        this.firstEyePressureDeltaFromPersonalAverage = firstEyePressureDeltaFromPersonalAverage;
    }

    public double getLastEyePressureDeltaFromPersonalAverage() {
        return lastEyePressureDeltaFromPersonalAverage;
    }

    public void setLastEyePressureDeltaFromPersonalAverage(double lastEyePressureDeltaFromPersonalAverage) {
        this.lastEyePressureDeltaFromPersonalAverage = lastEyePressureDeltaFromPersonalAverage;
    }

    public double getFirstEyePressureDeltaFromCohortAverage() {
        return firstEyePressureDeltaFromCohortAverage;
    }

    public void setFirstEyePressureDeltaFromCohortAverage(double firstEyePressureDeltaFromCohortAverage) {
        this.firstEyePressureDeltaFromCohortAverage = firstEyePressureDeltaFromCohortAverage;
    }

    public double getLastEyePressureDeltaFromCohortAverage() {
        return lastEyePressureDeltaFromCohortAverage;
    }

    public void setLastEyePressureDeltaFromCohortAverage(double lastEyePressureDeltaFromCohortAverage) {
        this.lastEyePressureDeltaFromCohortAverage = lastEyePressureDeltaFromCohortAverage;
    }

    public double getNumberOfEyePressureMeasurements() {
        return NumberOfEyePressureMeasurements;
    }

    public void setNumberOfEyePressureMeasurements(double NumberOfEyePressureMeasurements) {
        this.NumberOfEyePressureMeasurements = NumberOfEyePressureMeasurements;
    }

    public double getMinTimeBetweenEyePressureMeasurements() {
        return minTimeBetweenEyePressureMeasurements;
    }

    public void setMinTimeBetweenEyePressureMeasurements(double minTimeBetweenEyePressureMeasurements) {
        this.minTimeBetweenEyePressureMeasurements = minTimeBetweenEyePressureMeasurements;
    }

    public double getMaxTimeBetweenEyePressureMeasurements() {
        return maxTimeBetweenEyePressureMeasurements;
    }

    public void setMaxTimeBetweenEyePressureMeasurements(double maxTimeBetweenEyePressureMeasurements) {
        this.maxTimeBetweenEyePressureMeasurements = maxTimeBetweenEyePressureMeasurements;
    }

    public double getAverageTimeBetweenEyePressureMeasurements() {
        return averageTimeBetweenEyePressureMeasurements;
    }

    public void setAverageTimeBetweenEyePressureMeasurements(double averageTimeBetweenEyePressureMeasurements) {
        this.averageTimeBetweenEyePressureMeasurements = averageTimeBetweenEyePressureMeasurements;
    }

    public double getNumberOfInjections() {
        return numberOfInjections;
    }

    public void setNumberOfInjections(double numberOfInjections) {
        this.numberOfInjections = numberOfInjections;
    }

    public double getAverageTimeBetweenInjections() {
        return averageTimeBetweenInjections;
    }

    public void setAverageTimeBetweenInjections(double averageTimeBetweenInjections) {
        this.averageTimeBetweenInjections = averageTimeBetweenInjections;
    }

    public double getDifferenceToAverageInjectionNumber() {
        return differenceToAverageInjectionNumber;
    }

    public void setDifferenceToAverageInjectionNumber(double differenceToAverageInjectionNumber) {
        this.differenceToAverageInjectionNumber = differenceToAverageInjectionNumber;
    }

    public double getNumberOfMedications() {
        return numberOfMedications;
    }

    public void setNumberOfMedications(double numberOfMedications) {
        this.numberOfMedications = numberOfMedications;
    }

    public String getFirstMedication() {
        return firstMedication;
    }

    public void setFirstMedication(String firstMedication) {
        this.firstMedication = firstMedication;
    }

    public String getLastMedication() {
        return lastMedication;
    }

    public void setLastMedication(String lastMedication) {
        this.lastMedication = lastMedication;
    }

    public double getAverageVisusImprovementAfterInjection() {
        return averageVisusImprovementAfterInjection;
    }

    public void setAverageVisusImprovementAfterInjection(double averageVisusImprovementAfterInjection) {
        this.averageVisusImprovementAfterInjection = averageVisusImprovementAfterInjection;
    }

    public double getMinVisusImprovementAfterInjection() {
        return minVisusImprovementAfterInjection;
    }

    public void setMinVisusImprovementAfterInjection(double minVisusImprovementAfterInjection) {
        this.minVisusImprovementAfterInjection = minVisusImprovementAfterInjection;
    }

    public double getMaxVisusImprovementAfterInjection() {
        return maxVisusImprovementAfterInjection;
    }

    public void setMaxVisusImprovementAfterInjection(double maxVisusImprovementAfterInjection) {
        this.maxVisusImprovementAfterInjection = maxVisusImprovementAfterInjection;
    }

    public double getVisusImprovementsRateAfterInjection() {
        return visusImprovementsRateAfterInjection;
    }

    public void setVisusImprovementsRateAfterInjection(double visusImprovementsRateAfterInjection) {
        this.visusImprovementsRateAfterInjection = visusImprovementsRateAfterInjection;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public final void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    // Überladene Funktion für getValueFromFields, mit der Standardoption, dass der Attributname in den Rückgabewert aufgenommen wird
    public String getValueFromFields(List<Field> fields, Patient myDatum) {
        return getValueFromFields(fields, myDatum, true);
    }

    // Überladene Funktion für getValueFromFields, mit einem Feld, statt einer Feldliste
    @SuppressWarnings("unchecked")
    public String getValueFromFields(Field field, Patient myDatum) {
        List<Field> fields = new ArrayList();
        fields.add(field);
        return getValueFromFields(fields, myDatum, false);
    }

    // Überladene Funktion für getValueFromFields, mit einem Feld, statt einer Feldliste und mit Auswahl zur Übernahme des Attributnamen
    @SuppressWarnings("unchecked")
    public String getValueFromFields(Field field, Patient myDatum, boolean addAttName) {
        List<Field> fields = new ArrayList();
        fields.add(field);
        return getValueFromFields(fields, myDatum, addAttName);
    }

    // Funktion, um zu einem Field das entsprechende Element in einen Datensatz zu finden
    @SuppressWarnings("unchecked")
    public String getValueFromFields(List<Field> fields, Patient myDatum, boolean addAttName) {

        String returnString = "";
        for (Field field : fields) {

            String fieldType = field.getType().toString();
            String[] possibleTypes = {"IntegerProperty", "StringProperty", "DoubleProperty", "BooleanProperty", "ObjectProperty"};
            String key = "";

            for (String possType : possibleTypes) {
                if (fieldType.contains(possType)) {
                    key = possType;
                }
            }

            switch (key) {
                case "IntegerProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((IntegerProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((IntegerProperty) field.get(myDatum)).get());
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println("IntegerProperty Error: " + e.getMessage());
                    }
                    break;

                case "StringProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((StringProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((StringProperty) field.get(myDatum)).get());

                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println("StringProperty Error: " + e.getMessage());
                    }
                    break;

                case "DoubleProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((DoubleProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((DoubleProperty) field.get(myDatum)).get());
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println("ObjectProperty error:" + e.getMessage());
                    }
                    break;

                case "BooleanProperty":

                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((BooleanProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((BooleanProperty) field.get(myDatum)).get());
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println("BooleanProperty Error: " + e.getMessage());
                    }
                    break;

                case "ObjectProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((ObjectProperty<?>) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((ObjectProperty<?>) field.get(myDatum)).get());

                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println("ObjectProperty error:" + e.getMessage());
                    }
                    break;

                default:
                    //System.out.println(field.getName());
                    returnString = returnString + "_";
            } // switch key
        } // forEach field

        //System.out.println(returnString);
        return (returnString.length() > 1) ? returnString.substring(1) : returnString;

    } // procedure getValueFromField

    // für die Klasse Annotations
    // Überladene Funktion für getValueFromFields, mit der Standardoption, dass der Attributname in den Rückgabewert aufgenommen wird
    public String getValueFromFields(List<Field> fields, Annotations myDatum) {
        return getValueFromFields(fields, myDatum, true);
    }

    // Überladene Funktion für getValueFromFields, mit einem Feld, statt einer Feldliste
    @SuppressWarnings("unchecked")
    public String getValueFromFields(Field field, Annotations myDatum) {
        List<Field> fields = new ArrayList();
        fields.add(field);
        return getValueFromFields(fields, myDatum, false);
    }

    // Überladene Funktion für getValueFromFields, mit einem Feld, statt einer Feldliste und mit Auswahl zur Übernahme des Attributnamen
    @SuppressWarnings("unchecked")
    public String getValueFromFields(Field field, Annotations myDatum, boolean addAttName) {
        List<Field> fields = new ArrayList();
        fields.add(field);
        return getValueFromFields(fields, myDatum, addAttName);
    }

    // Funktion, um zu einem Field das entsprechende Element in einen Datensatz zu finden
    public String getValueFromFields(List<Field> fields, Annotations myDatum, boolean addAttName) {

        String returnString = "";
        for (Field field : fields) {

            String fieldType = field.getType().toString();
            String[] possibleTypes = {"IntegerProperty", "StringProperty", "DoubleProperty", "BooleanProperty", "ObjectProperty"};
            String key = "";

            for (String possType : possibleTypes) {
                if (fieldType.contains(possType)) {
                    key = possType;
                }
            }

            switch (key) {
                case "IntegerProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((IntegerProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((IntegerProperty) field.get(myDatum)).get());
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println("IntegerProperty Error: " + e.getMessage());
                    }
                    break;

                case "StringProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((StringProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((StringProperty) field.get(myDatum)).get());

                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println("StringProperty Error: " + e.getMessage());
                    }
                    break;

                case "DoubleProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((DoubleProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((DoubleProperty) field.get(myDatum)).get());
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println("ObjectProperty error:" + e.getMessage());
                    }
                    break;

                case "BooleanProperty":

                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((BooleanProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((BooleanProperty) field.get(myDatum)).get());
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println("BooleanProperty Error: " + e.getMessage());
                    }
                    break;

                case "ObjectProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((ObjectProperty<?>) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((ObjectProperty<?>) field.get(myDatum)).get());

                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println("ObjectProperty error:" + e.getMessage());
                    }
                    break;

                default:
                    //System.out.println(field.getName());
                    returnString = returnString + "_";
            } // switch key
        } // forEach field

        //System.out.println(returnString);
        return (returnString.length() > 1) ? returnString.substring(1) : returnString;

    } // procedure getValueFromField

    // Generate a list of TreeViewItems, from a list or map variable
//    @SuppressWarnings("unchecked")
//    public List<CheckBoxTreeItem> getTreeItemsfromField(String fieldName, String patientName, String annoName, ObservableMap<String, Patient> patients, AnnotationModel.FieldClass fieldClass) {
//
//        List<CheckBoxTreeItem> result = new ArrayList();
//        List<String> checkBoxNames = new ArrayList();
//        try {
//            switch (fieldClass) {
//                case GROUPDATAMODEL:
//                    Field groupField = GroupDataModel.class.getDeclaredField(fieldName);
//                    try {
//                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
//                        if (groupField.getType().toString().endsWith("ObservableMap")) {
//                            ObservableMap<?, ?> myObs;
//                            myObs = ((ObservableMap<?, ?>) groupField.get(myAnnoGroup));
//                            myObs.forEach((id, value) -> {
//                                checkBoxNames.add("" + id.toString());
//                            });
//                        } else if (groupField.getType().toString().endsWith("util.List")) {
//                            ArrayList<?> myList;
//                            myList = ((ArrayList<?>) groupField.get(myAnnoGroup));
//                            myList.forEach((value) -> {
//                                checkBoxNames.add("" + value.toString());
//                            });
//                        }
//
//                    } catch (IllegalAccessException | IllegalArgumentException e) {
//                        System.out.println(CLASSNAME + " - GroupDataModel error:" + e.getMessage());
//                    }
//                    break;
//                case ANNOTATIONMODEL:
//                    Field annoModelField = AnnotationModel.class.getDeclaredField(fieldName);
//                    try {
//                        if (annoModelField.getType().toString().endsWith("ObservableMap")) {
//                            // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
//                            ObservableMap<?, ?> myObs;
//                            myObs = ((ObservableMap<?, ?>) annoModelField.get(myAnnoGroup.getObservableAnnotations().get(patientName)));
//                            myObs.forEach((id, value) -> {
//                                checkBoxNames.add("" + id.toString() + ": " + value);
//                            });
//                        } else if (annoModelField.getType().toString().endsWith("util.List")) {
//                            ArrayList<?> myList;
//                            myList = ((ArrayList<?>) annoModelField.get(myAnnoGroup.getObservableAnnotations().get(patientName)));
//                            myList.forEach((value) -> {
//                                checkBoxNames.add("" + value.toString());
//                            });
//                        }
//
//                    } catch (IllegalAccessException | IllegalArgumentException e) {
//                        System.out.println(CLASSNAME + " - AnnotationModel error: " + e.getMessage());
//                    }
//                    break;
//                case ANNOTATIONS:
//                    Field annoField = Annotations.class.getDeclaredField(fieldName);
//                    try {
//                        if (annoField.getType().toString().endsWith("ObservableMap")) {
//                            // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
//                            ObservableMap<?, ?> myObs;
//                            myObs = ((ObservableMap<?, ?>) annoField.get(((AnnotationModel) myAnnoGroup.getObservableAnnotations().get(patientName)).getMyAnnotations().get(annoName)));
//                            myObs.forEach((id, value) -> {
//                                checkBoxNames.add("" + id.toString() + ": " + value);
//                            });
//                        } else if (annoField.getType().toString().endsWith("util.List")) {
//                            ArrayList<?> myList;
//                            myList = ((ArrayList<?>) annoField.get(((AnnotationModel) myAnnoGroup.getObservableAnnotations().get(patientName)).getMyAnnotations().get(annoName)));
//                            myList.forEach((value) -> {
//                                checkBoxNames.add("" + value.toString());
//                            });
//                        }
//                    } catch (IllegalAccessException | IllegalArgumentException e) {
//                        System.out.println(CLASSNAME + " - Annotations error:" + e.getMessage());
//                    }
//                    break;
//                default:
////                    field = DataModel.class.getDeclaredField(fieldName);
//            }
//
//        } catch (NoSuchFieldException | SecurityException ex) {
//            Logger.getLogger(GroupDataModel.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        checkBoxNames.sort(Comparator.naturalOrder());
//        checkBoxNames.forEach((checkName) -> {
//            result.add(new CheckBoxTreeItem(checkName));
//        });
//        return result;
//    } // procedure getTreeViewItems from Field
    public static List<CheckBoxTreeItem> TreeViewHelper(Patient patient) {

        /**
         * selected indicates, if the patient is selected for visualization
         */
        List<CheckBoxTreeItem> helperList = new ArrayList();

//    private final BooleanProperty selected = new SimpleBooleanProperty();
//    private String patientID;
//    private AugeLateralitaet lateralitaet;
//    private double ageAtBeginning;
        helperList.add(new CheckBoxTreeItem("Alter zu Beginn: " + patient.getAgeAtBeginning()));
//    private final DoubleProperty durationOfTreatment = new SimpleDoubleProperty();
//        helperList.add(new CheckBoxTreeItem("Dauer der Behandlung: " + patient.getDurationOfTreatment()));
//    private double differenceToCohortAverageAge;
//    private Geschlecht sex;
        helperList.add(new CheckBoxTreeItem("Geschlecht: " + patient.getSex()));
//    private double numberOfDoctors;
//    private LocalDate dateOfBirth;
//
//    private String firstDiagnosis;
//        helperList.add(new CheckBoxTreeItem("Erste Hauptdiagnose: " + patient.getFirstDiagnosis()));
//    private String lastDiagnosis;
//        helperList.add(new CheckBoxTreeItem("Letzte Hauptdiagnose: " + patient.getLastDiagnosis()));
//    private LocalDate firstVisusMeasurement;
        helperList.add(new CheckBoxTreeItem("Erster Visus: " + patient.getFirstVisus()));
//    private LocalDate lastVisusMeasurement;
        helperList.add(new CheckBoxTreeItem("Letzter Visus: " + patient.getLastVisus()));
//    private LocalDate firstInjection;
//    private LocalDate lastInjection;
//    private LocalDate beginMonitoring;
//    private LocalDate endMonitoring;
//
//    private double firstVisus;
//    private double lastVisus;
//    private double minVisus;
//    private double maxVisus;
//    private double meanVisus;
//    private double visusDelta;
//    private double visBeforeFirstInj;
//    private double visAfterLastInjection;
//    private double firstVisusDeltaFromPersonalAverage;
//    private double lastVisusDeltaFromPersonalAverage;
//    private double firstVisusDeltaFromCohortAverage;
//    private double lastVisusDeltaFromCohortAverage;
//    private double numberOfVisusMeasurements;
//    private double minTimeBetweenVisusMeasurements;
//    private double maxTimeBetweenVisusMeasurements;
//    private double averageTimeBetweenVisusMeasurements;
//
//    private double minVisClass;
//    private double maxVisClass;
//    private double minLogMarVis;
//    private double maxLogMarVis;
//    private double minVisDelta;
//    private double maxVisDelta;
//    private double minVisClassDelta;
//    private double maxVisClassDelta;
//    private double minLogMarVisDelta;
//    private double maxLogMarVisDelta;
//
//    private double firstEyePressure;
//    private double lastEyePressure;
//    private double minEyePressure;
//    private double maxEyePressure;
//    private double meanEyePressure;
//    private double firstEyePressureDeltaFromPersonalAverage;
//    private double lastEyePressureDeltaFromPersonalAverage;
//    private double firstEyePressureDeltaFromCohortAverage;
//    private double lastEyePressureDeltaFromCohortAverage;
//    private double NumberOfEyePressureMeasurements;
//    private double minTimeBetweenEyePressureMeasurements;
//    private double maxTimeBetweenEyePressureMeasurements;
//    private double averageTimeBetweenEyePressureMeasurements;
//
//    private double numberOfInjections;
//    private double minTimeBetweenInjections;
//    private double maxTimeBetweenInjections;
//    private double averageTimeBetweenInjections;
//    private double differenceToAverageInjectionNumber;
//    private double lengthOfMonitoring;
//
//    private double numberOfMedications;
//    private String firstMedication;
//    private String lastMedication;
//
//    private double averageVisusImprovementAfterInjection;
//    private double minVisusImprovementAfterInjection;
//    private double maxVisusImprovementAfterInjection;
//    private double visusImprovementsRateAfterInjection;
//
//    private double linearRegressionSlope;
//    private double linearRegressionIntercept;
//
//    private EyeTimePanel eyeTimePanel;
//    private final DoubleProperty xOffset = new SimpleDoubleProperty();
//
//    private final ObservableMap<LocalDate, Double> visen = FXCollections.observableHashMap();
        CheckBoxTreeItem visusWerte = new CheckBoxTreeItem("Dezimalvisuswerte");
        List<LocalDate> sortedDates = new ArrayList();
        patient.getVisen().keySet().forEach(key -> sortedDates.add(key));
        sortedDates.sort(Comparator.naturalOrder());
        sortedDates.forEach(date -> {

            visusWerte.getChildren().add(new CheckBoxTreeItem("" + date.toString() + ": " + patient.getVisen().get(date)));
        });
        helperList.add(visusWerte);

//    private final ObservableMap<LocalDate, Double> visenFromLetterExtract = FXCollections.observableHashMap();
//    private final ObservableMap<LocalDate, Double> visenLogMar = FXCollections.observableHashMap();
//    private final ObservableMap<LocalDate, Double> augendruecke = FXCollections.observableHashMap();
//    private final ObservableMap<LocalDate, DataModel.AntiVEGFMedikament> spritzen = FXCollections.observableHashMap();
        CheckBoxTreeItem spritzen = new CheckBoxTreeItem("Injektionen");
        sortedDates.clear();
        patient.getSpritzen().keySet().forEach(key -> sortedDates.add(key));
        sortedDates.sort(Comparator.naturalOrder());
        sortedDates.forEach(date -> {

            spritzen.getChildren().add(new CheckBoxTreeItem("" + date.toString() + ": " + patient.getSpritzen().get(date)));
        });
        helperList.add(spritzen);

//    private final ObservableMap<Period, Double> visusDelten = FXCollections.observableHashMap();
//    private final ObservableMap<LocalDate, Double> visClasses = FXCollections.observableHashMap();
//    private final ObservableMap<Period, Double> visusLogMarDelten = FXCollections.observableHashMap();
//    private final ObservableMap<Period, Double> visusClassDelten = FXCollections.observableHashMap();
//
//    private final ObservableMap<LocalDate, Period> visPausen = FXCollections.observableHashMap();
//    private final ObservableMap<LocalDate, Double> visMeasurementsSinceLastInjection = FXCollections.observableHashMap();
//
//    private final ObservableMap<LocalDate, Period> spritzPausen = FXCollections.observableHashMap();
//    private final ObservableMap<String, Annotations> myAnnotations = FXCollections.observableHashMap();
//
//    private final ObservableMap<LocalDate, String> diagnoses = FXCollections.observableHashMap();
//    private final ObservableMap<String, MedicationChange> myMedicationChanges = FXCollections.observableHashMap();
        return helperList;

    }

}
