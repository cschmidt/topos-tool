/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import java.time.LocalDate;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

/**
 *
 * @author lokal-admin
 */
public class PatientStructure {

    public enum Geschlecht {
        M,
        W,
        U,
        E;
    }

    public enum MainDiagnoses {
        E,
        UNKNOWN,
        AMD_FEUCHT,
        AMD_TROCKEN,
        AMD_ALLGEMEIN,
        DMOE,
        GEFAESSVERSCHLUSS,
        ZVV,
        VAV;
    }

    public enum DataSource {
        PDV_VERSCHLUESSELUNGEN,
        ARGOS_TL,
        OP_BERICHT_EXTRACT,
        LETTER_EXTRACT;
    }

    public enum AntiVEGFMedication {
        E,
        UNKNOWN,
        AVASTIN, //(Bevacizumab)
        LUCENTIS, //(Ranibizumab),
        EYLEA, //(Aflibercept),
        OZURDEX, //(Dexametason),
        TRIAMCINOLON;
    }

    public enum VisCategory {
        E,
        UNKNOWN,
        NULLA_LUX,
        LICHTSCHEIN,
        HANDBEWEGUNGEN,
        FINGERZAEHLEN,
        TAFELVISUS,
        DEZIMALVISUS;
    }

    public class Eye {

        ObservableMap<LocalDate, Incident> incidents = FXCollections.observableHashMap();

        public class Incident {

            ObservableMap<DataSource, Diagnosis> diagnosis = FXCollections.observableHashMap();

            public class Diagnosis {

                private final ObjectProperty<MainDiagnoses> mainDiagnosis = new SimpleObjectProperty<>(MainDiagnoses.E);
                private final BooleanProperty hasDiabetis = new SimpleBooleanProperty();
                ObservableList<String> sideDiagnosis = FXCollections.observableArrayList();

                public ObservableList<String> getSideDiagnosis() {
                    return sideDiagnosis;
                }

                public void setSideDiagnosis(ObservableList<String> sideDiagnosis) {
                    this.sideDiagnosis = sideDiagnosis;
                }

                public boolean isHasDiabetis() {
                    return hasDiabetis.get();
                }

                public void setHasDiabetis(boolean value) {
                    hasDiabetis.set(value);
                }

                public BooleanProperty hasDiabetisProperty() {
                    return hasDiabetis;
                }

                public MainDiagnoses getMainDiagnosis() {
                    return mainDiagnosis.get();
                }

                public void setMainDiagnosis(MainDiagnoses value) {
                    mainDiagnosis.set(value);
                }

                public ObjectProperty mainDiagnosisProperty() {
                    return mainDiagnosis;
                }

            }

            ObservableMap<DataSource, Pathology> pathology = FXCollections.observableHashMap();

            public class Pathology {

                private final DoubleProperty eyePressure = new SimpleDoubleProperty();
                private final StringProperty eyePressureDelta = new SimpleStringProperty();
                private final DoubleProperty logMarVisualAcuity = new SimpleDoubleProperty();
                private final StringProperty decimalVisualAcuity = new SimpleStringProperty();
                private final StringProperty rawVisValue = new SimpleStringProperty();
                private final ObjectProperty<VisCategory> visCategory = new SimpleObjectProperty<>();

                public VisCategory getVisCategory() {
                    return visCategory.get();
                }

                public void setVisCategory(VisCategory value) {
                    visCategory.set(value);
                }

                public ObjectProperty visCategoryProperty() {
                    return visCategory;
                }

                public String getRawVisValue() {
                    return rawVisValue.get();
                }

                public void setRawVisValue(String value) {
                    rawVisValue.set(value);
                }

                public StringProperty rawVisValueProperty() {
                    return rawVisValue;
                }

                public String getDecimalVisualAcuity() {
                    return decimalVisualAcuity.get();
                }

                public void setDecimalVisualAcuity(String value) {
                    decimalVisualAcuity.set(value);
                }

                public StringProperty decimalVisualAcuityProperty() {
                    return decimalVisualAcuity;
                }

                public double getLogMarVisualAcuity() {
                    return logMarVisualAcuity.get();
                }

                public void setLogMarVisualAcuity(double value) {
                    logMarVisualAcuity.set(value);
                }

                public DoubleProperty logMarVisualAcuityProperty() {
                    return logMarVisualAcuity;
                }

                public String getEyePressureDelta() {
                    return eyePressureDelta.get();
                }

                public void setEyePressureDelta(String value) {
                    eyePressureDelta.set(value);
                }

                public StringProperty eyePressureDeltaProperty() {
                    return eyePressureDelta;
                }

                public double getEyePressure() {
                    return eyePressure.get();
                }

                public void setEyePressure(double value) {
                    eyePressure.set(value);
                }

                public DoubleProperty eyePressureProperty() {
                    return eyePressure;
                }

            }

            ObservableMap<DataSource, Injection> injection = FXCollections.observableHashMap();

            public class Injection {

                private final ObjectProperty<AntiVEGFMedication> antiVEGFMedication = new SimpleObjectProperty<>();

                public AntiVEGFMedication getAntiVEGFMedication() {
                    return antiVEGFMedication.get();
                }

                public void setAntiVEGFMedication(AntiVEGFMedication value) {
                    antiVEGFMedication.set(value);
                }

                public ObjectProperty antiVEGFMedicationProperty() {
                    return antiVEGFMedication;
                }

            }
        }

        public ObservableMap<LocalDate, Incident> getIncidents() {
            return incidents;
        }

        public void setIncidents(ObservableMap<LocalDate, Incident> incidents) {
            this.incidents = incidents;
        }

    }

    //StringProperty P_ID = new SimpleStringProperty();
    private final StringProperty p_ID = new SimpleStringProperty("");
    private final ObjectProperty<Geschlecht> sex = new SimpleObjectProperty<>(Geschlecht.E);
    private final ObjectProperty<LocalDate> dateOfBirth = new SimpleObjectProperty<>(LocalDate.MIN);
    private final Eye leftEye = new Eye();
    private final Eye rightEye = new Eye();

    public LocalDate getDateOfBirth() {
        return dateOfBirth.get();
    }

    public void setDateOfBirth(LocalDate value) {
        dateOfBirth.set(value);
    }

    public ObjectProperty dateOfBirthProperty() {
        return dateOfBirth;
    }

    public Geschlecht getSex() {
        return sex.get();
    }

    public void setSex(Geschlecht value) {
        sex.set(value);
    }

    public ObjectProperty sexProperty() {
        return sex;
    }

    public String getP_ID() {
        return p_ID.get();
    }

    public void setP_ID(String value) {
        p_ID.set(value);
    }

    public StringProperty p_IDProperty() {
        return p_ID;
    }

}
