/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import java.time.LocalDate;
import va_topos.data.DataModel.AntiVEGFMedikament;

/**
 *
 * @author lokal-admin
 */
public class MedicationChange {

    private LocalDate oldInjDate;
    private LocalDate newInjDate;
    private AntiVEGFMedikament oldMed;
    private AntiVEGFMedikament newMed;

    private LocalDate oldVisDate;
    private LocalDate newVisDate;
    private double oldVisualAcuity;
    private double newVisualAcuity;

    private double oldVisualAcuityLetter;
    private double newVisualAcuityLetter;

    private double visChange;
    private double visChangeLetter;
    
    private String patientID;
    private String diagnosis;

    public MedicationChange(
            LocalDate oldInjDate, LocalDate newInjDate,
            AntiVEGFMedikament oldMed, AntiVEGFMedikament newMed,
            LocalDate oldVisDate, LocalDate newVisDate,
            double oldVisualAcuity, double newVisualAcuity, double visChange, double oldVisualAcuityLetter, double newVisualAcuityLetter, double visChangeLetter,
            String patientID, String diagnosis) {

        this.oldInjDate = oldInjDate;
        this.newInjDate = newInjDate;
        this.oldMed = oldMed;
        this.newMed = newMed;

        this.oldVisDate = oldVisDate;
        this.newVisDate = newVisDate;
        this.oldVisualAcuity = oldVisualAcuity;
        this.newVisualAcuity = newVisualAcuity;
        this.oldVisualAcuityLetter = oldVisualAcuityLetter;
        this.newVisualAcuityLetter = newVisualAcuityLetter;

        this.visChange = visChange;
        this.visChangeLetter = visChangeLetter;

        this.patientID = patientID;
        this.diagnosis = diagnosis;
    }

    public double getVisChangeLetter() {
        return visChangeLetter;
    }

    public void setVisChangeLetter(double visChangeLetter) {
        this.visChangeLetter = visChangeLetter;
    }
    

    public double getOldVisualAcuityLetter() {
        return oldVisualAcuityLetter;
    }

    public void setOldVisualAcuityLetter(double oldVisualAcuityLetter) {
        this.oldVisualAcuityLetter = oldVisualAcuityLetter;
    }

    public double getNewVisualAcuityLetter() {
        return newVisualAcuityLetter;
    }

    public void setNewVisualAcuityLetter(double newVisualAcuityLetter) {
        this.newVisualAcuityLetter = newVisualAcuityLetter;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public AntiVEGFMedikament getOldMed() {
        return oldMed;
    }

    public void setOldMed(AntiVEGFMedikament oldMed) {
        this.oldMed = oldMed;
    }

    public AntiVEGFMedikament getNewMed() {
        return newMed;
    }

    public void setNewMed(AntiVEGFMedikament newMed) {
        this.newMed = newMed;
    }

    public LocalDate getOldInjDate() {
        return oldInjDate;
    }

    public void setOldInjDate(LocalDate oldInjDate) {
        this.oldInjDate = oldInjDate;
    }

    public LocalDate getOldVisDate() {
        return oldVisDate;
    }

    public void setOldVisDate(LocalDate oldVisDate) {
        this.oldVisDate = oldVisDate;
    }

    public LocalDate getNewInjDate() {
        return newInjDate;
    }

    public void setNewInjDate(LocalDate newInjDate) {
        this.newInjDate = newInjDate;
    }

    public LocalDate getNewVisDate() {
        return newVisDate;
    }

    public void setNewVisDate(LocalDate newVisDate) {
        this.newVisDate = newVisDate;
    }

    public double getOldVisualAcuity() {
        return oldVisualAcuity;
    }

    public void setOldVisualAcuity(double oldVisualAcuity) {
        this.oldVisualAcuity = oldVisualAcuity;
    }

    public double getNewVisualAcuity() {
        return newVisualAcuity;
    }

    public void setNewVisualAcuity(double newVisualAcuity) {
        this.newVisualAcuity = newVisualAcuity;
    }

    public double getVisChange() {
        return visChange;
    }

    public void setVisChange(double visChange) {
        this.visChange = visChange;
    }

}
