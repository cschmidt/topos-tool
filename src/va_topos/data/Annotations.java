/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import java.time.LocalDate;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

/**
 *
 * @author lokal-admin
 */
@SuppressWarnings("unchecked")
public class Annotations {

    private enum What {
        CATEGORY,
        FREETEXT,
        GRAPHICALITEM,
        PROVENANCEINFORMATION;
    }
    private final ObjectProperty<What> whatAnnotation = new SimpleObjectProperty();

    private enum Why {
        DATAANNOTATION,
        USERANNOTATION,
        OUTCOMEANNOTATION;
    }
    private final ObjectProperty<Why> whyAnnotation = new SimpleObjectProperty();

    private enum HowGather {
        ALPHANUMERICALINPUT,
        SCREENSHOT,
        MARK,
        SELECTUNDBRUSH,
        AUTOMATIC;
    }
    private final ObjectProperty<HowGather> gatherAnnotation = new SimpleObjectProperty();

    private enum HowVisualize {
        VISUALSEPARATION,
        LAYEREDVISUALIZATION,
        VISUALENCODING;
    }
    private final ObjectProperty<HowVisualize> viusalizeAnnotation = new SimpleObjectProperty();

    private String autor;
    private LocalDate timeStamp;
    private double certainty;
    private int confirms;
    private ObservableMap<String, Object> content = FXCollections.observableHashMap();
    private ObservableMap<String, Object> relatedData = FXCollections.observableHashMap();

    public Annotations(String autor, LocalDate timeStamp, ObservableMap<String, Object> content) {
        this.autor = autor;
        this.timeStamp = timeStamp;
        this.certainty = 0d;
        this.confirms = 0;
        this.content = content;
    }

    public Annotations(String autor, LocalDate timeStamp, double certainty, int confirms, ObservableMap<String, Object> content) {
        this.autor = autor;
        this.timeStamp = timeStamp;
        this.certainty = certainty;
        this.confirms = confirms;
        this.content = content;
    }

    public int getConfirms() {
        return confirms;
    }

    public void setConfirms(int confirms) {
        this.confirms = confirms;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public LocalDate getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDate timeStamp) {
        this.timeStamp = timeStamp;
    }

    public double getCertainty() {
        return certainty;
    }

    public void setCertainty(double certainty) {
        this.certainty = certainty;
    }

    public ObservableMap<String, Object> getContent() {
        return content;
    }

    public void setContent(ObservableMap<String, Object> content) {
        this.content = content;
    }

    public ObservableMap<String, Object> getRelatedData() {
        return relatedData;
    }

    public void setRelatedData(ObservableMap<String, Object> relatedData) {
        this.relatedData = relatedData;
    }

    
}
