/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import javafx.collections.ObservableList;

/**
 *
 * @author Christian Schudt
 * @param <T>
 */
public interface HierarchyData<T extends HierarchyData> {

    /**
     * The children collection, which represents the recursive nature of the
     * hierarchy. Each child is again a {@link HierarchyData}.
     *
     * @return A list of children.
     */
    ObservableList<T> getChildren();
}
