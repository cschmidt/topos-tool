/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lokal-admin
 */
public class SerializableList implements Serializable {

    private List<DataModelNoProperty> data = new ArrayList();

    public SerializableList(List<DataModelNoProperty> data) {
        data.forEach(datum -> this.data.add(datum));
    }

    public List<DataModelNoProperty> getData() {
        return data;
    }

    public void setData(List<DataModelNoProperty> data) {
        this.data = data;
    }

}
