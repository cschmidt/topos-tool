/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

/**
 *
 * @author lokal-admin
 */
public class MasterPatient extends Patient {

    private double minLinearRegressionSlope;
    private double maxLinearRegressionSlope;
    private double minMeanEyePressure;
    private double maxMeanEyePressure;
    private double minMedianEyePressure;
    private double maxMedianEyePressure;
    private double minMeanVisus;
    private double maxMeanVisus;
    private double minMedianVisus;
    private double maxMedianVisus;

    public MasterPatient(DataModel incident, boolean initialize) {
        super(incident, initialize);
        if (initialize) {
            this.minLinearRegressionSlope = Double.NaN;
            this.maxLinearRegressionSlope = Double.NaN;
            this.minMeanEyePressure = Double.NaN;
            this.maxMeanEyePressure = Double.NaN;
            this.minMedianEyePressure = Double.NaN;
            this.maxMedianEyePressure = Double.NaN;
            this.minMeanVisus = Double.NaN;
            this.maxMeanVisus = Double.NaN;
            this.minMedianVisus = Double.NaN;
            this.maxMedianVisus = Double.NaN;

        }
    }

    public double getMinMeanEyePressure() {
        return minMeanEyePressure;
    }

    public void setMinMeanEyePressure(double minMeanEyePressure) {
        this.minMeanEyePressure = minMeanEyePressure;
    }

    public double getMaxMeanEyePressure() {
        return maxMeanEyePressure;
    }

    public void setMaxMeanEyePressure(double maxMeanEyePressure) {
        this.maxMeanEyePressure = maxMeanEyePressure;
    }

    public double getMinMedianEyePressure() {
        return minMedianEyePressure;
    }

    public void setMinMedianEyePressure(double minMedianEyePressure) {
        this.minMedianEyePressure = minMedianEyePressure;
    }

    public double getMaxMedianEyePressure() {
        return maxMedianEyePressure;
    }

    public void setMaxMedianEyePressure(double maxMedianEyePressure) {
        this.maxMedianEyePressure = maxMedianEyePressure;
    }

    public double getMinMeanVisus() {
        return minMeanVisus;
    }

    public void setMinMeanVisus(double minMeanVisus) {
        this.minMeanVisus = minMeanVisus;
    }

    public double getMaxMeanVisus() {
        return maxMeanVisus;
    }

    public void setMaxMeanVisus(double maxMeanVisus) {
        this.maxMeanVisus = maxMeanVisus;
    }

    public double getMinMedianVisus() {
        return minMedianVisus;
    }

    public void setMinMedianVisus(double minMedianVisus) {
        this.minMedianVisus = minMedianVisus;
    }

    public double getMaxMedianVisus() {
        return maxMedianVisus;
    }

    public void setMaxMedianVisus(double maxMedianVisus) {
        this.maxMedianVisus = maxMedianVisus;
    }

    public double getMinLinearRegressionSlope() {
        return minLinearRegressionSlope;
    }

    public void setMinLinearRegressionSlope(double minLinearRegressionSlope) {
        this.minLinearRegressionSlope = minLinearRegressionSlope;
    }

    public double getMaxLinearRegressionSlope() {
        return maxLinearRegressionSlope;
    }

    public void setMaxLinearRegressionSlope(double maxLinearRegressionSlope) {
        this.maxLinearRegressionSlope = maxLinearRegressionSlope;
    }

}
