/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;

/**
 *
 * @author lokal-admin
 */
public class DataModelNoProperty implements Serializable {

    public enum DataTypes {
        TEXT,
        NUMBER,
        DATE,
        BOOLEAN,
        OTHER;
    }

    // Patient
    public enum Geschlecht {
        m,
        w,
        i,
        u;
    }
    private String patientenID = "";
    private LocalDate geburtsdatum = LocalDate.MIN;
    private String name = "";
//    private String vorname = "";
    private Geschlecht geschlecht = Geschlecht.u;

    // Spritze
    public enum AntiVEGFMedikament {
        E,
        UNKNOWN,
        AVASTIN, //(Bevacizumab)
        LUCENTIS, //(Ranibizumab),
        EYLEA, //(Aflibercept),
        OZURDEX, //(Dexametason),
        TRIAMCINOLON;
    }

    public enum AugeLateralitaet {
        E,
        UNKNOWN,
        LEFT,
        RIGHT;
    }
    private String spritzenQuelle = "";
//    private LocalDate spritzenDatum = new     //private DataModel.AugeLateralitaet spritzeLateralitaet = new ct();
    private int vergangeneTage = 0;
    private AntiVEGFMedikament medikament = AntiVEGFMedikament.E;
    private Boolean diskrepanz = false;
    private LocalDate naechstesVisusDatum = LocalDate.MIN;
    private Double logMarVisusWert = 0d;

    // Untersuchung
    private String untersuchungsID = "";
//    private LocalDate untersuchungsDatum = new     private LocalDate ereignisDatum = new ();
    private LocalDate ereignisDatum = LocalDate.MIN;

    private String untersuchungsCode = "";
    private String briefDaten = "";
    private String annotationText = "";

    // Auge
    private String augenID = "";

    private AugeLateralitaet untersuchungLateralitaet = AugeLateralitaet.E;
    private Double augendruck = 0d;
    private Double augendruckFromLetterExtract = 0d;

    // Visus
    public enum Visuswerte {
        E,
        UNKNOWN,
        NULLA_LUX,
        LICHTSCHEIN,
        HANDBEWEGUNGEN,
        FINGERZAEHLEN,
        TAFELVISUS,
        DEZIMALVISUS;
    }

    public enum VisusKorrektur {
        E,
        UNKNOWN,
        AR,
        EB,
        KL;
    }

    private String visusID = "";
    private String rohWert = "";
    private Visuswerte visuswerte = Visuswerte.E;
    private Double dezimalVisusWert = 0d;
    private Double visusDelta = 0d;
    private Double visusKlasse = 0d;
    private Double visusKlasseDelta = 0d;
    private Period zeitSeitLetztemVisus = Period.ZERO;

    private Boolean meterVisus = false;
    private VisusKorrektur visusKorrektur = VisusKorrektur.E;
    private Double sphaere = 0d;
    private Double zylinder = 0d;
    private Double achse = 0d;
    private Boolean lochblende = false;
    private Boolean ar_nicht_moeglich = false;
//    private LocalDate visusDatum = new     
    private int daysPast = 0;
    private Boolean argosEreignis = false;
    private Boolean argosEreignisRelevanz = false;
    private String argosEreignisText = "";

    // Vorderer Augenabschnitt
    private String vaaID = "";
    private String lid = "";
    private String bindehaut = "";
    private String hornhaut = "";
    private String vorderkammer = "";
    private String iris = "";
    private String linse = "";

    // Fundus
    private String fundusID = "";
    private String glaskoerper = "";
    private String papille = "";
    private String makula = "";
    private String peripherie = "";

    // Fluoreszenzangiographie
    private String fagID = "";
    private String fagEigenschaften = "";

    // OptischeKoheränztomographie
    private String octID = "";
    private String octEigenschaften = "";
    private LocalTime octDatum = LocalTime.MIN;

    // Diagnose
    public enum HauptDiagnosen {
        E,
        UNKNOWN,
        AMD_FEUCHT,
        AMD_TROCKEN,
        AMD_ALLGEMEIN,
        DMOE,
        GEFAESSVERSCHLUSS,
        ZVV,
        VAV;
    }

    private String diagnoseID = "";

    private String argos_tl_Diagnose = "";
    private String op_bericht_extract_Diagnose = "";
    private String pdv_verschluesselungen_Diagnose = "";

    private LocalDate diagnoseDatum = LocalDate.MIN;
    private HauptDiagnosen hauptDiagnose = HauptDiagnosen.E;
    private HauptDiagnosen hauptDiagnoseFromLetterExtract = HauptDiagnosen.E;
    private String nebenDiagnose = "";
    private String diagnoseQuelle = "";

    // MetaDaten
    private String datenquelle = "";
    private Integer lfdNr = 0;
    private boolean annotated = false;
    private boolean cleansed = false;

    public DataModelNoProperty() {
    }

    public boolean isCleansed() {
        return cleansed;
    }

    public void setCleansed(boolean cleansed) {
        this.cleansed = cleansed;
    }

    public String getArgos_tl_Diagnose() {
        return argos_tl_Diagnose;
    }

    public void setArgos_tl_Diagnose(String argos_tl_Diagnose) {
        this.argos_tl_Diagnose = argos_tl_Diagnose;
    }

    public String getOp_bericht_extract_Diagnose() {
        return op_bericht_extract_Diagnose;
    }

    public void setOp_bericht_extract_Diagnose(String op_bericht_extract_Diagnose) {
        this.op_bericht_extract_Diagnose = op_bericht_extract_Diagnose;
    }

    public String getPdv_verschluesselungen_Diagnose() {
        return pdv_verschluesselungen_Diagnose;
    }

    public void setPdv_verschluesselungen_Diagnose(String pdv_verschluesselungen_Diagnose) {
        this.pdv_verschluesselungen_Diagnose = pdv_verschluesselungen_Diagnose;
    }

    public int getDaysPast() {
        return daysPast;
    }

    public void setDaysPast(int daysPast) {
        this.daysPast = daysPast;
    }

    public LocalDate getEreignisDatum() {
        return ereignisDatum;
    }

    public void setEreignisDatum(LocalDate ereignisDatum) {
        this.ereignisDatum = ereignisDatum;
    }

    public void setPatientenID(String patientenID) {
        this.patientenID = patientenID;
    }

    public void setGeburtsdatum(LocalDate geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public void setVorname(String vorname) {
//        this.vorname = vorname;
//    }
    public void setGeschlecht(Geschlecht geschlecht) {
        this.geschlecht = geschlecht;
    }

    public void setSpritzenQuelle(String spritzenQuelle) {
        this.spritzenQuelle = spritzenQuelle;
    }

    public void setVergangeneTage(int vergangeneTage) {
        this.vergangeneTage = vergangeneTage;
    }

    public void setMedikament(AntiVEGFMedikament medikament) {
        this.medikament = medikament;
    }

    public void setDiskrepanz(Boolean diskrepanz) {
        this.diskrepanz = diskrepanz;
    }

    public void setNaechstesVisusDatum(LocalDate naechstesVisusDatum) {
        this.naechstesVisusDatum = naechstesVisusDatum;
    }

    public void setLogMarVisusWert(Double logMarVisusWert) {
        this.logMarVisusWert = logMarVisusWert;
    }

    public void setUntersuchungsID(String untersuchungsID) {
        this.untersuchungsID = untersuchungsID;
    }

    public void setUntersuchungsCode(String untersuchungsCode) {
        this.untersuchungsCode = untersuchungsCode;
    }

    public void setBriefDaten(String briefDaten) {
        this.briefDaten = briefDaten;
    }

    public void setAnnotationText(String annotationText) {
        this.annotationText = annotationText;
    }

    public void setAugenID(String augenID) {
        this.augenID = augenID;
    }

    public void setUntersuchungLateralitaet(AugeLateralitaet untersuchungLateralitaet) {
        this.untersuchungLateralitaet = untersuchungLateralitaet;
    }

    public void setAugendruck(Double augendruck) {
        this.augendruck = augendruck;
    }

    public void setAugendruckFromLetterExtract(Double augendruckFromLetterExtract) {
        this.augendruckFromLetterExtract = augendruckFromLetterExtract;
    }

    public void setVisusID(String visusID) {
        this.visusID = visusID;
    }

    public void setRohWert(String rohWert) {
        this.rohWert = rohWert;
    }

    public void setVisuswerte(Visuswerte visuswerte) {
        this.visuswerte = visuswerte;
    }

    public void setDezimalVisusWert(Double dezimalVisusWert) {
        this.dezimalVisusWert = dezimalVisusWert;
    }

    public void setVisusDelta(Double visusDelta) {
        this.visusDelta = visusDelta;
    }

    public void setVisusKlasse(Double visusKlasse) {
        this.visusKlasse = visusKlasse;
    }

    public void setVisusKlasseDelta(Double visusKlasseDelta) {
        this.visusKlasseDelta = visusKlasseDelta;
    }

    public void setZeitSeitLetztemVisus(Period zeitSeitLetztemVisus) {
        this.zeitSeitLetztemVisus = zeitSeitLetztemVisus;
    }

    public void setMeterVisus(Boolean meterVisus) {
        this.meterVisus = meterVisus;
    }

    public void setVisusKorrektur(VisusKorrektur visusKorrektur) {
        this.visusKorrektur = visusKorrektur;
    }

    public void setSphaere(Double sphaere) {
        this.sphaere = sphaere;
    }

    public void setZylinder(Double zylinder) {
        this.zylinder = zylinder;
    }

    public void setAchse(Double achse) {
        this.achse = achse;
    }

    public void setLochblende(Boolean lochblende) {
        this.lochblende = lochblende;
    }

    public void setAr_nicht_moeglich(Boolean ar_nicht_moeglich) {
        this.ar_nicht_moeglich = ar_nicht_moeglich;
    }

    public void setArgosEreignisRelevanz(Boolean argosEreignisRelevanz) {
        this.argosEreignisRelevanz = argosEreignisRelevanz;
    }

    public void setArgosEreignis(Boolean argosEreignis) {
        this.argosEreignis = argosEreignis;
    }

    public void setArgosEreignisText(String argosEreignisText) {
        this.argosEreignisText = argosEreignisText;
    }

    public void setVaaID(String vaaID) {
        this.vaaID = vaaID;
    }

    public void setLid(String lid) {
        this.lid = lid;
    }

    public void setBindehaut(String bindehaut) {
        this.bindehaut = bindehaut;
    }

    public void setHornhaut(String hornhaut) {
        this.hornhaut = hornhaut;
    }

    public void setVorderkammer(String vorderkammer) {
        this.vorderkammer = vorderkammer;
    }

    public void setIris(String iris) {
        this.iris = iris;
    }

    public void setLinse(String linse) {
        this.linse = linse;
    }

    public void setFundusID(String fundusID) {
        this.fundusID = fundusID;
    }

    public void setGlaskoerper(String glaskoerper) {
        this.glaskoerper = glaskoerper;
    }

    public void setPapille(String papille) {
        this.papille = papille;
    }

    public void setMakula(String makula) {
        this.makula = makula;
    }

    public void setPeripherie(String peripherie) {
        this.peripherie = peripherie;
    }

    public void setFagID(String fagID) {
        this.fagID = fagID;
    }

    public void setFagEigenschaften(String fagEigenschaften) {
        this.fagEigenschaften = fagEigenschaften;
    }

    public void setOctID(String octID) {
        this.octID = octID;
    }

    public void setOctEigenschaften(String octEigenschaften) {
        this.octEigenschaften = octEigenschaften;
    }

    public void setOctDatum(LocalTime octDatum) {
        this.octDatum = octDatum;
    }

    public void setDiagnoseID(String diagnoseID) {
        this.diagnoseID = diagnoseID;
    }

    public void setDiagnoseDatum(LocalDate diagnoseDatum) {
        this.diagnoseDatum = diagnoseDatum;
    }

    public void setHauptDiagnose(HauptDiagnosen hauptDiagnose) {
        this.hauptDiagnose = hauptDiagnose;
    }

    public void setHauptDiagnoseFromLetterExtract(HauptDiagnosen hauptDiagnoseFromLetterExtract) {
        this.hauptDiagnoseFromLetterExtract = hauptDiagnoseFromLetterExtract;
    }

    public void setNebenDiagnose(String nebenDiagnose) {
        this.nebenDiagnose = nebenDiagnose;
    }

    public void setDiagnoseQuelle(String diagnoseQuelle) {
        this.diagnoseQuelle = diagnoseQuelle;
    }

    public void setDatenquelle(String datenquelle) {
        this.datenquelle = datenquelle;
    }

    public void setLfdNr(Integer lfdNr) {
        this.lfdNr = lfdNr;
    }

    public boolean isAnnotated() {
        return annotated;
    }

    public String getPatientenID() {
        return patientenID;
    }

    public LocalDate getGeburtsdatum() {
        return geburtsdatum;
    }

    public String getName() {
        return name;
    }

//    public String getVorname() {
//        return vorname;
//    }
    public Geschlecht getGeschlecht() {
        return geschlecht;
    }

    public String getSpritzenQuelle() {
        return spritzenQuelle;
    }

    public int getVergangeneTage() {
        return vergangeneTage;
    }

    public AntiVEGFMedikament getMedikament() {
        return medikament;
    }

    public Boolean getDiskrepanz() {
        return diskrepanz;
    }

    public LocalDate getNaechstesVisusDatum() {
        return naechstesVisusDatum;
    }

    public Double getLogMarVisusWert() {
        return logMarVisusWert;
    }

    public String getUntersuchungsID() {
        return untersuchungsID;
    }

    public String getUntersuchungsCode() {
        return untersuchungsCode;
    }

    public String getBriefDaten() {
        return briefDaten;
    }

    public String getAnnotationText() {
        return annotationText;
    }

    public String getAugenID() {
        return augenID;
    }

    public AugeLateralitaet getUntersuchungLateralitaet() {
        return untersuchungLateralitaet;
    }

    public Double getAugendruck() {
        return augendruck;
    }

    public Double getAugendruckFromLetterExtract() {
        return augendruckFromLetterExtract;
    }

    public String getVisusID() {
        return visusID;
    }

    public String getRohWert() {
        return rohWert;
    }

    public Visuswerte getVisuswerte() {
        return visuswerte;
    }

    public Double getDezimalVisusWert() {
        return dezimalVisusWert;
    }

    public Double getVisusDelta() {
        return visusDelta;
    }

    public Double getVisusKlasse() {
        return visusKlasse;
    }

    public Double getVisusKlasseDelta() {
        return visusKlasseDelta;
    }

    public Period getZeitSeitLetztemVisus() {
        return zeitSeitLetztemVisus;
    }

    public Boolean getMeterVisus() {
        return meterVisus;
    }

    public VisusKorrektur getVisusKorrektur() {
        return visusKorrektur;
    }

    public Double getSphaere() {
        return sphaere;
    }

    public Double getZylinder() {
        return zylinder;
    }

    public Double getAchse() {
        return achse;
    }

    public Boolean getLochblende() {
        return lochblende;
    }

    public Boolean getAr_nicht_moeglich() {
        return ar_nicht_moeglich;
    }

    public Boolean getArgosEreignis() {
        return argosEreignis;
    }

    public Boolean getArgosEreignisRelevanz() {
        return argosEreignisRelevanz;
    }

    public String getArgosEreignisText() {
        return argosEreignisText;
    }

    public String getVaaID() {
        return vaaID;
    }

    public String getLid() {
        return lid;
    }

    public String getBindehaut() {
        return bindehaut;
    }

    public String getHornhaut() {
        return hornhaut;
    }

    public String getVorderkammer() {
        return vorderkammer;
    }

    public String getIris() {
        return iris;
    }

    public String getLinse() {
        return linse;
    }

    public String getFundusID() {
        return fundusID;
    }

    public String getGlaskoerper() {
        return glaskoerper;
    }

    public String getPapille() {
        return papille;
    }

    public String getMakula() {
        return makula;
    }

    public String getPeripherie() {
        return peripherie;
    }

    public String getFagID() {
        return fagID;
    }

    public String getFagEigenschaften() {
        return fagEigenschaften;
    }

    public String getOctID() {
        return octID;
    }

    public String getOctEigenschaften() {
        return octEigenschaften;
    }

    public LocalTime getOctDatum() {
        return octDatum;
    }

    public String getDiagnoseID() {
        return diagnoseID;
    }

    public LocalDate getDiagnoseDatum() {
        return diagnoseDatum;
    }

    public HauptDiagnosen getHauptDiagnose() {
        return hauptDiagnose;
    }

    public HauptDiagnosen getHauptDiagnoseFromLetterExtract() {
        return hauptDiagnoseFromLetterExtract;
    }

    public String getNebenDiagnose() {
        return nebenDiagnose;
    }

    public String getDiagnoseQuelle() {
        return diagnoseQuelle;
    }

    public String getDatenquelle() {
        return datenquelle;
    }

    public Integer getLfdNr() {
        return lfdNr;
    }

    public void setAnnotated(boolean annotated) {
        this.annotated = annotated;
    }

}
