/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 *
 * @author lokal-admin
 */
public class GlobalData {

    private final DoubleProperty xFitFactor = new SimpleDoubleProperty();
    private final DoubleProperty windowWidth = new SimpleDoubleProperty();
    private final DoubleProperty daysCount = new SimpleDoubleProperty();

    private final DoubleProperty yFitFactor = new SimpleDoubleProperty();
    private final DoubleProperty windowHeight = new SimpleDoubleProperty();

    public double getWindowHeight() {
        return windowHeight.get();
    }

    public void setWindowHeight(double value) {
        windowHeight.set(value);
    }

    public DoubleProperty windowHeightProperty() {
        return windowHeight;
    }

    public double getDaysCount() {
        return daysCount.get();
    }

    public void setDaysCount(double value) {
        daysCount.set(value);
    }

    public DoubleProperty daysCountProperty() {
        return daysCount;
    }

    public double getWindowWidth() {
        return windowWidth.get();
    }

    public void setWindowWidth(double value) {
        windowWidth.set(value);
    }

    public DoubleProperty windowWidthProperty() {
        return windowWidth;
    }

    public double getxFitFactor() {
        return xFitFactor.get();
    }

    public void setxFitFactor(double value) {
        xFitFactor.set(value);
    }

    public DoubleProperty xFitFactorProperty() {
        return xFitFactor;
    }

    public double getyFitFactor() {
        return yFitFactor.get();
    }

    public void setyFitFactor(double value) {
        yFitFactor.set(value);
    }

    public DoubleProperty yFitFactorProperty() {
        return yFitFactor;
    }

    public void setXFitFactorProperty() {
        this.xFitFactorProperty().bind(this.windowWidth.divide(this.daysCountProperty()));
    }

}
