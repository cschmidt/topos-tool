/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.Background;
import static va_topos.VA_topos.LOGMAR_MAXIMUM;
import va_topos.data.DataModel.AugeLateralitaet;

/**
 *
 * @author Christoph Schmidt
 */
@SuppressWarnings("unchecked")
public class DataModel implements Serializable {

    private static final String CLASSNAME = DataModel.class.toString();

    public enum DataTypes {
        TEXT,
        NUMBER,
        DATE,
        BOOLEAN,
        OTHER;
    }

    // Patient
    public enum Geschlecht {
        m,
        w,
        i,
        u;
    }
    private final StringProperty patientenID = new SimpleStringProperty();
    private final ObjectProperty<LocalDate> geburtsdatum = new SimpleObjectProperty<>();
    private final StringProperty name = new SimpleStringProperty();
//    private final StringProperty vorname = new SimpleStringProperty();
    private final ObjectProperty<DataModel.Geschlecht> geschlecht = new SimpleObjectProperty<>();

    // Spritze
    public enum AntiVEGFMedikament {
        AVASTIN, //(Bevacizumab)
        LUCENTIS, //(Ranibizumab),
        EYLEA, //(Aflibercept),
        OZURDEX, //(Dexametason),
        TRIAMCINOLON,
        E,
        UNKNOWN;
    }

    public enum AugeLateralitaet {
        E,
        UNKNOWN,
        LEFT,
        RIGHT;
    }
    private final StringProperty spritzenQuelle = new SimpleStringProperty();
//    private final ObjectProperty<LocalDate> spritzenDatum = new SimpleObjectProperty<>();
    //private final ObjectProperty<DataModel.AugeLateralitaet> spritzeLateralitaet = new SimpleObjectProperty();
    private final IntegerProperty vergangeneTage = new SimpleIntegerProperty();
    private final ObjectProperty<DataModel.AntiVEGFMedikament> medikament = new SimpleObjectProperty<>();
    private final BooleanProperty diskrepanz = new SimpleBooleanProperty();
    private final ObjectProperty<LocalDate> naechstesVisusDatum = new SimpleObjectProperty<>();

    // Untersuchung
    private final StringProperty untersuchungsID = new SimpleStringProperty();
//    private final ObjectProperty<LocalDate> untersuchungsDatum = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDate> ereignisDatum = new SimpleObjectProperty<>();

    private final StringProperty untersuchungsCode = new SimpleStringProperty();
    private final StringProperty briefDaten = new SimpleStringProperty();

    // Auge
    private final StringProperty augenID = new SimpleStringProperty();
    @SuppressWarnings("unchecked")
    private final ObjectProperty<DataModel.AugeLateralitaet> untersuchungLateralitaet = new SimpleObjectProperty();
    private final DoubleProperty augendruck = new SimpleDoubleProperty();
    private final DoubleProperty augendruckFromLetterExtract = new SimpleDoubleProperty();

    // Visus
    public enum Visuswerte {
        E,
        UNKNOWN,
        NULLA_LUX,
        LICHTSCHEIN,
        HANDBEWEGUNGEN,
        FINGERZAEHLEN,
        TAFELVISUS,
        DEZIMALVISUS;
    }

    public enum VisusKorrektur {
        E,
        UNKNOWN,
        AR,
        EB,
        KL;
    }

    private final StringProperty visusID = new SimpleStringProperty();
    private final StringProperty rohWert = new SimpleStringProperty();
    private final ObjectProperty<DataModel.Visuswerte> visuswerte = new SimpleObjectProperty<>();
    private final DoubleProperty dezimalVisusWert = new SimpleDoubleProperty();
    private final DoubleProperty logMarVisusWert = new SimpleDoubleProperty();
    private final DoubleProperty visusDelta = new SimpleDoubleProperty();
    private final DoubleProperty visusLogMarDelta = new SimpleDoubleProperty();

    private final StringProperty rohWertFromLetterExtract = new SimpleStringProperty();
    private final ObjectProperty<Visuswerte> visuswerteFromLetterExtract = new SimpleObjectProperty<>();
    private final DoubleProperty dezimalVisusWertFromLetterExtract = new SimpleDoubleProperty();
    private final DoubleProperty logMarVisusWertFromLetterExtract = new SimpleDoubleProperty();

    private final DoubleProperty visusKlasse = new SimpleDoubleProperty();
    private final DoubleProperty visusKlasseDelta = new SimpleDoubleProperty();
    private final ObjectProperty<Period> zeitSeitLetztemVisus = new SimpleObjectProperty<>();

    private final BooleanProperty meterVisus = new SimpleBooleanProperty();
    private final ObjectProperty<DataModel.VisusKorrektur> visusKorrektur = new SimpleObjectProperty<>();
    private final DoubleProperty sphaere = new SimpleDoubleProperty();
    private final DoubleProperty zylinder = new SimpleDoubleProperty();
    private final DoubleProperty achse = new SimpleDoubleProperty();
    private final BooleanProperty lochblende = new SimpleBooleanProperty();
    private final BooleanProperty ar_nicht_moeglich = new SimpleBooleanProperty();
//    private final ObjectProperty<LocalDate> visusDatum = new SimpleObjectProperty<>();
    private final IntegerProperty daysPast = new SimpleIntegerProperty();
    private final BooleanProperty argosEreignis = new SimpleBooleanProperty();
    private final BooleanProperty argosEreignisRelevanz = new SimpleBooleanProperty();

    // Vorderer Augenabschnitt
    private final StringProperty vaaID = new SimpleStringProperty();
    private final StringProperty lid = new SimpleStringProperty();
    private final StringProperty bindehaut = new SimpleStringProperty();
    private final StringProperty hornhaut = new SimpleStringProperty();
    private final StringProperty vorderkammer = new SimpleStringProperty();
    private final StringProperty iris = new SimpleStringProperty();
    private final StringProperty linse = new SimpleStringProperty();

    // Fundus
    private final StringProperty fundusID = new SimpleStringProperty();
    private final StringProperty glaskoerper = new SimpleStringProperty();
    private final StringProperty papille = new SimpleStringProperty();
    private final StringProperty makula = new SimpleStringProperty();
    private final StringProperty peripherie = new SimpleStringProperty();

    // Fluoreszenzangiographie
    private final StringProperty fagID = new SimpleStringProperty();
    private final StringProperty fagEigenschaften = new SimpleStringProperty();

    // OptischeKoheränztomographie
    private final StringProperty octID = new SimpleStringProperty();
    private final StringProperty octEigenschaften = new SimpleStringProperty();
    private final ObjectProperty<LocalTime> octDatum = new SimpleObjectProperty<>();

    // Diagnose
    public enum HauptDiagnosen {
        E,
        UNKNOWN,
        AMD_FEUCHT,
        AMD_TROCKEN,
        AMD_ALLGEMEIN,
        DMOE,
        GEFAESSVERSCHLUSS,
        ZVV,
        VAV;
    }

    private final StringProperty diagnoseID = new SimpleStringProperty();

    private final StringProperty argos_tl_Diagnose = new SimpleStringProperty();
    private final StringProperty op_bericht_extract_Diagnose = new SimpleStringProperty();
    private final StringProperty pdv_verschluesselungen_Diagnose = new SimpleStringProperty();

    private final ObjectProperty<LocalDate> diagnoseDatum = new SimpleObjectProperty<>();
    private final ObjectProperty<DataModel.HauptDiagnosen> hauptDiagnose = new SimpleObjectProperty<>();
    private final ObjectProperty<DataModel.HauptDiagnosen> hauptDiagnoseFromLetterExtract = new SimpleObjectProperty<>();
    private final StringProperty nebenDiagnose = new SimpleStringProperty();
    private final StringProperty diagnoseQuelle = new SimpleStringProperty();

    // MetaDaten
    private final StringProperty datenquelle = new SimpleStringProperty();
    private final ObjectProperty<Background> backGround = new SimpleObjectProperty();
    private final IntegerProperty lfdNr = new SimpleIntegerProperty();
    private final BooleanProperty annotated = new SimpleBooleanProperty();
    private final StringProperty annotationText = new SimpleStringProperty();
    private final BooleanProperty cleansed = new SimpleBooleanProperty();

    public DataModel() {
        this.geschlecht.set(Geschlecht.u);

        // Spritze
        this.medikament.set(AntiVEGFMedikament.E);
        this.untersuchungLateralitaet.set(AugeLateralitaet.E);
        this.augendruck.set(Double.NaN);
        this.logMarVisusWert.set(Double.NaN);

        // Visus
        this.visuswerte.set(Visuswerte.E);
        this.dezimalVisusWert.set(Double.NaN);
        this.visusDelta.set(Double.NaN);
        this.visusLogMarDelta.set(Double.NaN);
        this.visusKlasse.set(Double.NaN);
        this.visusKlasseDelta.set(Double.NaN);
        this.visusKorrektur.set(VisusKorrektur.E);
        this.sphaere.set(Double.NaN);
        this.zylinder.set(Double.NaN);
        this.achse.set(Double.NaN);

        // Vorderer Augenabschnitt
        // Diagnose
        this.hauptDiagnose.set(HauptDiagnosen.E);
        this.hauptDiagnoseFromLetterExtract.set(HauptDiagnosen.E);

        // Metadaten
        this.annotated.set(false);
        this.cleansed.set(false);
    }

    public boolean isArgosEreignisRelevanz() {
        return argosEreignisRelevanz.get();
    }

    public void setArgosEreignisRelevanz(boolean value) {
        argosEreignisRelevanz.set(value);
    }

    public BooleanProperty argosEreignisRelevanzProperty() {
        return argosEreignisRelevanz;
    }

    private final StringProperty argosEreignisText = new SimpleStringProperty();

    public String getArgosEreignisText() {
        return argosEreignisText.get();
    }

    public void setArgosEreignisText(String value) {
        argosEreignisText.set(value);
    }

    public StringProperty argosEreignisTextProperty() {
        return argosEreignisText;
    }

    public Double getLogMarVisusWertFromLetterExtract() {
        return logMarVisusWertFromLetterExtract.get();
    }

    public void setLogMarVisusWertFromLetterExtract(Double value) {
        logMarVisusWertFromLetterExtract.set(value);
    }

    public DoubleProperty logMarVisusWertFromLetterExtractProperty() {
        return logMarVisusWertFromLetterExtract;
    }

    public double getDezimalVisusWertFromLetterExtract() {
        return dezimalVisusWertFromLetterExtract.get();
    }

    public void setDezimalVisusWertFromLetterExtract(double value) {
        dezimalVisusWertFromLetterExtract.set(value);
    }

    public DoubleProperty dezimalVisusWertFromLetterExtractProperty() {
        return dezimalVisusWertFromLetterExtract;
    }

    public Visuswerte getVisuswerteFromLetterExtract() {
        return visuswerteFromLetterExtract.get();
    }

    public void setVisuswerteFromLetterExtract(Visuswerte value) {
        visuswerteFromLetterExtract.set(value);
    }

    public ObjectProperty visuswerteFromLetterExtractProperty() {
        return visuswerteFromLetterExtract;
    }

    public String getRohWertFromLetterExtract() {
        return rohWertFromLetterExtract.get();
    }

    public void setRohWertFromLetterExtract(String value) {
        rohWertFromLetterExtract.set(value);
    }

    public StringProperty rohWertFromLetterExtractProperty() {
        return rohWertFromLetterExtract;
    }

    public String getArgos_tl_Diagnose() {
        return argos_tl_Diagnose.get();
    }

    public void setArgos_tl_Diagnose(String value) {
        argos_tl_Diagnose.set(value);
    }

    public StringProperty argos_tl_DiagnoseProperty() {
        return argos_tl_Diagnose;
    }

    public String getOp_bericht_extract_Diagnose() {
        return op_bericht_extract_Diagnose.get();
    }

    public void setOp_bericht_extract_Diagnose(String value) {
        op_bericht_extract_Diagnose.set(value);
    }

    public StringProperty op_bericht_extract_DiagnoseProperty() {
        return op_bericht_extract_Diagnose;
    }

    public String getPdv_verschluesselungen_Diagnose() {
        return pdv_verschluesselungen_Diagnose.get();
    }

    public void setPdv_verschluesselungen_Diagnose(String value) {
        pdv_verschluesselungen_Diagnose.set(value);
    }

    public StringProperty pdv_verschluesselungen_DiagnoseProperty() {
        return pdv_verschluesselungen_Diagnose;
    }

    public Background getBackGround() {
        return backGround.get();
    }

    public void setBackGround(Background value) {
        backGround.set(value);
    }

    public ObjectProperty backGroundProperty() {
        return backGround;
    }

    public String getDiagnoseQuelle() {
        return diagnoseQuelle.get();
    }

    public void setDiagnoseQuelle(String value) {
        diagnoseQuelle.set(value);
    }

    public StringProperty diagnoseQuelleProperty() {
        return diagnoseQuelle;
    }

    public double getLogMarVisusWert() {
        return logMarVisusWert.get();
    }

    public void setLogMarVisusWert(double value) {
        logMarVisusWert.set(value);
    }

    public DoubleProperty closestNaechsterProperty() {
        return logMarVisusWert;
    }

    public LocalDate getNaechstesVisusDatum() {
        return naechstesVisusDatum.get();
    }

    public void setNaechstesVisusDatum(LocalDate value) {
        naechstesVisusDatum.set(value);
    }

    public ObjectProperty NaechstesVisusDatumProperty() {
        return naechstesVisusDatum;
    }

    public Period getZeitSeitLetztemVisus() {
        return zeitSeitLetztemVisus.get();
    }

    public void setZeitSeitLetztemVisus(Period value) {
        zeitSeitLetztemVisus.set(value);
    }

    public ObjectProperty zeitSeitLetztemVisusProperty() {
        return zeitSeitLetztemVisus;
    }

    public double getVisusKlasseDelta() {
        return visusKlasseDelta.get();
    }

    public void setVisusKlasseDelta(double value) {
        visusKlasseDelta.set(value);
    }

    public DoubleProperty visusKlasseDeltaProperty() {
        return visusKlasseDelta;
    }

    public LocalDate getEreignisDatum() {
        return ereignisDatum.get();
    }

    public void setEreignisDatum(LocalDate value) {
        ereignisDatum.set(value);
    }

    public ObjectProperty ereignisDatumProperty() {
        return ereignisDatum;
    }

    public String getUntersuchungsCode() {
        return untersuchungsCode.get();
    }

    public void setUntersuchungsCode(String value) {
        untersuchungsCode.set(value);
    }

    public StringProperty untersuchungsCodeProperty() {
        return untersuchungsCode;
    }

    public double getVisusDelta() {
        return visusDelta.get();
    }

    public void setVisusDelta(double value) {
        visusDelta.set(value);
    }

    public DoubleProperty visusDeltaProperty() {
        return visusDelta;
    }

    public double getVisusLogMarDelta() {
        return visusLogMarDelta.get();
    }

    public void setVisusLogMarDelta(double value) {
        visusLogMarDelta.set(value);
    }

    public DoubleProperty visusLogMarDeltaProperty() {
        return visusLogMarDelta;
    }

    public AugeLateralitaet getUntersuchungLateralitaet() {
        return untersuchungLateralitaet.get();
    }

    public void setUntersuchungLateralitaet(AugeLateralitaet value) {
        untersuchungLateralitaet.set(value);
    }

    public ObjectProperty untersuchungLateralitaetProperty() {
        return untersuchungLateralitaet;
    }

    public LocalTime getOctDatum() {
        return octDatum.get();
    }

    public void setOctDatum(LocalTime value) {
        octDatum.set(value);
    }

    public ObjectProperty octDatumProperty() {
        return octDatum;
    }

    public HauptDiagnosen getHauptDiagnose() {
        return hauptDiagnose.get();
    }

    public void setHauptDiagnose(HauptDiagnosen value) {
        hauptDiagnose.set(value);
    }

    public ObjectProperty hauptDiagnoseProperty() {
        return hauptDiagnose;
    }

    public HauptDiagnosen gethauptDiagnoseFromLetterExtract() {
        return hauptDiagnoseFromLetterExtract.get();
    }

    public void setHauptDiagnoseFromLetterExtract(HauptDiagnosen value) {
        hauptDiagnoseFromLetterExtract.set(value);
    }

    public ObjectProperty hauptDiagnoseFromLetterExtractProperty() {
        return hauptDiagnoseFromLetterExtract;
    }

    public String getNebenDiagnose() {
        return nebenDiagnose.get();
    }

    public void setNebenDiagnose(String value) {
        nebenDiagnose.set(value);
    }

    public StringProperty nebenDiagnoseProperty() {
        return nebenDiagnose;
    }

    public LocalDate getDiagnoseDatum() {
        return diagnoseDatum.get();
    }

    public void setDiagnoseDatum(LocalDate value) {
        diagnoseDatum.set(value);
    }

    public ObjectProperty diagnoseDatumProperty() {
        return diagnoseDatum;
    }

    public String getDiagnoseID() {
        return diagnoseID.get();
    }

    public void setDiagnoseID(String value) {
        diagnoseID.set(value);
    }

    public StringProperty diagnoseIDProperty() {
        return diagnoseID;
    }

    public String getOctEigenschaften() {
        return octEigenschaften.get();
    }

    public void setOctEigenschaften(String value) {
        octEigenschaften.set(value);
    }

    public StringProperty octEigenschaftenProperty() {
        return octEigenschaften;
    }

    public String getOctID() {
        return octID.get();
    }

    public void setOctID(String value) {
        octID.set(value);
    }

    public StringProperty octIDProperty() {
        return octID;
    }

    public String getFagEigenschaften() {
        return fagEigenschaften.get();
    }

    public void setFagEigenschaften(String value) {
        fagEigenschaften.set(value);
    }

    public StringProperty fagEigenschaftenProperty() {
        return fagEigenschaften;
    }

    public String getFagID() {
        return fagID.get();
    }

    public void setFagID(String value) {
        fagID.set(value);
    }

    public StringProperty fagIDProperty() {
        return fagID;
    }

    public String getPeripherie() {
        return peripherie.get();
    }

    public void setPeripherie(String value) {
        peripherie.set(value);
    }

    public StringProperty peripherieProperty() {
        return peripherie;
    }

    public String getMakula() {
        return makula.get();
    }

    public void setMakula(String value) {
        makula.set(value);
    }

    public StringProperty makulaProperty() {
        return makula;
    }

    public String getPapille() {
        return papille.get();
    }

    public void setPapille(String value) {
        papille.set(value);
    }

    public StringProperty papilleProperty() {
        return papille;
    }

    public String getGlaskoerper() {
        return glaskoerper.get();
    }

    public void setGlaskoerper(String value) {
        glaskoerper.set(value);
    }

    public StringProperty glaskoerperProperty() {
        return glaskoerper;
    }

    public String getFundusID() {
        return fundusID.get();
    }

    public void setFundusID(String value) {
        fundusID.set(value);
    }

    public StringProperty fundusIDProperty() {
        return fundusID;
    }

    public String getLinse() {
        return linse.get();
    }

    public void setLinse(String value) {
        linse.set(value);
    }

    public StringProperty linseProperty() {
        return linse;
    }

    public String getIris() {
        return iris.get();
    }

    public void setIris(String value) {
        iris.set(value);
    }

    public StringProperty irisProperty() {
        return iris;
    }

    public String getVorderkammer() {
        return vorderkammer.get();
    }

    public void setVorderkammer(String value) {
        vorderkammer.set(value);
    }

    public StringProperty vorderkammerProperty() {
        return vorderkammer;
    }

    public String getHornhaut() {
        return hornhaut.get();
    }

    public void setHornhaut(String value) {
        hornhaut.set(value);
    }

    public StringProperty hornhautProperty() {
        return hornhaut;
    }

    public String getBindehaut() {
        return bindehaut.get();
    }

    public void setBindehaut(String value) {
        bindehaut.set(value);
    }

    public StringProperty bindehautProperty() {
        return bindehaut;
    }

    public String getLid() {
        return lid.get();
    }

    public void setLid(String value) {
        lid.set(value);
    }

    public StringProperty lidProperty() {
        return lid;
    }

    public String getVaaID() {
        return vaaID.get();
    }

    public void setVaaID(String value) {
        vaaID.set(value);
    }

    public StringProperty vaaIDProperty() {
        return vaaID;
    }

    public boolean isAnnotated() {
        return annotated.get();
    }

    public void setAnnotated(boolean value) {
        annotated.set(value);
    }

    public BooleanProperty annotatedProperty() {
        return annotated;
    }

    public boolean isCleansed() {
        return cleansed.get();
    }

    public void setCleansed(boolean value) {
        cleansed.set(value);
    }

    public BooleanProperty cleansedProperty() {
        return cleansed;
    }

    public boolean isArgosEreignis() {
        return argosEreignis.get();
    }

    public void setArgosEreignis(boolean value) {
        argosEreignis.set(value);
    }

    public BooleanProperty argosEreignisProperty() {
        return argosEreignis;
    }

    public int getDaysPast() {
        return daysPast.get();
    }

    public void setDaysPast(int value) {
        daysPast.set(value);
    }

    public IntegerProperty daysPastProperty() {
        return daysPast;
    }

    public boolean isAr_nicht_moeglich() {
        return ar_nicht_moeglich.get();
    }

    public void setAr_nicht_moeglich(boolean value) {
        ar_nicht_moeglich.set(value);
    }

    public BooleanProperty ar_nicht_moeglichProperty() {
        return ar_nicht_moeglich;
    }

    public boolean isLochblende() {
        return lochblende.get();
    }

    public void setLochblende(boolean value) {
        lochblende.set(value);
    }

    public BooleanProperty lochblendeProperty() {
        return lochblende;
    }

    public VisusKorrektur getVisusKorrektur() {
        return visusKorrektur.get();
    }

    public void setVisusKorrektur(VisusKorrektur value) {
        visusKorrektur.set(value);
    }

    public ObjectProperty visusKorrekturProperty() {
        return visusKorrektur;
    }

    public AntiVEGFMedikament getMedikament() {
        return medikament.get();
    }

    public void setMedikament(AntiVEGFMedikament value) {
        medikament.set(value);
    }

    public ObjectProperty medikamentProperty() {
        return medikament;
    }

    public Geschlecht getGeschlecht() {
        return geschlecht.get();
    }

    public void setGeschlecht(Geschlecht value) {
        geschlecht.set(value);
    }

    public ObjectProperty geschlechtProperty() {
        return geschlecht;
    }

    public Visuswerte getVisuswerte() {
        return visuswerte.get();
    }

    public void setVisuswerte(Visuswerte value) {
        visuswerte.set(value);
    }

    public ObjectProperty visuswerteProperty() {
        return visuswerte;
    }

    public double getAchse() {
        return achse.get();
    }

    public void setAchse(double value) {
        achse.set(value);
    }

    public DoubleProperty achseProperty() {
        return achse;
    }

    public double getZylinder() {
        return zylinder.get();
    }

    public void setZylinder(double value) {
        zylinder.set(value);
    }

    public DoubleProperty zylinderProperty() {
        return zylinder;
    }

    public double getSphaere() {
        return sphaere.get();
    }

    public void setSphaere(double value) {
        sphaere.set(value);
    }

    public DoubleProperty sphaereProperty() {
        return sphaere;
    }

    public boolean isMeterVisus() {
        return meterVisus.get();
    }

    public void setMeterVisus(boolean value) {
        meterVisus.set(value);
    }

    public BooleanProperty meterVisusProperty() {
        return meterVisus;
    }

    public double getVisusKlasse() {
        return visusKlasse.get();
    }

    public void setVisusKlasse(double value) {
        visusKlasse.set(value);
    }

    public DoubleProperty visusKlasseProperty() {
        return visusKlasse;
    }

    public double getDezimalVisusWert() {
        return dezimalVisusWert.get();
    }

    public void setDezimalVisusWert(double value) {
        dezimalVisusWert.set(value);
    }

    public DoubleProperty dezimalVisusWertProperty() {
        return dezimalVisusWert;
    }

    public String getRohWert() {
        return rohWert.get();
    }

    public void setRohWert(String value) {
        rohWert.set(value);
    }

    public StringProperty rohWertProperty() {
        return rohWert;
    }

    public String getVisusID() {
        return visusID.get();
    }

    public void setVisusID(String value) {
        visusID.set(value);
    }

    public StringProperty visusIDProperty() {
        return visusID;
    }

    public String getAugenID() {
        return augenID.get();
    }

    public void setAugenID(String value) {
        augenID.set(value);
    }

    public StringProperty augenIDProperty() {
        return augenID;
    }

    public String getAnnotationText() {
        return annotationText.get();
    }

    public void setAnnotationText(String value) {
        annotationText.set(value);
    }

    public StringProperty annotationTextProperty() {
        return annotationText;
    }

    public String getBriefDaten() {
        return briefDaten.get();
    }

    public void setBriefDaten(String value) {
        briefDaten.set(value);
    }

    public StringProperty briefDatenProperty() {
        return briefDaten;
    }

    public String getUntersuchungsID() {
        return untersuchungsID.get();
    }

    public void setUntersuchungsID(String value) {
        untersuchungsID.set(value);
    }

    public StringProperty untersuchungsIDProperty() {
        return untersuchungsID;
    }

    public boolean isDiskrepanz() {
        return diskrepanz.get();
    }

    public void setDiskrepanz(boolean value) {
        diskrepanz.set(value);
    }

    public BooleanProperty DiskrepanzProperty() {
        return diskrepanz;
    }

    public int getVergangeneTage() {
        return vergangeneTage.get();
    }

    public void setVergangeneTage(int value) {
        vergangeneTage.set(value);
    }

    public IntegerProperty vergangeneTageProperty() {
        return vergangeneTage;
    }

    public int getLfdNr() {
        return lfdNr.get();
    }

    public void setLfdNr(int value) {
        lfdNr.set(value);
    }

    public IntegerProperty lfdNrProperty() {
        return lfdNr;
    }

    public String getDatenquelle() {
        return datenquelle.get();
    }

    public void setDatenquelle(String value) {
        datenquelle.set(value);
    }

    public StringProperty datenquelleProperty() {
        return datenquelle;
    }

    public LocalDate getGeburtsdatum() {
        return geburtsdatum.get();
    }

    public void setGeburtsdatum(LocalDate value) {
        geburtsdatum.set(value);
    }

    public ObjectProperty<LocalDate> geburtsdatumProperty() {
        return geburtsdatum;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String value) {
        name.set(value);
    }

    public StringProperty nameProperty() {
        return name;
    }

//    public String getVorname() {
//        return vorname.get();
//    }
//
//    public void setVorname(String value) {
//        vorname.set(value);
//    }
//
//    public StringProperty vornameProperty() {
//        return vorname;
//    }
    public String getSpritzenQuelle() {
        return spritzenQuelle.get();
    }

    public void setSpritzenQuelle(String value) {
        spritzenQuelle.set(value);
    }

    public StringProperty spritzenQuelleProperty() {
        return spritzenQuelle;
    }

    public double getAugendruck() {
        return augendruck.get();
    }

    public void setAugendruck(double value) {
        augendruck.set(value);
    }

    public DoubleProperty augendruckProperty() {
        return augendruck;
    }

    public double getAugendruckFromLetterExtract() {
        return augendruckFromLetterExtract.get();
    }

    public void setAugendruckFromLetterExtract(double value) {
        augendruckFromLetterExtract.set(value);
    }

    public DoubleProperty augendruckFromLetterExtractProperty() {
        return augendruckFromLetterExtract;
    }

    public String getPatientenID() {
        return patientenID.get();
    }

    public void setPatientenID(String value) {
        patientenID.set(value);
    }

    public StringProperty patientenIDProperty() {
        return patientenID;
    }

    public DataModel initializeAll() {
        initializeAll(this, "", 0);
        return this;
    }

    public void initializeAll(DataModel datum) {
        initializeAll(datum, "", 0);
    }

    public void initializeAll(DataModel datum, String datenQuelle, int lfdNr) {

        // Zunächst alles initialisieren
        datum.patientenID.set("");
        datum.geburtsdatum.set(LocalDate.MIN);
        datum.name.set("");
//        datum.vorname.set("");
        datum.geschlecht.set(Geschlecht.u);

        // Spritze
        datum.spritzenQuelle.set("");
        datum.vergangeneTage.set(-1);
        datum.medikament.set(AntiVEGFMedikament.E);
        datum.diskrepanz.set(false);
        datum.logMarVisusWert.set(Double.NaN);
        datum.naechstesVisusDatum.set(LocalDate.MIN);

        // Untersuchung
        datum.untersuchungsID.set("");
        datum.untersuchungsCode.set("");
        datum.briefDaten.set("");
        datum.ereignisDatum.set(LocalDate.MIN);

        // argos_tl Ereignisse
        datum.argosEreignis.set(false);
        datum.argosEreignisRelevanz.set(true);
        datum.argosEreignisText.set("");

        // Auge
        datum.augenID.set("");
        datum.untersuchungLateralitaet.set(AugeLateralitaet.E);
        datum.augendruck.set(Double.NaN);
        datum.augendruckFromLetterExtract.set(Double.NaN);

        // Visus
        datum.visusID.set("");
        datum.rohWert.set("");
        datum.visuswerte.set(Visuswerte.E);
        datum.dezimalVisusWert.set(Double.NaN);
        datum.logMarVisusWert.set(Double.NaN);
        datum.visusDelta.set(Double.NaN);
        datum.visusLogMarDelta.set(Double.NaN);

        datum.rohWertFromLetterExtract.set("");
        datum.visuswerteFromLetterExtract.set(Visuswerte.E);
        datum.dezimalVisusWertFromLetterExtract.set(Double.NaN);
        datum.logMarVisusWertFromLetterExtract.set(Double.NaN);

        datum.visusKlasse.set(Double.NaN);
        datum.visusKlasseDelta.set(Double.NaN);
        datum.zeitSeitLetztemVisus.set(Period.ZERO);
        datum.meterVisus.set(false);
        datum.visusKorrektur.set(VisusKorrektur.E);
        datum.sphaere.set(Double.NaN);
        datum.zylinder.set(Double.NaN);
        datum.achse.set(Double.NaN);
        datum.lochblende.set(false);
        datum.ar_nicht_moeglich.set(false);
        datum.daysPast.set(-1);

        // Vorderer Augenabschnitt
        datum.vaaID.set("");
        datum.lid.set("");
        datum.bindehaut.set("");
        datum.hornhaut.set("");
        datum.vorderkammer.set("");
        datum.iris.set("");
        datum.linse.set("");

        // Fundus
        datum.fundusID.set("");
        datum.glaskoerper.set("");
        datum.papille.set("");
        datum.makula.set("");
        datum.peripherie.set("");

        // Fluoreszenzangiographie
        datum.fagID.set("");
        datum.fagEigenschaften.set("");

        // OptischeKoheränztomographie
        datum.octID.set("");
        datum.octEigenschaften.set("");
        datum.octDatum.set(LocalTime.MIN);

        // Diagnose
        datum.diagnoseID.set("");
        datum.diagnoseDatum.set(LocalDate.MIN);
        datum.hauptDiagnose.set(HauptDiagnosen.E);
        datum.hauptDiagnoseFromLetterExtract.set(HauptDiagnosen.E);
        datum.nebenDiagnose.set("");
        datum.diagnoseQuelle.set("");

        // Metadaten
        datum.datenquelle.set(datenQuelle);
        datum.lfdNr.set(lfdNr);
        datum.annotated.set(false);
        datum.annotationText.set("");
        datum.cleansed.set(false);

    }

    public List<DataModelNoProperty> convertToDataModelNoProperty(List<DataModel> data) {
        List<DataModelNoProperty> result = new ArrayList();

        data.stream().map((datum) -> {
            DataModelNoProperty tempNoProp = new DataModelNoProperty();
            tempNoProp.setPatientenID(datum.patientenID.get());
            tempNoProp.setGeburtsdatum(datum.geburtsdatum.get());
            tempNoProp.setName(datum.name.get());
//            tempNoProp.setVorname(datum.vorname.get());
            tempNoProp.setGeschlecht(DataModelNoProperty.Geschlecht.valueOf(datum.geschlecht.get().toString()));
            // Spritze
            tempNoProp.setSpritzenQuelle(datum.spritzenQuelle.get());
            tempNoProp.setVergangeneTage(datum.vergangeneTage.get());
            tempNoProp.setMedikament(DataModelNoProperty.AntiVEGFMedikament.valueOf(datum.medikament.get().toString()));
            tempNoProp.setDiskrepanz(datum.diskrepanz.get());
            tempNoProp.setLogMarVisusWert(datum.logMarVisusWert.get());
            tempNoProp.setNaechstesVisusDatum(datum.naechstesVisusDatum.get());
            // Untersuchung
            tempNoProp.setUntersuchungsID(datum.untersuchungsID.get());
            tempNoProp.setUntersuchungsCode(datum.untersuchungsCode.get());
            tempNoProp.setBriefDaten(datum.briefDaten.get());
            tempNoProp.setEreignisDatum(datum.ereignisDatum.get());
            // Auge
            tempNoProp.setAugenID(datum.augenID.get());
            tempNoProp.setUntersuchungLateralitaet(DataModelNoProperty.AugeLateralitaet.valueOf(datum.untersuchungLateralitaet.get().toString()));
            tempNoProp.setAugendruck(datum.augendruck.get());
            tempNoProp.setAugendruckFromLetterExtract(datum.augendruck.get());
            // Visus
            tempNoProp.setVisusID(datum.visusID.get());
            // TODO letterExtract
            tempNoProp.setRohWert(datum.rohWert.get());
            tempNoProp.setVisuswerte(DataModelNoProperty.Visuswerte.valueOf(datum.visuswerte.get().toString()));
            tempNoProp.setDezimalVisusWert(datum.dezimalVisusWert.get());
            tempNoProp.setRohWert(datum.rohWert.get());
            tempNoProp.setVisuswerte(DataModelNoProperty.Visuswerte.valueOf(datum.visuswerte.get().toString()));
            tempNoProp.setDezimalVisusWert(datum.dezimalVisusWert.get());

            tempNoProp.setVisusDelta(datum.visusDelta.get());
            tempNoProp.setVisusDelta(datum.visusLogMarDelta.get());
            tempNoProp.setVisusKlasse(datum.visusKlasse.get());
            tempNoProp.setVisusKlasseDelta(datum.visusKlasseDelta.get());
            tempNoProp.setZeitSeitLetztemVisus(datum.zeitSeitLetztemVisus.get());
            tempNoProp.setMeterVisus(datum.meterVisus.get());
            tempNoProp.setVisusKorrektur(DataModelNoProperty.VisusKorrektur.valueOf(datum.visusKorrektur.get().toString()));
            tempNoProp.setSphaere(datum.sphaere.get());
            tempNoProp.setZylinder(datum.zylinder.get());
            tempNoProp.setAchse(datum.achse.get());
            tempNoProp.setLochblende(datum.lochblende.get());
            tempNoProp.setAr_nicht_moeglich(datum.ar_nicht_moeglich.get());
            tempNoProp.setDaysPast(datum.daysPast.get());
            tempNoProp.setArgosEreignis(datum.argosEreignis.get());
            tempNoProp.setArgosEreignisRelevanz(datum.argosEreignisRelevanz.get());
            tempNoProp.setArgosEreignisText(datum.argosEreignisText.get());
            // Vorderer Augenabschnitt
            tempNoProp.setVaaID(datum.vaaID.get());
            tempNoProp.setLid(datum.lid.get());
            tempNoProp.setBindehaut(datum.bindehaut.get());
            tempNoProp.setHornhaut(datum.hornhaut.get());
            tempNoProp.setVorderkammer(datum.vorderkammer.get());
            tempNoProp.setIris(datum.iris.get());
            tempNoProp.setLinse(datum.linse.get());
            // Fundus
            tempNoProp.setFundusID(datum.fundusID.get());
            tempNoProp.setGlaskoerper(datum.glaskoerper.get());
            tempNoProp.setPapille(datum.papille.get());
            tempNoProp.setMakula(datum.makula.get());
            tempNoProp.setPeripherie(datum.peripherie.get());
            // Fluoreszenzangiographie
            tempNoProp.setFagID(datum.fagID.get());
            tempNoProp.setFagEigenschaften(datum.fagEigenschaften.get());
            // OptischeKoheränztomographie
            tempNoProp.setOctID(datum.octID.get());
            tempNoProp.setOctEigenschaften(datum.octEigenschaften.get());
            tempNoProp.setOctDatum(datum.octDatum.get());
            // Diagnose
            tempNoProp.setDiagnoseID(datum.diagnoseID.get());
            tempNoProp.setDiagnoseDatum(datum.diagnoseDatum.get());
            tempNoProp.setHauptDiagnose(DataModelNoProperty.HauptDiagnosen.valueOf(datum.hauptDiagnose.get().toString()));
            tempNoProp.setHauptDiagnoseFromLetterExtract(DataModelNoProperty.HauptDiagnosen.valueOf(datum.hauptDiagnoseFromLetterExtract.get().toString()));

            tempNoProp.setNebenDiagnose(datum.nebenDiagnose.get());
            tempNoProp.setDiagnoseQuelle(datum.diagnoseQuelle.get());
            // Metadaten
            tempNoProp.setDatenquelle(datum.datenquelle.get());
            tempNoProp.setLfdNr(datum.lfdNr.get());
            tempNoProp.setAnnotated(datum.isAnnotated());
            tempNoProp.setAnnotationText(datum.annotationText.get());
            tempNoProp.setCleansed(datum.isCleansed());

            return tempNoProp;
        }).forEachOrdered((tempNoProp) -> {
            result.add(tempNoProp);
//            for (final Field dataModelField : dataModelFields) {
//                
//                try {
//                    noPropFields[0].set(tempNoProp, dataModelField.get(datum)); //dataModelField.get(datum);
////               field.getName() + ": '" + ((IntegerProperty) field.get(myDatum)).get() + "'")
//                } catch (IllegalArgumentException | IllegalAccessException ex) {
//                    Logger.getLogger(DataModel.class.getName()).log(Level.SEVERE, null, ex);
//                }
//
//            }
        });
        return result;
    }

    public List<DataModel> convertFromDataModelNoProperty(List<DataModelNoProperty> data) {
        List<DataModel> result = new ArrayList();

        data.stream().map((datum) -> {
            DataModel tempModel = new DataModel();
            tempModel.setPatientenID(datum.getPatientenID());
            tempModel.setGeburtsdatum(datum.getGeburtsdatum());
            tempModel.setName(datum.getName());
//            tempModel.setVorname(datum.getVorname());
            tempModel.setGeschlecht(DataModel.Geschlecht.valueOf(datum.getGeschlecht().toString()));
            // Spritze
            tempModel.setSpritzenQuelle(datum.getSpritzenQuelle());
            tempModel.setVergangeneTage(datum.getVergangeneTage());
            tempModel.setMedikament(DataModel.AntiVEGFMedikament.valueOf(datum.getMedikament().toString()));
            tempModel.setDiskrepanz(datum.getDiskrepanz());
            tempModel.setLogMarVisusWert(datum.getLogMarVisusWert());
            tempModel.setNaechstesVisusDatum(datum.getNaechstesVisusDatum());
            // Untersuchung
            tempModel.setUntersuchungsID(datum.getUntersuchungsID());
            tempModel.setUntersuchungsCode(datum.getUntersuchungsCode());
            tempModel.setBriefDaten(datum.getBriefDaten());
            tempModel.setEreignisDatum(datum.getEreignisDatum());
            // Auge
            tempModel.setAugenID(datum.getAugenID());
            tempModel.setUntersuchungLateralitaet(DataModel.AugeLateralitaet.valueOf(datum.getUntersuchungLateralitaet().toString()));
            tempModel.setAugendruck(datum.getAugendruck());
            tempModel.setAugendruckFromLetterExtract(datum.getAugendruckFromLetterExtract());
            // Visus
            tempModel.setVisusID(datum.getVisusID());
            tempModel.setRohWert(datum.getRohWert());
            tempModel.setVisuswerte(DataModel.Visuswerte.valueOf(datum.getVisuswerte().toString()));
            tempModel.setDezimalVisusWert(datum.getDezimalVisusWert());
            tempModel.setVisusDelta(datum.getVisusDelta());
            tempModel.setVisusKlasse(datum.getVisusKlasse());
            tempModel.setVisusKlasseDelta(datum.getVisusKlasseDelta());
            tempModel.setZeitSeitLetztemVisus(datum.getZeitSeitLetztemVisus());
            tempModel.setMeterVisus(datum.getMeterVisus());
            tempModel.setVisusKorrektur(DataModel.VisusKorrektur.valueOf(datum.getVisusKorrektur().toString()));
            tempModel.setSphaere(datum.getSphaere());
            tempModel.setZylinder(datum.getZylinder());
            tempModel.setAchse(datum.getAchse());
            tempModel.setLochblende(datum.getLochblende());
            tempModel.setAr_nicht_moeglich(datum.getAr_nicht_moeglich());
            tempModel.setDaysPast(datum.getDaysPast());
            tempModel.setArgosEreignis(datum.getArgosEreignis());
            tempModel.setArgosEreignisRelevanz(datum.getArgosEreignisRelevanz());
            tempModel.setArgosEreignisText(datum.getArgosEreignisText());
            // Vorderer Augenabschnitt
            tempModel.setVaaID(datum.getVaaID());
            tempModel.setLid(datum.getLid());
            tempModel.setBindehaut(datum.getBindehaut());
            tempModel.setHornhaut(datum.getHornhaut());
            tempModel.setVorderkammer(datum.getVorderkammer());
            tempModel.setIris(datum.getIris());
            tempModel.setLinse(datum.getLinse());
            // Fundus
            tempModel.setFundusID(datum.getFundusID());
            tempModel.setGlaskoerper(datum.getGlaskoerper());
            tempModel.setPapille(datum.getPapille());
            tempModel.setMakula(datum.getMakula());
            tempModel.setPeripherie(datum.getPeripherie());
            // Fluoreszenzangiographie
            tempModel.setFagID(datum.getFagID());
            tempModel.setFagEigenschaften(datum.getFagEigenschaften());
            // OptischeKoheränztomographie
            tempModel.setOctID(datum.getOctID());
            tempModel.setOctEigenschaften(datum.getOctEigenschaften());
            tempModel.setOctDatum(datum.getOctDatum());
            // Diagnose
            tempModel.setDiagnoseID(datum.getDiagnoseID());
            tempModel.setDiagnoseDatum(datum.getDiagnoseDatum());
            tempModel.setHauptDiagnose(DataModel.HauptDiagnosen.valueOf(datum.getHauptDiagnose().toString()));
            tempModel.setHauptDiagnoseFromLetterExtract(DataModel.HauptDiagnosen.valueOf(datum.getHauptDiagnoseFromLetterExtract().toString()));
            tempModel.setNebenDiagnose(datum.getNebenDiagnose());
            tempModel.setDiagnoseQuelle(datum.getDiagnoseQuelle());
            // Metadaten
            tempModel.setDatenquelle(datum.getDatenquelle());
            tempModel.setLfdNr(datum.getLfdNr());
            tempModel.setAnnotated(datum.isAnnotated());
            tempModel.setAnnotationText(datum.getAnnotationText());
            tempModel.setCleansed(datum.isCleansed());

            return tempModel;
        }).forEachOrdered((tempModel) -> {
            result.add(tempModel);
//            for (final Field dataModelField : dataModelFields) {
//                
//                try {
//                    noPropFields[0].set(tempNoProp, dataModelField.get(datum)); //dataModelField.get(datum);
////               field.getName() + ": '" + ((IntegerProperty) field.get(myDatum)).get() + "'")
//                } catch (IllegalArgumentException | IllegalAccessException ex) {
//                    Logger.getLogger(DataModel.class.getName()).log(Level.SEVERE, null, ex);
//                }
//
//            }
        });
        return result;
    }

    /**
     * Function to set a single attribut of the data model
     *
     * @param attribut String containing the name of the attribut in the
     * datamodel to be set
     * @param value value of the field as a String
     */
    public void setSingleValue(String attribut, String value) {

        HashMap myMap = new HashMap();
        String[] myDataLine = new String[]{value};
        String datenQuelle = "";
//        Integer lfdNr = 0;
        setAll(myMap, myDataLine, datenQuelle, 0, false);

    }

    public String setAll(HashMap myMap, String[] myDataLine, String datenQuelle, Integer lfdNr) {
        return setAll(myMap, myDataLine, datenQuelle, lfdNr, true);
    }

    @SuppressWarnings("unchecked")
    public String setAll(HashMap myMap, String[] myDataLine, String datenQuelle, Integer lfdNr, boolean initialize) {

        final String DATE_FORMAT = "dd.MM.yyyy";
        final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

        StringProperty result = new SimpleStringProperty();

        // Zunächst alles initialisieren
        if (initialize) {
            initializeAll(this, datenQuelle, lfdNr);
        }

        result.set("");

        myMap.forEach((mapKey, mapValue) -> {
            //System.out.println("mapKey: " + mapKey + " mapValue: " + mapValue);
            try {

                if ((int) mapValue != -1) {
                    if ((int) mapValue < myDataLine.length) {
                        String thisData = myDataLine[(int) mapValue];
                        thisData = !thisData.isEmpty() ? thisData : "";

                        switch (mapKey.toString()) {

                            // Patient
                            // this.patientenID.set("");
                            // this.geburtsdatum.set(null);
                            // this.name.set("");
                            // this.vorname.set("");
                            // this.geschlecht.set(Geschlecht.u);
                            case "patientenid":
                                this.setPatientenID(thisData);
                                break;

                            case "geburtsdatum":
                                try {
                                    this.setGeburtsdatum(LocalDate.parse(thisData, FORMATTER));
                                } catch (Exception e) {
                                    StringProperty myFehler = new SimpleStringProperty();
                                    myFehler.set("Das Geburtsdatum konnte nicht korrekt umgewandelt werden. Fehlermeldung: " + e.getMessage());
                                    result.bind(myFehler);
                                }
                                break;

                            case "name":
                                this.setName(thisData);
                                break;

//                            case "vorname":
//                                this.setVorname(thisData);
//                                break;
                            case "geschlecht":
                                try {
                                    this.setGeschlecht(Geschlecht.valueOf(thisData));
                                } catch (Exception e) {
                                    System.out.println(CLASSNAME + " - Error on saving Geschlecht: " + e.getMessage());
                                    this.setGeschlecht(Geschlecht.u);
                                }
                                break;

                            // Spritze
                            // this.spritzenQuelle.set("");
                            // this.spritzelateralitaet.set("");
                            // this.vergangeneTage.set(0);
                            // this.medikament.set(AntiVEGFMedikament.UNKNOWN);
                            // this.Diskrepanz.set(true);
                            case "spritzenquelle":
                                this.setSpritzenQuelle(thisData);
                                break;

                            case "vergangenetage":
                                try {
                                    this.setVergangeneTage(Integer.parseInt(thisData));
                                } catch (NumberFormatException e) {
                                    StringProperty myFehler = new SimpleStringProperty();
                                    myFehler.set("Konnte den Wert für die vergangenen Tage nicht in Integer umwandeln. Datenwert: " + thisData);
                                    result.bind(myFehler);
                                }
                                break;
                            case "medikament":
                                this.setMedikament(convertToAntiVEGFMedikament(thisData));
                                break;

                            case "diskrepanz":
                                this.setDiskrepanz(thisData.toLowerCase().equals("true") || thisData.toLowerCase().equals("ja") || thisData.toLowerCase().equals("yes"));
                                break;

                            case "naechstesvisusdatum":
                                try {
                                    this.setNaechstesVisusDatum(LocalDate.parse(thisData, FORMATTER));
                                } catch (Exception e) {
                                }
                                break;

                            case "logmarvisuswert":
                                try {
                                    this.setLogMarVisusWert(Double.parseDouble(thisData));
                                } catch (NumberFormatException e) {
                                    this.setLogMarVisusWert(calculateLogMarVisusWert(LOGMAR_MAXIMUM, this.getDezimalVisusWert()));
                                    //StringProperty myFehler = new SimpleStringProperty();
                                    //myFehler.set("Konnte den Wert für den zeitlich dichtesten Visus nicht in eine Zahl umwandeln. Datenwert: " + thisData);
                                    //result.bind(myFehler);
                                }
                                break;

                            //        Untersuchung
                            //        this.untersuchungsID.set("");
                            //        this.untersuchungsCode.set("");
                            //        this.briefDaten.set("");
                            case "untersuchungsid":
                                this.setUntersuchungsID(thisData);
                                break;

                            case "untersuchungscode":
                                this.setUntersuchungsCode(thisData);
                                break;

                            case "briefdaten":
                                this.setBriefDaten(thisData);
                                break;

                            case "ereignisdatum":
                                try {
                                    this.setEreignisDatum(LocalDate.parse(thisData, FORMATTER));
                                } catch (Exception e) {
                                    StringProperty myFehler = new SimpleStringProperty();
                                    myFehler.set("Ereignisdatum fehlt.");
                                    result.bind(myFehler);
                                }
                                break;

                            //        Auge
                            //        this.augenID.set("");
                            //        this.untersuchunglateralitaet.set("");
                            //        this.augendruck.set(0.0);
                            case "augenid":
                                this.setAugenID(thisData);
                                break;

                            case "untersuchunglateralitaet":
                                this.setUntersuchungLateralitaet(convertToAugeLateralitaet(thisData));
                                break;

                            case "augendruck":
                                try {
                                    if (thisData.lastIndexOf(",") >= thisData.length() - 3) {
                                        thisData = thisData.replaceAll("\\.", "");
                                        thisData = thisData.replaceAll(",", ".");
                                    } else if (thisData.lastIndexOf(",") != -1) {
                                        thisData = thisData.replaceAll(",", "");
                                    }
                                    this.setAugendruck(Double.parseDouble(thisData));
                                } catch (NumberFormatException e) {

                                }

                                break;

                            case "augendruckfromletterextract":
                                try {
                                    if (thisData.lastIndexOf(",") >= thisData.length() - 3) {
                                        thisData = thisData.replaceAll("\\.", "");
                                        thisData = thisData.replaceAll(",", ".");
                                    } else if (thisData.lastIndexOf(",") != -1) {
                                        thisData = thisData.replaceAll(",", "");
                                    }
                                    this.setAugendruckFromLetterExtract(Double.parseDouble(thisData));
                                } catch (NumberFormatException e) {

                                }

                                break;

                            //        Visus
                            //        this.visusID.set("");
                            //        this.rohWert.set("");
                            //        this.visuswerte.set(Visuswerte.UNKNOWN);
                            //        this.dezimalVisusWert.set(0.0);
                            //        this.visusKlasse.set(0);
                            //        this.meterVisus.set(false);
                            //        this.visusKorrektur.set(VisusKorrektur.UNKNOWN);
                            //        this.sphaere.set(0.0);
                            //        this.zylinder.set(0.0);
                            //        this.achse.set(0.0);
                            //        this.lochblende.set(false);
                            //        this.ar_nicht_moeglich.set(false);
                            //        this.visusDatum.set(null);
                            //        this.daysPast.set(0);
                            //        this.aktuell.set(true);
                            //        this.visusDelta.set(0);
                            case "visusid":
                                this.setVisusID(thisData);
                                break;

                            case "rohwert":
                                this.setRohWert(thisData);
                                // Falls noch keine Visuswertgruppe gesetzt wurde
                                if (this.getVisuswerte().equals(Visuswerte.E)) {
                                    this.setVisuswerte(convertRohWertToVisusWert(thisData));
                                }
                                // Falls noch kein Dezimalvisuswert bestimmt wurde
                                if (Double.isNaN(this.getDezimalVisusWert())) {
                                    this.setDezimalVisusWert(Math.round(calculateDezimalVisusWert(this.getVisuswerte(), thisData) * 1000));
                                    this.setDezimalVisusWert(this.getDezimalVisusWert() / 1000);
                                }

                                if (!Double.isNaN(this.getVisusKlasse())) {
                                    this.setVisusKlasse(calculateVisusClass(this.getDezimalVisusWert()));
                                }
                                break;

                            case "visuswerte":
                                // Falls in den Daten bereits eine Visuswertgruppe enthalten ist
                                try {
                                    this.setVisuswerte(Visuswerte.valueOf(thisData));
                                } catch (Exception e) {
                                }
                                break;

                            case "dezimalvisuswert":
                                // Falls in den Daten bereits ein Dezimalvisuswert enthalten ist
                                try {
                                    this.setDezimalVisusWert(Double.parseDouble(thisData));
                                } catch (NumberFormatException e) {
                                }
                                break;

                            case "visusdelta":
                                try {
                                    this.setVisusDelta(Double.parseDouble(thisData));
                                } catch (NumberFormatException e) {
                                }
                                break;

                            case "visusklasse":
                                try {
                                    this.setVisusKlasse(Double.parseDouble(thisData));
                                } catch (NumberFormatException e) {
                                }
                                break;

                            case "visusklassedelta":
                                try {
                                    this.setVisusKlasseDelta(Double.parseDouble(thisData));
                                } catch (NumberFormatException e) {
                                }
                                break;

                            case "zeitSeitLetztemVisus":
                                try {
                                    this.setZeitSeitLetztemVisus(Period.parse(thisData));
                                } catch (NumberFormatException e) {
                                }
                                break;

                            case "visuskorrektur":
                                this.setVisusKorrektur(convertToVisusKorrektur(thisData));
                                break;

                            case "metervisus":
                                this.setMeterVisus(thisData.toLowerCase().equals("true") || thisData.toLowerCase().equals("ja") || thisData.toLowerCase().equals("yes"));
                                break;

                            case "sphaere":
                                try {
                                    this.setSphaere(Double.parseDouble(thisData));
                                } catch (NumberFormatException e) {
                                }
                                break;

                            case "zylinder":
                                try {
                                    this.setZylinder(Double.parseDouble(thisData));
                                } catch (NumberFormatException e) {
                                }
                                break;

                            case "achse":
                                try {
                                    this.setAchse(Double.parseDouble(thisData));
                                } catch (NumberFormatException e) {
                                }
                                break;

                            case "lochblende":
                                this.setLochblende(thisData.toLowerCase().equals("true") || thisData.toLowerCase().equals("ja") || thisData.toLowerCase().equals("yes"));
                                break;

                            case "ar_nicht_moeglich":
                                this.setAr_nicht_moeglich(thisData.toLowerCase().equals("true") || thisData.toLowerCase().equals("ja") || thisData.toLowerCase().equals("yes"));
                                break;

//                            case "visusdatum":
//                                try {
//                                    this.setVisusDatum(LocalDate.parse(thisData, FORMATTER));
//                                } catch (Exception e) {
//                                    StringProperty myFehler = new SimpleStringProperty();
//                                    myFehler.set("Visusdatum fehlt.");
//                                    result.bind(myFehler);
//                                }
//                                break;
                            case "dayspast":
                                try {
                                    this.setDaysPast(Integer.parseInt(thisData));
                                } catch (NumberFormatException e) {
                                }
                                break;

                            case "argosereignis":
                                this.setArgosEreignis(thisData.toLowerCase().equals("true") || thisData.toLowerCase().equals("ja") || thisData.toLowerCase().equals("yes"));
                                break;

                            case "argosereignisrelevanz":
                                this.setArgosEreignisRelevanz(thisData.toLowerCase().equals("true") || thisData.toLowerCase().equals("ja") || thisData.toLowerCase().equals("yes"));
                                break;

                            case "argosereignistext":
                                this.setArgosEreignisText(thisData);
                                break;

                            //        // Vorderer Augenabschnitt
                            //        this.vaaID.set("");
                            //        this.lid.set("");
                            //        this.bindehaut.set("");
                            //        this.hornhaut.set("");
                            //        this.vorderkammer.set("");
                            //        this.iris.set("");
                            //        this.linse.set("");
                            case "vaaid":
                                this.setVaaID(thisData);
                                break;

                            case "lid":
                                this.setLid(thisData);
                                break;

                            case "bindehaut":
                                this.setBindehaut(thisData);
                                break;

                            case "hornhaut":
                                this.setHornhaut(thisData);
                                break;

                            case "vorderkammer":
                                this.setVorderkammer(thisData);
                                break;

                            case "iris":
                                this.setIris(thisData);
                                break;

                            case "linse":
                                this.setLinse(thisData);
                                break;

                            //        // Fundus
                            //        this.fundusID.set("");
                            //        this.glaskoerper.set("");
                            //        this.papille.set("");
                            //        this.makula.set("");
                            //        this.peripherie.set("");
                            case "fundusid":
                                this.setFundusID(thisData);
                                break;

                            case "glaskoerper":
                                this.setGlaskoerper(thisData);
                                break;

                            case "papille":
                                this.setPapille(thisData);
                                break;

                            case "makula":
                                this.setMakula(thisData);
                                break;

                            case "peripherie":
                                this.setPeripherie(thisData);
                                break;

                            //        // Fluoreszenzangiographie
                            //        this.fagID.set("");
                            //        this.fagEigenschaften.set("");
                            case "fagid":
                                this.setFagID(thisData);
                                break;

                            case "fageigenschaften":
                                this.setFagEigenschaften(thisData);
                                break;

                            //        // OptischeKoheränztomographie
                            //        this.octID.set("");
                            //        this.octEigenschaften.set("");
                            //        this.octDatum.set(null);
                            case "octid":
                                this.setOctID(thisData);
                                break;

                            case "octeigenschaften":
                                this.setOctEigenschaften(thisData);
                                break;

                            case "octdatum":
                                try {
                                    this.setOctDatum(LocalTime.parse(thisData, FORMATTER));
                                } catch (Exception e) {
//                                            StringProperty myFehler = new SimpleStringProperty();
//                                            myFehler.set("Octdatum fehlt.");
//                                            result.bind(myFehler);
                                }
                                break;

                            //        // Diagnose
                            //        this.diagnoseID.set("");
                            //        this.diagnoseDatum.set(null);
                            //        this.hauptDiagnose.set(HauptDiagnosen.UNKNOWN);
                            //        this.nebenDiagnose.set("");
                            case "diagnoseid":
                                this.setDiagnoseID(thisData);
                                break;

                            case "diagnosedatum":
                                try {
                                    this.setDiagnoseDatum(LocalDate.parse(thisData, FORMATTER));
                                } catch (Exception e) {
                                }
                                break;

                            case "hauptdiagnose":
                                this.setHauptDiagnose(convertToHauptDiagnosen(thisData));
                                break;

                            case "hauptdiagnosefromletterextract":
                                this.setHauptDiagnoseFromLetterExtract(convertToHauptDiagnosen(thisData));
                                break;

                            case "nebendiagnose":
                                this.setNebenDiagnose(thisData);
                                break;

                            case "diagnosequelle":
                                this.setNebenDiagnose(thisData);
                                break;

                            case "annotated":
                                this.setAnnotated(thisData.toLowerCase().equals("true") || thisData.toLowerCase().equals("ja") || thisData.toLowerCase().equals("yes"));
                                break;
                            //        this.annotationText.set("");
                            case "annotationtext":
                                this.setAnnotationText(thisData);
                                break;

                            case "cleansed":
                                this.setCleansed(thisData.toLowerCase().equals("true") || thisData.toLowerCase().equals("ja") || thisData.toLowerCase().equals("yes"));
                                break;
                            default:
                            //System.out.println(CLASSNAME + " - Kann keine Spalte zuordnen:     MapKey: _" + mapKey + "_ MapValue: " + (int) mapValue + " FeldInhalt: " + thisData);

                        } // switch mapKey.toString()
                    } else {
                        StringProperty myBindString = new SimpleStringProperty("Der aktuelle Datensatz ist inkonsistent. Eventuell kann das entfernen von fehlerhaften Zeilenumbrüchen das Problem beheben.");
                        result.bind(myBindString);
                    } // Länge des Array nicht überschritten
                } // mapValue != -1

            } catch (NumberFormatException e) {
                System.out.println(CLASSNAME + " - NumberFormatException: MapKey: " + mapKey + " MapValue: " + (int) mapValue + " -- Fehlermeldung: " + e.getMessage());
            }

        } // myMap.forEach
        );

        String result2 = "";
        // Pflichtfelder prüfen
//        if (this.geburtsdatum
//                == null && this.name.get()
//                        .equals("")) {
//            result2 = "Die Pflichtfelder 'Geburtsdatum' und 'Name' haben keinen Wert.";
//        }
//
//        if (this.geburtsdatum
//                == null) {
//            //System.out.println(this.geburtsdatum);
//            result2 = "Pflichtfeld 'Geburtsdatum' hat keinen Wert.";
//        }
//
//        if (this.name.get()
//                .equals("")) {
//            result2 = "Pflichtfeld 'Name' hat keinen Wert.";
//        }

        return (result.get()
                .equals("")) ? result2 : result.get();

    } // procedure setAll

    // procedure to set the enum visuswerte to a specific value according to the rohwert
    public Visuswerte convertRohWertToVisusWert(String rohWert) {

        try {
            return Visuswerte.valueOf(rohWert);
        } catch (IllegalArgumentException e) {

            switch (rohWert.toUpperCase()) {
                case "NULLA LUX":
                case "NULLA-LUX":
                case "NLP":
                    return Visuswerte.NULLA_LUX;
                case "LICHTSCHEIN":
                case "LS":
                case "LP":
                    return Visuswerte.LICHTSCHEIN;
                case "HANDBEWEGUNGEN":
                case "HBW":
                case "HM":
                    return Visuswerte.HANDBEWEGUNGEN;
                case "FINGERZAEHLEN":
                case "FZ":
                case "CF":
                case "FINGERZÄHLEN":
                    return Visuswerte.FINGERZAEHLEN;
                case "TV":
                    return Visuswerte.TAFELVISUS;
                default:
                    if (rohWert.contains("TV")) {
                        return Visuswerte.TAFELVISUS;
                    }
                    try {
                        rohWert = rohWert.replace(",", ".");
                        double NumTest = Double.parseDouble(rohWert);
                        return Visuswerte.DEZIMALVISUS;
                    } catch (NumberFormatException eN) {
                        if (rohWert.contains("/")) {
                            return Visuswerte.DEZIMALVISUS;
                        }

                        return Visuswerte.UNKNOWN;
                    }
            }
        } catch (Exception e) {
            return Visuswerte.UNKNOWN;
        }

    } // procedure convertRohWertToVisusWert

    public double calculateDezimalVisusWert(Visuswerte normVisusWert, String rohWert) {

//        if (rohWert == null || rohWert.isEmpty()) {
//            System.out.println("now -----------------------------------------------------------------------------------");
//            return 0.0;
//        }
        rohWert = rohWert.toUpperCase();
        switch (normVisusWert) {
            case NULLA_LUX:
                return 0;
            case LICHTSCHEIN:
                return 0.002;
            case HANDBEWEGUNGEN:
                return 0.005;
            case FINGERZAEHLEN:
                return 0.01;
            case TAFELVISUS:
                try {
                    rohWert = rohWert.replace("TV", "");
                    String[] rohWerte = rohWert.split("/");
                    return Double.parseDouble(rohWerte[0]) / Double.parseDouble(rohWerte[1]);
                } catch (NumberFormatException e) {
                    return Double.NaN;
                }
            case DEZIMALVISUS:
                try {
                    rohWert = rohWert.replace(",", ".");
                    return Double.parseDouble(rohWert);
                } catch (NumberFormatException e) {
                    return Double.NaN;
                }
            default:
                return Double.NaN;
        }
    } // Procedure calculateLogMarVisusWert

    public double calculateLogMarVisusWert(double maxLogMar, double dezimalVisusWert) {

        double visLogMar;
        if (dezimalVisusWert == 0.0) {
            return maxLogMar;
        }
        if (dezimalVisusWert == Double.NaN) {
            return Double.NaN;
        }

        visLogMar = Math.log10(1 / dezimalVisusWert);
        return visLogMar;

    } // Procedure calculateLogMarVisusWert

    // procedure to set the enum medikament to a specific value
    public static AntiVEGFMedikament convertToAntiVEGFMedikament(String rohMedikament) {

        String[] possibleMedication = {"AVASTIN", "BEVA", "BEVACIZUMAB", "LUCENTIS", "RANI", "RANIBIZUMAB", "EYLEA", "AFLI", "AFLIBERCEPT", "OZUR", "OZURDEX", "DEXAMETASON", "TRIAMCINOLON"};
        String key = "";

        for (String possType : possibleMedication) {
            if (rohMedikament.toUpperCase().contains(possType)) {
                key = possType;
            }
        }

        try {
            return AntiVEGFMedikament.valueOf(rohMedikament);
        } catch (IllegalArgumentException e) {
            switch (key.toUpperCase()) {
                case "AVASTIN":
                case "BEVACIZUMAB":
                case "BEVA":
                    return AntiVEGFMedikament.AVASTIN;

                case "LUCENTIS":
                case "RANIBIZUMAB":
                case "RANI":
                    return AntiVEGFMedikament.LUCENTIS;

                case "EYLEA":
                case "AFLIBERCEPT":
                case "AFLI":
                    return AntiVEGFMedikament.EYLEA;

                case "OZURDEX":
                case "DEXAMETASON":
                case "DEXA":
                    return AntiVEGFMedikament.OZURDEX;

                case "TRIAMCINOLON":
                case "TRIAM":
                    return AntiVEGFMedikament.TRIAMCINOLON;

                default:
                    return AntiVEGFMedikament.UNKNOWN;
            }
        } catch (Exception e) {
            return AntiVEGFMedikament.UNKNOWN;
        }

//        Beva(cizumab) = Avastin
//        Rani(bizumab) = Lucentis
//        Afli(bercept) = Eylea
//        Dexamethason = Ozur(dex)
//        Triamcinolon(das steht nicht in der Liste
//        , kann aber nicht sein, ich schau, warum
    } // procedure convertHauptDiagnosen

    // procedure to set the enum visuskorrekutr to a specific value according to the rohKorrektur
    public VisusKorrektur convertToVisusKorrektur(String rohKorrektur) {

        try {
            return VisusKorrektur.valueOf(rohKorrektur);
        } catch (IllegalArgumentException e) {

            switch (rohKorrektur.toUpperCase()) {
                case "EB":
                case "EIGENE BRILLE":
                    return VisusKorrektur.EB;
                case "AR":
                case "AUTOREFRAKTION":
                    return VisusKorrektur.AR;
                case "KL":
                case "KONTAKTLINSEN":
                    return VisusKorrektur.KL;
                default:
                    return VisusKorrektur.UNKNOWN;
            }
        } catch (Exception e) {

            return VisusKorrektur.UNKNOWN;
        }

    } // procedure convertToVisusKorrektur

    // procedure to set the enum visuswerte to a specific value according to the rohwert
    public static HauptDiagnosen convertToHauptDiagnosen(String rohDiagnose) {

        try {
            return HauptDiagnosen.valueOf(rohDiagnose);
        } catch (IllegalArgumentException e) {
            switch (rohDiagnose.toUpperCase()) {
                case "FEUCHTE ALTERSBEDINGTE MAKULADEGENERATION":
                case "FEUCHTE AMD":
                    return HauptDiagnosen.AMD_FEUCHT;
                case "AMD":
                case "MAKULADEGENERATION":
                case "ALTERSABHÄNGIGE MAKULADEGENERATION":
                case "EXSUDATIVE ALTERSABHÄNGIGE MAKULADEGENERATION":
                    return HauptDiagnosen.AMD_ALLGEMEIN;
                case "TROCKENE ALTERSBEDINGTE MAKULADEGENERATION":
                case "TROCKENE ALTERSABHÄNGIGE MAKULADEGENERATION":
                case "TROCKENE AMD":
                    return HauptDiagnosen.AMD_TROCKEN;
                case "DMOE":
                case "RETINOPATHIA DIABETICA":
                case "DIABETISCHES MAKULAÖDEM":
                    return HauptDiagnosen.DMOE;
                case "VAV":
                case "VENENASTVERSCHLUSS":
                    return HauptDiagnosen.VAV;
                case "ZVV":
                case "ZENTRALVENENVERSCHLUSS":
                case "HEMIZENTRALVENENVERSCHLUSS":
                    return HauptDiagnosen.ZVV;
                default:
                    return HauptDiagnosen.UNKNOWN;
            }
        } catch (Exception e) {
            return HauptDiagnosen.UNKNOWN;
        }

    } // procedure convertToHauptDiagnosen

    // procedure to set the enum lateralitaet und auge_lateralitaet to a specific value
    public AugeLateralitaet convertToAugeLateralitaet(String rohLateralitaet) {

        try {
            return AugeLateralitaet.valueOf(rohLateralitaet);
        } catch (IllegalArgumentException e) {
            switch (rohLateralitaet.toUpperCase()) {
                case "L":
                case "LEFT":
                case "LINKS":
                    return AugeLateralitaet.LEFT;
                case "R":
                case "RIGHT":
                case "RECHTS":
                    return AugeLateralitaet.RIGHT;
                default:
                    return AugeLateralitaet.UNKNOWN;
            }
        } catch (Exception e) {
            return AugeLateralitaet.UNKNOWN;
        }

    } // procedure convertHauptDiagnosen

    // Prozedur, um eine Map mit allen Attributen sowie deren Typ zurückzugeben
    public static ObservableMap<String, DataModel.DataTypes> getDimensions() {

        ObservableMap<String, DataModel.DataTypes> myMap = FXCollections.observableHashMap();
        Field[] fields = DataModel.class.getDeclaredFields();
        for (Field field : fields) {
            String[] values = {"IntegerProperty", "DoubleProperty", "StringProperty", "ObjectProperty", "NumberProperty"};
            String key = "";
            for (String value : values) {
                if (field.getType().toString().contains(value)) {
                    key = value;
                }
            }

            DataTypes myType;
            switch (key) {
                case "IntegerProperty":
                case "NumberProperty":
                case "DoubleProperty":
                    myType = DataTypes.NUMBER;
                    break;
                case "StringProperty":
                    myType = DataTypes.TEXT;
                    break;
                case "BooleanProperty":
                    myType = DataTypes.BOOLEAN;
                    break;
                default:
                    myType = DataTypes.OTHER;
            }
            try {
                myMap.put(field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1), myType);
            } catch (Exception e) {
                System.out.println(CLASSNAME + " - Exception: " + e.getMessage());
            }
        }
        return myMap;
    } // getDimensions

    // Funktion zur Ermittlung aller Gruppen einer Dimension sowie zur Ermittlung aller Datensätze für die jeweiligen Gruppen. 
    // Es wird eine Map zurück gegeben, die für eine Dimension alle unterschiedlichen Werte als Schlüssel enthält und als Value eine neue leere PunkteMenge für einen Chart
    @SuppressWarnings("unchecked")
    public ObservableMap<String, XYChart.Series> getValueList(List<DataModel> myData, List<String> groups, String xDimension, String yDimension) {

        // System.out.println("Gruppe: " + group + " xDimension: " + xDimension + " yDimension" + yDimension);
        // zurückzugebende Map
        ObservableMap<String, XYChart.Series> myMap = FXCollections.observableHashMap();

        // Aufgabe: eine Map erstellen, welche alle verschiedenen Werte der Gruppierungsdimension als Schlüssel enthält und darüber hinaus zu jedem Schlüssel alle Wertepaare zu ausgewählten Dimensionen (xDimension, yDimension) als Value
        try {

            // Alle ausgewählten (gecheckten) Attribute ermitteln, welche als Gruppierung dienen sollen
            List<Field> groupFields = new ArrayList();
            groups.forEach((group) -> {
                try {
                    groupFields.add(DataModel.class.getDeclaredField(group.substring(0, 1).toLowerCase() + group.substring(1)));

                } catch (NoSuchFieldException | SecurityException e) {
                    System.out.println(CLASSNAME + " - Could not get Field of Group: " + e.getMessage());
                }
            });

            Field xField = DataModel.class.getDeclaredField(xDimension);
            Field yField = DataModel.class.getDeclaredField(yDimension);

            // Nun muss für dieses Attribut geprüft werden, um welchen Datentyp es sich handelt.
            // Dann wird die Map mit den unterschiedlichen Werten des Attributs gefüllt.
            myData.forEach(myDatum -> {
                //System.out.println("Value: " + getValueFromFields(groupFields, myDatum));
                myMap.put("" + getValueFromFields(groupFields, myDatum), new XYChart.Series<>());
            });

            // Jetzt wird für jede Gruppe die Datenserie gefüllt
            myData.forEach((DataModel myDatum) -> {
                String groupMapID = getValueFromFields(groupFields, myDatum);
                XYChart.Series<?, ?> serie = myMap.get(groupMapID);
//                try {
                serie.setName(groupMapID);
                serie.getData().add(new XYChart.Data(getValueFromField(xField, myDatum), getValueFromField(yField, myDatum)));
//                } catch (NullPointerException e) {
//                    System.out.println("Konnte Datenwert nicht hinzufügen: x - " + getValueFromField(xField, myDatum) + " y - " + getValueFromField(yField, myDatum));
//                }
                myMap.put(groupMapID, serie);
            });

        } catch (IllegalArgumentException | NoSuchFieldException | SecurityException e) {
            System.out.println(CLASSNAME + " - General Error: " + e.getMessage());
        }

        //field.getType()
        //System.out.println(myMap);
        return myMap;
    }

    // Funktion, um zu einem Field das entsprechende Element in einen Datensatz zu finden
    public Object getValueFromField(Field field, DataModel myDatum) {

        String fieldType = field.getType().toString();
        String[] possibleTypes = {"IntegerProperty", "StringProperty", "DoubleProperty", "BooleanProperty", "ObjectProperty"};
        String key = "";

        for (String possType : possibleTypes) {
            if (fieldType.contains(possType)) {
                key = possType;
            }
        }

        switch (key) {
            case "IntegerProperty":
                try {
                    // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                    return ((IntegerProperty) field.get(myDatum)).get();
                } catch (IllegalAccessException | IllegalArgumentException e) {
                    System.out.println(CLASSNAME + " - getValueFromField - IntegerProperty Error: " + e.getMessage());
                }
                break;

            case "StringProperty":
                try {
                    // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                    return ((StringProperty) field.get(myDatum)).get();
                } catch (IllegalAccessException | IllegalArgumentException e) {
                    System.out.println(CLASSNAME + " - getValueFromField - StringProperty Error: " + e.getMessage());
                }
                break;

            case "DoubleProperty":
                try {
                    // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                    return ((DoubleProperty) field.get(myDatum)).get();
                } catch (IllegalAccessException | IllegalArgumentException e) {
                    System.out.println(CLASSNAME + " - getValueFromField - ObjectProperty error:" + e.getMessage());
                }
                break;

            case "BooleanProperty":

                try {
                    // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                    return field.getBoolean(myDatum); // ? "true" : "false";
                } catch (IllegalAccessException | IllegalArgumentException e) {
                    System.out.println(CLASSNAME + " - getValueFromField - BooleanProperty Error: " + e.getMessage());
                }
                break;

            case "ObjectProperty":
                try {
                    // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                    return "" + ((ObjectProperty<?>) field.get(myDatum)).get();
                } catch (IllegalAccessException | IllegalArgumentException e) {
                    System.out.println(CLASSNAME + " - ObjectProperty error:" + e.getMessage());
                }
                break;

            default:
                return "";
        }
        return "";
    } // procedure getValueFromField

    // Überladene Funktion für getValueFromFields, mit der Standardoption, dass der Attributname in den Rückgabewert aufgenommen wird
    public String getValueFromFields(List<Field> fields, DataModel myDatum) {
        return getValueFromFields(fields, myDatum, true);
    }

    // Überladene Funktion für getValueFromFields, mit einem Feld, statt einer Feldliste
    @SuppressWarnings("unchecked")
    public String getValueFromFields(Field field, DataModel myDatum) {
        List<Field> fields = new ArrayList();
        fields.add(field);
        return getValueFromFields(fields, myDatum, false);
    }

    // Funktion, um zu einem Field das entsprechende Element in einen Datensatz zu finden
    public String getValueFromFields(List<Field> fields, DataModel myDatum, boolean addAttName) {

        String returnString = "";
        for (Field field : fields) {

            String fieldType = field.getType().toString();
            String[] possibleTypes = {"IntegerProperty", "StringProperty", "DoubleProperty", "BooleanProperty", "ObjectProperty"};
            String key = "";

            for (String possType : possibleTypes) {
                if (fieldType.contains(possType)) {
                    key = possType;
                }
            }

            switch (key) {
                case "IntegerProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((IntegerProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((IntegerProperty) field.get(myDatum)).get());
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println(CLASSNAME + " - IntegerProperty Error: " + e.getMessage());
                    }
                    break;

                case "StringProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((StringProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((StringProperty) field.get(myDatum)).get());

                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println(CLASSNAME + " - StringProperty Error: " + e.getMessage());
                    }
                    break;

                case "DoubleProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((DoubleProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((DoubleProperty) field.get(myDatum)).get());
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println(CLASSNAME + " - ObjectProperty error:" + e.getMessage());
                    }
                    break;

                case "BooleanProperty":

                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((BooleanProperty) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((BooleanProperty) field.get(myDatum)).get());
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println(CLASSNAME + " - BooleanProperty Error: " + e.getMessage());
                    }
                    break;

                case "ObjectProperty":
                    try {
                        // Den Wert dieses Datensatzes auf dem Attribut ermitteln und zurückgeben
                        returnString = addAttName
                                ? (returnString + "\n" + field.getName() + ": '" + ((ObjectProperty<?>) field.get(myDatum)).get() + "'")
                                : (returnString + "_" + ((ObjectProperty<?>) field.get(myDatum)).get());

                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        System.out.println(CLASSNAME + " - ObjectProperty error:" + e.getMessage());
                    }
                    break;

                default:
                    //System.out.println(field.getName());
                    returnString = returnString + "_";
            } // switch key
        } // forEach field

        //System.out.println(returnString);
        return (returnString.length() > 1) ? returnString.substring(1) : ""; //returnString;

    } // procedure getValueFromField

    // Procedure to generate dataGroups out of the data model 
    // The return value is a map, that has a data collection for each group.
    // Each group members are identified with an individual key within the map.
    @SuppressWarnings("unchecked")
    public void getDataGroups(ObservableMap<String, HashMap<String, Double[]>> myMap, List<DataModel> myData, List<String> groups, String xDimension, String yDimension, int yIndex, int maxIndex) {

        // Aufgabe: eine Map erstellen, welche alle verschiedenen Werte der Gruppierungsdimension als Schlüssel enthält und darüber hinaus zu jedem Schlüssel alle Wertepaare zu ausgewählten Dimensionen (xDimension, yDimension) als Value
        try {

            // Alle ausgewählten Attribute ermitteln, welche als Gruppierung dienen sollen
            List<Field> groupFields = new ArrayList();
            groups.forEach((group) -> {

                try {
                    //System.out.println(group.substring(0, 1).toLowerCase() + group.substring(1));
                    groupFields.add(DataModel.class.getDeclaredField(group.substring(0, 1).toLowerCase() + group.substring(1)));

                } catch (NoSuchFieldException | SecurityException e) {
                    System.out.println(CLASSNAME + " - Could not get Field of Group: " + e.getMessage());
                }
            });

            // Die X-Dimension (für VisusFirst --> Datum)
            Field xField = DataModel.class.getDeclaredField(xDimension);

            // Das entsprechende Listenfeld ermitteln
            Field yField = DataModel.class.getDeclaredField(yDimension);
            // Nun muss für dieses Attribut geprüft werden, um welchen Datentyp es sich handelt.
            // Dann wird die Map mit den unterschiedlichen Werten des Attributs gefüllt.
            myData.forEach(myDatum -> {
                //System.out.println("Value: " + getValueFromFields(groupFields, myDatum));
                String fieldValue = getValueFromFields(groupFields, myDatum, false);
                if (!myMap.containsKey(fieldValue)) {
                    myMap.put("" + getValueFromFields(groupFields, myDatum, false), new HashMap());
                }
            });

            // Jetzt wird für jede Gruppe die Datenserie gefüllt
            myData.forEach((DataModel myDatum) -> {
                // groupMapID : 
                String groupMapID = getValueFromFields(groupFields, myDatum, false);
                String xDataKey = getValueFromField(xField, myDatum).toString();
                Double[] myValues = (myMap.get(groupMapID).containsKey(xDataKey)) ? myMap.get(groupMapID).get(xDataKey) : new Double[maxIndex];
                myValues[yIndex] = convertToDouble(yField, myDatum);
                myMap.get(groupMapID).put(xDataKey, myValues);
            });
        } catch (IllegalArgumentException | NoSuchFieldException | SecurityException e) {
            System.out.println(CLASSNAME + " - General Error: " + e.getMessage());
        }

        ObservableList<String> removeKeys = FXCollections.observableArrayList();
        myMap.forEach((key, item) -> {
            if (key.contains("null")) {
                removeKeys.add(key);
            }
            if (key.endsWith("_E")) {
                removeKeys.add(key);
            }
        });
        removeKeys.forEach(key -> myMap.remove(key));
        //myMap.forEach((key,value) -> System.out.println("Key: " + key + " size: " + value.size()));
    }

    //Funktion, um ein Element mit bestimmten Attributen in einer Liste zu finden
    @SuppressWarnings("unchecked")
    public List<DataModel> getElement(List<DataModel> data, HashMap<String, String> filterFields) {
        List<DataModel> result = new ArrayList();
        data.forEach((datum) -> {
            BooleanProperty isMember = new SimpleBooleanProperty(true);
            filterFields.forEach((fieldString, content) -> {
                Field field;
                try {
                    field = DataModel.class.getDeclaredField(fieldString);
                    if (!getValueFromFields(field, datum).equals(content)) {
                        isMember.set(false);
                    }
                } catch (NoSuchFieldException | SecurityException ex) {
                    Logger.getLogger(DataModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }); // forEach field
            if (isMember.get()) {
                result.add(datum);
            }
        }); // forEach datum
        return result;
    }

    /**
     * Funktion, um einen Datensatz zu erstellen, der alle Werte eines anderen
     * Datensatzes sowie deren Häufigkeiten redundanzfrei enthält
     *
     * @param filteredListData Map, mit den Attributen als Schlüssel und jeweils
     * einer Liste mit den Werten nebst Häufigkeiten als value
     * @param myData alle Datensätze, für die Häufigkeiten ermittelt werden
     * sollen
     */
    @SuppressWarnings("unchecked")
    public void getValueQuantities(ObservableMap<String, List<String>> filteredListData, List<DataModel> myData) {

        // alte Häufigkeiten löschen
        filteredListData.clear();
        ObservableMap<String, HashMap<String, Integer>> filteredData = FXCollections.observableHashMap();
        // Aufgabe: eine Map erstellen, welche alle verschiedenen Werte der jeweiligen Attribute, sowie deren Häufigkeit enthält
        try {

            // Zunächst die Liste löschen
            filteredData.clear();

            // Alle ausgewählten Attribute ermitteln
            List<Field> groupFields = Arrays.asList(DataModel.class
                    .getDeclaredFields());

            groupFields.forEach((groupField) -> {
                filteredData.put(groupField.getName(), new HashMap());
            });

            // Jetzt wird für jedes Datenfeld die Liste unterschiedlicher Werte, sowie deren Häufigkeit ermittelt
            myData.forEach((myDatum) -> {

                // Jedes Attribut durchgehen
                groupFields.forEach((groupField) -> {
                    // Den aktuellen Datenwert für das aktuelle Attribut lesen
                    String valueKey = getValueFromFields(groupField, myDatum);
                    // Falls dieser Datenwert noch nicht vorkommt
                    if (!filteredData.get(groupField.getName()).containsKey(valueKey)) {
                        // Diesen Datenwert mit der Häufigkeit 1 schreiben
                        filteredData.get(groupField.getName()).put(valueKey, 1);
                    } else {
                        // Falls der Datenwert schon vorkommt, die Häufigkeit um eines erhöhen
                        filteredData.get(groupField.getName()).put(valueKey, filteredData.get(groupField.getName()).get(valueKey) + 1);
                    }

                });
            });
        } catch (IllegalArgumentException | SecurityException e) {
            System.out.println(CLASSNAME + " - General Error: " + e.getMessage());
        }

        // Die zusammengestellte Map mit den Werten und Häufigkeiten in eine Liste überführen
        filteredData.forEach((key, datum) -> {
            filteredListData.put(key, new ArrayList());
            datum.forEach((key2, datum2) -> {
                filteredListData.get(key).add(key2 + " (" + datum2 + ")");
            });
            filteredListData.get(key).sort(Comparator.naturalOrder());
        });
    }

    public double calculateVisusClass(double dezVisusWert) {

        // If an undefined value has been proceeded to this function
        if (Double.isNaN(dezVisusWert)) {
            return Double.NaN;
        }

        double tempClass = 6.0f;
        // Better than 0.5

        // above 0.5 allows several everyday skills like driving and reading
        if (dezVisusWert < 0.5f) {
            tempClass = 5.0f;
        }
        // accepted standard for visual disability by the WHO
        if (dezVisusWert <= 0.3f) {
            tempClass = 4.0f;
        }
        // intermediate value to split a large class
        if (dezVisusWert < 0.16f) {
            tempClass = 3.0f;
        }
        // 0.05 is the border for IVOM treatment
        if (dezVisusWert < 0.05f) {
            tempClass = 2.0f;
        }
        // Can see hardly anything
        if (dezVisusWert <= 0.01f) {
            tempClass = 1.0f;
        }

        return tempClass;
    }

    private double convertToDouble(Field field, DataModel myDatum) {

        String fieldType = field.getType().toString();
        String[] possibleTypes = {"IntegerProperty", "StringProperty", "DoubleProperty", "BooleanProperty", "ObjectProperty"};
        String key = "";
        String value = getValueFromFields(field, myDatum);

        for (String possType : possibleTypes) {
            if (fieldType.contains(possType)) {
                key = possType;
            }
        }

        switch (key) {
            case "IntegerProperty":
            case "DoubleProperty":
                return Double.parseDouble(value);
            case "StringProperty":
                System.out.println("string");
                return 1.0f;
            case "BooleanProperty":
                return value.equals("true") ? 1.0f : 0.0f;
            case "ObjectProperty":
                switch (field.getName().toLowerCase()) {
                    case "medikament":
                        double ord = AntiVEGFMedikament.valueOf(getValueFromFields(field, myDatum)).ordinal();
                        // Die ersten beiden Werte des Medikaments sind leer und unknown --> ungültige Werte
                        return ord < 2f ? Double.NaN : ord - 2f;
                }
            default:
                return 1.0f;
        }
    }

    public void setValue(Object myValue, String attribut) {

    }

    public void copyFromOther(DataModel original) {
        this.initializeAll(this, "Manueller Eintrag", -1);
        this.setAnnotated(true);
        this.setPatientenID(original.getPatientenID());
        this.setUntersuchungLateralitaet(original.getUntersuchungLateralitaet());
        this.setGeburtsdatum(original.getGeburtsdatum());
        this.setGeschlecht(original.getGeschlecht());
        this.setEreignisDatum(LocalDate.now());
    }

    public static String customCellFactory(Field field) {

        String returnString = "";

        String fieldType = field.getType().toString();
        String[] possibleTypes = {"IntegerProperty", "StringProperty", "DoubleProperty", "BooleanProperty", "ObjectProperty", "java.lang.String"};

        for (String possType : possibleTypes) {
            if (fieldType.contains(possType)) {
                returnString = possType;
            }
        }

        //System.out.println(returnString);
        return returnString;

    } // procedure getValueFromField

    public VisusTafel parseVisusWert(String rohWert) {

        VisusTafel result = VisusTafel.DUNKNOWN;
        try {
            double testVar = Math.round(Double.parseDouble(rohWert) * 1000.0) / 1000.0;
            String testString = "" + testVar;
            result = VisusTafel.valueOf("D" + testString.replace(".", "K"));
            //System.out.println("YES!!!!  - D" + testString.replace(".", "K"));

        } catch (Exception e) {
            System.out.println("NO         D" + rohWert.replace(".", "K"));
        }
//        try {
//            return Visuswerte.valueOf(rohWert);
//        } catch (IllegalArgumentException e) {
//
//            switch (rohWert.toUpperCase()) {
//                case "NULLA LUX":
//                case "NULLA-LUX":
//                case "NLP":
//                    return Visuswerte.NULLA_LUX;
//                case "LICHTSCHEIN":
//                case "LS":
//                case "LP":
//                    return Visuswerte.LICHTSCHEIN;
//                case "HANDBEWEGUNGEN":
//                case "HBW":
//                case "HM":
//                    return Visuswerte.HANDBEWEGUNGEN;
//                case "FINGERZAEHLEN":
//                case "FZ":
//                case "CF":
//                case "FINGERZÄHLEN":
//                    return Visuswerte.FINGERZAEHLEN;
//                case "TV":
//                    return Visuswerte.TAFELVISUS;
//                default:
//                    if (rohWert.contains("TV")) {
//                        return Visuswerte.TAFELVISUS;
//                    }
//                    try {
//                        rohWert = rohWert.replace(",", ".");
//                        double NumTest = Double.parseDouble(rohWert);
//                        return Visuswerte.DEZIMALVISUS;
//                    } catch (NumberFormatException eN) {
//                        if (rohWert.contains("/")) {
//                            return Visuswerte.DEZIMALVISUS;
//                        }
//
//                        return Visuswerte.UNKNOWN;
//                    }
//            }
//        } catch (Exception e) {
//            return Visuswerte.UNKNOWN;
//        }
//        
//        
//        
        return VisusTafel.D0K0;
    }

    public static void createSingleEreignis(Map<String, String> ereignisData, ObservableList<DataModel> ereignisse) {

        if (ereignisData.containsKey("Patient: ") && ereignisData.containsKey("Lateralität: ")) {

            //System.out.println(ereignisData);

            DataModel ereignis = new DataModel();
            ereignis.initializeAll();

            //(ereignis.getPatientenID() + "_" + ereignis.getUntersuchungLateralitaet(), new DataModel());
            ereignis.setPatientenID(ereignisData.get("Patient: ")); // + "_" + ereignisData.get("Lateralität: "));
            ereignis.setUntersuchungLateralitaet(ereignisData.get("Lateralität: ").equals("LEFT") ? AugeLateralitaet.LEFT : AugeLateralitaet.RIGHT);
            ereignis.setAugenID(ereignisData.get("Lateralität: ").equals("LEFT") ? "_l_" + ereignis.getEreignisDatum() : "_r_" + ereignis.getEreignisDatum());
            ereignis.setDatenquelle("User Annotation vom " + ereignisData.get("Aktuelles Datum: "));
            ereignis.setAnnotated(true);

            if (ereignisData.containsKey("Benutzer: ")) {
                ereignis.setAnnotationText(ereignis.getAnnotationText() + "\n + Benutzer: " + ereignisData.get("Benutzer: "));
            }
            if (ereignisData.containsKey("Aktuelles Datum: ")) {
                ereignis.setAnnotationText(ereignis.getAnnotationText() + "\n + Aktuelles Datum: " + ereignisData.get("Aktuelles Datum: "));
            }
            if (ereignisData.containsKey("Datum des neuen Wertes: ")) {
                final String DATE_FORMAT = "yyyy-MM-dd";
                final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
                ereignis.setEreignisDatum(LocalDate.parse(ereignisData.get("Datum des neuen Wertes: "), FORMATTER));
            }
            if (ereignisData.containsKey("Datenattribut für den neuen Wert: ")) {
                switch (ereignisData.get("Datenattribut für den neuen Wert: ")) {
                    case "Außergewöhnliches Ereignis":
                        ereignis.setArgosEreignis(true);
                        ereignis.setArgosEreignisText(ereignisData.get("Neuer Datenwert: "));
                        break;
                    case "Injektion":
                        ereignis.setMedikament(convertToAntiVEGFMedikament(ereignisData.get("Neuer Datenwert: ")));
                    case "Visusmessung":
                    default:
                        try {
                            ereignis.setRohWert(ereignisData.get("Neuer Datenwert: "));
                            ereignis.setVisuswerte(ereignis.convertRohWertToVisusWert(ereignis.getRohWert()));
                            ereignis.setDezimalVisusWert(ereignis.calculateDezimalVisusWert(ereignis.getVisuswerte(), ereignis.getRohWert()));
                            ereignis.setLogMarVisusWert(ereignis.calculateLogMarVisusWert(LOGMAR_MAXIMUM, ereignis.getDezimalVisusWert()));
                            ereignis.setVisusKlasse(ereignis.calculateVisusClass(ereignis.getDezimalVisusWert()));
                            //                        ereignis.setDezimalVisusWert(Double.parseDouble(ereignisData.get("Neuer Datenwert: ")));
                        } catch (NumberFormatException e) {
                        }
                }

            }
            ereignis.setAnnotationText(ereignis.getAnnotationText() + "\n + Aktuelles Datum: " + ereignisData.get("Datenattribut für den neuen Wert: "));
           System.out.println(ereignis.getPatientenID());
            ereignisse.add(ereignis);
        }
//        cleansingDateFieldLabel.setText("Datum des neuen Wertes: ");
//        cleansingDataAttributFieldLabel.setText("Datenattribut für den neuen Wert: ");
//        cleansingDataTypeFieldLabel.setText("Datentyp des neuen Wertes: ");
//        cleansingValueFieldLabel.setText("Neuer Datenwert: ");
//            rightTemp.get().setPatientenID(stammdaten.getId());
//            .rightTemp.get().setGeburtsdatum(LocalDate.parse(stammdaten.getGeburtsdatum(), FORMATTER));
//            rightTemp.get().setGeschlecht(DataModel.Geschlecht.valueOf(stammdaten.getGeschlecht()));
//            rightTemp.get().setEreignisDatum(letterDate);
    }

}
