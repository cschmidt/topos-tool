/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

/**
 *
 * @author lokal-admin
 */
public class JsonAnnotation {

    private String patientID;
    private String patientNo;
    private String lateralitaet;
    private String annoField;
    private String annoDate;
    private String oldValue;
    private String newValue;
    private String comment;
    private String user;
    private String confirmations;
    private String status;

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(String patientNo) {
        this.patientNo = patientNo;
    }

    public String getLateralitaet() {
        return lateralitaet;
    }

    public void setLateralitaet(String lateralitaet) {
        this.lateralitaet = lateralitaet;
    }

    public String getAnnoField() {
        return annoField;
    }

    public void setAnnoField(String annoField) {
        this.annoField = annoField;
    }

    public String getAnnoDate() {
        return annoDate;
    }

    public void setAnnoDate(String annoDate) {
        this.annoDate = annoDate;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getConfirmations() {
        return confirmations;
    }

    public void setConfirmations(String confirmations) {
        this.confirmations = confirmations;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
