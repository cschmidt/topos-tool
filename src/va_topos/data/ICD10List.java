/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import java.util.HashMap;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableMap;

/**
 *
 * @author lokal-admin
 */
@SuppressWarnings("unchecked")
public class ICD10List {

    /**
     *
     * @author Christoph Schmidt
     */
    private static final String CLASSNAME = ICD10List.class.toString();

    //kodierung	lfdNr	Druckkennzeichen	primaerschluessel	sternschluessel	zusatzschluessel	text
    private String kodierung = "";
    private String lfdNr = "";
    private String druckkennzeichen = "";
    private String primaerschluessel = "";
    private String sternschluessel = "";
    private String zusatzschluessel = "";
    private String text = "";

    public String getKodierung() {
        return kodierung;
    }

    public void setKodierung(String kodierung) {
        this.kodierung = kodierung;
    }

    public String getLfdNr() {
        return lfdNr;
    }

    public void setLfdNr(String lfdNr) {
        this.lfdNr = lfdNr;
    }

    public String getDruckkennzeichen() {
        return druckkennzeichen;
    }

    public void setDruckkennzeichen(String druckkennzeichen) {
        this.druckkennzeichen = druckkennzeichen;
    }

    public String getPrimaerschluessel() {
        return primaerschluessel;
    }

    public void setPrimaerschluessel(String primaerschluessel) {
        this.primaerschluessel = primaerschluessel;
    }

    public String getSternschluessel() {
        return sternschluessel;
    }

    public void setSternschluessel(String sternschluessel) {
        this.sternschluessel = sternschluessel;
    }

    public String getZusatzschluessel() {
        return zusatzschluessel;
    }

    public void setZusatzschluessel(String zusatzschluessel) {
        this.zusatzschluessel = zusatzschluessel;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean setAll(HashMap myMap, String[] myDataLine, String datenQuelle) {

        BooleanProperty result = new SimpleBooleanProperty(true);

        // Zunächst alles initialisieren
        this.kodierung = "";
        this.lfdNr = "";
        this.druckkennzeichen = "";
        this.primaerschluessel = "";
        this.sternschluessel = "";
        this.zusatzschluessel = "";
        this.text = "";

        myMap.forEach((mapKey, mapValue) -> {
            try {
//                System.out.println("mapKey: " + mapKey + " mapValue: " + mapValue);
                if ((int) mapValue != -1) {
                    if ((int) mapValue < myDataLine.length) {
                        String thisData = myDataLine[(int) mapValue];
                        thisData = !thisData.isEmpty() ? thisData : "";

                        switch (mapKey.toString().toLowerCase()) {

                            case "kodierung":
                                this.setKodierung(thisData);
                                break;
                            case "lfdnr":
                                this.setLfdNr(thisData);
                                break;
                            case "druckkennzeichen":
                                this.setDruckkennzeichen(thisData);
                                break;
                            case "primaerschluessel":
                                this.setPrimaerschluessel(thisData);
                                break;
                            case "sternschluessel":
                                this.setSternschluessel(thisData);
                                break;
                            case "zusatzschluessel":
                                this.setZusatzschluessel(thisData);
                                break;
                            case "text":
                                this.setText(thisData);
                                break;

                            default:
                                //System.out.println(CLASSNAME + " - Kann keine Spalte zuordnen:     MapKey: " + mapKey + " MapValue: " + (int) mapValue + " FeldInhalt: " + thisData);
                                result.set(false);
                        } // switch mapKey.toString()
                    } else {
                        System.out.println("Der aktuelle Datensatz ist inkonsistent. Eventuell kann das entfernen von fehlerhaften Zeilenumbrüchen das Problem beheben.");
                        //StringProperty myBindString = new SimpleStringProperty("Der aktuelle Datensatz ist inkonsistent. Eventuell kann das entfernen von fehlerhaften Zeilenumbrüchen das Problem beheben.");
                        result.set(false);
                        //result.bind(myBindString);
                    } // Länge des Array nicht überschritten
                } // mapValue != -1

            } catch (NumberFormatException e) {
                System.out.println(CLASSNAME + " - NumberFormatException: MapKey: " + mapKey + " MapValue: " + (int) mapValue + " -- Fehlermeldung: " + e.getMessage());
//                result.set(CLASSNAME + " - NumberFormatException: MapKey: " + mapKey + " MapValue: " + (int) mapValue + " -- Fehlermeldung: " + e.getMessage());
                result.set(false);

            }
        });

        return result.get();
    }

    // Prozedur, um einen Textwert (key) in den ICD10 POrimaerschluesseln zu finden und dann den ICD10 Primaerschluessel zurückzugeben
    public static String getValueFromKey(ObservableMap<String, List<ICD10List>> icd10List, String key) {

        // KeyProp ist der bereinigte key
        StringProperty keyProp = new SimpleStringProperty(key.toUpperCase().replaceAll("[^a-zäöüA-ZÄÖÜ0-9.,;+-]/", "")); // Sonderzeichen entfernen

        // Falls der key in der Map enthalten ist, dann den key zurückgeben
        StringProperty returnString = new SimpleStringProperty("");
        List<ICD10List> icd10s = icd10List.get(keyProp.get());
        if (icd10s != null) {
            icd10s.forEach((icd10)
                    -> returnString.set(
                            (returnString.get().equals(""))
                            ? icd10.getText()
                            : returnString.get() + ", " + icd10.getText()));
        }
        return returnString.get();
    }
}
