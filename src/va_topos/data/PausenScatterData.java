/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

/**
 *
 * @author lokal-admin
 */
@SuppressWarnings("unchecked")
public class PausenScatterData {

    private boolean gewollt = true;
    private double pausenLaenge = 0;
    private double anfangsVisus = 0.0d;
    private double endVisus = 0.0d;

    public PausenScatterData(boolean gewollt, double pausenLaenge, double anfangsVisus, double endVisus) {
        this.anfangsVisus = anfangsVisus;
        this.endVisus = endVisus;
        this.gewollt = gewollt;
        this.pausenLaenge = pausenLaenge;
    }

    public void setGewollt(boolean gewollt) {
        this.gewollt = gewollt;
    }

    /**
     * Set the value of pausenLaenge
     *
     * @param pausenLaenge new value of pausenLaenge
     */
    public void setPausenLaenge(double pausenLaenge) {
        this.pausenLaenge = pausenLaenge;
    }

    public void setAnfangsVisus(double anfangsVisus) {
        this.anfangsVisus = anfangsVisus;
    }

    public void setEndVisus(double endVisus) {
        this.endVisus = endVisus;
    }

    public boolean isGewollt() {
        return gewollt;
    }

    /**
     * Get the value of pausenLaenge
     *
     * @return the value of pausenLaenge
     */
    public double getPausenLaenge() {
        return pausenLaenge;
    }

    public double getAnfangsVisus() {
        return anfangsVisus;
    }

    public double getEndVisus() {
        return endVisus;
    }

}
