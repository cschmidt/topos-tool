/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.data;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lokal-admin
 */
public class JsonTextMining_letter_extract {

    // Basic attributes for all objects
    public class BaseType {

        int begin;
        int end;
        String type;
        String coveredText;
        int id;        

        public int getBegin() {
            return begin;
        }

        public void setBegin(int begin) {
            this.begin = begin;
        }

        public int getEnd() {
            return end;
        }

        public void setEnd(int end) {
            this.end = end;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCoveredText() {
            return coveredText;
        }

        public void setCoveredText(String coveredText) {
            this.coveredText = coveredText;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
        
    }

    // Measurement attributes
    public class Measurement extends BaseType {

        String unit;
        String normalizedUnit;
        double normalizedValue;
        double value;
        String dimension;

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getNormalizedUnit() {
            return normalizedUnit;
        }

        public void setNormalizedUnit(String normalizedUnit) {
            this.normalizedUnit = normalizedUnit;
        }

        public double getNormalizedValue() {
            return normalizedValue;
        }

        public void setNormalizedValue(double normalizedValue) {
            this.normalizedValue = normalizedValue;
        }

        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public String getDimension() {
            return dimension;
        }

        public void setDimension(String dimension) {
            this.dimension = dimension;
        }
        
    }

    // Json Structure after export
    public class AnnotationDtos extends BaseType {

        // BEGIN Medication
        // Drug Attributes
        public class Drugs extends BaseType {

            public class Ingredient extends BaseType {

                String dictCanon;
                String matchedTerm;
                String uniqueId;
                String conceptId;
                String source;

                public String getDictCanon() {
                    return dictCanon;
                }

                public void setDictCanon(String dictCanon) {
                    this.dictCanon = dictCanon;
                }

                public String getMatchedTerm() {
                    return matchedTerm;
                }

                public void setMatchedTerm(String matchedTerm) {
                    this.matchedTerm = matchedTerm;
                }

                public String getUniqueId() {
                    return uniqueId;
                }

                public void setUniqueId(String uniqueId) {
                    this.uniqueId = uniqueId;
                }

                public String getConceptId() {
                    return conceptId;
                }

                public void setConceptId(String conceptId) {
                    this.conceptId = conceptId;
                }

                public String getSource() {
                    return source;
                }

                public void setSource(String source) {
                    this.source = source;
                }
                
            }
            Ingredient ingredient;

            public class Strength extends BaseType {

                String dictCanon;
                String uniqueId;
                String conceptId;
                String source;

                Measurement measurement;

                public String getDictCanon() {
                    return dictCanon;
                }

                public void setDictCanon(String dictCanon) {
                    this.dictCanon = dictCanon;
                }

                public String getUniqueId() {
                    return uniqueId;
                }

                public void setUniqueId(String uniqueId) {
                    this.uniqueId = uniqueId;
                }

                public String getConceptId() {
                    return conceptId;
                }

                public void setConceptId(String conceptId) {
                    this.conceptId = conceptId;
                }

                public String getSource() {
                    return source;
                }

                public void setSource(String source) {
                    this.source = source;
                }

                public Measurement getMeasurement() {
                    return measurement;
                }

                public void setMeasurement(Measurement measurement) {
                    this.measurement = measurement;
                }
                

            }
            Strength strength;

            public Ingredient getIngredient() {
                return ingredient;
            }

            public void setIngredient(Ingredient ingredient) {
                this.ingredient = ingredient;
            }

            public Strength getStrength() {
                return strength;
            }

            public void setStrength(Strength strength) {
                this.strength = strength;
            }

        }
        List<Drugs> drugs;

        // Dose Frequency
        public class DoseFrequency extends BaseType {

            public class TotalDose extends BaseType {

                String unit;
                String normalizedUnit;
                double normalizedValue;
                double value;
                String dimension;

                public String getUnit() {
                    return unit;
                }

                public void setUnit(String unit) {
                    this.unit = unit;
                }

                public String getNormalizedUnit() {
                    return normalizedUnit;
                }

                public void setNormalizedUnit(String normalizedUnit) {
                    this.normalizedUnit = normalizedUnit;
                }

                public double getNormalizedValue() {
                    return normalizedValue;
                }

                public void setNormalizedValue(double normalizedValue) {
                    this.normalizedValue = normalizedValue;
                }

                public double getValue() {
                    return value;
                }

                public void setValue(double value) {
                    this.value = value;
                }

                public String getDimension() {
                    return dimension;
                }

                public void setDimension(String dimension) {
                    this.dimension = dimension;
                }
                
            }
            TotalDose totalDose;

            double midday;
            String concept;
            String interval;
            double totalCount;
            double evening;
            double atNight;
            double morning;

            public TotalDose getTotalDose() {
                return totalDose;
            }

            public void setTotalDose(TotalDose totalDose) {
                this.totalDose = totalDose;
            }

            public double getMidday() {
                return midday;
            }

            public void setMidday(double midday) {
                this.midday = midday;
            }

            public String getConcept() {
                return concept;
            }

            public void setConcept(String concept) {
                this.concept = concept;
            }

            public String getInterval() {
                return interval;
            }

            public void setInterval(String interval) {
                this.interval = interval;
            }

            public double getTotalCount() {
                return totalCount;
            }

            public void setTotalCount(double totalCount) {
                this.totalCount = totalCount;
            }

            public double getEvening() {
                return evening;
            }

            public void setEvening(double evening) {
                this.evening = evening;
            }

            public double getAtNight() {
                return atNight;
            }

            public void setAtNight(double atNight) {
                this.atNight = atNight;
            }

            public double getMorning() {
                return morning;
            }

            public void setMorning(double morning) {
                this.morning = morning;
            }
            

        }
        DoseFrequency doseFrequency;

        // Dose Form
        public class DoseForm extends BaseType {

            String matchedTerm;
            String dictCanon;
            String conceptId;
            String source;
            String uniqueId;

            public String getMatchedTerm() {
                return matchedTerm;
            }

            public void setMatchedTerm(String matchedTerm) {
                this.matchedTerm = matchedTerm;
            }

            public String getDictCanon() {
                return dictCanon;
            }

            public void setDictCanon(String dictCanon) {
                this.dictCanon = dictCanon;
            }

            public String getConceptId() {
                return conceptId;
            }

            public void setConceptId(String conceptId) {
                this.conceptId = conceptId;
            }

            public String getSource() {
                return source;
            }

            public void setSource(String source) {
                this.source = source;
            }

            public String getUniqueId() {
                return uniqueId;
            }

            public void setUniqueId(String uniqueId) {
                this.uniqueId = uniqueId;
            }
            
        }
        DoseForm doseForm;

        public class Date {
            String kind;
            String value;

            public String getKind() {
                return kind;
            }

            public void setKind(String kind) {
                this.kind = kind;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
            
        }
        Date date;
        List<String> administrations = new ArrayList();
        List<Double> additionalAmounts = new ArrayList();
        String status;

        // END Medication
        //        
        // BEGIN Ophthalmology
        String dictCanon;
        String matchedTerm;
        String uniqueId;
        String conceptId;
        String source;
        String negatedBy;
        String side;

        // Tensio


        // Visual Acuity
        
        public class Eye extends Measurement {
            String fact;
            boolean meter;
            String correction;
            public class Refraction extends BaseType {
                double sphere;
                double cylinder;
                double axis;

                public double getSphere() {
                    return sphere;
                }

                public void setSphere(double sphere) {
                    this.sphere = sphere;
                }

                public double getCylinder() {
                    return cylinder;
                }

                public void setCylinder(double cylinder) {
                    this.cylinder = cylinder;
                }

                public double getAxis() {
                    return axis;
                }

                public void setAxis(double axis) {
                    this.axis = axis;
                }
                
            }
            Refraction refraction;
            boolean pinHole;
            public class AdditionalInformation extends BaseType {
                String normalized;

                public String getNormalized() {
                    return normalized;
                }

                public void setNormalized(String normalized) {
                    this.normalized = normalized;
                }
                
            }
            AdditionalInformation additionalInformation;

            public String getFact() {
                return fact;
            }

            public void setFact(String fact) {
                this.fact = fact;
            }

            public boolean isMeter() {
                return meter;
            }

            public void setMeter(boolean meter) {
                this.meter = meter;
            }

            public String getCorrection() {
                return correction;
            }

            public void setCorrection(String correction) {
                this.correction = correction;
            }

            public Refraction getRefraction() {
                return refraction;
            }

            public void setRefraction(Refraction refraction) {
                this.refraction = refraction;
            }

            public boolean isPinHole() {
                return pinHole;
            }

            public void setPinHole(boolean pinHole) {
                this.pinHole = pinHole;
            }

            public AdditionalInformation getAdditionalInformation() {
                return additionalInformation;
            }

            public void setAdditionalInformation(AdditionalInformation additionalInformation) {
                this.additionalInformation = additionalInformation;
            }
            
        }
        Eye rightEye;
        Eye leftEye;

        // END Ophthalmology 
                
        // BEGIN General attributes
        String version;
        String verificationStatus;

        String laterality;
        String lastName;
        String language;
        String label;
        String kind;
        String gender;
        String firstName;

        String deathDate;

        String clinicalStatus;
        String belongsTo;
        // END General Attributes

        public List<Drugs> getDrugs() {
            return drugs;
        }

        public void setDrugs(List<Drugs> drugs) {
            this.drugs = drugs;
        }

        public DoseFrequency getDoseFrequency() {
            return doseFrequency;
        }

        public void setDoseFrequency(DoseFrequency doseFrequency) {
            this.doseFrequency = doseFrequency;
        }

        public DoseForm getDoseForm() {
            return doseForm;
        }

        public void setDoseForm(DoseForm doseForm) {
            this.doseForm = doseForm;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public List<String> getAdministrations() {
            return administrations;
        }

        public void setAdministrations(List<String> administrations) {
            this.administrations = administrations;
        }

        public List<Double> getAdditionalAmounts() {
            return additionalAmounts;
        }

        public void setAdditionalAmounts(List<Double> additionalAmounts) {
            this.additionalAmounts = additionalAmounts;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDictCanon() {
            return dictCanon;
        }

        public void setDictCanon(String dictCanon) {
            this.dictCanon = dictCanon;
        }

        public String getMatchedTerm() {
            return matchedTerm;
        }

        public void setMatchedTerm(String matchedTerm) {
            this.matchedTerm = matchedTerm;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }

        public String getConceptId() {
            return conceptId;
        }

        public void setConceptId(String conceptId) {
            this.conceptId = conceptId;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getNegatedBy() {
            return negatedBy;
        }

        public void setNegatedBy(String negatedBy) {
            this.negatedBy = negatedBy;
        }

        public String getSide() {
            return side;
        }

        public void setSide(String side) {
            this.side = side;
        }

        public Eye getRightEye() {
            return rightEye;
        }

        public void setRightEye(Eye rightEye) {
            this.rightEye = rightEye;
        }

        public Eye getLeftEye() {
            return leftEye;
        }

        public void setLeftEye(Eye leftEye) {
            this.leftEye = leftEye;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getVerificationStatus() {
            return verificationStatus;
        }

        public void setVerificationStatus(String verificationStatus) {
            this.verificationStatus = verificationStatus;
        }

        public String getLaterality() {
            return laterality;
        }

        public void setLaterality(String laterality) {
            this.laterality = laterality;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getKind() {
            return kind;
        }

        public void setKind(String kind) {
            this.kind = kind;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getDeathDate() {
            return deathDate;
        }

        public void setDeathDate(String deathDate) {
            this.deathDate = deathDate;
        }

        public String getClinicalStatus() {
            return clinicalStatus;
        }

        public void setClinicalStatus(String clinicalStatus) {
            this.clinicalStatus = clinicalStatus;
        }

        public String getBelongsTo() {
            return belongsTo;
        }

        public void setBelongsTo(String belongsTo) {
            this.belongsTo = belongsTo;
        }
        

    }
    List<AnnotationDtos> annotationDtos;

    public List<AnnotationDtos> getAnnotationDtos() {
        return annotationDtos;
    }

    public void setAnnotationDtos(List<AnnotationDtos> annotationDtos) {
        this.annotationDtos = annotationDtos;
    }

}
