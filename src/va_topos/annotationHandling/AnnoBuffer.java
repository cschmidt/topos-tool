/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.annotationHandling;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import static va_topos.ui.MainWindowBuilder.annoRedoButton;
import static va_topos.ui.MainWindowBuilder.annoUndoButton;

/**
 *
 * @author Christoph Schmidt
 *
 * Class to store graphical Annotations
 */
@SuppressWarnings("unchecked")
public class AnnoBuffer {

    public ObservableList<DrawInfo> drawInfos = FXCollections.observableArrayList();
    private final IntegerProperty drawIndex = new SimpleIntegerProperty();
    private final IntegerProperty maxIndex = new SimpleIntegerProperty();
    private Canvas canvas = new Canvas(0, 0);
    private AnchorPane annoThumbNail = new AnchorPane();
    private Color annoColor = Color.rgb(0, 0, 0);
    private final IntegerProperty index = new SimpleIntegerProperty(0);
    private final DoubleProperty zoomFactor = new SimpleDoubleProperty();
    private final DoubleProperty xTranslate = new SimpleDoubleProperty();
    private final DoubleProperty yTranslate = new SimpleDoubleProperty();

    public AnnoBuffer() {

        drawIndex.set(0);
        annoUndoButton.disableProperty().bind(this.drawIndex.lessThanOrEqualTo(0));
        annoRedoButton.disableProperty().bind(this.drawIndex.greaterThanOrEqualTo(this.maxIndex));

        drawInfos.addListener(new ListChangeListener() {
            @Override
            public void onChanged(ListChangeListener.Change change) {
                maxIndex.set(drawInfos.size());
            }
        });

    }

    public double getYTranslate() {
        return yTranslate.get();
    }

    public void setYTranslate(double value) {
        yTranslate.set(value);
    }

    public DoubleProperty yTranslateProperty() {
        return yTranslate;
    }

    public double getXTranslate() {
        return xTranslate.get();
    }

    public void setXTranslate(double value) {
        xTranslate.set(value);
    }

    public DoubleProperty xTranslateProperty() {
        return xTranslate;
    }

    public double getZoomFactor() {
        return zoomFactor.get();
    }

    public void setZoomFactor(double value) {
        zoomFactor.set(value);
    }

    public DoubleProperty zoomFactorProperty() {
        return zoomFactor;
    }

    public int getIndex() {
        return index.get();
    }

    public void setIndex(int value) {
        index.set(value);
    }

    public IntegerProperty indexProperty() {
        return index;
    }

    public Color getAnnoColor() {
        return annoColor;
    }

    public void setAnnoColor(Color annoColor) {
        this.annoColor = annoColor;
    }

    public AnchorPane getAnnoThumbNail() {
        return annoThumbNail;
    }

    public void setAnnoThumbNail(AnchorPane annoThumbNail) {
        this.annoThumbNail = annoThumbNail;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }

    public int getMaxIndex() {
        return maxIndex.get();
    }

    public void setMaxIndex(int value) {
        maxIndex.set(value);
    }

    public IntegerProperty maxIndexProperty() {
        return maxIndex;
    }

    public int getDrawIndex() {
        return drawIndex.get();
    }

    public void setDrawIndex(int value) {
        drawIndex.set(value);
    }

    public IntegerProperty drawIndexProperty() {
        return drawIndex;
    }

    public ObservableList<DrawInfo> getDrawInfos() {
        return drawInfos;
    }

    public void setDrawInfos(ObservableList<DrawInfo> drawInfos) {
        this.drawInfos = drawInfos;
    }

}
