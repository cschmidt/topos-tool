/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.annotationHandling;

import java.util.List;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;

/**
 *
 * @author lokal-admin
 */
@SuppressWarnings("unchecked")
public class DrawingOperations {

    private double annoNum;
    private Canvas annoPanel;
    private AnchorPane annoThumbNail;
    private double zoomFactor;
    private double xTranslate;
    private double yTranslate;

    public DrawingOperations(AnnoBuffer annoBuff) {

        this.annoNum = annoBuff.getIndex();
        this.annoPanel = annoBuff.getCanvas();
        this.annoThumbNail = annoBuff.getAnnoThumbNail();
        this.zoomFactor = annoBuff.getZoomFactor();
        this.xTranslate = annoBuff.getXTranslate();
        this.yTranslate = annoBuff.getYTranslate();
    }

    public double getAnnoNum() {
        return annoNum;
    }

    public void setAnnoNum(double annoNum) {
        this.annoNum = annoNum;
    }

    public Canvas getAnnoPanel() {
        return annoPanel;
    }

    public void setAnnoPanel(Canvas annoPanel) {
        this.annoPanel = annoPanel;
    }

    public AnchorPane getAnnoThumbNail() {
        return annoThumbNail;
    }

    public void setAnnoThumbNail(AnchorPane annoThumbNail) {
        this.annoThumbNail = annoThumbNail;
    }

    public double getZoomFactor() {
        return zoomFactor;
    }

    public void setZoomFactor(double zoomFactor) {
        this.zoomFactor = zoomFactor;
    }

    public double getxTranslate() {
        return xTranslate;
    }

    public void setxTranslate(double xTranslate) {
        this.xTranslate = xTranslate;
    }

    public double getyTranslate() {
        return yTranslate;
    }

    public void setyTranslate(double yTranslate) {
        this.yTranslate = yTranslate;
    }

    public boolean drawAnnoNum() {
        return false;
    }

    // Draw all annos in grey, except the active one
    public boolean annoFocusChanged(List<AnnoBuffer> drawBuffer, int index) {

        drawBuffer.forEach(drawBuff -> {
            //Color drawCol
            //drawBuff.getCanvas()

        });
        return false;
    }

    // Draw an annotation element in a canvas
    public void draw(Canvas canvas, DrawInfo drawInfo, boolean Xnegative, boolean Ynegative) {

        double startX = Xnegative ? drawInfo.getStartX() - drawInfo.getWidth() : drawInfo.getStartX();
        double startY = Ynegative ? drawInfo.getStartY() - drawInfo.getHeight() : drawInfo.getStartY();

        GraphicsContext gc = canvas.getGraphicsContext2D();

        switch (drawInfo.getShape()) {
            case RECTANGLE:
                gc.setFill(drawInfo.getBordercolor());
                gc.fillRect(startX, startY, drawInfo.getWidth(), drawInfo.getHeight());
                if (drawInfo.getFillColor() == Color.TRANSPARENT) {
                    gc.clearRect(startX + 3d, startY + 3d, drawInfo.getWidth() - 5d, drawInfo.getHeight() - 5d);
                } else {
                    gc.setFill(drawInfo.getFillColor());
                    gc.fillRect(startX + 2d, startY + 2d, drawInfo.getWidth() - 4d, drawInfo.getHeight() - 4d);
                }
                break;
            case LINE:
                gc.setStroke(drawInfo.getFillColor());
                gc.strokeLine(startX, startY, drawInfo.getWidth(), drawInfo.getHeight());
                break;
            case ELLIPSE:
                gc.setFill(drawInfo.getFillColor());
                gc.fillOval(startX, startY, drawInfo.getWidth(), drawInfo.getHeight());
                break;
            default:
                gc.setFill(drawInfo.getFillColor());
                gc.fillOval(startX, startY, drawInfo.getWidth(), drawInfo.getHeight());
        }
    }

}
