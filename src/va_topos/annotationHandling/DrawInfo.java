/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.annotationHandling;

import javafx.scene.paint.Color;

/**
 *
 * @author lokal-admin
 */
public class DrawInfo {

    public enum Shape {
        RECTANGLE,
        CIRCLE,
        ELLIPSE,
        DOT,
        LINE;
    }

    private Shape shape;
    private Color bordercolor;
    private Color fillColor;
    private double startX;
    private double startY;
    private double width;
    private double height;
    private double radius;
    private double minorAxis;
    private double majorAxis;

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public Color getBordercolor() {
        return bordercolor;
    }

    public void setBordercolor(Color bordercolor) {
        this.bordercolor = bordercolor;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public double getStartX() {
        return startX;
    }

    public void setStartX(double startX) {
        this.startX = startX;
    }

    public double getStartY() {
        return startY;
    }

    public void setStartY(double startY) {
        this.startY = startY;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }


    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getMinorAxis() {
        return minorAxis;
    }

    public void setMinorAxis(double minorAxis) {
        this.minorAxis = minorAxis;
    }

    public double getMajorAxis() {
        return majorAxis;
    }

    public void setMajorAxis(double majorAxis) {
        this.majorAxis = majorAxis;
    }

    public DrawInfo(Shape shape, Color bordercolor, Color fillColor, double startX, double startY, double radius) {

        this.shape = shape;
        this.bordercolor = bordercolor;
        this.fillColor = fillColor;
        this.startX = startX;
        this.startY = startY;
        this.radius = radius;
    }

    public DrawInfo(Shape shape, Color bordercolor, Color fillColor, double startX, double startY, double width, double height) {

        this.shape = shape;
        this.bordercolor = bordercolor;
        this.fillColor = fillColor;
        this.startX = startX;
        this.startY = startY;
        this.width = width;
        this.height = height;
    }


}
