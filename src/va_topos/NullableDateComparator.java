/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos;

import va_topos.data.DataModel;
import java.time.LocalDate;
import java.time.Month;
import java.util.Comparator;

/**
 *
 * @author lokal-admin
 */
@SuppressWarnings("unchecked")
public class NullableDateComparator implements Comparator<DataModel> {

    @Override
    public int compare(DataModel datum1, DataModel datum2) {

        if (datum1 != null && datum2 != null) {
            if (datum1.getEreignisDatum() == null) {
                datum1.setEreignisDatum(LocalDate.of(1900, Month.JANUARY, 1));
            }
            if (datum2.getEreignisDatum() == null) {
                datum2.setEreignisDatum(LocalDate.of(1900, Month.JANUARY, 1));
            }
            int returnvalue = datum1.getEreignisDatum().compareTo(datum2.getEreignisDatum());
            if (datum1.getEreignisDatum().equals(LocalDate.of(1900, Month.JANUARY, 1))) {
                datum1.setEreignisDatum(null);
            }
            if (datum2.getEreignisDatum().equals(LocalDate.of(1900, Month.JANUARY, 1))) {
                datum2.setEreignisDatum(null);
            }
            return returnvalue;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "PauseComparator";
    }

}
