/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.ui;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.Toggle;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import va_topos.events.EventHandlers;
//import static va_topos.ui.MainWindowBuilder.annoBackButton;
//import static va_topos.ui.MainWindowBuilder.annoDeleteButton;
//import static va_topos.ui.MainWindowBuilder.annoFwdButton;
//import static va_topos.ui.MainWindowBuilder.annoNewButton;
//import static va_topos.ui.MainWindowBuilder.annoRedoButton;
//import static va_topos.ui.MainWindowBuilder.annoUndoButton;
import static va_topos.ui.MainWindowBuilder.borSlidLabel;
import static va_topos.ui.MainWindowBuilder.borSlidVal;
import static va_topos.ui.MainWindowBuilder.borderWidthSlider;
import static va_topos.ui.MainWindowBuilder.controlVisusFirstAnchorPane;
import static va_topos.ui.MainWindowBuilder.detailAnchorPane;
import static va_topos.ui.MainWindowBuilder.eyeRadio;
import static va_topos.ui.MainWindowBuilder.groupingClassCountLabel;
import static va_topos.ui.MainWindowBuilder.groupingComboBox;
import static va_topos.ui.MainWindowBuilder.groupingLabel;
import static va_topos.ui.MainWindowBuilder.groupingSpinner;
import static va_topos.ui.MainWindowBuilder.normBegin;
import static va_topos.ui.MainWindowBuilder.normStartToggle;
import static va_topos.ui.MainWindowBuilder.orderVisusFirstTab;
import static va_topos.ui.MainWindowBuilder.orderVisusFirstVis;
import static va_topos.ui.MainWindowBuilder.patientRadio;
import static va_topos.ui.MainWindowBuilder.restoreButton;
import static va_topos.ui.MainWindowBuilder.setAnchor;
import static va_topos.ui.MainWindowBuilder.setStyle;
import static va_topos.ui.MainWindowBuilder.spareEight;
import static va_topos.ui.MainWindowBuilder.spareFive;
import static va_topos.ui.MainWindowBuilder.spareFour;
import static va_topos.ui.MainWindowBuilder.spareOne;
import static va_topos.ui.MainWindowBuilder.spareSeven;
import static va_topos.ui.MainWindowBuilder.spareSix;
import static va_topos.ui.MainWindowBuilder.spareThree;
import static va_topos.ui.MainWindowBuilder.spareTwo;
import static va_topos.ui.MainWindowBuilder.summaryAnchorPane;
import static va_topos.ui.MainWindowBuilder.summaryVisusCanvas;
import static va_topos.ui.MainWindowBuilder.timeBeginn;
import static va_topos.ui.MainWindowBuilder.zoomSlidLabel;
import static va_topos.ui.MainWindowBuilder.zoomSlidVal;
import static va_topos.ui.MainWindowBuilder.zoomSlider;
import va_topos.VA_topos;
import va_topos.data.DataModel;
import va_topos.data.DataModel.AntiVEGFMedikament;
import va_topos.visualization.VisusFirstMaker;
import static va_topos.visualization.VisusFirstMaker.growZoomFactor;
import static va_topos.ui.MainWindowBuilder.visusFirstDrawObjectAnchorPane;
import static va_topos.ui.MainWindowBuilder.visusFirstAnnotationAnchorPane;
import static va_topos.ui.SubWindowsBuilder.annoTilePane;
import va_topos.events.VisFirstControlEvents;
import va_topos.events.VisFirstViewEvents;
import static va_topos.ui.MainWindowBuilder.backColPaintObject;
import static va_topos.ui.MainWindowBuilder.backColSlidLabel;
import static va_topos.ui.MainWindowBuilder.backColSlidVal;
import static va_topos.ui.MainWindowBuilder.backColorSlider;
import static va_topos.ui.MainWindowBuilder.brightnessSlidLabel;
import static va_topos.ui.MainWindowBuilder.brightnessSlidVal;
import static va_topos.ui.MainWindowBuilder.brightnessSlider;
import static va_topos.ui.MainWindowBuilder.detailCloseButton;
import static va_topos.ui.MainWindowBuilder.groupingToggle;
import static va_topos.ui.MainWindowBuilder.saturationSlidLabel;
import static va_topos.ui.MainWindowBuilder.saturationSlidVal;
import static va_topos.ui.MainWindowBuilder.saturationSlider;
import static va_topos.ui.MainWindowBuilder.showMedChangeCheckBox;
import static va_topos.ui.MainWindowBuilder.summaryCloseButton;
import static va_topos.ui.MainWindowBuilder.summaryVisShowCheckBox;
import static va_topos.ui.MainWindowBuilder.detailVisShowCheckBox;
import static va_topos.ui.MainWindowBuilder.legendAnchorPane;
import static va_topos.ui.MainWindowBuilder.legendMaxValueLabel;
import static va_topos.ui.MainWindowBuilder.legendMidValueLabel;
import static va_topos.ui.MainWindowBuilder.legendMinValueLabel;
import static va_topos.ui.MainWindowBuilder.legendScaleRectangle;
import static va_topos.ui.MainWindowBuilder.legendShowCheckBox;
import static va_topos.ui.MainWindowBuilder.regressionShowCheckBox;
import static va_topos.ui.MainWindowBuilder.scaleComboBox;
import static va_topos.ui.MainWindowBuilder.scaleDescriptionLabel;
import static va_topos.ui.MainWindowBuilder.scenarioComboBox;
import static va_topos.ui.MainWindowBuilder.scenarioDescriptionLabel;
import static va_topos.ui.MainWindowBuilder.sortingDescriptionLabel;
import static va_topos.ui.MainWindowBuilder.suddenIncidentShowCheckBox;
import static va_topos.ui.MainWindowBuilder.visusDimensionComboBox;
import static va_topos.ui.MainWindowBuilder.visusDimensionDescriptionLabel;
import static va_topos.visualization.VisusFirstMaker.timeBeginOffset;
import static va_topos.visualization.VisusFirstMaker.timeBeginOffsetSave;
import static va_topos.visualization.VisusFirstMaker.timeBeginYearOffset;
import static va_topos.visualization.VisusFirstMaker.timeBeginYearOffsetSave;
import static va_topos.ui.MainWindowBuilder.sortingComboBox;
import static va_topos.ui.MainWindowBuilder.spareNine;

/**
 *
 * @author lokal-admin
 */
public class VisusFirstTabBuilder {

    public static final int[] STDNODECOLORRGB = {60, 190, 250};
    private static final double STDOPACITY = 0.15;
    private static final double STDCHILDRENOPACITY = 0.75;
    private static final double STDCORNERRADII = 5d;
    private static final double STDINSET = 5d;

    private static final double TOPOFFSET = 20 + STDINSET;
    private static final double BOTTOMOFFSET = 20 + STDINSET;
    private static final double LEFTOFFSET = 10 + STDINSET;
    private static final double RIGHTOFFSET = 10 + STDINSET;
    private static final double TEXTFIELDSPACE = 20;
    private static final double RADIOSPACE = 20;
    private static final double SLIDEROFFSET = 50;
    private static double heightFactor = 0.0;

    @SuppressWarnings("unchecked")
    public VisusFirstTabBuilder(Tab myVisusFirstTab) {

        EventHandlers eh = new EventHandlers();

        // annoPane ist ein Panel zur anzeige von MetaDaten und Annodaten während der Visualisierung
        annoTilePane = new TilePane();
        annoTilePane.setBackground(setStyle(annoTilePane, MainWindowBuilder.NodeStyles.ANNOTILEPANE));

        // Das Visualization-AnchorPane erzeugen, in welchem die Vis angezeigt werden.
        visusFirstAnnotationAnchorPane = new MainWindowBuilder.DotGrid(1);
        setAnchor(visusFirstAnnotationAnchorPane, 0d, 0d, 0d, 0d);

        // Das Panel initialisieren, in dem die Legende und das Koordinatensystem für die Visualisierung angezeigt wird
        //visusFirstAnnotationAnchorPane = new MainWindowBuilder.DotGrid(1);
        legendAnchorPane = new AnchorPane();
        setAnchor(legendAnchorPane, 0d, 0d, 0d, 0d);

        // Initialize the AnchorPane, where all the patients will be drawn to
        visusFirstDrawObjectAnchorPane = new MainWindowBuilder.DotGrid(1);
        setAnchor(visusFirstDrawObjectAnchorPane, 30d, 0d, 0d, 0d);

        // If on the drawing field is scrolled
        visusFirstDrawObjectAnchorPane.setOnScroll(e -> VisFirstViewEvents.onVisViewScrolled(e));
        // If on the drawing field a mousebutton is pressed
        visusFirstDrawObjectAnchorPane.setOnMousePressed(e -> VisFirstViewEvents.onVisMousePressed(e));
        visusFirstDrawObjectAnchorPane.setOnMouseReleased(e -> VisFirstViewEvents.onDrawObjectMouseReleased(e, null));
        // DetailAnchorPane ist ein Panel zur anzeige von MetaDaten und Annodaten während der Visualisierung
        detailAnchorPane = new AnchorPane();
//        setAnchor(detailAnchorPane, Double.NaN, 0d, 0d, Double.NaN);
        setAnchor(detailAnchorPane, Double.NaN, 0d, 0d, Double.NaN);
        detailAnchorPane.setMinHeight(150);
        detailAnchorPane.setPrefHeight(240);
        detailAnchorPane.setMaxHeight(450);
        detailAnchorPane.setMinWidth(150);
        detailAnchorPane.prefWidthProperty().bind(visusFirstDrawObjectAnchorPane.widthProperty()); //.setPrefWidth(300);
        //detailAnchorPane.setMaxWidth(450);
        detailAnchorPane.setBackground(setStyle(detailAnchorPane, MainWindowBuilder.NodeStyles.TRANSPARENT));

        // Align the Labels were the Details are shown in the DetailAnchorPane
        heightFactor = 0;
        spareOne.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
        spareOne.setText("");
        heightFactor++;

        spareTwo.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
        spareTwo.setText("");
        heightFactor++;

        spareThree.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
        spareThree.setText("");
        heightFactor++;

        spareFour.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
        spareFour.setText("");
        heightFactor++;

        spareFive.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
        spareFive.setText("");
        heightFactor++;

        spareSix.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
        spareSix.setText("");
        heightFactor++;

        spareSeven.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
        spareSeven.setText("");
        heightFactor++;

        spareEight.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
        spareEight.setText("");
        heightFactor++;

        spareNine.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
        spareNine.setText("");
        heightFactor++;

        setAnchor(detailCloseButton, Double.NaN, 5d, 5d, Double.NaN);

        detailCloseButton.setOnAction(event -> {
            detailVisShowCheckBox.setSelected(false);
        });

        detailAnchorPane.visibleProperty().bind(detailVisShowCheckBox.selectedProperty());
        detailAnchorPane.getChildren().addAll(spareOne, spareTwo, spareThree, spareFour, spareFive, spareSix, spareSeven, spareEight, spareNine, detailCloseButton);

        // Factor for the location of the controls on the control panel for the vis
        heightFactor = 0;

        // Das Steuerungspanel erzeugen, in welchem die Elemente für die Steuerung der Visualisierungen enthalten sind
        controlVisusFirstAnchorPane = new AnchorPane();
        setAnchor(controlVisusFirstAnchorPane, 0d, 0d, 0d, 0d);
        controlVisusFirstAnchorPane.setMinWidth(150);
        controlVisusFirstAnchorPane.setMaxWidth(300);
        controlVisusFirstAnchorPane.setBackground(setStyle(controlVisusFirstAnchorPane, MainWindowBuilder.NodeStyles.TRANSPARENT));

        setAnchor(legendScaleRectangle, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        legendScaleRectangle.widthProperty().bind(controlVisusFirstAnchorPane.widthProperty().subtract(LEFTOFFSET + RIGHTOFFSET + 10));
        legendScaleRectangle.setHeight(20.0);
        legendScaleRectangle.setStroke(Color.BLACK);
        legendScaleRectangle.setStrokeWidth(1.0);
        heightFactor++;
        setAnchor(legendMinValueLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        setAnchor(legendMidValueLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        legendMidValueLabel.translateXProperty().bind(legendScaleRectangle.widthProperty().divide(2).subtract(legendMidValueLabel.widthProperty().divide(2)));
        setAnchor(legendMaxValueLabel, Double.NaN, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        heightFactor++;

        List<Rectangle> mediRects = new ArrayList();
        List<Label> mediLabels = new ArrayList();
        for (AntiVEGFMedikament medi : DataModel.AntiVEGFMedikament.values()) {
            if (medi != AntiVEGFMedikament.E && medi != AntiVEGFMedikament.UNKNOWN) {
                Rectangle mediRect = new Rectangle();
                setAnchor(mediRect, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
                mediRect.setWidth(10.0);
                mediRect.setHeight(10.0);
                mediRect.setStroke(Color.BLACK);
                mediRect.setStrokeWidth(1.0);
                mediRect.setFill(VisusFirstMaker.colorPicker(medi.ordinal()));
                mediRects.add(mediRect);
                Label mediLabel = new Label(medi.toString());
                setAnchor(mediLabel, LEFTOFFSET + 20.0, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor) - 3, Double.NaN);
                mediLabels.add(mediLabel);
                heightFactor++;
            }
        }
        Rectangle mediDiscrepancyRect = new Rectangle();
        setAnchor(mediDiscrepancyRect, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        mediDiscrepancyRect.setWidth(10.0);
        mediDiscrepancyRect.setHeight(10.0);
        mediDiscrepancyRect.setStroke(Color.BLACK);
        mediDiscrepancyRect.setStrokeWidth(1.0);
        mediDiscrepancyRect.setFill(VisusFirstMaker.colorPicker(6));
        mediRects.add(mediDiscrepancyRect);
        Label mediDiscrepancyLabel = new Label("Diskrepanz");
        setAnchor(mediDiscrepancyLabel, LEFTOFFSET + 20.0, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor) - 3, Double.NaN);
        mediLabels.add(mediDiscrepancyLabel);
        controlVisusFirstAnchorPane.getChildren().addAll(mediRects);
        controlVisusFirstAnchorPane.getChildren().addAll(mediLabels);

        heightFactor++;

        // Originale Position wieder herstellen
        setAnchor(restoreButton, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        restoreButton.setOnAction(e -> visusFirstDrawObjectAnchorPane.getChildren().forEach(child -> {
            try {
                child.setTranslateX(0);
                child.setTranslateY(0);
            } catch (Exception ex) {

            }

        }));
        heightFactor++;
        heightFactor++;

        setAnchor(summaryVisShowCheckBox, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        summaryVisShowCheckBox.setSelected(true);
        heightFactor++;

        setAnchor(detailVisShowCheckBox, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        detailVisShowCheckBox.setSelected(true);
        detailVisShowCheckBox.selectedProperty().addListener((listener, oldValue, newValue) -> {
            if (newValue) {
                setAnchor(visusFirstDrawObjectAnchorPane, 30d, 0d, detailAnchorPane.getHeight(),  0d);
            } else {
                setAnchor(visusFirstDrawObjectAnchorPane, 30d, 0d, 0d, 0d);
            }
        });
        heightFactor++;

        setAnchor(legendShowCheckBox, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        legendShowCheckBox.setSelected(true);
        heightFactor++;

        setAnchor(regressionShowCheckBox, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        regressionShowCheckBox.setSelected(false);
        heightFactor++;

        setAnchor(suddenIncidentShowCheckBox, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        suddenIncidentShowCheckBox.setSelected(true);
        heightFactor++;

        setAnchor(showMedChangeCheckBox, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        showMedChangeCheckBox.setSelected(false);

        heightFactor++;
        heightFactor++;

        //public static CheckBox summaryVisShowCheckBox = new CheckBox("Zeige Aggregationsfenster");
        //public static CheckBox DetailVisShow = new CheckBox("Zeige Detailfenster");
        // Radiobuttons für das Design der Glyphs
//        setAnchor(horizontalRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        horizontalRadio.setText("Horizontale Balken");
//        horizontalRadio.setToggleGroup(visusDesignToggle);
//        heightFactor++;
//
//        setAnchor(verticalRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        verticalRadio.setText("Vertikale Balken");
//        verticalRadio.setToggleGroup(visusDesignToggle);
//        heightFactor++;
//
//        setAnchor(circleRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        circleRadio.setText("Kreise");
//        circleRadio.setToggleGroup(visusDesignToggle);
//        circleRadio.setSelected(true);
//        visusDesignToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
//            VisFirstControlEvents.refreshScreen();
//        });
//        heightFactor++;
//        heightFactor++;
        // Radiobuttons für die Farbskalaberechnung
//        setAnchor(linearRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        linearRadio.setText("Linear");
//        linearRadio.setToggleGroup(scaleToggle);
//        heightFactor++;
//
//        setAnchor(logarithmicRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        logarithmicRadio.setText("Logarithmic");
//        logarithmicRadio.setToggleGroup(scaleToggle);
//        heightFactor++;
//
//        setAnchor(diverging3Radio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        diverging3Radio.setText("Diverging 3-Class");
//        diverging3Radio.setToggleGroup(scaleToggle);
//        diverging3Radio.setSelected(true);
//        scaleToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
//            VisFirstControlEvents.refreshScreen();
//        });
//        heightFactor++;
//        heightFactor++;
        // Auswahl via ComboBox für dioe Skalierung der Visus-Farbwerte
        scaleDescriptionLabel.setText("Skalierung der Farbkodierung der Visuswerte");
        setAnchor(scaleDescriptionLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        heightFactor++;

        scaleComboBox.getItems().clear();
        scaleComboBox.getItems().add("Linear");
        scaleComboBox.getItems().add("Logarithmic");
        scaleComboBox.getItems().add("Diverging 3-Class");
        scaleComboBox.getSelectionModel().select(1);

        scaleComboBox.setEditable(false);
        scaleComboBox.setOnAction(e -> VisFirstControlEvents.refreshScreen());
        setAnchor(scaleComboBox, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        heightFactor++;
        heightFactor++;

        // Auswahl via ComboBox für dioe Skalierung der Visus-Farbwerte
        visusDimensionDescriptionLabel.setText("Auswahl der Daten zum Visus");
        setAnchor(visusDimensionDescriptionLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        heightFactor++;

        visusDimensionComboBox.getItems().clear();
        visusDimensionComboBox.getItems().add("Dezimalvisus");
        visusDimensionComboBox.getItems().add("LogMar Visus");
        visusDimensionComboBox.getItems().add("Visusklasse");
        visusDimensionComboBox.getItems().add("Visusdelta");
        visusDimensionComboBox.getItems().add("Visusdelta (LogMar)");
        visusDimensionComboBox.getItems().add("Visusklassendelta");
        visusDimensionComboBox.getSelectionModel().selectFirst();

        visusDimensionComboBox.setEditable(false);
        visusDimensionComboBox.setOnAction(e -> {
            switch (visusDimensionComboBox.getSelectionModel().getSelectedIndex()) {
                case 0: // Dezimalvisus
                    scaleComboBox.getSelectionModel().select(1);
                    break;
                case 1: // LogMar
                    scaleComboBox.getSelectionModel().select(0);
                    break;
                case 2: // VisClass
                    scaleComboBox.getSelectionModel().select(0);
                    break;
                case 3: // Visusdelta
                    scaleComboBox.getSelectionModel().select(2);
                    break;
                case 4: // LogMar Delta
                    scaleComboBox.getSelectionModel().select(2);
                    break;
                case 5: // VisClassDelta
                    scaleComboBox.getSelectionModel().select(2);
                    break;
                default:
                    scaleComboBox.getSelectionModel().select(1);
            }
            VisFirstControlEvents.reloadVisualization();
        });
        setAnchor(visusDimensionComboBox, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        heightFactor++;
        heightFactor++;

        // Auswahl via ComboBox für die Szenarien (data cleansing oder data exploration)
        scenarioDescriptionLabel.setText("Auswahl des Analyseszenarios");
        setAnchor(scenarioDescriptionLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        heightFactor++;

        scenarioComboBox.getItems().clear();
        scenarioComboBox.getItems().add("Datenbereinigung");
        scenarioComboBox.getItems().add("Datenexploration");
//        scenarioComboBox.getItems().add("Visusklasse");
//        scenarioComboBox.getItems().add("Visusdelta");
//        scenarioComboBox.getItems().add("Visusdelta (LogMar)");
//        scenarioComboBox.getItems().add("Visusklassendelta");
        scenarioComboBox.getSelectionModel().select(0);
        scenarioComboBox.setUserData(new SimpleBooleanProperty(true));

        scenarioComboBox.setEditable(false);
        scenarioComboBox.setOnAction(e -> {
            if (scenarioComboBox.getSelectionModel().isSelected(0)) {
                //patientRadio.setSelected(true);
                borderWidthSlider.setValue(1.5);
                backColorSlider.setValue(240);
                saturationSlider.setValue(0);

                legendShowCheckBox.setSelected(false);
                summaryVisShowCheckBox.setSelected(false);
                suddenIncidentShowCheckBox.setSelected(false);

            } else {
                saturationSlider.setValue(1);
                //eyeRadio.setSelected(true);
                backColorSlider.setValue(200);
                borderWidthSlider.setValue(0.5);

                legendShowCheckBox.setSelected(true);
                summaryVisShowCheckBox.setSelected(true);
                suddenIncidentShowCheckBox.setSelected(true);

            }
            ((BooleanProperty) scenarioComboBox.getUserData()).set(scenarioComboBox.getSelectionModel().isSelected(0));
            VisFirstControlEvents.reloadVisualization();
        });
        setAnchor(scenarioComboBox, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        heightFactor++;
        heightFactor++;

//        // Radiobuttons für VisusKlasse oder Dezimalvisuswert
//        setAnchor(dezVisusRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        dezVisusRadio.setText("Dezimalvisus");
//        dezVisusRadio.setToggleGroup(visusToggle);
//        heightFactor++;
//
//        setAnchor(logMarVisusRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        logMarVisusRadio.setText("LogMar Visus");
//        logMarVisusRadio.setToggleGroup(visusToggle);
//        heightFactor++;
//
//        setAnchor(visusKlasseRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        visusKlasseRadio.setText("Visusklasse");
//        visusKlasseRadio.setToggleGroup(visusToggle);
//        heightFactor++;
//
//        setAnchor(deltaVisusRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        deltaVisusRadio.setText("Visusdelta");
//        deltaVisusRadio.setToggleGroup(visusToggle);
//        heightFactor++;
//
//        setAnchor(deltaLogMarVisusRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        deltaLogMarVisusRadio.setText("Visusdelta (LogMar)");
//        deltaLogMarVisusRadio.setToggleGroup(visusToggle);
//        heightFactor++;
//
//        setAnchor(deltaVisusClassRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        deltaVisusClassRadio.setText("Visusklassendelta");
//        deltaVisusClassRadio.setToggleGroup(visusToggle);
//        deltaVisusClassRadio.setSelected(true);
//        visusToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
//            VisFirstControlEvents.reloadVisualization();
//        });
//        heightFactor++;
//        heightFactor++;
        // Radiobuttons für die Auswahl zur Farbe
//        setAnchor(greyRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        greyRadio.setText("Graustufen");
//        greyRadio.setToggleGroup(colorToggle);
//        heightFactor++;
//
//        setAnchor(colorRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        colorRadio.setText("Farben");
//        colorRadio.setSelected(true);
//        colorRadio.setToggleGroup(colorToggle);
//        colorToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
//            VisFirstControlEvents.refreshScreen();
//        });
//        heightFactor++;
//        heightFactor++;
        // Radiobuttons für die Auswahl ob die ersten Ereignisse auf ein Datum gelegt werden sollen
        setAnchor(normBegin, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        normBegin.setText("Genormter Startzeitpunkt");
        normBegin.setToggleGroup(normStartToggle);
        heightFactor++;

        setAnchor(timeBeginn, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        timeBeginn.setText("Realer Startzeitpunkt");
        timeBeginn.setSelected(true);
        timeBeginn.setToggleGroup(normStartToggle);
        normStartToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
            timeBeginOffset.set(timeBeginn.isSelected() ? timeBeginOffsetSave.get() : 0.0);
            timeBeginYearOffset.set(timeBeginn.isSelected() ? timeBeginYearOffsetSave.get() : 0);
            VisFirstControlEvents.refreshScreen();
        });
        heightFactor++;
        heightFactor++;

        // Radiobuttons für die Auswahl, ob Patienten zusammen bleiben sollen
        setAnchor(eyeRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        eyeRadio.setText("Augen einzeln");
        eyeRadio.setToggleGroup(groupingToggle);
        heightFactor++;

        setAnchor(patientRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        patientRadio.setText("nach Patienten sortieren");
        patientRadio.setSelected(true);
        patientRadio.setToggleGroup(groupingToggle);
        groupingToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
            VisFirstControlEvents.reloadVisualization();
        });
        heightFactor++;
        heightFactor++;

//    public static RadioButton lengthOfTreatmentRadio = new RadioButton();
//    public static RadioButton numberOfInjectionsRadio = new RadioButton();
//    public static RadioButton numberOfMedicationRadio = new RadioButton();
//    public static RadioButton visusImprovementRadio = new RadioButton();
//    public static RadioButton visusImprovementAfterMedicationSwitchRadio = new RadioButton();
        // Auswahl via ComboBox für die Sortierung der Patienten
        sortingDescriptionLabel.setText("Sortierung der Augen der Patienten");
        setAnchor(sortingDescriptionLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        heightFactor++;

        sortingComboBox.getItems().clear();
        sortingComboBox.getItems().add("Länge der Behandlung");
        sortingComboBox.getItems().add("Anzahl der besonderen Ereignisse");
        sortingComboBox.getItems().add("Anzahl der Injektionen");
        sortingComboBox.getItems().add("Anzahl der verschiedenen Medikamente");
        sortingComboBox.getItems().add("Visusentwicklung");
        sortingComboBox.getItems().add("Zeit stabil");
        sortingComboBox.getItems().add("Eingangsvisus");
        sortingComboBox.getItems().add("Ausgangsvisus");
        sortingComboBox.getItems().add("Durschnittsvisus");
        sortingComboBox.getItems().add("Medianvisus");
        sortingComboBox.getItems().add("Anzahl Diagnosen im System");
        sortingComboBox.getSelectionModel().selectFirst();

        sortingComboBox.setEditable(false);
        sortingComboBox.setOnAction(e -> VisFirstControlEvents.reloadVisualization());
        setAnchor(sortingComboBox, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
        heightFactor++;
        heightFactor++;

        // Radiobuttons für die Sortierung der Augen
//        setAnchor(suddenIncidentRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        suddenIncidentRadio.setText("Länge der Behandlung");
//        suddenIncidentRadio.setToggleGroup(sortingToggle);
//        heightFactor++;
//
//        setAnchor(lengthOfTreatmentRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        lengthOfTreatmentRadio.setText("Anzahl der besonderen Ereignisse");
//        lengthOfTreatmentRadio.setToggleGroup(sortingToggle);
//        heightFactor++;
//
//        setAnchor(numberOfInjectionsRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        numberOfInjectionsRadio.setText("Anzahl der Injektionen");
//        numberOfInjectionsRadio.setToggleGroup(sortingToggle);
//        heightFactor++;
//
//        setAnchor(numberOfMedicationRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        numberOfMedicationRadio.setText("Anzahl der verschiedenen Medikamente");
//        numberOfMedicationRadio.setToggleGroup(sortingToggle);
//        heightFactor++;
//
//        setAnchor(visusImprovementRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        visusImprovementRadio.setText("Visusentwicklung");
//        visusImprovementRadio.setToggleGroup(sortingToggle);
//        heightFactor++;
//
//        setAnchor(stableTimeRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        stableTimeRadio.setText("Zeit stabil");
//        stableTimeRadio.setToggleGroup(sortingToggle);
//        heightFactor++;
//
//        setAnchor(visusImprovementAfterMedicationSwitchRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        visusImprovementAfterMedicationSwitchRadio.setText("Visus nach Medikamentenwechsel");
//        visusImprovementAfterMedicationSwitchRadio.setSelected(true);
//        visusImprovementAfterMedicationSwitchRadio.setToggleGroup(sortingToggle);
//        sortingToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
//            VisFirstControlEvents.reloadVisualization();
//        });
//        heightFactor++;
//        heightFactor++;
        // Sliderdefinition und initialisierung
        // Der Dividend, der festlegt, in welche Helligkeitsklasse ein Dezimalvisuwert einsortiert wird
//        divSlidLabel.setText("Faktor für die logarithmische Skala");
//        setAnchor(divSlidLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        heightFactor++;
//
//        dividendSlider.setMin(1.01);
//        dividendSlider.setMax(3);
//        dividendSlider.setValue(2);
//        divSlidVal.textProperty().bind(dividendSlider.valueProperty().asString());
//        divSlidVal.textProperty().addListener((textV, oldV, newV) -> {
//            VisFirstControlEvents.refreshScreen();
//        });
//        setAnchor(divSlidVal, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        divSlidVal.setPrefWidth(50);
//        setAnchor(dividendSlider, LEFTOFFSET + SLIDEROFFSET, RIGHTOFFSET, 5 + TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        heightFactor++;
//        heightFactor++;
        // Hintergrundhelligkeit
//            public static Label backColSlidLabel = new Label();
//    public static Slider backColorSlider = new Slider();
//    public static TextField backColSlidVal = new TextField();
        backColSlidLabel.setText("Hintergrundhelligkeit");
        setAnchor(backColSlidLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        heightFactor++;

        backColorSlider.setMin(60);
        backColorSlider.setMax(255);
        backColorSlider.setValue(235);
        backColSlidVal.textProperty().bind(backColorSlider.valueProperty().asString());
        setAnchor(backColSlidVal, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        backColSlidVal.setPrefWidth(50);
        ((AnchorPane) legendAnchorPane).setBackground(MainWindowBuilder.setStyle(legendAnchorPane, MainWindowBuilder.NodeStyles.DYNAMIC_BACKCOLOR));
        backColSlidVal.textProperty().addListener((textV, oldV, newV) -> {
            //((MainWindowBuilder.DotGrid) visusFirstDrawObjectAnchorPane).setBackground(MainWindowBuilder.setStyle(visusFirstDrawObjectAnchorPane, MainWindowBuilder.NodeStyles.DYNAMIC_BACKCOLOR));
            // Set the color (brightness) of the legend elements (Koordinatensystem) dynamically in dependence of the background brightness
            backColPaintObject.bind(Bindings.createObjectBinding(() -> {
                int col = (int) (backColorSlider.getValue() - 60);
                return Color.rgb(col, col, col);
            }));
            ((AnchorPane) legendAnchorPane).setBackground(MainWindowBuilder.setStyle(legendAnchorPane, MainWindowBuilder.NodeStyles.DYNAMIC_BACKCOLOR));
        });

        setAnchor(backColorSlider, LEFTOFFSET + SLIDEROFFSET, RIGHTOFFSET, 5 + TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        heightFactor++;
        heightFactor++;

        saturationSlidLabel.setText("Sättigung der Farbanzeige");
        setAnchor(saturationSlidLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        heightFactor++;

        saturationSlider.setMin(0);
        saturationSlider.setMax(1);
        saturationSlider.setValue(1);
        saturationSlidVal.textProperty().bind(saturationSlider.valueProperty().asString());
        setAnchor(saturationSlidVal, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        saturationSlidVal.setPrefWidth(50);
        saturationSlidVal.textProperty().addListener((textV, oldV, newV) -> {
            VisFirstControlEvents.refreshScreen();
        });

        setAnchor(saturationSlider, LEFTOFFSET + SLIDEROFFSET, RIGHTOFFSET, 5 + TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        heightFactor++;
        heightFactor++;

        brightnessSlidLabel.setText("Helligkeit der Farbanzeige");
        setAnchor(brightnessSlidLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        heightFactor++;

        brightnessSlider.setMin(0);
        brightnessSlider.setMax(1);
        brightnessSlider.setValue(0);
        brightnessSlidVal.textProperty().bind(brightnessSlider.valueProperty().asString());
        setAnchor(brightnessSlidVal, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        brightnessSlidVal.setPrefWidth(50);
        brightnessSlidVal.textProperty().addListener((textV, oldV, newV) -> {
            VisFirstControlEvents.refreshScreen();
        });

        setAnchor(brightnessSlider, LEFTOFFSET + SLIDEROFFSET, RIGHTOFFSET, 5 + TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        heightFactor++;
        heightFactor++;

        borSlidLabel.setText("Dicke der Visusereignisse (in Pixel)");
        setAnchor(borSlidLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        heightFactor++;

        // Breite der VisusStriche
        borderWidthSlider.setMin(0);
        borderWidthSlider.setMax(2);
        borderWidthSlider.setValue(0.25);
        borSlidVal.textProperty().bind(borderWidthSlider.valueProperty().asString());
        setAnchor(borSlidVal, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        borSlidVal.setPrefWidth(50);

        setAnchor(borderWidthSlider, LEFTOFFSET + SLIDEROFFSET, RIGHTOFFSET, 5 + TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        heightFactor++;
        heightFactor++;

        // Zoomfaktor
        zoomSlidLabel.setText("Zoomstufe");
        setAnchor(zoomSlidLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        growZoomFactor.bind(zoomSlider.valueProperty().divide(10d));

        heightFactor++;

        zoomSlider.setMin(VisusFirstMaker.MIN_ZOOM_FACTOR);
        zoomSlider.setMax(VisusFirstMaker.MAX_ZOOM_FACTOR);
        zoomSlider.setValue(VisusFirstMaker.zoomFactor);
        zoomSlidVal.textProperty().bind(zoomSlider.valueProperty().asString());
        zoomSlidVal.textProperty().addListener((textV, oldV, newV) -> {
            VisusFirstMaker.zoomFactor = zoomSlider.getValue();
        });
        setAnchor(zoomSlidVal, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        zoomSlidVal.setPrefWidth(50);

        setAnchor(zoomSlider, LEFTOFFSET + SLIDEROFFSET, RIGHTOFFSET, 5 + TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        heightFactor++;
        heightFactor++;

        groupingLabel.setText("Gruppierung nach");
        setAnchor(groupingLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);

        heightFactor++;
        groupingComboBox.setItems(VA_topos.observableData);
        setAnchor(groupingComboBox, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        ObservableList<String> myFields = FXCollections.observableArrayList();
        VA_topos.myDimensions.keySet().forEach(key -> myFields.add(key));
        myFields.sort(Comparator.naturalOrder());
        groupingComboBox.setItems(myFields);
        heightFactor++;
        heightFactor++;

        groupingClassCountLabel.setText("Anzahl der Gruppen");
        setAnchor(groupingClassCountLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        heightFactor++;

//        groupingSpinner.setEditable(true);
        groupingSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100, 5));

        setAnchor(groupingSpinner, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
        heightFactor++;
        heightFactor++;

        controlVisusFirstAnchorPane.getChildren().addAll(restoreButton,
                legendScaleRectangle,
                legendMinValueLabel,
                legendMidValueLabel,
                legendMaxValueLabel,
                summaryVisShowCheckBox,
                detailVisShowCheckBox,
                legendShowCheckBox,
                regressionShowCheckBox,
                suddenIncidentShowCheckBox,
                showMedChangeCheckBox,
                scaleDescriptionLabel,
                scaleComboBox,
                visusDimensionDescriptionLabel,
                visusDimensionComboBox,
                //linearRadio,
                //logarithmicRadio,
                //diverging3Radio,
                //                horizontalRadio,
                //                verticalRadio,
                //                circleRadio,
                //                visusKlasseRadio,
                //                dezVisusRadio,
                //                logMarVisusRadio,
                //                deltaVisusRadio,
                //                deltaLogMarVisusRadio,
                //                deltaVisusClassRadio,
                scenarioDescriptionLabel,
                scenarioComboBox,
                //divSlidLabel, dividendSlider, divSlidVal,
                backColSlidLabel, backColorSlider, backColSlidVal,
                saturationSlidLabel, saturationSlider, saturationSlidVal,
                brightnessSlidLabel, brightnessSlider, brightnessSlidVal,
                borSlidLabel, borderWidthSlider, borSlidVal,
                zoomSlidLabel, zoomSlider, zoomSlidVal,
                //groupingLabel, groupingComboBox, groupingClassCountLabel, groupingSpinner,
                //annoButton,
                //                greyRadio,
                //                colorRadio,
                normBegin,
                timeBeginn,
                eyeRadio,
                patientRadio,
                sortingDescriptionLabel,
                sortingComboBox
        //                suddenIncidentRadio,
        //                lengthOfTreatmentRadio,
        //                numberOfInjectionsRadio,
        //                numberOfMedicationRadio,
        //                visusImprovementRadio,
        //                visusImprovementAfterMedicationSwitchRadio,
        //                stableTimeRadio,
        //legend
        );

        // Bildschirmaufbau:
        // 1. Aufteilung links/rechts durch SplitPane orderVisusFirstTab: 
        //      links:  AnchorPane controlVisusFirstAnchorPane
        //      rechts: AnchorPane orderVisusFirstVis
        //
        // 2. rechts unterteilung von orderVisusFirstVis in:
        //      oben (teiltransparent): annoAnchorPane
        //      ganze Fläche:           visusFirstAnchorPane
        //
        // Den rechten Teil (Visus First Visualisierung) in zwei Teile aufteilen
        // Container für die Aufteilung links / rechts
        orderVisusFirstTab = new SplitPane();
        setAnchor(orderVisusFirstTab, 0d, 0d, 0d, 0d);
        orderVisusFirstTab.setBackground(setStyle(orderVisusFirstTab, MainWindowBuilder.NodeStyles.SOLID));

        // Container für zwei weitere Panel, die auf der rechten Seite übereinander angezeigt werden
        orderVisusFirstVis = new AnchorPane();
        setAnchor(orderVisusFirstVis, 0d, 0d, 0d, 0d);

        // Die Legende für die Datenanzeige formatieren
        summaryAnchorPane = new AnchorPane();

        summaryAnchorPane.setMinHeight(250);
        summaryAnchorPane.setPrefHeight(250);
        summaryAnchorPane.setMaxHeight(250);
        summaryAnchorPane.setMinWidth(530);
        summaryAnchorPane.setPrefWidth(530);
        summaryAnchorPane.setMaxWidth(530);
        summaryVisusCanvas = new Canvas(500, 220);
        summaryAnchorPane.visibleProperty().bind(summaryVisShowCheckBox.selectedProperty());
        setAnchor(summaryVisusCanvas, 10d, 10d, 10d, 10d);

        setAnchor(summaryCloseButton, Double.NaN, 15d, 15d, Double.NaN);
        summaryCloseButton.setOnAction(event -> summaryVisShowCheckBox.setSelected(false));

        summaryAnchorPane.getChildren().addAll(summaryVisusCanvas, summaryCloseButton);
        setAnchor(summaryAnchorPane, Double.NaN, detailAnchorPane.getPrefWidth(), 0d, Double.NaN);

        summaryAnchorPane.setBackground(MainWindowBuilder.setStyle(summaryAnchorPane, 1.0, MainWindowBuilder.NodeStyles.TRANSPARENT, 255, 255, 255, 1.0, Color.BLACK));

        detailAnchorPane.setOnMousePressed(e -> MainWindowBuilder.movePane(detailAnchorPane, e.getX(), e.getY()));
        summaryAnchorPane.setOnMousePressed(e -> MainWindowBuilder.movePane(summaryAnchorPane, e.getX(), e.getY()));

        orderVisusFirstVis.getChildren().addAll(legendAnchorPane, visusFirstDrawObjectAnchorPane, detailAnchorPane, summaryAnchorPane);
//        orderVisusFirstVis.getChildren().addAll(visusFirstAnnotationAnchorPane, visusFirstDrawObjectAnchorPane, detailAnchorPane, summaryAnchorPane);

        orderVisusFirstTab.getItems().addAll(controlVisusFirstAnchorPane, orderVisusFirstVis);
        orderVisusFirstTab.setDividerPositions(0.3d);

        myVisusFirstTab.setContent(orderVisusFirstTab);

    }

    // Function to move a Panel on the screen
//    private void movePane(Node myPane, double deltaX, double deltaY) {
//        myPane.setOnMouseDragged(e -> {
//            myPane.setTranslateX(myPane.getTranslateX() + e.getX() - deltaX);
//            myPane.setTranslateY(myPane.getTranslateY() + e.getY() - deltaY);
//        });
//    }
}
