/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.ui;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Tab;
import javafx.scene.paint.Color;
import va_topos.events.EventHandlers;
import va_topos.ui.MainWindowBuilder.DotGrid;
//import static va_topos.ui.MainWindowBuilder.annoBackButton;
//import static va_topos.ui.MainWindowBuilder.annoDeleteButton;
//import static va_topos.ui.MainWindowBuilder.annoFwdButton;
//import static va_topos.ui.MainWindowBuilder.annoNewButton;
//import static va_topos.ui.MainWindowBuilder.annoRedoButton;
//import static va_topos.ui.MainWindowBuilder.annoUndoButton;
import static va_topos.ui.MainWindowBuilder.visPanel;

/**
 *
 * @author lokal-admin
 */
public class SpritzenFirstTabBuilder {

    public enum NodeStyles {
        TRANSPARENT,
        ANNOTHUMBNAIL,
        ANNOTILEPANE,
        SCROLLTILEPANE,
        SOLID;
    }

//    private static final int[] STDNODECOLORRGB = {90, 90, 90};
    public static final int[] STDNODECOLORRGB = {60, 190, 250};
    private static final double STDOPACITY = 0.15;
    private static final double STDCHILDRENOPACITY = 0.75;
    private static final double STDCORNERRADII = 5d;
    private static final double STDINSET = 5d;

    private static final double TOPOFFSET = 20 + STDINSET;
    private static final double BOTTOMOFFSET = 20 + STDINSET;
    private static final double LEFTOFFSET = 10 + STDINSET;
    private static final double RIGHTOFFSET = 10 + STDINSET;
    private static final double TEXTFIELDSPACE = 20;
    private static final double RADIOSPACE = 20;
    private static final double SLIDEROFFSET = 50;
    private static double heightFactor = 0;

    @SuppressWarnings("unchecked")
    public SpritzenFirstTabBuilder(Tab myVisusFirstTab) {

        EventHandlers eh = new EventHandlers();
        

//        Canvas myCan = (Canvas) visPanel.getChildren().get(0);
        Canvas myCan = new Canvas(500,500);
        DotGrid myDotGrid = new DotGrid(1);
        //myCan = myDotGrid.getc
        GraphicsContext gc = myCan.getGraphicsContext2D();
        gc.setFill(Color.BLACK);
        //gc.fillRect(100, 200, 300, 400);
        gc.setStroke(Color.BLUE);
        gc.strokeLine(100d, 200d, 300d, 400d);
        //System.out.println("This is it: " + visPanel.getChildren().get(0).toString());
        myVisusFirstTab.setContent(myCan);        
//        // some spare Labels to show information during the development of the program
//        heightFactor = 0;
//        spareOne.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
//        spareOne.setText("");
//        heightFactor++;
//
//        spareTwo.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
//        spareTwo.setText("");
//        heightFactor++;
//
//        spareThree.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
//        spareThree.setText("");
//        heightFactor++;
//
//        spareFour.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
//        spareFour.setText("");
//        heightFactor++;
//
//        spareFive.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
//        spareFive.setText("");
//        heightFactor++;
//
//        spareSix.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
//        spareSix.setText("");
//        heightFactor++;
//
//        spareSeven.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
//        spareSeven.setText("");
//        heightFactor++;
//
//        spareEight.relocate(LEFTOFFSET, TOPOFFSET + TEXTFIELDSPACE * heightFactor);
//        spareEight.setText("");
//        heightFactor++;
//
//        // annoPane ist ein Panel zur anzeige von MetaDaten und Annodaten während der Visualisierung
//        detailAnchorPane = new AnchorPane();
//        setAnchor(detailAnchorPane, Double.NaN, 0d, 0d, Double.NaN);
//        detailAnchorPane.setMinHeight(150);
//        detailAnchorPane.setPrefHeight(200);
//        detailAnchorPane.setMaxHeight(450);
//        detailAnchorPane.setMinWidth(150);
//        detailAnchorPane.setPrefWidth(300);
//        detailAnchorPane.setMaxWidth(450);
//        detailAnchorPane.setBackground(setStyle(detailAnchorPane, MainWindowBuilder.NodeStyles.TRANSPARENT));
//
//        detailAnchorPane.getChildren().addAll(spareOne, spareTwo, spareThree, spareFour, spareFive, spareSix, spareSeven, spareEight);
//
//        // annoPane ist ein Panel zur anzeige von MetaDaten und Annodaten während der Visualisierung
//        annoTilePane = new TilePane();
//        annoTilePane.setBackground(setStyle(annoTilePane, MainWindowBuilder.NodeStyles.ANNOTILEPANE));
//
//        // Das Visualization-AnchorPane erzeugen, in welchem die Vis angezeigt werden.
//        visusFirstAnnotationAnchorPane = new MainWindowBuilder.DotGrid(1);
//        setAnchor(visusFirstAnnotationAnchorPane, 0d, 0d, 0d, 0d);
//
//        // Initialize the AnchorPane, where all the patients will be drawn to
//        visusFirstDrawObjectAnchorPane = new MainWindowBuilder.DotGrid(1);
//        setAnchor(visusFirstDrawObjectAnchorPane, 0d, 0d, 0d, 0d);
//
//        // If on the drawing field is scrolled
//        visusFirstDrawObjectAnchorPane.setOnScroll(e -> VisFirstViewEvents.onVisViewScrolled(e));
//        // If on the drawing field a mousebutton is pressed
//        visusFirstDrawObjectAnchorPane.setOnMousePressed(e -> VisFirstViewEvents.onVisMousePressed(e));
//
//        // Factor for the location of the controls on the control panel for the vis
//        heightFactor = 0;
//
//        // Das Steuerungspanel erzeugen, in welchem die Elemente für die Steuerung der Visualisierungen enthalten sind
//        controlVisusFirstAnchorPane = new AnchorPane();
//        setAnchor(controlVisusFirstAnchorPane, 0d, 0d, 0d, 0d);
//        controlVisusFirstAnchorPane.setMinWidth(150);
//        controlVisusFirstAnchorPane.setMaxWidth(300);
//        controlVisusFirstAnchorPane.setBackground(setStyle(controlVisusFirstAnchorPane, MainWindowBuilder.NodeStyles.TRANSPARENT));
//
//        // Originale Position wieder herstellen
//        restoreButton = new Button("Originalpositionierung wieder herstellen");
//        setAnchor(restoreButton, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        restoreButton.setOnAction(e -> visusFirstDrawObjectAnchorPane.getChildren().forEach(child -> {
//            child.setTranslateX(0);
//            child.setTranslateY(0);
//        }));
//        heightFactor++;
//        heightFactor++;
//
//        // Radiobuttons für die Farbskalaberechnung
//        setAnchor(linearRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        linearRadio.setText("Linear");
//        linearRadio.setToggleGroup(scaleToggle);
//        heightFactor++;
//
//        setAnchor(logarithmicRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        logarithmicRadio.setText("Logarithmic");
//        logarithmicRadio.setToggleGroup(scaleToggle);
//        heightFactor++;
//
//        setAnchor(diverging3Radio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        diverging3Radio.setText("Diverging 3-Class");
//        diverging3Radio.setToggleGroup(scaleToggle);
//        diverging3Radio.setSelected(true);
//        scaleToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
//            VisFirstControlEvents.refreshScreen();
//        });
//        heightFactor++;
//        heightFactor++;
//
//        // Radiobuttons für VisusKlasse oder Dezimalvisuswert
//        setAnchor(visusKlasseRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        visusKlasseRadio.setText("Visusklasse");
//        visusKlasseRadio.setToggleGroup(visusToggle);
//        heightFactor++;
//
//        setAnchor(dezVisusRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        dezVisusRadio.setText("Dezimalvisus");
//        dezVisusRadio.setToggleGroup(visusToggle);
//        heightFactor++;
//
//        setAnchor(deltaVisusRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        deltaVisusRadio.setText("Visusdelta");
//        deltaVisusRadio.setToggleGroup(visusToggle);
//        heightFactor++;
//
//        setAnchor(deltaVisusClassRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        deltaVisusClassRadio.setText("Visusklassendelta");
//        deltaVisusClassRadio.setToggleGroup(visusToggle);
//        deltaVisusClassRadio.setSelected(true);
//        visusToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
//            VisFirstControlEvents.refreshScreen();
//        });
//        heightFactor++;
//        heightFactor++;
//
//        // Radiobuttons für die Auswahl zur Farbe
//        setAnchor(greyRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        greyRadio.setText("Graustufen");
//        greyRadio.setToggleGroup(colorToggle);
//        heightFactor++;
//
//        setAnchor(colorRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        colorRadio.setText("Farben");
//        colorRadio.setSelected(true);
//        colorRadio.setToggleGroup(colorToggle);
//        colorToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
//            VisFirstControlEvents.refreshScreen();
//        });
//        heightFactor++;
//        heightFactor++;
//
//        // Radiobuttons für die Auswahl ob die ersten Ereignisse auf ein Datum gelegt werden sollen
//        setAnchor(normBegin, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        normBegin.setText("Genormter Startzeitpunkt");
//        normBegin.setToggleGroup(normStartToggle);
//        heightFactor++;
//
//        setAnchor(timeBeginn, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        timeBeginn.setText("Realer Startzeitpunkt");
//        timeBeginn.setSelected(true);
//        timeBeginn.setToggleGroup(normStartToggle);
//        normStartToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
//            VisFirstControlEvents.refreshScreen();
//        });
//        heightFactor++;
//        heightFactor++;
//
//        // Radiobuttons für die Auswahl, ob Patienten zusammen bleiben sollen
//        setAnchor(eyeRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        eyeRadio.setText("Augen einzeln");
//        eyeRadio.setToggleGroup(groupingToggle);
//        heightFactor++;
//
//        setAnchor(patientRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        patientRadio.setText("nach Patienten sortieren");
//        patientRadio.setSelected(true);
//        patientRadio.setToggleGroup(groupingToggle);
//        groupingToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
//            VisFirstControlEvents.reloadVisualization();
//        });
//        heightFactor++;
//        heightFactor++;
//
////    public static RadioButton lengthOfTreatmentRadio = new RadioButton();
////    public static RadioButton numberOfInjectionsRadio = new RadioButton();
////    public static RadioButton numberOfMedicationRadio = new RadioButton();
////    public static RadioButton visusImprovementRadio = new RadioButton();
////    public static RadioButton visusImprovementAfterMedicationSwitchRadio = new RadioButton();
//        // Radiobuttons für die Sortierung der Augen
//        setAnchor(lengthOfTreatmentRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        lengthOfTreatmentRadio.setText("Länge der Behandlung");
//        lengthOfTreatmentRadio.setToggleGroup(sortingToggle);
//        heightFactor++;
//
//        setAnchor(numberOfInjectionsRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        numberOfInjectionsRadio.setText("Anzahl der Injektionen");
//        numberOfInjectionsRadio.setToggleGroup(sortingToggle);
//        heightFactor++;
//
//        setAnchor(numberOfMedicationRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        numberOfMedicationRadio.setText("Anzahl der verschiedenen Medikamente");
//        numberOfMedicationRadio.setToggleGroup(sortingToggle);
//        heightFactor++;
//
//        setAnchor(visusImprovementRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        visusImprovementRadio.setText("Visusentwicklung");
//        visusImprovementRadio.setToggleGroup(sortingToggle);
//        heightFactor++;
//
//        setAnchor(visusImprovementAfterMedicationSwitchRadio, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        visusImprovementAfterMedicationSwitchRadio.setText("Visus nach Medikamentenwechsel");
//        visusImprovementAfterMedicationSwitchRadio.setSelected(true);
//        visusImprovementAfterMedicationSwitchRadio.setToggleGroup(sortingToggle);
//        sortingToggle.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
//            VisFirstControlEvents.reloadVisualization();
//        });
//        heightFactor++;
//        heightFactor++;

        // Sliderdefinition und initialisierung
        // Der Dividend, der festlegt, in welche Helligkeitsklasse ein Dezimalvisuwert einsortiert wird
//        divSlidLabel.setText("Faktor für die logarithmische Skala");
//        setAnchor(divSlidLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        heightFactor++;
//
//        dividendSlider.setMin(1.01);
//        dividendSlider.setMax(3);
//        dividendSlider.setValue(2);
//        divSlidVal.textProperty().bind(dividendSlider.valueProperty().asString());
//        divSlidVal.textProperty().addListener((textV, oldV, newV) -> {
//            VisFirstControlEvents.refreshScreen();
//        });
//        setAnchor(divSlidVal, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        divSlidVal.setPrefWidth(50);
//        setAnchor(dividendSlider, LEFTOFFSET + SLIDEROFFSET, RIGHTOFFSET, 5 + TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        heightFactor++;
//        heightFactor++;

        // Breite des Feldrandes
//        borSlidLabel.setText("Dicke der Umrandung (in Pixel)");
//        setAnchor(borSlidLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        heightFactor++;
//
//        borderWidthSlider.setMin(0);
//        borderWidthSlider.setMax(2);
//        borderWidthSlider.setValue(0.25);
//        borSlidVal.textProperty().bind(borderWidthSlider.valueProperty().asString());
//        setAnchor(borSlidVal, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        borSlidVal.setPrefWidth(50);
//
//        setAnchor(borderWidthSlider, LEFTOFFSET + SLIDEROFFSET, RIGHTOFFSET, 5 + TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        heightFactor++;
//        heightFactor++;
//
//        // Zoomfaktor
//        zoomSlidLabel.setText("Zoomstufe");
//        setAnchor(zoomSlidLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        growZoomFactor.bind(zoomSlider.valueProperty().divide(10d));
//
//        heightFactor++;
//
//        zoomSlider.setMin(VisusFirstMaker.MIN_ZOOM_FACTOR);
//        zoomSlider.setMax(VisusFirstMaker.MAX_ZOOM_FACTOR);
//        zoomSlider.setValue(VisusFirstMaker.zoomFactor);
//        zoomSlidVal.textProperty().bind(zoomSlider.valueProperty().asString());
//        zoomSlidVal.textProperty().addListener((textV, oldV, newV) -> {
//            VisusFirstMaker.zoomFactor = zoomSlider.getValue();
//        });
//        setAnchor(zoomSlidVal, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        zoomSlidVal.setPrefWidth(50);
//
//        setAnchor(zoomSlider, LEFTOFFSET + SLIDEROFFSET, RIGHTOFFSET, 5 + TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        heightFactor++;
//        heightFactor++;
//
//        showMedChangeCheckBox = new CheckBox("Visus nach Medikamentenwechsel zeigen.");
//        setAnchor(showMedChangeCheckBox, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (RADIOSPACE * heightFactor), Double.NaN);
//        heightFactor++;
//        heightFactor++;
//
//        groupingLabel.setText("Gruppierung nach");
//        setAnchor(groupingLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//
//        heightFactor++;
//        groupingComboBox.setItems(VA_topos.observableData);
//        setAnchor(groupingComboBox, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        ObservableList<String> myFields = FXCollections.observableArrayList();
//        VA_topos.myDimensions.keySet().forEach(key -> myFields.add(key));
//        myFields.sort(Comparator.naturalOrder());
//        groupingComboBox.setItems(myFields);
//        heightFactor++;
//        heightFactor++;
//
//
//
//        groupingClassCountLabel.setText("Anzahl der Gruppen");
//        setAnchor(groupingClassCountLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        heightFactor++;
//
//
//        
//        
////        groupingSpinner.setEditable(true);
//        groupingSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100, 5));
//
//        setAnchor(groupingSpinner, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
//        heightFactor++;
//        heightFactor++;
//
//
//
//
//        controlVisusFirstAnchorPane.getChildren().addAll(
//                restoreButton,
//                linearRadio,
//                logarithmicRadio,
//                diverging3Radio,
//                visusKlasseRadio,
//                dezVisusRadio,
//                deltaVisusRadio,
//                deltaVisusClassRadio,
//                //divSlidLabel, dividendSlider, divSlidVal,
//                borSlidLabel, borderWidthSlider, borSlidVal,
//                zoomSlidLabel, zoomSlider, zoomSlidVal,
//                //groupingLabel, groupingComboBox, groupingClassCountLabel, groupingSpinner,
//                //annoButton,
//                greyRadio,
//                colorRadio,
//                normBegin,
//                timeBeginn,
//                eyeRadio,
//                patientRadio,
//                lengthOfTreatmentRadio,
//                numberOfInjectionsRadio,
//                numberOfMedicationRadio,
//                visusImprovementRadio,
//                visusImprovementAfterMedicationSwitchRadio,
//                showMedChangeCheckBox
//                //legend
//        );

        // Bildschirmaufbau:
        // 1. Aufteilung links/rechts durch SplitPane orderVisusFirstTab: 
        //      links:  AnchorPane controlVisusFirstAnchorPane
        //      rechts: AnchorPane orderVisusFirstVis
        //
        // 2. rechts unterteilung von orderVisusFirstVis in:
        //      oben (teiltransparent): annoAnchorPane
        //      ganze Fläche:           visusFirstAnchorPane
        //
        // Den rechten Teil (Visus First Visualisierung) in zwei Teile aufteilen
        // Container für die Aufteilung links / rechts
//        orderVisusFirstTab = new SplitPane();
//        setAnchor(orderVisusFirstTab, 0d, 0d, 0d, 0d);
//        orderVisusFirstTab.setBackground(setStyle(orderVisusFirstTab, MainWindowBuilder.NodeStyles.SOLID));
//
//        // Container für zwei weitere Panel, die auf der rechten Seite übereinander angezeigt werden
//        orderVisusFirstVis = new AnchorPane();
//        setAnchor(orderVisusFirstVis, 0d, 0d, 0d, 0d);
//
//        // Die Legende für die Datenanzeige formatieren
//        setAnchor(legend, Double.NaN, 20d, 20d, Double.NaN);
//        legend.setWidth(100);
//        legend.setHeight(20);
//
//        GraphicsContext gc = legend.getGraphicsContext2D();
//
//        gc.setFill(Color.BLACK);
//        gc.fillRect(0d, 0d, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
//
//        gc.setFill(Color.WHITE);
//        gc.fillRect(1d, 1d, gc.getCanvas().getWidth() - 2, gc.getCanvas().getHeight() - 2);
//
//        summaryAnchorPane = new AnchorPane();
//
//        summaryAnchorPane.setMinHeight(200);
//        summaryAnchorPane.setPrefHeight(200);
//        summaryAnchorPane.setMaxHeight(200);
//        summaryAnchorPane.setMinWidth(530);
//        summaryAnchorPane.setPrefWidth(530);
//        summaryAnchorPane.setMaxWidth(530);
//        summaryVisusCanvas = new Canvas(500, 170);
//        setAnchor(summaryVisusCanvas, 10d, 10d, 10d, 10d);
//
//        summaryAnchorPane.getChildren().add(summaryVisusCanvas);
//        setAnchor(summaryAnchorPane, Double.NaN, detailAnchorPane.getPrefWidth(), 0d, Double.NaN);
//
//        summaryAnchorPane.setBackground(MainWindowBuilder.setStyle(summaryAnchorPane, 1.0, MainWindowBuilder.NodeStyles.TRANSPARENT, 255, 255, 255, 1.0, Color.BLACK));
//
//        detailAnchorPane.setOnMousePressed(e -> MainWindowBuilder.movePane(detailAnchorPane, e.getX(), e.getY()));
//        summaryAnchorPane.setOnMousePressed(e -> MainWindowBuilder.movePane(summaryAnchorPane, e.getX(), e.getY()));
//
//        orderVisusFirstVis.getChildren().addAll(visusFirstAnnotationAnchorPane, visusFirstDrawObjectAnchorPane, detailAnchorPane, summaryAnchorPane);
//
//        orderVisusFirstTab.getItems().addAll(controlVisusFirstAnchorPane, orderVisusFirstVis);
//        orderVisusFirstTab.setDividerPositions(0.3d);
//        myVisusFirstTab.setContent(orderVisusFirstTab);

    }

    // Function to move a Panel on the screen
//    private void movePane(Node myPane, double deltaX, double deltaY) {
//        myPane.setOnMouseDragged(e -> {
//            myPane.setTranslateX(myPane.getTranslateX() + e.getX() - deltaX);
//            myPane.setTranslateY(myPane.getTranslateY() + e.getY() - deltaY);
//        });
//    }
}
