/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.ui;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;

import va_topos.events.EventHandlers;

import static va_topos.ui.MainWindowBuilder.setAnchor;
import static va_topos.ui.MainWindowBuilder.stdVisCheckBox;
import static va_topos.ui.MainWindowBuilder.ladeTabMenu;

import va_topos.VA_topos;
import static va_topos.ui.MainWindowBuilder.visFirstCheckBox;

/**
 *
 * @author lokal-admin
 */
public class LadeTabBuilder {

    public LadeTabBuilder(Tab myLadeTab) {

        EventHandlers eh = new EventHandlers();
        // Menü für den Import/Export Laden und Speichern der Daten
        Menu importMenu = new Menu("Import");

        // ImportMenüItem
        MenuItem importFromFolder = new MenuItem();
        importFromFolder.setText("Daten aus Ordner importieren");
        importFromFolder.setOnAction((event) -> {
            VA_topos.ladeText.set("Starte das Importieren der Daten...");
            eh.menuLoadClicked(event, 0);
            String myString = "\n\nImportieren der Daten beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });

        MenuItem importPartFromFolder = new MenuItem();
        importPartFromFolder.setText("Teildaten aus Ordner importieren");
        importPartFromFolder.setOnAction((event) -> {
            VA_topos.ladeText.set("Starte das Importieren der Teildaten...");
            eh.menuLoadClicked(event, 3);
            String myString = "\n\nImportieren der Daten beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });

        // Automatische Berechnung der zusätzlichen Daten
        MenuItem calculateData = new MenuItem();
        calculateData.setText("Automatische Berechnung starten");
        calculateData.setOnAction((event) -> {
            String myString = "\n\nStarte die automatische Berechnung...\n";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
            eh.menuCalculateClicked(event);
            myString = "\n\nAutomatische Berechnung beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });

        // Import und Berechnung
        MenuItem importFromFolderAndCalc = new MenuItem();
        importFromFolderAndCalc.setText("Daten aus Ordner importieren + automatische Berechnung");
        importFromFolderAndCalc.setOnAction((event) -> {
            VA_topos.ladeText.set("Starte das Importieren der Daten...");
            eh.menuLoadClicked(event, 0);
            String myString = "\n\nImportieren der Daten beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
            myString = "\n\nStarte die automatische Berechnung...\n";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
            eh.menuCalculateClicked(event);
            myString = "\n\nAutomatische Berechnung beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });

        // TeilImport und Berechnung
        MenuItem importPartFromFolderAndCalc = new MenuItem();
        importPartFromFolderAndCalc.setText("Teildaten aus Ordner importieren + automatische Berechnung");
        importPartFromFolderAndCalc.setOnAction((event) -> {
            VA_topos.ladeText.set("Starte das Importieren der Teildaten...");
            eh.menuLoadClicked(event, 3);
            String myString = "\n\nImportieren der Teildaten beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
            myString = "\n\nStarte die automatische Berechnung...\n";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
            eh.menuCalculateClicked(event);
            myString = "\n\nAutomatische Berechnung beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });
        
        importMenu.getItems().addAll(importFromFolder, importPartFromFolder, calculateData, importFromFolderAndCalc, importPartFromFolderAndCalc);

        // Laden von bereits vorverarbeiteten Daten
        Menu ladeMenu = new Menu("Laden");

        // LadeMenüItem
        MenuItem loadFromCSV = new MenuItem();
        loadFromCSV.setText("Daten aus CSV laden");
        loadFromCSV.setOnAction((event) -> {
            VA_topos.ladeText.set("Starte das Laden der Daten aus der CSV...");
            eh.menuLoadClicked(event, 1);
            String myString = "\n\nLaden der Daten beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });

        // LadeMenüItem
        MenuItem loadFromObject = new MenuItem();
        loadFromObject.setText("Daten aus Speicherdatei laden");
        loadFromObject.setOnAction((event) -> {
            VA_topos.ladeText.set("Starte das Laden der Daten aus der Speicherdatei...");
            eh.menuLoadClicked(event, 2);
            String myString = "\n\nLaden der Daten beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });

        ladeMenu.getItems().addAll(loadFromCSV, loadFromObject);

        // Speichern von geladenen Daten
        Menu speicherMenu = new Menu("Speichern");

        // SpeicherMenüItem
        MenuItem saveToCSV = new MenuItem();
        saveToCSV.setText("Daten in CSV speichern");
        saveToCSV.setOnAction((event) -> {
            VA_topos.ladeText.set("Starte das Speichern der Daten in eine CSV...");
            eh.menuSaveClicked(event, true);
            String myString = "\n\nSpeichern der Daten beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });

        // SpeicherMenüItem
        MenuItem saveToObj = new MenuItem();
        saveToObj.setText("Daten als Object speichern");
        saveToObj.setOnAction((event) -> {
            VA_topos.ladeText.set("Starte das Speichern der Daten als Objekt...");
            eh.menuSaveObjClicked(event);
            String myString = "\n\nSpeichern der Daten beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });

        // SpeicherMenüItem
        MenuItem saveToJson = new MenuItem();
        saveToJson.setText("Daten als Object speichern");
        saveToJson.setOnAction((event) -> {
            VA_topos.ladeText.set("Starte das Speichern der Daten als JSON...");
            eh.menuSaveJsonClicked(event);
            String myString = "\n\nSpeichern der Daten beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });

        // SpeicherMenüItem
        MenuItem saveVisChange = new MenuItem();
        saveVisChange.setText("Visus nach Medikamentenwechsel exportieren");
        saveVisChange.setOnAction((event) -> {
            VA_topos.ladeText.set("Starte das Exportieren der Visen nach Medikamentenwechsel...");
            eh.menuSaveVisChangeClicked(event);
            String myString = "\n\nExportieren der Daten beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });

        // SpeicherMenüItem
        MenuItem printOutData = new MenuItem();
        printOutData.setText("Datensätze auf Bildschirm ausgeben");
        printOutData.setOnAction((event) -> {
            VA_topos.ladeText.set("Starte das Ausgeben der Daten...");
            eh.menuSaveClicked(event, false);
            String myString = "\n\nAusgabe der Daten beendet.";
            VA_topos.ladeText.set(VA_topos.ladeText.get() + myString);
        });

        speicherMenu.getItems().addAll(saveToCSV, saveToObj, saveToJson, saveVisChange, printOutData);

        // Die Menübar in der alle Menüpunkte enthalten sind
        ladeTabMenu = new MenuBar();
        ladeTabMenu.getMenus().addAll(importMenu, ladeMenu, speicherMenu);

        stdVisCheckBox = new CheckBox();
        stdVisCheckBox.relocate(475, 13);
        stdVisCheckBox.setText("Standardvisualisierungen laden.");
        //stdVisCheckBox.setSelected(true);
        
        visFirstCheckBox = new CheckBox();
        visFirstCheckBox.relocate(475, 33);
        visFirstCheckBox.setText("Visus First Visualisierung laden.");
        visFirstCheckBox.setSelected(true);
        
        //Label für die Bezeichnung des LadeTextfeldes
        Label logLabel = new Label();
        logLabel.relocate(10, 50);
        logLabel.setText("Log für das Laden der Daten: ");

        // LadeLog Textfeld
        TextArea ladeLog = new TextArea();
        ladeLog.textProperty().bind(VA_topos.ladeText);
        ladeLog.setEditable(false);

        setAnchor(ladeLog, 10d, 10d, 80d, 10d);

        // Ein AnchorPanel im ladeTab definieren, um darin die Elemente anzuordnen
        AnchorPane ladeAnchorPane = new AnchorPane();
        ladeAnchorPane.getChildren().addAll(ladeTabMenu, stdVisCheckBox, visFirstCheckBox, logLabel, ladeLog);
        myLadeTab.setContent(ladeAnchorPane);

    }

}
