/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.ui;

import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.time.LocalDate;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.FormatStringConverter;

import static va_topos.ui.MainWindowBuilder.patientTreeView;
import static va_topos.ui.MainWindowBuilder.scrolltableFilterTreeView;
import static va_topos.ui.MainWindowBuilder.setAnchor;
import static va_topos.ui.MainWindowBuilder.table;
import static va_topos.ui.MainWindowBuilder.tableAnchorPane;
import static va_topos.ui.MainWindowBuilder.tableFilterTreeView;

import va_topos.VA_topos;
import va_topos.data.Annotations;
import va_topos.data.DataModel;

/**
 *
 * @author lokal-admin
 */
public class TableTabBuilder {

    @SuppressWarnings("unchecked")
    public TableTabBuilder(Tab myDatenTableTab) {

        // Einen tableView hinzufügen, der die Daten als Tabelle zeigt
        setAnchor(table, 300d, 0d, 0d, 0d);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        table.setEditable(true);

        // Function to enable the editing of cells in the table
        Callback<TableColumn, TableCell> cellFactory = (TableColumn p) -> new TextFieldTableCell();

        CheckBoxTreeItem filterTreeViewRoot = new CheckBoxTreeItem("DatenFilter");
        tableFilterTreeView.setRoot(filterTreeViewRoot);
        tableFilterTreeView.setCellFactory(CheckBoxTreeCell.<String>forTreeView());

        // Add the columns to the table (on column for each attribute in the DataModel)
        final Field[] fields = DataModel.class.getDeclaredFields();

        for (Field field : fields) {

            TableColumn<DataModel, ?> column = new TableColumn();

            // Function to color code empty fields in the table
            switch (DataModel.customCellFactory(field)) {
                case "java.lang.String":
                case "StringProperty":
                    TableColumn<DataModel, String> textColumn = new TableColumn<>(field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1));
                    // Die Spalten befüllen
                    textColumn.setCellValueFactory(new PropertyValueFactory<>(field.getName()));
                    // change the color of empty cells and show the distribution for that column as tooltip
                    textColumn.setCellFactory(myColumn -> {

                        return new TextFieldTableCell<DataModel, String>() {
                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    if (item.equals("")) {
                                        setText("");
                                        setStyle("-fx-background-color: lavenderblush");
                                    } else {
                                        setStyle("");
                                        setText(item);
                                    }
                                } else {
                                    setStyle("");
                                    setText(item);
                                } // else
                            } // updateItem
                        }; // return new tableCell
                    }); // column.setCellFacotry

                    // Setzen des Tooltips
                    Callback<TableColumn<DataModel, String>, TableCell<DataModel, String>> existingCellFactory = textColumn.getCellFactory();
                    textColumn.setCellFactory(c -> {
                        TableCell<DataModel, String> cell = existingCellFactory.call(c);
                        cell.setTooltip(new Tooltip((String) textColumn.getUserData()));
                        return cell;
                    });

                    // Funktion, um die editierten Daten in das Datenmodell zu schreiben
                    // TODO -> schreibbefehl
//                    column.setOnEditStart(eve -> {
//                        System.out.println("Started");
//                    });
                    textColumn.setOnEditCommit((CellEditEvent<DataModel, String> t) -> {
                        System.out.println("Edited");
//                            ((DataModel) t.getTableView().getItems().get(
//                                    t.getTablePosition().getRow())).setFirstName(t.getNewValue());
                    });
                    column = textColumn;
                    break;
                case "IntegerProperty":
                    TableColumn<DataModel, Integer> integerColumn = new TableColumn<>(field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1));
                    // Die Spalten befüllen
                    integerColumn.setCellValueFactory(new PropertyValueFactory<>(field.getName()));
                    // change the color of empty cells and show the distribution for that column as tooltip
                    integerColumn.setCellFactory(myColumn -> {

                        return new TextFieldTableCell<DataModel, Integer>(new FormatStringConverter<Integer>(NumberFormat.getInstance())) {
                            @Override
                            public void updateItem(Integer item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    if (Double.isNaN(item)) {
                                        setText("NaN");
                                        setStyle("-fx-background-color: lavenderblush");
                                    } else {
                                        setStyle("");
                                        setText(item.toString());
                                    }
                                } else {
                                    setStyle("");
                                    setText("NaN");
                                } // else
                            } // updateItem
                        }; // return new tableCell
                    }); // column.setCellFacotry

                    // Setzen des Tooltips
                    Callback<TableColumn<DataModel, Integer>, TableCell<DataModel, Integer>> existingIntegerCellFactory = integerColumn.getCellFactory();
                    integerColumn.setCellFactory(c -> {
                        TableCell<DataModel, Integer> cell = existingIntegerCellFactory.call(c);
                        cell.setTooltip(new Tooltip((String) integerColumn.getUserData()));
                        return cell;
                    });

                    // Funktion, um die editierten Daten in das Datenmodell zu schreiben
                    // TODO -> schreibbefehl
//                    column.setOnEditStart(eve -> {
//                        System.out.println("Started");
//                    });
                    integerColumn.setOnEditCommit((CellEditEvent<DataModel, Integer> t) -> {
                        System.out.println("Edited");
//                            ((DataModel) t.getTableView().getItems().get(
//                                    t.getTablePosition().getRow())).setFirstName(t.getNewValue());
                    });
                    column = integerColumn;
                    break;

                case "DoubleProperty":
                    TableColumn<DataModel, Double> doubleColumn = new TableColumn<>(field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1));
                    // Die Spalten befüllen
                    doubleColumn.setCellValueFactory(new PropertyValueFactory<>(field.getName()));
                    // change the color of empty cells and show the distribution for that column as tooltip

                    final String ToolTipText = "";

                    doubleColumn.setCellFactory(myColumn -> {

                        //TextFieldTableCell<Trade, Number>(new FormatStringConverter<String>(df))
                        return new TextFieldTableCell<DataModel, Double>(new FormatStringConverter<Double>(NumberFormat.getInstance())) {
                            @Override
                            public void updateItem(Double item, boolean empty) {
                                //super.setConverter(new StringConverter(new FormatStringConverter<Double>(df)));
                                super.updateItem(item, empty);
                                if (item != null) {
                                    if (Double.isNaN(item)) {
                                        setText("NaN");
                                        setStyle("-fx-background-color: lavenderblush");
                                    } else {
                                        setStyle("");
                                        setText(item.toString());
                                    }
                                } else {
                                    setStyle("");
                                    setText("NaN");
                                } // else
                            } // updateItem

                        }; // return new tableCell
                    }); // column.setCellFacotry

                    // Setzen des Tooltips
                    Callback<TableColumn<DataModel, Double>, TableCell<DataModel, Double>> existingDoubleCellFactory = doubleColumn.getCellFactory();
                    doubleColumn.setCellFactory(c -> {
                        TableCell<DataModel, Double> cell = existingDoubleCellFactory.call(c);
                        cell.setTooltip(new Tooltip((String) doubleColumn.getUserData()));
                        return cell;
                    });

                    // Funktion, um die editierten Daten in das Datenmodell zu schreiben
                    // TODO -> schreibbefehl
//                    column.setOnEditStart(eve -> {
//                        System.out.println("Started");
//                    });
                    doubleColumn.setOnEditCommit((CellEditEvent<DataModel, Double> t) -> {
                        System.out.println("Edited");
//                            ((DataModel) t.getTableView().getItems().get(
//                                    t.getTablePosition().getRow())).setFirstName(t.getNewValue());
                    });
                    column = doubleColumn;
                    break;
                case "ObjectProperty":
                    TableColumn<DataModel, Object> objectColumn = new TableColumn<>(field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1));
                    // Die Spalten befüllen
                    objectColumn.setCellValueFactory(new PropertyValueFactory<>(field.getName()));
                    // change the color of empty cells and show the distribution for that column as tooltip
                    objectColumn.setCellFactory(myColumn -> {

                        return new TextFieldTableCell<DataModel, Object>() {
                            @Override
                            public void updateItem(Object item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setStyle("");
                                    setText(item.toString());
                                } else {
                                    setStyle("-fx-background-color: lavenderblush");
                                    setText("NaN");
                                } // else
                            } // updateItem
                        }; // return new tableCell
                    }); // column.setCellFacotry

                    // Setzen des Tooltips                    
                    Callback<TableColumn<DataModel, Object>, TableCell<DataModel, Object>> existingObjectCellFactory = objectColumn.getCellFactory();
                    objectColumn.setCellFactory(c -> {
                        TableCell<DataModel, Object> cell = existingObjectCellFactory.call(c);
                        cell.setTooltip(new Tooltip((String) objectColumn.getUserData()));
                        return cell;
                    });

                    // Funktion, um die editierten Daten in das Datenmodell zu schreiben
                    // TODO -> schreibbefehl
//                    column.setOnEditStart(eve -> {
//                        System.out.println("Started");
//                    });
                    objectColumn.setOnEditCommit((CellEditEvent<DataModel, Object> t) -> {
                        System.out.println("Edited");
//                            ((DataModel) t.getTableView().getItems().get(
//                                    t.getTablePosition().getRow())).setFirstName(t.getNewValue());
                    });
                    column = objectColumn;
                    break;

                case "BooleanProperty":
                    TableColumn<DataModel, Boolean> booleanColumn = new TableColumn<>(field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1));
                    // Die Spalten befüllen
                    booleanColumn.setCellValueFactory(new PropertyValueFactory<>(field.getName()));
                    // change the color of empty cells and show the distribution for that column as tooltip
                    booleanColumn.setCellFactory(myColumn -> {

                        return new TextFieldTableCell<DataModel, Boolean>() {
                            @Override
                            public void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setStyle("");
                                    setText(item.toString());
                                } else {
                                    setStyle("-fx-background-color: lavenderblush");
                                    setText("");
                                } // else
                            } // updateItem
                        }; // return new tableCell
                    }); // column.setCellFacotry

                    // Setzen des Tooltips
                    Callback<TableColumn<DataModel, Boolean>, TableCell<DataModel, Boolean>> existingBooleanCellFactory = booleanColumn.getCellFactory();
                    booleanColumn.setCellFactory(c -> {
                        TableCell<DataModel, Boolean> cell = existingBooleanCellFactory.call(c);
                        cell.setTooltip(new Tooltip((String) booleanColumn.getUserData()));
                        return cell;
                    });

                    // Funktion, um die editierten Daten in das Datenmodell zu schreiben
                    // TODO -> schreibbefehl
//                    column.setOnEditStart(eve -> {
//                        System.out.println("Started");
//                    });
                    booleanColumn.setOnEditCommit((CellEditEvent<DataModel, Boolean> t) -> {
                        System.out.println("Edited");
//                            ((DataModel) t.getTableView().getItems().get(
//                                    t.getTablePosition().getRow())).setFirstName(t.getNewValue());
                    });
                    column = booleanColumn;
                    break;

                default:
                    System.out.println("Unknown Datatype -> column in table could not be created. Field: " + field.toString());
                    break;

            } // switch columnType

            table.getColumns()
                    .add(column);

            // Das TreeItem für das aktuelle Attribut generieren
            CheckBoxTreeItem branch = new CheckBoxTreeItem(field.getName());
            column.visibleProperty().bindBidirectional(branch.selectedProperty()); // Die visibility der Tabellenspalte an den check-Status binden

            branch.setSelected(true); // auf gecheckt -> "visible in der table" setzen
            branch.setIndependent(true);
//            branch.selectedProperty().addListener((change, oldValue, newValue) -> {
////                    System.out.println("newValeu: " + newValue);
//                    branch.getChildren().forEach((child) -> {
//                        ((CheckBoxTreeItem) child).setSelected(true);
//                    });
//                    VA_topos.observableData.forEach((datum) -> {
//                        //datum.setDaysPast((newValue) ? datum.getDaysPast() + 1 : datum.getDaysPast() - 1);
//                    });
//            });
            filterTreeViewRoot.getChildren()
                    .add(branch);
        }

        tableFilterTreeView.setPrefWidth(300);
        tableFilterTreeView.setMinWidth(300);
        tableFilterTreeView.setMaxWidth(300);
        setAnchor(tableFilterTreeView, 0d, 0d, 0d, 0d);

        // scrolltableFilterTreeView ist ein container mit den Meta und annodaten, um Scrollbalken zuzulassen
        scrolltableFilterTreeView = new ScrollPane();
        setAnchor(scrolltableFilterTreeView, 0d, 0d, 0d, 0d);
        scrolltableFilterTreeView.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrolltableFilterTreeView.setContent(tableFilterTreeView);
        AnchorPane scrollContainer = new AnchorPane();
        scrollContainer.getChildren().add(scrolltableFilterTreeView);
        tableFilterTreeView.prefHeightProperty().bind(tableAnchorPane.heightProperty().subtract(patientTreeView.heightProperty().add(2)));
        scrolltableFilterTreeView.prefHeightProperty().bind(tableAnchorPane.heightProperty().subtract(patientTreeView.heightProperty()));
        scrollContainer.prefHeightProperty().bind(tableAnchorPane.heightProperty().subtract(patientTreeView.heightProperty()));

        VA_topos.sortedFilteredData.comparatorProperty().bind(table.comparatorProperty());

        table.setRowFactory(
                new Callback<TableView<DataModel>, TableRow<DataModel>>() {

            @Override
            public TableRow<DataModel> call(TableView<DataModel> tableView
            ) {
                final TableRow<DataModel> row = new TableRow<>();
                final ContextMenu rowMenu = new ContextMenu();

                // TODO: This block has to be recoded.
                // 1. Create an annotation, when a row added
                // 2. Create an annotation, when a row is changed
                // 3. Create an annotation, when a row is deleted
                // Create the respective design/visual coding for the table rows for all items above
                // 4. Forschungsfrage: How can the user interaction need be combined with smart, reusable, visualizable annotation? 
                // Menuitem für das Erzeugen eines neuen Eintrages in der Tabelle
                MenuItem newItem = new MenuItem("Neuen Eintrag erzeugen");
                newItem.setOnAction((ActionEvent ae) -> {
                    DataModel newElement = new DataModel();
                    newElement.copyFromOther(row.getItem());
                    TextInputDialog myDiag = new TextInputDialog();
                    VA_topos.observableData.add(newElement);

                });

                // Menuitem für das Editieren eines neuen Eintrages in der Tabelle
                MenuItem editItem = new MenuItem("Editieren");
                editItem.setOnAction((ActionEvent ae) -> {
                    System.out.println(row.getItem().getDatenquelle());
                });

                //ObjectProperty<Background> bgProp = new SimpleObjectProperty(new Background(new BackgroundFill(Color.BLUE, CornerRadii.EMPTY, Insets.EMPTY)));
                MenuItem removeItem = new MenuItem("Deaktivieren");
                // Action performed, when a row in the table shall be deleted
                removeItem.setOnAction((ActionEvent event) -> {
                    Background delBack = new Background(new BackgroundFill(Color.rgb(130, 130, 130, 1), CornerRadii.EMPTY, Insets.EMPTY));
                    //row.backgroundProperty().bind(row.getItem().backGroundProperty());
                    row.getItem().setBackGround(delBack);
//                            VA_topos.observableData.remove(row.getItem().getLfdNr());
                    String myRow = row.getItem().getPatientenID() + "_" + row.getItem().getUntersuchungLateralitaet();
//                    AnnotationModel modd = (AnnotationModel) VA_topos.groupData.get().getObservableAnnotations().get(myRow);
                    ObservableMap<String, Object> content = FXCollections.observableHashMap();
                    content.put("Deaktiviertes Element: " + row.getItem().getAugenID(), row.getItem());
                    Annotations myAnno = new Annotations(VA_topos.USER_NAME.get(), LocalDate.now(), content);
//                    modd.getMyAnnotations().put("Deaktiviertes Element: " + row.getItem().getAugenID(), myAnno);
                    System.out.println("Deaktiviert.");
                });
                rowMenu.getItems().addAll(newItem, editItem, removeItem);

                // only display context menu for non-null items:
                row.contextMenuProperty().bind(
                        Bindings.when(Bindings.isNotNull(row.itemProperty()))
                                .then(rowMenu)
                                .otherwise((ContextMenu) null));
                return row;
            }
        }
        ); // setRowFactory

        table.setItems(VA_topos.sortedFilteredData);

        setAnchor(patientTreeView,
                0d, 0d, 0d, 0d);
        patientTreeView.setRoot(
                new CheckBoxTreeItem("Patients"));
        patientTreeView.setCellFactory(CheckBoxTreeCell.<String>forTreeView());

        // Divide the left side of the screen into two parts (top -> attributes and distribution, bottom : patients)
        SplitPane tableFilterPatientSplitPane = new SplitPane();

        setAnchor(tableFilterPatientSplitPane, 0d, 0d, 0d, 0d);
        tableFilterPatientSplitPane.setDividerPosition(0, 0.5);
        tableFilterPatientSplitPane.setOrientation(Orientation.VERTICAL);

        tableFilterPatientSplitPane.getItems()
                .addAll(scrollContainer, patientTreeView);

        // Ein AnchorPanel im TableViewTab definieren, um darin die Elemente anzuordnen
        tableAnchorPane.getChildren().addAll(tableFilterPatientSplitPane, table);
        myDatenTableTab.setContent(tableAnchorPane);

    }

}
