/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.ui;

//import impl.org.controlsfx.*;
import javafx.scene.paint.Color;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import va_topos.VA_topos;
import static va_topos.color.CustomColors.RGB_STDNODECOLOR;

/**
 *
 * @author Christoph Schmidt
 */
@SuppressWarnings("unchecked")
public class MainWindowBuilder {

    // Different Style Types for a node
    public enum NodeStyles {
        TRANSPARENT,
        ANNOTHUMBNAIL,
        ANNOTILEPANE,
        SCROLLTILEPANE,
        SOLID,
        DYNAMIC_BACKCOLOR,
        INVERTED_DYNAMIC_BACKCOLOR,
        LEGEND_STYLE;
    }

//    private static final int[] RGB_STDNODECOLOR = {90, 90, 90};
//    public static final int[] RGB_STDNODECOLOR = {60, 190, 250};
    private static final double STDOPACITY = 0.75;
    private static final double STDCHILDRENOPACITY = 0.75;
    private static final double STDCORNERRADII = 5d;
    private static final double STDINSET = 5d;

    public enum DIALOGPANEL {
        SAMPLEPANEL,
        CLEANSINGPANEL,
        ANNOPANEL
    }

// General 
    public static AnchorPane mainWindow;
    public static TabPane myTabPane;

//    // Detail Pane for the doctoral Letters
//    public static Stage secondaryStage = new Stage();
//    public static TabPane letterTabPane = new TabPane();
//
//    // Pane for the annotations (in an extra window)
//    public static Stage annotationStage = new Stage();
//    public static ScrollPane scrollTilePane;
//    public static TilePane annoTilePane;
//    public static AnchorPane annoTileAnchorPane;
    // Data Load Panel
    //public static Button loadBtn;
    //public static Button calculateBtn;
    //public static Button saveBtn;
    public static CheckBox stdVisCheckBox;
    public static CheckBox visFirstCheckBox;
    public static MenuBar ladeTabMenu;

    // DialogPanel
    // DialogPanel -> General
    public static AnchorPane paramAnchorPane = new AnchorPane();
    public static Button paramCloseButton = new Button();
    // DialogPanel -> AnnoPanel
    public static Label annUserLabel = new Label();
    public static Label annoTextAreaLabel = new Label();
    public static TextArea annoText = new TextArea();
    // DialogPanel -> Cleansingpanel
    public static Label cleansingUserLabel = new Label();
    public static Label cleansingUser = new Label();
    public static Label cleansingCurrentDateLabel = new Label();
    public static Label cleansingCurrentDate = new Label();
    public static Label cleansingPatientLabel = new Label();
    public static TextField cleansingPatient = new TextField();
    public static Label cleansingLateralityLabel = new Label();
    public static Label cleansingLaterality = new Label();
    public static Label cleansingDateFieldLabel = new Label();
    public static DatePicker cleansingDateField = new DatePicker();
    public static Label cleansingDataAttributFieldLabel = new Label();
    public static ComboBox cleansingDataAttributField = new ComboBox();
    public static Label cleansingDataTypeFieldLabel = new Label();
    public static ComboBox cleansingDataTypeField = new ComboBox();
    public static Label cleansingValueFieldLabel = new Label();
    public static TextField cleansingValueField = new TextField();

    // DialogPanel -> SamplePanel
    public static Label sampleCountLabel = new Label();
    public static Slider sampleCountSlider = new Slider();
    public static TextField sampleCountVal = new TextField();
    public static Spinner<Double> sampleCountSpinner = new Spinner();
    public static Label samplePercentLabel = new Label();
    public static Slider samplePercentSlider = new Slider();
    public static TextField samplePercentVal = new TextField();
    public static Spinner<Double> samplePercentSpinner = new Spinner();

    // TableViewPanel
    public static AnchorPane tableAnchorPane = new AnchorPane();
    public static Button clearFilterButton = new Button();
    public static TextField filterTextField = new TextField();
    public static TableView table = new TableView();
    public static ScrollPane scrolltableFilterTreeView;
    public static TreeView tableFilterTreeView = new TreeView();
    public static TreeView patientTreeView = new TreeView();

    // StandardVisPanel
    public static AnchorPane stdVisAnchorPane;
    public static AnchorPane controlStdVisAnchorPane;

    // VisusFirstPanel
    public static DotGrid visusFirstDrawObjectAnchorPane;
    public static DotGrid visusFirstAnnotationAnchorPane;
    public static SplitPane orderVisusFirstTab;
    public static AnchorPane orderVisusFirstVis;
    public static AnchorPane controlVisusFirstAnchorPane;
    public static AnchorPane detailAnchorPane;
    public static AnchorPane summaryAnchorPane;
    public static AnchorPane legendAnchorPane;
    public static Canvas summaryVisusCanvas;

    // VisusFirstPanel - ControlPanel
    public static Button restoreButton = new Button("Originalpositionierung wieder herstellen");

    public static Rectangle legendScaleRectangle = new Rectangle();
    public static Label legendMinValueLabel = new Label("min");
    public static Label legendMaxValueLabel = new Label("max");
    public static Label legendMidValueLabel = new Label("mid");

    public static CheckBox summaryVisShowCheckBox = new CheckBox("Zeige Aggregationsfenster");
    public static CheckBox detailVisShowCheckBox = new CheckBox("Zeige Detailfenster");
    public static CheckBox legendShowCheckBox = new CheckBox("Zeige Zeitachseninformation");
    public static CheckBox regressionShowCheckBox = new CheckBox("Zeige Regressionskurve");
    public static CheckBox suddenIncidentShowCheckBox = new CheckBox("Zeige Phako Ereignisse aus argos_tl");
    public static CheckBox showMedChangeCheckBox = new CheckBox("Zeige Visustendenz nach Medikamentenwechsel"); // Idee -> statt nächstem Visus, die Regression nach dem nächsten Visus bis zum nächsten Wechsel

    public static Label scaleDescriptionLabel = new Label();
    public static ComboBox scaleComboBox = new ComboBox();
    //public static ToggleGroup scaleToggle = new ToggleGroup();
    //public static RadioButton linearRadio = new RadioButton();
    //public static RadioButton logarithmicRadio = new RadioButton();
    //public static RadioButton diverging3Radio = new RadioButton();

    //public static ToggleGroup visusDesignToggle = new ToggleGroup();
    //public static RadioButton horizontalRadio = new RadioButton();
    //public static RadioButton verticalRadio = new RadioButton();
    //public static RadioButton circleRadio = new RadioButton();
    public static Label visusDimensionDescriptionLabel = new Label();
    public static ComboBox visusDimensionComboBox = new ComboBox();

//    public static ToggleGroup visusToggle = new ToggleGroup();
//    public static RadioButton visusKlasseRadio = new RadioButton();
//    public static RadioButton dezVisusRadio = new RadioButton();
//    public static RadioButton logMarVisusRadio = new RadioButton();
//    public static RadioButton deltaVisusRadio = new RadioButton();
//    public static RadioButton deltaLogMarVisusRadio = new RadioButton();
//    public static RadioButton deltaVisusClassRadio = new RadioButton();
    public static Label scenarioDescriptionLabel = new Label();
    public static ComboBox scenarioComboBox = new ComboBox();

    public static ToggleGroup normStartToggle = new ToggleGroup();
    public static RadioButton normBegin = new RadioButton();
    public static RadioButton timeBeginn = new RadioButton();

    public static ToggleGroup colorToggle = new ToggleGroup();
    public static RadioButton greyRadio = new RadioButton();
    public static RadioButton colorRadio = new RadioButton();

    public static Label divSlidLabel = new Label();
    public static Slider dividendSlider = new Slider();
    public static TextField divSlidVal = new TextField();

    public static Label backColSlidLabel = new Label();
    public static Slider backColorSlider = new Slider();
    public static TextField backColSlidVal = new TextField();
    //public static ObjectProperty<Paint> backColPaintObject = new SimpleObjectProperty();
    public static ObjectProperty<Paint> backColPaintObject = new SimpleObjectProperty<>(Color.DARKGRAY);
    public static IntegerProperty legendLabelYearOffset = new SimpleIntegerProperty(0);

    public static Label saturationSlidLabel = new Label();
    public static Slider saturationSlider = new Slider();
    public static TextField saturationSlidVal = new TextField();

    public static Label brightnessSlidLabel = new Label();
    public static Slider brightnessSlider = new Slider();
    public static TextField brightnessSlidVal = new TextField();

    public static Label borSlidLabel = new Label();
    public static Slider borderWidthSlider = new Slider();
    public static TextField borSlidVal = new TextField();

    public static Label zoomSlidLabel = new Label();
    public static Slider zoomSlider = new Slider();
    public static TextField zoomSlidVal = new TextField();

    public static Label groupingLabel = new Label();
    public static ComboBox groupingComboBox = new ComboBox();
    public static Label groupingClassCountLabel = new Label();
    public static Spinner groupingSpinner = new Spinner();

    public static ToggleGroup groupingToggle = new ToggleGroup();
    public static RadioButton eyeRadio = new RadioButton();
    public static RadioButton patientRadio = new RadioButton();

    public static Label sortingDescriptionLabel = new Label();
    public static ComboBox sortingComboBox = new ComboBox();

//    public static ToggleGroup sortingToggle = new ToggleGroup();
//    public static RadioButton lengthOfTreatmentRadio = new RadioButton();
//    public static RadioButton numberOfInjectionsRadio = new RadioButton();
//    public static RadioButton numberOfMedicationRadio = new RadioButton();
//    public static RadioButton suddenIncidentRadio = new RadioButton();
//    public static RadioButton visusImprovementRadio = new RadioButton();
//    public static RadioButton visusImprovementAfterMedicationSwitchRadio = new RadioButton();
//    public static RadioButton stableTimeRadio = new RadioButton();
    // Anno-Steuerungspanel
    public static Button annoNewButton = new Button();
    public static Button annoDeleteButton = new Button();
    public static Button annoFwdButton = new Button();
    public static Button annoBackButton = new Button();
    public static Button annoRedoButton = new Button();
    public static Button annoUndoButton = new Button();

    // DetailAnchorPane Panel
    public static Button detailCloseButton = new Button("x");
    public static Label spareOne = new Label();
    public static Label spareTwo = new Label();
    public static Label spareThree = new Label();
    public static Label spareFour = new Label();
    public static Label spareFive = new Label();
    public static Label spareSix = new Label();
    public static Label spareSeven = new Label();
    public static Label spareEight = new Label();
    public static Label spareNine = new Label();

    // SummaryAnchorPane
    public static Button summaryCloseButton = new Button("x");

    // SpritzenFirstPanel
    public static DotGrid visPanel = new DotGrid(1);
    public static List<Label> yearLegendLabel = new ArrayList();

    // ICA Panel
    public static StackPane identifySimilarStackPane;

    public Paint getBackColPaintObject() {
        return backColPaintObject.get();
    }

    public void setBackColPaintObject(Paint value) {
        backColPaintObject.set(value);
    }

    public ObjectProperty backColPaintObjectProperty() {
        return backColPaintObject;
    }

    @SuppressWarnings("unchecked")
    public static Scene makeMainWindow() {

        SubWindowsBuilder.makeSubWindows();

// Definition TabPane allgemein
        myTabPane = new TabPane();
        myTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        myTabPane.setBackground(setStyle(myTabPane, NodeStyles.SOLID));

// Das GesamtPanel --> BEGINN
// Bildschirmaufbau:
        // Grundsätzlich mehrere Tabs
        // Erstes Tab --> Datenladen
        // Zweites Tab --> Tabellarische anzeige
        // Drittes Tab --> Standardvisualisierung
        // Viertes Tab --> Visus First Visualisierung
        // Fünftes Tab --> Alternative Visualisierung (Neuanfang November 2018)
        // Sechstes Tab --> Independent Component Analysis -> Finden ähnlicher Patienten
        mainWindow = new AnchorPane();
        setAnchor(mainWindow, 0d, 0d, 0d, 0d);
        mainWindow.getChildren().add(myTabPane);

// Das GesamtPanel --> ENDE
//
// Für das Ladetab --> BEGINN
        // Das LadeTab definieren und die anderen Elemente hinzufügen
        Tab myLadeTab = new Tab();
        myLadeTab.setText("Daten Laden");
        LadeTabBuilder lb = new LadeTabBuilder(myLadeTab);

// Für das LadeTab --> ENDE
//
// Für das TableViewTab --> BEGINN
        Tab myDatenTableTab = new Tab();
        myDatenTableTab.setText("Daten Aufbereiten");
        TableTabBuilder tb = new TableTabBuilder(myDatenTableTab);
// Für das TableViewTab --> ENDE      
//
// Für das StandardVisTab --> BEGINN
        // Das Tab erzeugen
        Tab myStandardVisTab = new Tab();
        myStandardVisTab.setText("Standardvisualisierungen der Daten");
        StandardVisTabBuilder sb = new StandardVisTabBuilder(myStandardVisTab);
// Für das StandardVisTab --> ENDE  
//
// Für das VisusFirstTab --> BEGINN
        Tab myVisusFirstTab = new Tab();
        myVisusFirstTab.setText("Visualisierung der Patientendaten");
        VisusFirstTabBuilder vb = new VisusFirstTabBuilder(myVisusFirstTab);
// Für das VisusFirstTab --> ENDE
//
// Für das InjectionFirstTab --> BEGINN
        Tab mySpritzenFirstTab = new Tab();
        mySpritzenFirstTab.setText("Alternative Visualisierung der Patientendaten");
        SpritzenFirstTabBuilder ifb = new SpritzenFirstTabBuilder(mySpritzenFirstTab);
// Für das InjectionFirstTab --> ENDE
//
// Für das ICATab --> BEGINN
        Tab myICATab = new Tab();
        myICATab.setText("Identifikation ähnlicher Patienten");
        ICATabBuilder ib = new ICATabBuilder(myICATab);
// Für das ICA --> ENDE
//
// TabPane allgemein --> BEGINN
        myTabPane.getTabs().addAll(
                myLadeTab,
                myDatenTableTab,
                myStandardVisTab,
                myVisusFirstTab,
                //mySpritzenFirstTab,
                myICATab
        );
        setAnchor(myTabPane, 0d, 0d, 0d, 0d);
// TabPane allgemein --> ENDE
        AnchorPane root = new AnchorPane();
        root.getChildren().add(mainWindow);

        Scene scene = new Scene(root, 1600, 1000);
        // add Cascading StyleSheet to application
        String css = VA_topos.class.getResource("toposCSS.css").toExternalForm();
        scene.getStylesheets().add(css);
        return scene;
    }

    public static class DotGrid extends Pane {

        private final List<Canvas> canvas = new ArrayList();

        public DotGrid(double width, double height) {
            this.canvas.add(new Canvas(width, height));
            this.getChildren().setAll(canvas);
        }

        public DotGrid(int canvasZahl) {
            for (int i = 1; i <= canvasZahl; i++) {
                addCanvas(new Canvas());
            }
        }

        public void deleteCanvas(int index) {
            canvas.remove(index);
            this.getChildren().setAll(canvas);
            IntegerProperty canvasNum = new SimpleIntegerProperty(this.getChildren().size() - 1);
            VA_topos.maxAnnoIndex.bind(canvasNum);
        }

        public void addCanvas() {
            addCanvas(new Canvas());
        }

        public void addCanvas(Canvas can) {
            canvas.add(VA_topos.currentAnnoIndex.get(), can);
            this.getChildren().setAll(canvas);
            IntegerProperty canvasNum = new SimpleIntegerProperty(this.getChildren().size() - 1);
            VA_topos.maxAnnoIndex.bind(canvasNum);
            //System.out.println(canvasNum + " Canvas: " + canvas);
            //can.requestFocus();
            //System.out.println("can: " + can + " Fokus: " + can.focusedProperty().get());

        }

        @Override
        protected void layoutChildren() {
            final int top = (int) snappedTopInset();
            final int right = (int) snappedRightInset();
            final int bottom = (int) snappedBottomInset();
            final int left = (int) snappedLeftInset();
            final int w = (int) getWidth() - left - right;
            final int h = (int) getHeight() - top - bottom;
            canvas.forEach(can -> {
                can.setLayoutX(left);
                can.setLayoutY(top);
                if (w != can.getWidth() || h != can.getHeight()) {
                    can.setWidth(w);
                    can.setHeight(h);
                }
            });
        }
    }

    // Sets a predefined Style for a node
    public static Background setStyle(Node myPane, NodeStyles myType) {
        return setStyle(myPane, STDCHILDRENOPACITY, myType, RGB_STDNODECOLOR[0], RGB_STDNODECOLOR[1], RGB_STDNODECOLOR[2], STDOPACITY, Color.BLACK);
    }

    public static Background setStyle(Node myPane, NodeStyles myType, Color backCol, Color borCol) {
        if (backCol == null) {
            backCol = Color.rgb(RGB_STDNODECOLOR[0], RGB_STDNODECOLOR[1], RGB_STDNODECOLOR[2], STDOPACITY);
        }
        return setStyle(myPane, STDCHILDRENOPACITY, myType, (int) Math.round(backCol.getRed() * 255d), (int) Math.round(backCol.getGreen() * 255d), (int) Math.round(backCol.getBlue() * 255d), (int) Math.round(backCol.getOpacity()), borCol);
    }

    public static Background setStyle(Node myPane, double paneOpacity, NodeStyles myType, int red, int green, int blue, double opacity, Color borderCol) {
        switch (myType) {

            case LEGEND_STYLE:
                if (myPane instanceof AnchorPane) {
                    ((AnchorPane) myPane).setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT, Insets.EMPTY)));
                    return new Background((new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
                }
                return null;
            // dynamische Hintergrundhelligkeit, keine abgerundeten Ecken
            case DYNAMIC_BACKCOLOR:
                return new Background((new BackgroundFill(Color.rgb((int) backColorSlider.getValue(), (int) backColorSlider.getValue(), (int) backColorSlider.getValue()), CornerRadii.EMPTY, Insets.EMPTY)));

            case INVERTED_DYNAMIC_BACKCOLOR:
                if (myPane instanceof Label) {
                    return new Background((new BackgroundFill(Color.rgb((int) (255 - backColorSlider.getValue()), (int) (255 - backColorSlider.getValue()), (int) (255 - backColorSlider.getValue())), CornerRadii.EMPTY, Insets.EMPTY)));
                }
                return new Background((new BackgroundFill(Color.rgb((int) backColorSlider.getValue(), (int) backColorSlider.getValue(), (int) backColorSlider.getValue()), CornerRadii.EMPTY, Insets.EMPTY)));
            // weißer Hintergrund, keine abgerundeten Ecken
            case SOLID:
                return new Background((new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));

            case ANNOTILEPANE:
                myPane.setOpacity(paneOpacity);
                if (myPane instanceof TilePane) {
                    ((TilePane) myPane).setPrefWidth(320);

                    ((TilePane) myPane).setPrefTileWidth(310);
                    ((TilePane) myPane).setPrefTileHeight(200);
                    ((TilePane) myPane).setTileAlignment(Pos.CENTER);
                    ((TilePane) myPane).setVgap(10);
                    ((TilePane) myPane).setPrefColumns(1);
                    ((TilePane) myPane).setLayoutX(0);
                    ((TilePane) myPane).setLayoutY(0);
                    ((TilePane) myPane).relocate(0, 0);

//                    ((TilePane) myPane).setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(STDCORNERRADII), BorderWidths.DEFAULT, Insets.EMPTY)));
                }
                return new Background(new BackgroundFill(Color.rgb(red, green, blue, 0), new CornerRadii(STDCORNERRADII), new Insets(STDINSET)));
            case SCROLLTILEPANE:
                myPane.setOpacity(1);
                if (myPane instanceof ScrollPane) {
                    String styleText = "-fx-background: rgba(255,255,255,1);";
                    styleText = "-fx-background: transparent;";
//                    ((ScrollPane) myPane).setStyle(styleText);
                    ((ScrollPane) myPane).setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
                    ((ScrollPane) myPane).setPrefWidth(350);
                    ((ScrollPane) myPane).setBorder(new Border(new BorderStroke(borderCol, BorderStrokeStyle.SOLID, new CornerRadii(STDCORNERRADII), BorderWidths.DEFAULT, new Insets(STDINSET))));
                }
//                return new Background((new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
                return new Background(new BackgroundFill(Color.rgb(red, green, blue, opacity), new CornerRadii(STDCORNERRADII), new Insets(STDINSET)));

            case ANNOTHUMBNAIL:
                myPane.setOpacity(1);
                ((DotGrid) myPane).setBorder(new Border(new BorderStroke(borderCol, BorderStrokeStyle.SOLID, new CornerRadii(STDCORNERRADII), BorderWidths.DEFAULT, new Insets(STDINSET))));
                return new Background(new BackgroundFill(Color.rgb(red, green, blue, opacity), new CornerRadii(STDCORNERRADII), new Insets(STDINSET)));

            case TRANSPARENT:
                myPane.setOpacity(paneOpacity);
                try {
                    String[] myClasses = {"StackPane", "AnchorPane", "HBox", "VBox", "DotGrid"};
                    String key = "";
                    for (String myClass : myClasses) {
                        if (myPane.getClass().toString().contains(myClass)) {
                            key = myClass;
                        }
                    }
                    switch (key) {
                        case "AnchorPane":
                            ((AnchorPane) myPane).setBorder(new Border(new BorderStroke(borderCol, BorderStrokeStyle.SOLID, new CornerRadii(STDCORNERRADII), BorderWidths.DEFAULT, new Insets(STDINSET))));
                        case "DotGrid":
                            ((DotGrid) myPane).setBorder(new Border(new BorderStroke(borderCol, BorderStrokeStyle.SOLID, new CornerRadii(STDCORNERRADII), BorderWidths.DEFAULT, new Insets(STDINSET))));
                        case "HBox":
                            ((HBox) myPane).setBorder(new Border(new BorderStroke(borderCol, BorderStrokeStyle.SOLID, new CornerRadii(STDCORNERRADII), BorderWidths.DEFAULT, new Insets(STDINSET))));
                        case "VBox":
                            ((VBox) myPane).setBorder(new Border(new BorderStroke(borderCol, BorderStrokeStyle.SOLID, new CornerRadii(STDCORNERRADII), BorderWidths.DEFAULT, new Insets(STDINSET))));
                        case "StackPane":
                            ((StackPane) myPane).setBorder(new Border(new BorderStroke(borderCol, BorderStrokeStyle.SOLID, new CornerRadii(STDCORNERRADII), BorderWidths.DEFAULT, new Insets(STDINSET))));
                    }
                } catch (Exception e) {
                }
                //System.out.println(myPane.getClass().toString());

                return new Background(new BackgroundFill(Color.rgb(red, green, blue, opacity), new CornerRadii(STDCORNERRADII), new Insets(STDINSET)));
        }
        return null;
    }
    // Sets the Anchor of a Pane

    public static void setAnchor(Node myPane, double left, double right, double top, double bottom) {
        if (!Double.isNaN(left)) {
            AnchorPane.setLeftAnchor(myPane, left);
        }
        if (!Double.isNaN(right)) {
            AnchorPane.setRightAnchor(myPane, right);
        }
        if (!Double.isNaN(top)) {
            AnchorPane.setTopAnchor(myPane, top);
        }
        if (!Double.isNaN(bottom)) {
            AnchorPane.setBottomAnchor(myPane, bottom);
        }
    }

    public static void movePane(Node myPane, double deltaX, double deltaY) {
        myPane.setOnMouseDragged(e -> {
            myPane.setTranslateX(myPane.getTranslateX() + e.getX() - deltaX);
            myPane.setTranslateY(myPane.getTranslateY() + e.getY() - deltaY);
        });
    }

}
