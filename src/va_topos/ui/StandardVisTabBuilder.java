/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.ui;

import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import static va_topos.ui.MainWindowBuilder.controlStdVisAnchorPane;
import static va_topos.ui.MainWindowBuilder.setAnchor;
import static va_topos.ui.MainWindowBuilder.setStyle;
import static va_topos.ui.MainWindowBuilder.stdVisAnchorPane;
import static va_topos.ui.MainWindowBuilder.stdVisCheckBox;

/**
 *
 * @author lokal-admin
 */
public class StandardVisTabBuilder {

    public StandardVisTabBuilder(Tab myStandardVisTab) {
            myStandardVisTab.disableProperty().bind(stdVisCheckBox.selectedProperty().not());
        // Das Standardvis-AnchorPane erzeugen, in welchem die Vis angezeigt werden.
        stdVisAnchorPane = new AnchorPane();
        stdVisAnchorPane.setBackground(setStyle(stdVisAnchorPane, MainWindowBuilder.NodeStyles.SOLID));

        setAnchor(stdVisAnchorPane, 0d, 0d, 0d, 0d);

        // Das Steuerungspanel erzeugen, in welchem die Elemente für die Steuerung der Visualisierungen enthalten sind
        controlStdVisAnchorPane = new AnchorPane();
        setAnchor(controlStdVisAnchorPane, 0d, 0d, 0d, 0d);
        controlStdVisAnchorPane.setMinWidth(150);
        controlStdVisAnchorPane.setMaxWidth(300);

        // Ein Splitpanel erzeugen, welches das Tab in zwei Bereiche unterteilt --> den Steuerungsteil und den Vis-Teil
        SplitPane orderStdVisTab = new SplitPane();
        setAnchor(orderStdVisTab, 0d, 0d, 0d, 0d);
        orderStdVisTab.getItems().addAll(controlStdVisAnchorPane, stdVisAnchorPane);
        orderStdVisTab.setDividerPositions(0.3d);

        // Das StandardvisAnchorPane wirde genutzt, um die zur Laufzeit erzeugeten Steuerelemente zu platzieren
        myStandardVisTab.setContent(orderStdVisTab);

    
    
    
    }
    
}
