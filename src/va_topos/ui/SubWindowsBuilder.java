/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.ui;

import com.sun.glass.ui.Screen;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import static va_topos.ui.MainWindowBuilder.paramAnchorPane;
import static va_topos.ui.MainWindowBuilder.setAnchor;

/**
 *
 * @author lokal-admin
 */
public class SubWindowsBuilder {

    // Detail Pane for the doctoral Letters
    public static Stage doctoralLetterStage = new Stage();
    public static TabPane letterTabPane = new TabPane();

    // Panel to parameterize the patient sampling
    public static Stage dialogPanelStage = new Stage();

    // Pane for the annotations (in an extra window)
    public static Stage annotationStage = new Stage();
    public static ScrollPane scrollTilePane;
    public static TilePane annoTilePane;
    public static AnchorPane annoTileAnchorPane;
    public static AnchorPane annoControlAnchorPane;

    public static void makeSubWindows() {

        // design the letterWindow for detailled information
        doctoralLetterStage.setTitle("Arztbriefe");
        AnchorPane detailsAnchorPane = new AnchorPane();
        letterTabPane = new TabPane();
        setAnchor(letterTabPane, 0, 0, 0, 0);
        detailsAnchorPane.getChildren().add(letterTabPane);
        Scene detailLetterScene = new Scene(detailsAnchorPane, 500, Screen.getMainScreen().getHeight() - 140);

        doctoralLetterStage.setScene(detailLetterScene);
        if (Screen.getScreens().size() > 1) {
            doctoralLetterStage.setX(0);
            doctoralLetterStage.setY(10);
        } else {
            doctoralLetterStage.setX(Screen.getMainScreen().getWidth() - 500);
            doctoralLetterStage.setY(10);
        }

        // design the sampleParamsWindow to allow the paramterization of the samples to select from a folder
        dialogPanelStage.setTitle("Parameter für das Patient Sampling");
        Scene sampleParameterScene = new Scene(paramAnchorPane, 300, 400);

        dialogPanelStage.setScene(sampleParameterScene);
        dialogPanelStage.centerOnScreen();
        //sampleParameterStage.setY(10);

        // design the panel for the annotations
        annotationStage.setTitle("Annotationen");

        AnnoPanelBuilder.buildAnnoPanel();

//        annoTileAnchorPane.getChildren().add(scrollTilePane);
        Scene annoPanelScene = new Scene(annoTileAnchorPane, 500, Screen.getMainScreen().getHeight() - 140);

        annotationStage.setScene(annoPanelScene);
        if (Screen.getScreens().size() > 1) {
            annotationStage.setX(0);
            annotationStage.setY(10);
        } else {
            annotationStage.setX(Screen.getMainScreen().getWidth() - 500);
            annotationStage.setY(10);
        }
        //annotationStage.show();
    }

}
