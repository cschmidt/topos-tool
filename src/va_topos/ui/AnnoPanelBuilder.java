/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.ui;

import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import va_topos.VA_topos;
import va_topos.events.AnnotationEvents;
import static va_topos.ui.MainWindowBuilder.annoBackButton;
import static va_topos.ui.MainWindowBuilder.annoDeleteButton;
import static va_topos.ui.MainWindowBuilder.annoFwdButton;
import static va_topos.ui.MainWindowBuilder.annoNewButton;
import static va_topos.ui.MainWindowBuilder.annoRedoButton;
import static va_topos.ui.MainWindowBuilder.annoUndoButton;
import static va_topos.ui.MainWindowBuilder.setAnchor;
import static va_topos.ui.MainWindowBuilder.setStyle;
import static va_topos.ui.MainWindowBuilder.visusFirstAnnotationAnchorPane;
import static va_topos.ui.SubWindowsBuilder.annoControlAnchorPane;
import static va_topos.ui.SubWindowsBuilder.annoTileAnchorPane;
import static va_topos.ui.SubWindowsBuilder.annoTilePane;
import static va_topos.ui.SubWindowsBuilder.scrollTilePane;

/**
 *
 * @author varie
 */
public class AnnoPanelBuilder {
 
    public static void buildAnnoPanel() {

        annoTileAnchorPane = new AnchorPane();
        setAnchor(annoTileAnchorPane, 0d, 0d, 0d, 0d);
        
        // Container für die Annotationssteuerung, die halbtransparent im oberen Teil der rechten Seite angezeigt wird.
        annoControlAnchorPane = new AnchorPane();
        annoControlAnchorPane.setMaxHeight(100);
        annoControlAnchorPane.setPrefHeight(100);
        setAnchor(annoControlAnchorPane, 0d, 0d, Double.NaN, 0d);
        annoControlAnchorPane.setBackground(setStyle(annoControlAnchorPane, MainWindowBuilder.NodeStyles.TRANSPARENT));

        annoTileAnchorPane.getChildren().add(annoControlAnchorPane);
        annoControlAnchorPane.setOnMousePressed(e -> MainWindowBuilder.movePane(annoControlAnchorPane, e.getX(), e.getY()));

                // Elemente des Annopanels
        annoNewButton.setText("neu");
        setAnchor(annoNewButton, 20d, Double.NaN, 20d, Double.NaN);

        annoNewButton.setOnAction(value -> {
            visusFirstAnnotationAnchorPane.addCanvas(AnnotationEvents.startAnnotation());
        });

        annoBackButton.setText("vorherige");
        setAnchor(annoBackButton, 20d, Double.NaN, 50d, Double.NaN);
        annoBackButton.disableProperty().bind(VA_topos.currentAnnoIndex.lessThanOrEqualTo(1));

        annoBackButton.setOnAction(value -> {
            AnnotationEvents.previousAnnotation();
        });

        annoFwdButton.setText("nächste");
        setAnchor(annoFwdButton, 95d, Double.NaN, 50d, Double.NaN);
        annoFwdButton.disableProperty().bind(VA_topos.currentAnnoIndex.greaterThanOrEqualTo(VA_topos.maxAnnoIndex));

        annoFwdButton.setOnAction(value -> {
            AnnotationEvents.nextAnnotation();
        });

        annoDeleteButton.setText("löschen");
        setAnchor(annoDeleteButton, 65d, Double.NaN, 20d, Double.NaN);
        annoDeleteButton.setOnAction(value -> {
            AnnotationEvents.deleteAnnotation();
//            visusFirstAnchorPane.addCanvas(eh.startAnnotation());
//            MainWindowMaker.spareOne.setText("Key: " + visusFirstAnchorPane.getChildren().size() + " Canvas: " + visusFirstAnchorPane.getChildren().get(visusFirstAnchorPane.getChildren().size() - 1));
        });
        annoDeleteButton.disableProperty().bind(VA_topos.maxAnnoIndex.isEqualTo(0));
//        spareOne.textProperty().bind(VA_topos.currentAnnoIndex.asString());

//        annoLoadButton.setText("laden");
//        setAnchor(annoLoadButton, 140d, Double.NaN, 20d, Double.NaN);
//
//        annoLoadButton.setOnAction(value -> {
////            visusFirstAnchorPane.addCanvas(eh.startAnnotation());
////            MainWindowMaker.spareOne.setText("Key: " + visusFirstAnchorPane.getChildren().size() + " Canvas: " + visusFirstAnchorPane.getChildren().get(visusFirstAnchorPane.getChildren().size() - 1));
//        });
        annoUndoButton.setText("<");
        annoUndoButton.setDisable(true);
        setAnchor(annoUndoButton, 350d, Double.NaN, 20d, Double.NaN);

        annoUndoButton.setOnAction(value -> {
            AnnotationEvents.undoAnnotation();
        });

        annoRedoButton.setText(">");
        annoRedoButton.setDisable(true);
        setAnchor(annoRedoButton, 380d, Double.NaN, 20d, Double.NaN);

        annoRedoButton.setOnAction(value -> {
            AnnotationEvents.redoAnnotation();
        });

        
        
        annoControlAnchorPane.getChildren().addAll(annoNewButton,
                annoDeleteButton,
                //annoLoadButton,
                annoFwdButton,
                annoBackButton,
                annoRedoButton,
                annoUndoButton
        );
        annoControlAnchorPane.setOnMousePressed(e -> MainWindowBuilder.movePane(annoControlAnchorPane, e.getX(), e.getY()));        
        
        // ScrollTilePane ist ein container mit den Meta und annodaten, um Scrollbalken zuzulassen
        scrollTilePane = new ScrollPane();
        setAnchor(scrollTilePane, 0d, 0d, 0d, 0d);
        scrollTilePane.setBackground(setStyle(scrollTilePane, MainWindowBuilder.NodeStyles.SCROLLTILEPANE));
        scrollTilePane.setContent(annoTilePane);

    }
}
