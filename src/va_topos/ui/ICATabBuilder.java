/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.ui;

import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.layout.StackPane;
import static va_topos.VA_topos.observablePatients;
import static va_topos.dataHandling.ICACalculation.generateDataFromPatients;
import static va_topos.ui.MainWindowBuilder.setAnchor;
import static va_topos.ui.MainWindowBuilder.identifySimilarStackPane;

/**
 *
 * @author lokal-admin
 */
public class ICATabBuilder {

    public ICATabBuilder(Tab myICATab) {
        identifySimilarStackPane = new StackPane();
        setAnchor(identifySimilarStackPane, 0, 0, 0, 0);
        Button myButton = new Button("Calculate ICA");
        myButton.setOnAction((event) -> {
            generateDataFromPatients(observablePatients);
        });
        identifySimilarStackPane.getChildren().add(myButton);
        myICATab.setContent(identifySimilarStackPane);
    }

}
