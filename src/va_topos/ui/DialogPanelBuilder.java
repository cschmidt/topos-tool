/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos.ui;

import java.time.LocalDate;
import javafx.scene.control.SpinnerValueFactory;
import static va_topos.VA_topos.USER_NAME;
import va_topos.ui.MainWindowBuilder.DIALOGPANEL;
import static va_topos.ui.MainWindowBuilder.annoTextAreaLabel;
import static va_topos.ui.MainWindowBuilder.annoText;
import static va_topos.ui.MainWindowBuilder.paramAnchorPane;
import static va_topos.ui.MainWindowBuilder.paramCloseButton;
import static va_topos.ui.MainWindowBuilder.sampleCountLabel;
import static va_topos.ui.MainWindowBuilder.sampleCountSlider;
import static va_topos.ui.MainWindowBuilder.samplePercentLabel;
import static va_topos.ui.MainWindowBuilder.samplePercentSlider;
import static va_topos.ui.MainWindowBuilder.setAnchor;
import static va_topos.ui.SubWindowsBuilder.dialogPanelStage;
import static va_topos.ui.MainWindowBuilder.annUserLabel;
import static va_topos.ui.MainWindowBuilder.cleansingCurrentDate;
import static va_topos.ui.MainWindowBuilder.cleansingCurrentDateLabel;
import static va_topos.ui.MainWindowBuilder.cleansingDataAttributField;
import static va_topos.ui.MainWindowBuilder.cleansingDataAttributFieldLabel;
import static va_topos.ui.MainWindowBuilder.cleansingDataTypeField;
import static va_topos.ui.MainWindowBuilder.cleansingDataTypeFieldLabel;
import static va_topos.ui.MainWindowBuilder.cleansingDateField;
import static va_topos.ui.MainWindowBuilder.cleansingDateFieldLabel;
import static va_topos.ui.MainWindowBuilder.cleansingLaterality;
import static va_topos.ui.MainWindowBuilder.cleansingLateralityLabel;
import static va_topos.ui.MainWindowBuilder.cleansingPatient;
import static va_topos.ui.MainWindowBuilder.cleansingPatientLabel;
import static va_topos.ui.MainWindowBuilder.cleansingUser;
import static va_topos.ui.MainWindowBuilder.cleansingUserLabel;
import static va_topos.ui.MainWindowBuilder.cleansingValueField;
import static va_topos.ui.MainWindowBuilder.cleansingValueFieldLabel;
import static va_topos.ui.MainWindowBuilder.sampleCountSpinner;
import static va_topos.ui.MainWindowBuilder.samplePercentSpinner;

/**
 *
 * @author lokal-admin
 */
public class DialogPanelBuilder {

    private static final double STDINSET = 5d;
    private static final double TOPOFFSET = 20 + STDINSET;
    private static final double BOTTOMOFFSET = 20 + STDINSET;
    private static final double LEFTOFFSET = 10 + STDINSET;
    private static final double RIGHTOFFSET = 10 + STDINSET;
    private static final double TEXTFIELDSPACE = 25;
    private static final double COMBOSPACE = 30;
    private static final double LABELOFFSET = 200;
    private static final double SLIDEROFFSET = 70;

    public static void buildDialogPanel(int sampleCount, DIALOGPANEL diagPan) {

        double heightFactor = 0;
        paramAnchorPane.getChildren().clear();
        paramCloseButton.setText("Ok");
        setAnchor(paramCloseButton, LEFTOFFSET, RIGHTOFFSET, Double.NaN, BOTTOMOFFSET);
        paramAnchorPane.getChildren().add(paramCloseButton);

        switch (diagPan) {
            case ANNOPANEL:
                annUserLabel.setText("Benutzer: " + USER_NAME);
                setAnchor(annUserLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
                heightFactor++;
                annoTextAreaLabel.setText("Bitte geben Sie hier Ihre Bemerkung ein.");
                setAnchor(annoTextAreaLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
                heightFactor++;
                setAnchor(annoText, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
                heightFactor++;

                paramAnchorPane.getChildren().addAll(
                        annUserLabel,
                        annoTextAreaLabel,
                        annoText
                );
                dialogPanelStage.setTitle("Annotation");
                dialogPanelStage.setWidth(300);
                dialogPanelStage.setHeight(360);
                paramCloseButton.setOnAction(e -> dialogPanelStage.close());
                break;
            case CLEANSINGPANEL:

                cleansingUserLabel.setText("Benutzer: ");
                setAnchor(cleansingUserLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                cleansingUser.setText(USER_NAME.get());
                setAnchor(cleansingUser, LEFTOFFSET + LABELOFFSET, RIGHTOFFSET, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                heightFactor++;

                cleansingCurrentDateLabel.setText("Aktuelles Datum: ");
                setAnchor(cleansingCurrentDateLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                cleansingCurrentDate.setText(LocalDate.now().toString());
                setAnchor(cleansingCurrentDate, LEFTOFFSET + LABELOFFSET, RIGHTOFFSET, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                heightFactor++;

                cleansingPatientLabel.setText("Patient: ");
                setAnchor(cleansingPatientLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                cleansingPatient.setText("00003606");
                setAnchor(cleansingPatient, LEFTOFFSET + LABELOFFSET, RIGHTOFFSET, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                heightFactor++;

                cleansingLateralityLabel.setText("Lateralität: ");
                setAnchor(cleansingLateralityLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                cleansingLaterality.setText("LEFT");
                setAnchor(cleansingLaterality, LEFTOFFSET + LABELOFFSET, RIGHTOFFSET, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                heightFactor++;

                cleansingDateFieldLabel.setText("Datum des neuen Wertes: ");
                setAnchor(cleansingDateFieldLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                cleansingDateField.setValue(LocalDate.of(2016, 7, 25));
                cleansingDateField.setShowWeekNumbers(true);
                setAnchor(cleansingDateField, LEFTOFFSET + LABELOFFSET, RIGHTOFFSET, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                heightFactor++;

                cleansingDataAttributFieldLabel.setText("Datenattribut für den neuen Wert: ");
                cleansingDataAttributField.getItems().clear();
                cleansingDataAttributField.getItems().add("Außergewöhnliches Ereignis");
                cleansingDataAttributField.getItems().add("Injektion");
                cleansingDataAttributField.getItems().add("Visusmessung");
                cleansingDataAttributField.getSelectionModel().select(2);
                setAnchor(cleansingDataAttributFieldLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                setAnchor(cleansingDataAttributField, LEFTOFFSET + LABELOFFSET, RIGHTOFFSET, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                heightFactor++;

                cleansingDataTypeFieldLabel.setText("Datentyp des neuen Wertes: ");
                setAnchor(cleansingDataTypeFieldLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                cleansingDataTypeField.getItems().clear();
                cleansingDataTypeField.getItems().add("Freitext");
                cleansingDataTypeField.getItems().add("Visuswert (Dezimal)");
                cleansingDataTypeField.getItems().add("Medikament");
                cleansingDataTypeField.getSelectionModel().select(1);
                setAnchor(cleansingDataTypeField, LEFTOFFSET + LABELOFFSET, RIGHTOFFSET, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                heightFactor++;

                cleansingValueFieldLabel.setText("Neuer Datenwert: ");
                setAnchor(cleansingValueFieldLabel, LEFTOFFSET, Double.NaN, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                setAnchor(cleansingValueField, LEFTOFFSET + LABELOFFSET, RIGHTOFFSET, TOPOFFSET + (COMBOSPACE * heightFactor), Double.NaN);
                heightFactor++;

                paramAnchorPane.getChildren().addAll(
                        cleansingPatientLabel,
                        cleansingPatient,
                        cleansingUserLabel,
                        cleansingUser,
                        cleansingLateralityLabel,
                        cleansingLaterality,
                        cleansingCurrentDateLabel,
                        cleansingCurrentDate,
                        cleansingDateFieldLabel,
                        cleansingDateField,
                        cleansingDataAttributFieldLabel,
                        cleansingDataAttributField,
                        cleansingDataTypeFieldLabel,
                        cleansingDataTypeField,
                        cleansingValueFieldLabel,
                        cleansingValueField
                );
                dialogPanelStage.setTitle("Neuen Datenwert erstellen");
                dialogPanelStage.setWidth(400);
                dialogPanelStage.setHeight(370);
                paramCloseButton.setOnAction(e -> {
                    dialogPanelStage.close();
                });
                break;
            case SAMPLEPANEL:
            default: // SamplePanel

                sampleCountLabel.setText("Anzahl der zufällig auszuwählenden Patienten");
                setAnchor(sampleCountLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
                heightFactor++;

                SpinnerValueFactory<Double> sampleCountFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(1, sampleCount, 30);
                sampleCountSpinner.setValueFactory(sampleCountFactory);
                setAnchor(sampleCountSpinner, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
                sampleCountSpinner.setPrefWidth(SLIDEROFFSET);
                sampleCountFactory.valueProperty().bindBidirectional(sampleCountSlider.valueProperty().asObject()); //.bind(samplePercentSlider.valueProperty().asObject());

                sampleCountSlider.setBlockIncrement(sampleCount / 10);
                sampleCountSlider.setMin(1);
                sampleCountSlider.setMax(sampleCount);
                sampleCountSlider.setValue(30);
                //sampleCountSlider.valueProperty().addListener((obs, oldval, newVal) -> {
                //sampleCountSlider.setValue(newVal.intValue());
                //sampleCountSpinner.getValueFactory().setValue((int) Math.round((double) newVal));
                //});
                sampleCountSlider.valueProperty().bindBidirectional(samplePercentSlider.valueProperty()); //.multiply(sampleCount).divide(100.0)));
                setAnchor(sampleCountSlider, LEFTOFFSET + SLIDEROFFSET, RIGHTOFFSET, 5 + TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
                heightFactor++;
                heightFactor++;

                samplePercentLabel.setText("Prozentangabe des Samples");
                setAnchor(samplePercentLabel, LEFTOFFSET, RIGHTOFFSET, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
                heightFactor++;

                SpinnerValueFactory<Double> samplePercentFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(Math.min(1.0, (double) 100.0 / sampleCount), 100.0, 1);
                samplePercentFactory.valueProperty().bindBidirectional(samplePercentSlider.valueProperty().asObject()); //.bind(samplePercentSlider.valueProperty().asObject());
                samplePercentSpinner.setValueFactory(samplePercentFactory);
                setAnchor(samplePercentSpinner, LEFTOFFSET, Double.NaN, TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
                samplePercentSpinner.setPrefWidth(SLIDEROFFSET);

                samplePercentSlider.setBlockIncrement(sampleCount / 10);
                samplePercentSlider.setMin(Math.min(1.0, (double) 100.0 / sampleCount));
                samplePercentSlider.setMax(100);
                //samplePercentSlider.setValue(5.0);
                setAnchor(samplePercentSlider, LEFTOFFSET + SLIDEROFFSET, RIGHTOFFSET, 5 + TOPOFFSET + (TEXTFIELDSPACE * heightFactor), Double.NaN);
                heightFactor++;
                heightFactor++;

//                DoubleProperty oneValue = sampleCountSlider.valueProperty();
//                DoubleProperty twoValue = samplePercentSlider.valueProperty();
//                DoubleProperty scaleValue = new SimpleDoubleProperty(1.0 / (double) sampleCount * 100);
//
//                twoValue.set(oneValue.get() * scaleValue.get());
//
//                oneValue.addListener((observable, oldValue, newValue) -> {
//                    twoValue.set(newValue.doubleValue() * scaleValue.get());
//                });
//
//                scaleValue.addListener((observable, oldValue, newValue) -> {
//                    twoValue.set(oneValue.get() * newValue.doubleValue());
//                });
//
//                twoValue.addListener((observable, oldValue, newValue) -> {
//                    double theScale = scaleValue.get();
//                    if (theScale != 0) {
//                        oneValue.set(newValue.doubleValue() / scaleValue.get());
//                    } else {
//                        twoValue.set(0);
//                    }
//                });
                paramAnchorPane.getChildren().addAll(
                        sampleCountLabel,
                        sampleCountSlider,
                        sampleCountSpinner,
                        samplePercentLabel,
                        samplePercentSlider,
                        samplePercentSpinner
                );
                dialogPanelStage.setTitle("Patientenauswahl");
                dialogPanelStage.setWidth(300);
                dialogPanelStage.setHeight(250);
                paramCloseButton.setOnAction(e -> dialogPanelStage.close());
        }
    }

}
