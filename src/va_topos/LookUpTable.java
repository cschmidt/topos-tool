/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package va_topos;

import va_topos.data.DataModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import va_topos.data.Annotations;

/**
 *
 * @author lokal-admin
 */
@SuppressWarnings("unchecked")
public class LookUpTable {

    private double xMinLoc = 0d;
    private double yMinLoc = 0d;
    private double xMaxLoc = 0d;
    private double yMaxLoc = 0d;

    private DataModel patient = new DataModel();
    public ObservableMap<String, Annotations> myAnnotations = FXCollections.observableHashMap();

    private String xDimension = "";
    private String yDimension = "";
    private String xValue = "";
    private String yValue = "";
    private String patientName = "";

    public ObservableMap<String, Annotations> getMyAnnotations() {
        return this.myAnnotations;
    }

    public void setMyAnnotations(ObservableMap<String, Annotations> value) {
        this.myAnnotations = value;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public double getxMinLoc() {
        return xMinLoc;
    }

    public void setxMinLoc(double xMinLoc) {
        this.xMinLoc = xMinLoc;
    }

    public double getyMinLoc() {
        return yMinLoc;
    }

    public void setyMinLoc(double yMinLoc) {
        this.yMinLoc = yMinLoc;
    }

    public double getxMaxLoc() {
        return xMaxLoc;
    }

    public void setxMaxLoc(double xMaxLoc) {
        this.xMaxLoc = xMaxLoc;
    }

    public double getyMaxLoc() {
        return yMaxLoc;
    }

    public void setyMaxLoc(double yMaxLoc) {
        this.yMaxLoc = yMaxLoc;
    }

    public DataModel getPatient() {
        return patient;
    }

    public void setPatient(DataModel patient) {
        this.patient = patient;
    }

    public String getxDimension() {
        return xDimension;
    }

    public void setxDimension(String xDimension) {
        this.xDimension = xDimension;
    }

    public String getyDimension() {
        return yDimension;
    }

    public void setyDimension(String yDimension) {
        this.yDimension = yDimension;
    }

    public String getxValue() {
        return xValue;
    }

    public void setxValue(String xValue) {
        this.xValue = xValue;
    }

    public String getyValue() {
        return yValue;
    }

    public void setyValue(String yValue) {
        this.yValue = yValue;
    }

    public int getLocationIndex(int x, int y) {
        if ((this.getxMinLoc() < x) && (this.getxMaxLoc() > x) && (this.getyMinLoc() < y) && (this.getyMaxLoc() > y)) {
            return 1;
        }
        return -1;
    }

}
